package user;

import fm.util.HuanXinSdkUtil;
import fm.entity.WxUser;
import fm.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.codec.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "classpath:spring-data-mongo.xml"})
public class UserTest {

    @Autowired
    UserService userService;


    @Test
    public void updateUserHxUserName() {
        List<WxUser> users = userService.getAllUser();
        for (WxUser user : users) {
            String hxUserName = user.getHxUsername();
            if (StringUtils.isNotEmpty(hxUserName)) {
                if (hxUserName.equals("wx_" + user.getId()) == false) {
                    String hxUsername = "wx_" + user.getId();
                    String hxPassword = Base64.encodeToString(String.valueOf(user.getId()).getBytes());
                    boolean isRegist = HuanXinSdkUtil.registUser(hxUsername, hxPassword, user.getNickname());
                    if (isRegist) {
                        user.setHxUsername(hxUsername);
                        user.setHxPassword(hxPassword);
                    }
                }
            } else {
                String hxUsername = "wx_" + user.getId();
                String hxPassword = Base64.encodeToString(String.valueOf(user.getId()).getBytes());
                boolean isRegist = HuanXinSdkUtil.registUser(hxUsername, hxPassword, user.getNickname());
                if (isRegist) {
                    user.setHxUsername(hxUsername);
                    user.setHxPassword(hxPassword);
                }
            }
            userService.saveOrUpdate(user);
        }
    }




}
