package fm.realm;

import com.alibaba.fastjson.JSON;
import fm.util.HuanXinSdkUtil;
import fm.service.UserService;
import fm.cheyiwang.token.CheYiWechatToken;
import fm.entity.WxUser;
import fm.util.CommonUtils;
import fm.util.Constant;
import fm.util.EmojiFilterUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.codec.*;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import java.util.Set;
import java.util.HashSet;

/**
 * Created by CM on 2015/9/3.
 */
public class WechatRealm extends AuthorizingRealm {
    private static Logger log = LoggerFactory.getLogger(WechatRealm.class);

    @Autowired
    HttpSession session;

    @Autowired
    UserService userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String uuid = (String) principalCollection.getPrimaryPrincipal();
        log.info("checking authorization information : {}", principalCollection.getPrimaryPrincipal());

        Set<String> roles = new HashSet<>();
        roles.add("ROLE_SERVICE_FANS");
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRoles(roles);
        log.info("authorization finish ,info: {}", JSON.toJSONString(info));
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws
            AuthenticationException {
        CheYiWechatToken token = (CheYiWechatToken) authenticationToken;
        WxUser user = token.getWxUser();
        String nickName = EmojiFilterUtils.filterEmoji(user.getNickname());
        user.setNickname(nickName);
        if (CommonUtils.isEmpty(user)) {
            log.info("Authentication finish, user information query failed,query user information from tencent!");
            throw new UnknownAccountException("用户信息尚未拉取，授权失败!");
        }

        userService.saveOrUpdate(user);

        if("".equals(user.getHxUsername()) || CommonUtils.isEmpty(user.getHxUsername())){
            String hxUsername = "wx_" + user.getId();
            String hxPassword = Base64.encodeToString(String.valueOf(user.getId()).getBytes());
             HuanXinSdkUtil.registUser(hxUsername, hxPassword,user.getNickname());
            user.setHxUsername(hxUsername);
            user.setHxPassword(hxPassword);
            userService.saveOrUpdate(user);
        }

        session.setAttribute(Constant.SESSION_LOGIN_USER, user);
        log.info("Authentication finish, user:{}", user.getId());
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(token.getOpenId(), token.getDevType(), getName());
        return info;
    }

    /**
     * 添加token支持
     *
     * @param token
     * @return
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        if (token instanceof CheYiWechatToken) {
            log.error("wechat authentication token! support!");
            return true;
        } else {
            log.error("unknown authentication token!");
            return super.supports(token);
        }
    }
}
