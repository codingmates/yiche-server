package fm.token;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;


public class WeChatToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
    private String uuid;
    private String openId;
    private String host;

    public WeChatToken(String uuid, String openId) {
        this(uuid, openId, null);
    }

    public WeChatToken(String uuid, String openId, String host) {
        this.uuid = uuid;
        this.openId = openId;
        this.host = host;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public boolean isRememberMe() {
        return false;
    }

    @Override
    public Object getPrincipal() {
        return this.uuid;
    }

    @Override
    public Object getCredentials() {
        return this.openId;
    }

    public void clear() {
        this.uuid = null;
        this.host = null;
        this.openId = null;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "WeChatToken{" +
                "uuid='" + uuid + '\'' +
                ", openId='" + openId + '\'' +
                ", host='" + host + '\'' +
                '}';
    }
}
