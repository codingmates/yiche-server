package fm.shop.controller;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.shop.service.ShopService;
import fm.web.MediaTypes;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @修改人：CM
 * @修改时间：2017/3/15 12:36
 */
@Controller
@RequestMapping("/shop/about")
public class AboutController extends BaseController {
    @Autowired
    ShopService shopService;

    @ResponseBody
    @RequestMapping(value = "/contact", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map saveContact(String name, String phone, String email, String qq, String msg) {
        Map response = new HashMap();
        try {
            if (StringUtils.isEmpty(phone) && StringUtils.isEmpty(email) && StringUtils.isEmpty(qq)) {
                throw new RuntimeException("尊姓的客户，手机、QQ、邮箱等联系方式请至少留下一项，方便我们联系您！");
            }
            DBObject contactInfo = new BasicDBObject();
            if (StringUtils.isNotEmpty(name)) {
                name = URLDecoder.decode(name,"UTF-8");
                contactInfo.put("name", name);
            }
            if (StringUtils.isNotEmpty(phone)) {
                contactInfo.put("phone", phone);
            }
            if (StringUtils.isNotEmpty(email)) {
                contactInfo.put("email", email);
            }
            if (StringUtils.isNotBlank(qq)) {
                contactInfo.put("qq", qq);
            }
            if (StringUtils.isNotEmpty(msg)) {
                msg = URLDecoder.decode(msg,"UTF-8");
                contactInfo.put("msg", msg);
            }
            contactInfo.put("uuid",getCurrentUserId());
            shopService.saveContactInfo(contactInfo);
            this.success(response);
        } catch (Exception ex) {
            this.failed(response, ex.getMessage());
            LOGGER.error("请求数据发生错误", ex);
        }
        return response;
    }


}
