package fm.shop.controller;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.exception.TokenInvalidException;
import fm.shop.service.ShopService;
import fm.web.MediaTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @修改人：CM
 * @修改时间：2017/2/26 10:09
 */
@Controller
@RequestMapping("/shop")
public class IndexController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private ShopService shopService;

    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        return "/shop/index";
    }

    @RequestMapping("/pu")
    public String tradeList() {
        return "/shop/tradeList";
    }

    @RequestMapping("/about")
    public String about(ModelMap modelMap) throws Exception {
        DBObject param = new BasicDBObject();
        param.put("uuid", "123");
        modelMap.put("config", shopService.getAboutConfig(param));
        return "/shop/about";
    }

}
