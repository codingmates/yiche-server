package fm.shop.service;

import com.mongodb.DBObject;

import java.util.Collection;
import java.util.Map;

/**
 * @修改人：CM
 * @修改时间：2017/2/27 21:13
 */
public interface ShopService {
    Map<String, Collection> getIndexPros(String uuid) throws Exception;


    Collection getPurchase(Map params, int pageSize, int pageNum) throws Exception;

    Collection getProducts(Map params, int pageSize, int pageNum) throws Exception;

    DBObject getPurchaseUnit(Map params) throws Exception;

    DBObject getProductUnit(Map params) throws Exception;

    void updatePurchase(DBObject purchase,Map query) throws Exception;

    void updateProduct(DBObject product,Map query) throws Exception;

    /**
     * 保存关于界面的联系我们信息
     * @param contactInfo
     * @throws Exception
     */
    void saveContactInfo(DBObject contactInfo) throws Exception;

    DBObject getAboutConfig(DBObject query) throws Exception;
}
