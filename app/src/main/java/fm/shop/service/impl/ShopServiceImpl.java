package fm.shop.service.impl;

import com.mongodb.DBObject;
import fm.cheyiwang.model.Product;
import fm.dao.MongoBaseDao;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import fm.shop.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @修改人：CM
 * @修改时间：2017/2/27 21:14
 */
@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    private MongoBaseDao mongoDao;

    @Override
    public Map<String, Collection> getIndexPros(String uuid) throws Exception {
        Map params = new HashMap();
        params.put("uuid", uuid);
        params.put("flag", "newPros");
        Collection<Product> newPros = mongoDao.getPageList(params, DBObject.class,
                4, 1, MongoTable.cyw_products);
        params.put("flag", "hotPros");
        Collection<Product> hotPros = mongoDao.getPageList(params, DBObject.class,
                4, 1, MongoTable.cyw_products);
        params.put("flag", "adPros");
        Collection<Product> adPros = mongoDao.getPageList(params, DBObject.class,
                4, 1, MongoTable.cyw_products);
        Collection<Product> adPros2 = mongoDao.getPageList(params, DBObject.class,
                4, 2, MongoTable.cyw_products);
        Map data = new HashMap();
        data.put("adPros", adPros);
        data.put("hotPros", hotPros);
        data.put("newPros", newPros);
        data.put("adPros2", adPros2);
        return data;
    }

    @Override
    public Collection getPurchase(Map params, int pageSize, int pageNum) throws Exception {
        if (params.containsKey("uuid") == false) {
            throw new BizException("无法获取公众号信息，请求失败");
        }
        Collection result = mongoDao.getPageList(params, DBObject.class, pageSize,
                pageNum, MongoTable.cyw_purchase);
        return result;
    }

    @Override
    public Collection getProducts(Map params, int pageSize, int pageNum) throws Exception {
        if (params.containsKey("uuid") == false) {
            throw new BizException("无法获取公众号信息，请求失败");
        }
        Collection result = mongoDao.getPageList(params, DBObject.class, pageSize,
                pageNum, MongoTable.cyw_products);
        return result;
    }

    @Override
    public DBObject getPurchaseUnit(Map params) throws Exception {
        return (DBObject) mongoDao.findOne(params,DBObject.class,MongoTable.cyw_purchase);
    }

    @Override
    public DBObject getProductUnit(Map params) throws Exception {
        return (DBObject) mongoDao.findOne(params,DBObject.class,MongoTable.cyw_products);
    }

    @Override
    public void updatePurchase(DBObject purchase, Map query) throws Exception {
        mongoDao.updateOne(query,purchase.toMap(),MongoTable.cyw_purchase);
    }

    @Override
    public void updateProduct(DBObject product, Map query) throws Exception {
        mongoDao.updateOne(query,product.toMap(),MongoTable.cyw_products);
    }

    @Override
    public void saveContactInfo(DBObject contactInfo) throws Exception {
        mongoDao.insert(contactInfo,MongoTable.contact);
    }

    @Override
    public DBObject getAboutConfig(DBObject query) throws Exception {
        return (DBObject) mongoDao.findOne(query.toMap(), DBObject.class, MongoTable.aboutConfig);
    }
}
