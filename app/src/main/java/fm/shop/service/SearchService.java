package fm.shop.service;

import fm.exception.BizException;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @修改人：CM
 * @修改时间：2017/3/2 20:57
 */
public interface SearchService {

    ConcurrentHashMap<String, Collection> searchByKeyword(String keywords, Collection<String> tables) throws InterruptedException, BizException;
}
