package fm.cheyiwang.model;

import java.io.Serializable;

/**
 * @修改人：CM
 * @修改时间：2017/2/26 20:23
 */
public class Product implements Serializable {

    private String ProPic;
    private String ProName;
    private String AuthCode;
    private Double Lng;
    private Integer refresh;
    private Long ViewCount;
    private Double TradePrice;
    private String Area2;
    private String ProCode;
    private String Area1;
    private Integer Sotrage;
    private String ProId;
    private Integer State;
    private String Auth;
    private Double SalePrice;
    private String vtradeprice;
    private Double Lat;

    public Product() {
    }

    public String getProPic() {
        return ProPic;
    }

    public void setProPic(String proPic) {
        ProPic = proPic;
    }

    public String getProName() {
        return ProName;
    }

    public void setProName(String proName) {
        ProName = proName;
    }

    public String getAuthCode() {
        return AuthCode;
    }

    public void setAuthCode(String authCode) {
        AuthCode = authCode;
    }

    public Double getLng() {
        return Lng;
    }

    public void setLng(Double lng) {
        Lng = lng;
    }

    public Integer getRefresh() {
        return refresh;
    }

    public void setRefresh(Integer refresh) {
        this.refresh = refresh;
    }

    public Long getViewCount() {
        return ViewCount;
    }

    public void setViewCount(Long viewCount) {
        ViewCount = viewCount;
    }

    public Double getTradePrice() {
        return TradePrice;
    }

    public void setTradePrice(Double tradePrice) {
        TradePrice = tradePrice;
    }

    public String getArea2() {
        return Area2;
    }

    public void setArea2(String area2) {
        Area2 = area2;
    }

    public String getProCode() {
        return ProCode;
    }

    public void setProCode(String proCode) {
        ProCode = proCode;
    }

    public String getArea1() {
        return Area1;
    }

    public void setArea1(String area1) {
        Area1 = area1;
    }

    public Integer getSotrage() {
        return Sotrage;
    }

    public void setSotrage(Integer sotrage) {
        Sotrage = sotrage;
    }

    public String getProId() {
        return ProId;
    }

    public void setProId(String proId) {
        ProId = proId;
    }

    public Integer getState() {
        return State;
    }

    public void setState(Integer state) {
        State = state;
    }

    public String getAuth() {
        return Auth;
    }

    public void setAuth(String auth) {
        Auth = auth;
    }

    public Double getSalePrice() {
        return SalePrice;
    }

    public void setSalePrice(Double salePrice) {
        SalePrice = salePrice;
    }

    public String getVtradeprice() {
        return vtradeprice;
    }

    public void setVtradeprice(String vtradeprice) {
        this.vtradeprice = vtradeprice;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double lat) {
        Lat = lat;
    }
}
