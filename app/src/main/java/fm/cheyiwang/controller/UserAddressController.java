package fm.cheyiwang.controller;

import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.web.MediaTypes;
import fm.yichenet.mongo.service.UserAddressService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-06-19.
 */
@Controller
@RequestMapping("userAddress")
public class UserAddressController extends BaseController {

    @Autowired
    UserAddressService userAddressService;

    @ResponseBody
    @RequestMapping(value = "mergeAddress", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map mergeAddress(@RequestBody Map address) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            Map data = userAddressService.mergeAddress(address,wxUser);
            res.put("data",data);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "deleteAddress", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map deleteAddress(@RequestParam(value = "addressId",required = true) String addressId) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            userAddressService.deleteAddress(wxUser,addressId);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "addressList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map addressList() throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            List list = userAddressService.addressList(wxUser);
            res.put("data",list);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }
}
