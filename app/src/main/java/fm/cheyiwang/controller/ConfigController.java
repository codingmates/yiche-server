package fm.cheyiwang.controller;

import com.alibaba.fastjson.JSON;
import fm.config.MainConfig;
import fm.controller.BaseController;
import fm.web.MediaTypes;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.calcite.plan.TableAccessMap;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by 宏炜 on 2017-05-16.
 */
@Controller
@RequestMapping("config")
public class ConfigController extends BaseController {

    @Autowired
    MainConfig mainConfig;

    @Autowired
    protected WxMpService wxMpService;

    /**
     * 拉取微信公众号信息
     *
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getAppConfig", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getAppConfig(@RequestParam(value = "state", required = true) String state, ModelMap modelMap) {
        if (MainConfig.iosApp.equals(state) || mainConfig.androidApp.equals(state)) {
            Map<String, Object> data = new HashedMap();
            data.put("appid", mainConfig.getMobileAppid());
            data.put("appsecret", mainConfig.getMobileAppsecret());
            modelMap.put("data", data);
            this.success(modelMap);
        } else if (mainConfig.weChatApp.equals(state)) {
            Map<String, Object> data = new HashedMap();
            data.put("appid", mainConfig.getAppid());
            data.put("appsecret", mainConfig.getAppsecret());
            modelMap.put("data", data);
            this.success(modelMap);
        } else {
            this.failed(modelMap, "错误的state");
        }
        return JSON.toJSONString(modelMap);
    }


    @ResponseBody
    @RequestMapping(value = "getWechatSignatureInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getWechatSignatureInfo(@RequestParam(value = "url", required = true) String url, ModelMap modelMap) {
        Map<String, Object> data = new HashedMap();
        try {
            LOGGER.info("获取签名的URL：{}", url);
            WxJsapiSignature wxJsapiSignature = wxMpService.createJsapiSignature(url);
            data.put("appId", wxJsapiSignature.getAppId());
            data.put("nonceStr", wxJsapiSignature.getNonceStr());
            data.put("timestamp", wxJsapiSignature.getTimestamp());
            data.put("url", wxJsapiSignature.getUrl());
            data.put("signature", wxJsapiSignature.getSignature());
            modelMap.put("data", data);
        } catch (WxErrorException e) {
            this.failed(modelMap, e.getError().getErrorMsg());
        }
        this.success(modelMap);
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "getWechatAccessToken", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getWechatAccessToken(ModelMap modelMap) {
        try {
            String accessToken = wxMpService.getAccessToken();
            Map<String, Object> data = new HashedMap();
            data.put("accessToken", accessToken);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (WxErrorException wx) {
            String msg = wx.getError().getErrorCode() + " : " + wx.getError().getErrorMsg();
            this.failed(modelMap, msg);
        }
        return JSON.toJSONString(modelMap);
    }

}
