package fm.cheyiwang.controller;

import com.github.binarywang.wxpay.bean.WxPayOrderNotifyResponse;
import com.github.binarywang.wxpay.bean.request.WxPayBaseRequest;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.WxPayBaseResult;
import com.github.binarywang.wxpay.bean.result.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryResult;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.github.binarywang.wxpay.util.SignUtils;
import fm.config.MainConfig;
import fm.controller.BaseController;
import fm.entity.OrderGood;
import fm.entity.OrderTransaction;
import fm.entity.WxUser;
import fm.entityEnum.OrderEnum;
import fm.entityEnum.PayStatusEnum;
import fm.exception.BizException;
import fm.util.CommonUtils;
import fm.yichenet.service.OrderService;
import me.chanjar.weixin.common.exception.WxErrorException;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-06-27.
 */
@Controller
@RequestMapping("wxpay")
public class WxPayController extends BaseController {

    @Autowired
    MainConfig mainConfig;

    private static Logger log = LoggerFactory.getLogger(WxPayController.class);

    @Autowired
    WxPayService wxPayService;

    @Autowired
    OrderService orderService;

    @ResponseBody
    @RequestMapping(value = "payorder")
    public Map payorder(@RequestBody List<String> orderGoodIds, HttpServletRequest request) throws Exception {
        Map res = new HashedMap();
        OrderTransaction orderTransaction = null;
        try {
            orderTransaction = orderService.addOrderTransaction(orderGoodIds);
            WxUser wxUser = (WxUser) getCurrentUser();
            WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
            orderRequest.setBody("车衣网订单中心");
            orderRequest.setOutTradeNo(orderTransaction.getId());
            log.info("user pay info OutTradeNo : {}", orderTransaction.getId());
            orderRequest.setTotalFee(WxPayBaseRequest.yuanToFee(String.valueOf(orderTransaction.getAmount())));//元转成分
            log.info("user pay info TotalFee : {}", WxPayBaseRequest.yuanToFee(String.valueOf(orderTransaction.getAmount())));
            orderRequest.setOpenid(wxUser.getOpenid());
            log.info("user pay info openid : {}", wxUser.getOpenid());
            orderRequest.setNotifyURL(mainConfig.getDomain() + "/wxpay/payresult");
            log.info("user pay info NotifyURL : {}", mainConfig.getDomain() + "/wxpay/payresult");
            orderRequest.setTradeType("JSAPI");

            String ip = request.getHeader("x-forwarded-for");
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("PRoxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }

            log.info("user pay info real ip : {}", ip);

            if (ip.contains(",")) {
                ip = ip.split(",")[0];
            }
            orderRequest.setSpbillCreateIp(ip);

            Timestamp now = new Timestamp(System.currentTimeMillis());
            Timestamp expiredTime = new Timestamp(now.getTime() + (30 * 60 * 1000));
            DateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");

            orderRequest.setTimeStart(sdf1.format(now));
            orderRequest.setTimeExpire(sdf1.format(expiredTime));
            Map data = wxPayService.getPayInfo(orderRequest);
            orderService.updateOrderTransactionStatus(orderTransaction.getId(), PayStatusEnum.PAYING);
            //data.put("partenerId",mainConfig.getPartenerId());
            res.put("data", data);

            this.success(res);
        } catch (BizException e) {
            log.error("支付失败", e);
        } catch (WxErrorException e) {
            log.error("微信支付失败！订单号：{},原因:{}", orderTransaction.getId(), e);
            this.failed(res, "微信支付失败！订单号：" + orderTransaction.getId() + ",原因:" + e.getMessage());
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "apppayorder")
    public Map apppayorder(@RequestBody List<String> orderGoodIds, HttpServletRequest request) throws Exception {
        Map res = new HashedMap();
        OrderTransaction orderTransaction = null;
        try {
            orderTransaction = orderService.addOrderTransaction(orderGoodIds);
            WxUser wxUser = (WxUser) getCurrentUser();
            WxPayService appWxPayService = new WxPayServiceImpl();
            WxPayConfig wxPayConfig = new WxPayConfig();

            wxPayConfig.setAppId(mainConfig.getMobileAppid());
            wxPayConfig.setMchId(mainConfig.getMobilePartenerId());
            wxPayConfig.setMchKey(mainConfig.getMobilePartenerKey());
            wxPayConfig.setNotifyUrl(mainConfig.getDomain() + "/wxpay/apppayresult");
            wxPayConfig.setTradeType("APP");
            appWxPayService.setConfig(wxPayConfig);
            WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();

            orderRequest.setBody("车衣网订单中心");
            orderRequest.setOutTradeNo(orderTransaction.getId());
            log.info("user pay info OutTradeNo : {}", orderTransaction.getId());
            orderRequest.setTotalFee(WxPayBaseRequest.yuanToFee(String.valueOf(orderTransaction.getAmount())));//元转成分
            log.info("user pay info TotalFee : {}", WxPayBaseRequest.yuanToFee(String.valueOf(orderTransaction.getAmount())));
            String openid = "";
            if (!CommonUtils.isEmpty(wxUser.getIosOpenid())) {
                openid = wxUser.getIosOpenid();
            }
            if (!CommonUtils.isEmpty(wxUser.getAndroidOpenid())) {
                openid = wxUser.getAndroidOpenid();
            }
            orderRequest.setOpenid(openid);
            log.info("user pay info openid : {}", openid);
            orderRequest.setNotifyURL(mainConfig.getDomain() + "/wxpay/apppayresult");
            log.info("user pay info NotifyURL : {}", mainConfig.getDomain() + "/wxpay/apppayresult");
            orderRequest.setTradeType("APP");

            String ip = request.getHeader("x-forwarded-for");
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("PRoxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
            orderRequest.setSpbillCreateIp(ip);
            log.info("user pay info real ip : {}", ip);
            Timestamp now = new Timestamp(System.currentTimeMillis());
            Timestamp expiredTime = new Timestamp(now.getTime() + (30 * 60 * 1000));
            DateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");
            orderRequest.setTimeStart(sdf1.format(now));
            orderRequest.setTimeExpire(sdf1.format(expiredTime));


            Map data = appWxPayService.getPayInfo(orderRequest);
            Map<String, String> signParams = new HashedMap();
            signParams.put("appid", wxPayConfig.getAppId());
            signParams.put("partnerid", wxPayConfig.getMchId());
            signParams.put("prepayid", data.get("package").toString().split("=")[1]);
            signParams.put("package", "Sign=WXPay");
            signParams.put("noncestr", data.get("nonceStr").toString());
            signParams.put("timestamp", data.get("timeStamp").toString());
            String sign = SignUtils.createSign(signParams, wxPayConfig.getMchKey());
            signParams.put("sign", sign);

            orderService.updateOrderTransactionStatus(orderTransaction.getId(), PayStatusEnum.PAYING);
            data.put("partenerId", mainConfig.getMobilePartenerId());
            res.put("data", signParams);

            this.success(res);
        } catch (BizException e) {
            this.failed(res, e);
        } catch (WxErrorException e) {
            log.error("微信支付失败！订单号：{},原因:{}", orderTransaction.getId(), e);
            this.failed(res, "微信支付失败！订单号：" + orderTransaction.getId() + ",原因:" + e.getMessage());
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "payresult")
    public String payNotify(HttpServletRequest request, HttpServletResponse response) {

        try {
            String xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            WxPayOrderNotifyResult result = wxPayService.getOrderNotifyResult(xmlResult);
            String orderId = result.getOutTradeNo();
            String tradeNo = result.getTransactionId();

            String totalFee = WxPayBaseResult.feeToYuan(result.getTotalFee());
            log.info("outTradeNo: {} ,tradeNo: {} ,real pay toalfee:{}", orderId, tradeNo, totalFee);
            orderService.updateOrderTransactionWxOrder(orderId, tradeNo, totalFee);
            WxPayOrderQueryResult wxPayOrderQueryResult = wxPayService.queryOrder(null, orderId);
            if (wxPayOrderQueryResult.getTradeState().equals("SUCCESS")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAID);
                log.info("user openid :{} ,pay succeed update OrderTransaction id {} ,wxOrderId: {}", result.getOpenid(), orderId, tradeNo);
//                orderService.updateOrderGoodStatus(orderId, OrderEnum.PAYMENT_SUCCESS);
                log.info("user openid :{} ,pay succeed update OrderGood by OrderTransaction id {} ,wxOrderId: {}", result.getOpenid(), orderId, tradeNo);
            } else if (wxPayOrderQueryResult.getTradeState().equals("CLOSED") || wxPayOrderQueryResult.getTradeState().equals("PAYERROR")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAY_FAILED);
                log.info("user openid :{} ,pay failed update OrderTransaction id {} ,wxOrderId: {}", result.getOpenid(), orderId, tradeNo);
            }
            //自己处理订单的业务逻辑，需要判断订单是否已经支付过，否则可能会重复调用
            return WxPayOrderNotifyResponse.success("处理成功!");
        } catch (BizException ex) {
            log.info("wxpay/payresult info BizException msg : {} ", ex);
            return WxPayOrderNotifyResponse.success("处理成功!");
        } catch (Exception e) {
            log.error("微信回调结果异常,异常原因{}", e);
            return WxPayOrderNotifyResponse.fail(e.getMessage());
        }
    }


    /**
     * TODO 这里应该是接受回调后主动向微信服务器发起一次查询
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "apppayresult")
    public String appPayNotify(HttpServletRequest request, HttpServletResponse response) {

        try {
            WxPayService appWxPayService = new WxPayServiceImpl();
            WxPayConfig wxPayConfig = new WxPayConfig();
            wxPayConfig.setAppId(mainConfig.getMobileAppid());
            wxPayConfig.setMchId(mainConfig.getMobilePartenerId());
            wxPayConfig.setMchKey(mainConfig.getMobilePartenerKey());
            wxPayConfig.setNotifyUrl(mainConfig.getDomain() + "/wxpay/apppayresult");
            wxPayConfig.setTradeType("APP");
            appWxPayService.setConfig(wxPayConfig);

            String xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            log.info("微信APP支付回调信息:{}", xmlResult);

            WxPayOrderNotifyResult result = appWxPayService.getOrderNotifyResult(xmlResult);
            String orderId = result.getOutTradeNo();
            String tradeNo = result.getTransactionId();
            String totalFee = WxPayBaseResult.feeToYuan(result.getTotalFee());

            //主动查询微信服务器
            log.info("outTradeNo: {} ,tradeNo: {} ,real pay toalfee:{}", orderId, tradeNo, totalFee);
            orderService.updateOrderTransactionWxOrder(orderId, tradeNo, totalFee);
            WxPayOrderQueryResult wxPayOrderQueryResult = appWxPayService.queryOrder(null, orderId);

            if (wxPayOrderQueryResult.getTradeState().equals("SUCCESS")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAID);
                log.info("user openid :{} ,pay succeed update OrderTransaction id {} ,wxOrderId: {}", result.getOpenid(), orderId, tradeNo);

//                orderService.updateOrderGoodStatus(orderId, OrderEnum.PAYMENT_SUCCESS);
                log.info("user openid :{} ,pay succeed update OrderGood by OrderTransaction id {} ,wxOrderId: {}", result.getOpenid(), orderId, tradeNo);

            } else if (wxPayOrderQueryResult.getTradeState().equals("CLOSED") || wxPayOrderQueryResult.getTradeState().equals("PAYERROR")) {

                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAY_FAILED);
                log.info("user openid :{} ,pay failed update OrderTransaction id {} ,wxOrderId: {}", result.getOpenid(), orderId, tradeNo);

            }

            //自己处理订单的业务逻辑，需要判断订单是否已经支付过，否则可能会重复调用
            return WxPayOrderNotifyResponse.success("处理成功!");

        } catch (BizException ex) {
            log.info("wxpay/payresult info BizException msg : {} ", ex);
            return WxPayOrderNotifyResponse.success("处理成功!");
        } catch (Exception e) {
            log.error("微信回调结果异常,异常原因{}", e);
            return WxPayOrderNotifyResponse.fail(e.getMessage());
        }
    }


    @ResponseBody
    @RequestMapping(value = "checkOrderResult")
    public Map checkOrderResult(@RequestParam(value = "orderGoodId", required = true) String orderGoodId, HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> res = new HashedMap();
        try {

            OrderGood orderGood = orderService.getOrderById(orderGoodId);
            String orderId = orderGood.getTransactionOrderId();
            if (CommonUtils.isEmpty(orderId)) {
                throw new BizException("交易订单信息不存在");
            }
            WxUser wxUser = (WxUser) getCurrentUser();
            if (!orderGood.getUserId().equals(wxUser.getId())) {
                throw new BizException("非本用户订单，无法查询");
            }
            WxPayOrderQueryResult wxPayOrderQueryResult = wxPayService.queryOrder(null, orderId);
            if (wxPayOrderQueryResult.getTradeState().equals("SUCCESS")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAID);
                log.info("user openid :{} ,pay succeed update OrderTransaction id {} ,wxOrderId: {}", wxPayOrderQueryResult.getOpenid(), orderId, wxPayOrderQueryResult.getTransactionId());
                log.info("user openid :{} ,pay succeed update OrderGood by OrderTransaction id {} ,wxOrderId: {}", wxPayOrderQueryResult.getOpenid(), orderId, wxPayOrderQueryResult.getTransactionId());
            } else if (wxPayOrderQueryResult.getTradeState().equals("CLOSED") || wxPayOrderQueryResult.getTradeState().equals("PAYERROR")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAY_FAILED);
                log.info("user openid :{} ,pay failed update OrderTransaction id {} ,wxOrderId: {}", wxPayOrderQueryResult.getOpenid(), orderId, wxPayOrderQueryResult.getTransactionId());
            }
            res.put("data", wxPayOrderQueryResult);
            this.success(res);
        } catch (BizException ex) {
            log.info("wxpay/checkOrderResult info BizException msg : {} ", ex);
            this.failed(res, ex);
        } catch (Exception e) {
            log.error("微信订单查询异常,异常原因{}", e);
            this.failed(res, "微信订单查询异常");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "checkAppOrderResult")
    public Map checkAppOrderResult(@RequestParam(value = "orderGoodId", required = true) String orderGoodId, HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> res = new HashedMap();
        try {
            WxPayService appWxPayService = new WxPayServiceImpl();
            WxPayConfig wxPayConfig = new WxPayConfig();
            wxPayConfig.setAppId(mainConfig.getMobileAppid());
            wxPayConfig.setMchId(mainConfig.getMobilePartenerId());
            wxPayConfig.setMchKey(mainConfig.getMobilePartenerKey());
            wxPayConfig.setNotifyUrl(mainConfig.getDomain() + "/wxpay/apppayresult");
            wxPayConfig.setTradeType("APP");
            appWxPayService.setConfig(wxPayConfig);

            OrderGood orderGood = orderService.getOrderById(orderGoodId);
            String orderId = orderGood.getTransactionOrderId();
            if (CommonUtils.isEmpty(orderId)) {
                throw new BizException("交易订单信息不存在");
            }
            WxUser wxUser = (WxUser) getCurrentUser();
            if (!orderGood.getUserId().equals(wxUser.getId())) {
                throw new BizException("非本用户订单，无法查询");
            }
            WxPayOrderQueryResult wxPayOrderQueryResult = appWxPayService.queryOrder(null, orderId);
            if (wxPayOrderQueryResult.getTradeState().equals("SUCCESS")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAID);
                log.info("user openid :{} ,pay succeed update OrderTransaction id {} ,wxOrderId: {}", wxPayOrderQueryResult.getOpenid(), orderId, wxPayOrderQueryResult.getTransactionId());
                log.info("user openid :{} ,pay succeed update OrderGood by OrderTransaction id {} ,wxOrderId: {}", wxPayOrderQueryResult.getOpenid(), orderId, wxPayOrderQueryResult.getTransactionId());
            } else if (wxPayOrderQueryResult.getTradeState().equals("CLOSED") || wxPayOrderQueryResult.getTradeState().equals("PAYERROR")) {
                orderService.updateOrderTransactionStatus(orderId, PayStatusEnum.PAY_FAILED);
                log.info("user openid :{} ,pay failed update OrderTransaction id {} ,wxOrderId: {}", wxPayOrderQueryResult.getOpenid(), orderId, wxPayOrderQueryResult.getTransactionId());
            }
            res.put("data", wxPayOrderQueryResult);
            this.success(res);
        } catch (BizException ex) {
            log.info("wxpay/checkAppOrderResult info BizException msg : {} ", ex);
            this.failed(res, ex);
        } catch (Exception e) {
            log.error("微信订单查询异常,异常原因{}", e);
            this.failed(res, "微信订单查询异常");
        }
        return res;
    }
}
