package fm.cheyiwang.controller;

import com.alibaba.fastjson.JSON;
import fm.yichenet.mongo.service.MerchantService;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.UserService;
import fm.web.MediaTypes;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/21.
 */
@Controller
@RequestMapping("merchant")
public class MerchantController extends BaseController {

    @Autowired
    MerchantService merchantService;

    @Autowired
    UserService userService;

    /**
     * 提交商户认证信息
     *
     * @param params
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "commitAuthInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String commitAuthInfo(@RequestBody Map<String, Object> params, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            WxUser user = (WxUser) getCurrentUser();
            params.put("id", user.getId());
            merchantService.addMerchantAuthInfo(params);
            this.success(modelMap);
        } catch (BizException e) {
            this.failed(modelMap, e);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 更新商户认知信息
     *
     * @param params
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "updateAuthInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String updateAuthInfo(@RequestBody Map params, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            WxUser user = (WxUser) getCurrentUser();
            params.put("id", user.getId());
            merchantService.updateAuthInfo(params);
            this.success(modelMap);
        } catch (BizException e) {
            this.failed(modelMap, e);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 获取已上传的商户认证信息
     *
     * @param modelMap
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "getAuthInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getAuthInfo(ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            WxUser user = (WxUser) getCurrentUser();
            Map<String, Object> params = new HashMap();
            params.put("id", user.getId());
            Map merchant = merchantService.getMerchantAuthInfo(params);
            merchant.remove("_id");
            merchant.put("user_type", user.getUserType());
            Map<String, Object> data = new HashedMap();
            data.put("merchant", merchant);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException e) {
            this.failed(modelMap, e);
        }
        return JSON.toJSONString(modelMap);
    }
}
