package fm.cheyiwang.controller;

import com.alibaba.fastjson.JSON;
import fm.yichenet.mongo.service.EnginemenService;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.UserService;
import fm.web.MediaTypes;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/21.
 */
@Controller
@RequestMapping("enginemen")
public class EnginemenController extends BaseController {

    @Autowired
    EnginemenService enginemenService;

    @Autowired
    UserService userService;

    /**
     * 提交维修技师认证资料
     * @param params
     * @param modelMap
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "commitAuthInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String commitAuthInfo(@RequestBody Map<String,Object> params, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try{
            WxUser user = (WxUser) getCurrentUser();
            params.put("id",user.getId());
            enginemenService.addEnginemenAuthInfo(params);
            this.success(modelMap);
        }catch (BizException e){
            this.failed(modelMap,e);
        }
        return JSON.toJSONString(modelMap);

    }

    /**
     * 更新维修技师认证资料
     * @param params
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "updateAuthInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String updateAuthInfo(@RequestBody Map params,ModelMap modelMap) throws Exception{
        modelMap.clear();
        try{
            WxUser user = (WxUser) getCurrentUser();
            params.put("id",user.getId());
            enginemenService.updateAuthInfo(params);
            this.success(modelMap);
        }catch (BizException e){
            this.failed(modelMap,e);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 获取已上传的认证资料
     * @param modelMap
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "getAuthInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getAuthInfo(ModelMap modelMap) throws Exception {
        modelMap.clear();
        try{
            WxUser user = (WxUser) getCurrentUser();
            Map<String,Object> params = new HashMap();
            params.put("id",user.getId());
            Map enginemen = enginemenService.getEnginemenAuthInfo(params);
            enginemen.remove("_id");
            Map<String,Object> data = new HashedMap();
            data.put("enginemen",enginemen);
            modelMap.put("data",data);
            this.success(modelMap);
        }catch (BizException e){
            this.failed(modelMap,e);
        }
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "mergeExperience", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map mergeExperience(@RequestBody Map params) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser user = (WxUser) getCurrentUser();
            params.put("user_id",user.getId());
            params = enginemenService.mergeExperience(params);
            res.put("data",params);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;

    }

    @ResponseBody
    @RequestMapping(value = "mergeSkill", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map mergeSkill(@RequestBody Map params) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser user = (WxUser) getCurrentUser();
            params.put("user_id",user.getId());
            params = enginemenService.mergeSkill(params);
            res.put("data",params);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "deleteSkill", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map deleteSkill(@RequestParam(value = "id",required = true) String id) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            enginemenService.deleteSkill(id);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "deleteExperience", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map deleteExperience(@RequestParam(value = "id",required = true) String id) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            enginemenService.deleteExperience(id);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "getExperienceList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map getExperienceList() throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            Map<String,Object> params = new HashedMap();
            WxUser user = (WxUser) getCurrentUser();
            params.put("user_id",user.getId());
            List list = enginemenService.getExperienceList(params);
            res.put("data",list);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "getSkillList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map getSkillList() throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            Map<String,Object> params = new HashedMap();
            WxUser user = (WxUser) getCurrentUser();
            params.put("user_id",user.getId());
            List list = enginemenService.getSkillList(params);
            res.put("data",list);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }
}
