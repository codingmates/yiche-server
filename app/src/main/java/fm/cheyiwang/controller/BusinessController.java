package fm.cheyiwang.controller;

import com.mongodb.BasicDBObject;
import fm.controller.BaseController;
import fm.dao.MongoBaseDao;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/busi")
public class BusinessController extends BaseController {

    @Autowired
    MongoBaseDao mongoBaseDao;

    @ResponseBody
    @RequestMapping("/add")
    public Map sendLoginVerifyCode(String name, String phone, String qq, String content) {
        Map res = new HashMap();
        try {
            if (StringUtils.isNotEmpty(name)) {
                name = URLDecoder.decode(name, "UTF-8");
            }
            if (StringUtils.isNotEmpty(content)) {
                content = URLDecoder.decode(content, "UTF-8");
            }
            if (StringUtils.isEmpty(phone)) {
                throw new BizException("请留下您的手机号码以方便我们联系您!");
            }

            BasicDBObject object = new BasicDBObject();
            object.put("name", name);
            object.put("phone", phone);
            object.put("content", content);
            object.put("qq", qq);

            mongoBaseDao.insert(object, MongoTable.business_log);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
