package fm.cheyiwang.controller;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.cache.GoodClassCache;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.exception.TokenInvalidException;
import fm.util.CommonUtils;
import fm.web.CurrentRequest;
import fm.web.MediaTypes;
import fm.yichenet.mongo.service.ExpressService;
import fm.yichenet.mongo.service.GoodMgrService;
import fm.yichenet.mongo.service.VisitHistoryService;
import org.apache.commons.collections.map.HashedMap;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by 63267 on 2017/6/9.
 */
@Controller
@RequestMapping("appgood")
public class AppGoodController extends BaseController {

    @Autowired
    GoodMgrService goodMgrService;
    @Autowired
    VisitHistoryService visitHistoryService;

    @Autowired
    ExpressService expressService;
    /**
     * 获取商品信息
     *
     * @param goodId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "goodInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map goodInfo(@RequestParam(value = "goodId", required = true) String goodId) throws Exception {
        Map<String, Object> res = new HashedMap();
        Map<String, Object> params = new HashedMap();
        params.put("good_id", goodId);
        DBObject good = goodMgrService.getGood(params);
        if(good != null){
            Long goodClassId = Long.parseLong(""+ good.get("good_class_id"));
            //TODO 商品价格是否显示为0 要根据类别是否开启了隐藏价格 如果开启了此属性，该类商品的价格全部显示为零 ，订单流程也要按照待改价的商品来处理
            if (GoodClassCache.isHidePrice(goodClassId)) {
                good.put("sale_price", 0);
                good.put("source_price", 0);
            }
        }
        if (!"1".equals(String.valueOf(good.get("status")))) {
            this.failed(res, "商品已下架");
            return res;
        }

        try {
            WxUser user = (WxUser) CurrentRequest.getCurrentUser();
            if (user != null) {
                DBObject log= new BasicDBObject();
                log.put("good_class_id",good.get("good_class_id"));
                log.put("shop_id",good.get("wx_user_id"));
                log.put("bigType",good.get("bigType"));
                log.put("visit_time",new Date());
                log.put("wx_user_id",user.getId());
                visitHistoryService.addVisitLog(log);
            }
        }catch (TokenInvalidException ex){
        }catch (Exception ex){
            LOGGER.error("获取用户登陆信息失败:",ex);
        }

        //获取评论的相关数据
        good.put("comments_count", goodMgrService.facetCommentLevel(goodId));
        good.put("comments_preview", goodMgrService.getGoodCommentListForPage(params, 5, 1));
        res.put("data", good.toMap());
        this.success(res);
        return res;
    }

    /**
     * 商品评论列表
     *
     * @param pageSize
     * @param pageNum
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "goodCommentsListForPage", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map goodCommentsListForPage(@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                                       @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                       @RequestParam(value = "goodId", required = true) String goodId,
                                       @RequestParam(value = "level", required = true) Integer level) throws Exception {
        Map<String, Object> res = new HashedMap();
        Map<String, Object> params = new HashedMap();
        params.put("good_id", goodId);
        params.put("level",level);
        List list = goodMgrService.getGoodCommentListForPage(params, pageSize, pageNum);
        res.put("data", list);
        this.success(res);
        return res;
    }

    /**
     * 评论商品
     *
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "goodComment", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map goodComment(@RequestBody Map params) throws Exception {
        Map<String, Object> res = new HashedMap();
        try {
            WxUser wxUser = (WxUser) getCurrentUser();
            Map data = goodMgrService.goodComment(params, wxUser);
            res.put("data", data);
            this.success(res);
        } catch (BizException ex) {
            this.failed(res, ex);
        }

        return res;
    }

    /**
     * 商品发布
     *
     * @param good
     * @return
     */
    @ResponseBody
    @RequestMapping("/publish")
    public Map goodPublish(@RequestBody Map good) {
        Map res = new HashMap();
        try {
            WxUser user = (WxUser) CurrentRequest.getCurrentUser();
            if (user.getReviewStatus() != 2) {
                throw new BizException("您的认证资料尚未通过审核无法发布商品，请耐心等待，或者联系平台客服了解进度！");
            }
            Map param = getEmptyParamMap();
            param.put("wx_user_id", getCurrentUserId());
            param.putAll(good);
            param.put("priority",0);
            param.put("good_id", UUID.randomUUID().toString().replaceAll("-", ""));
            BigDecimal lat = user.getLatitude();
            BigDecimal lon = user.getLongitude();
            if (lat == null || lon == null) {
                throw new BizException("您的店铺尚未提交实体店地址经纬度，请先配置店铺地址后再发布商品！");
            }
            double latd = lat != null ? lat.doubleValue() : 0d;
            double lngd = lon != null ? lon.doubleValue() : 0d;

            param.put("loc", Math.min(latd, lngd) + "," + Math.max(latd, lngd));
            param.put("address", user.getCity() + "·" + user.getDistrict());
            param.put("status", 1+"");

            goodMgrService.addGood(param);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
