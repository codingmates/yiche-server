package fm.cheyiwang.controller;

import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.web.MediaTypes;
import fm.yichenet.mongo.service.ShoppingCartService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by 63267 on 2017/6/18.
 */
@Controller
@RequestMapping("cart")
public class ShoppingCartController extends BaseController {

    @Autowired
    ShoppingCartService shoppingCartService;


    @ResponseBody
    @RequestMapping(value = "addCart", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map addCart(@RequestParam(value = "goodId",required = true) String goodId,
                       @RequestParam(value = "goodNum",required = true) Integer goodNum) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            DBObject dbObject = shoppingCartService.addShoppingCart(wxUser,goodNum,goodId);
            res.put("data",dbObject);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "updateCart", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map updateCart(@RequestParam(value = "goodId",required = true) String goodId,
                       @RequestParam(value = "goodNum",required = true) Integer goodNum) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            DBObject dbObject = shoppingCartService.editShoppingCart(wxUser,goodId,goodNum);
            res.put("data",dbObject);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "deleteCart", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map deleteCart(@RequestParam(value = "goodId",required = true) String goodId) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            shoppingCartService.deleteShoppingCart(wxUser,goodId);

            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "listForPage", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map listForPage(@RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize,
                           @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum) throws Exception {
        Map<String,Object> res = new HashedMap();
        try{
            WxUser wxUser = (WxUser)getCurrentUser();
            List list = shoppingCartService.getShoppingCartForPage(wxUser,pageSize,pageNum);
            res.put("data",list);
            this.success(res);
        }catch (BizException ex){
            this.failed(res,ex);
        }
        return res;
    }
}

