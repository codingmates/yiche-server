package fm.cheyiwang.controller;

import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.UserService;
import fm.web.MediaTypes;
import fm.yichenet.mongo.service.SmsService;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-09-19.
 */
@Controller
@RequestMapping("/app/sms")
public class AppSmsController extends BaseController {

    @Autowired
    SmsService smsService;

    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping(value = "/send", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map sendLoginVerifyCode(String phone) {
        Map res = new HashMap();
        try {
            if (StringUtils.isEmpty(phone)) {
                throw new BizException("手机号码不能为空");
            }
            WxUser wxUser = userService.getUserByPhone(phone);
            if(wxUser == null){
                throw new BizException("未找到该手机用户，请先绑定后再登录!");
            }
            smsService.businessSmsSend(phone, "LOGIN_VERIFY");
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur app error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }

        return res;
    }
}
