package fm.cheyiwang.controller;

import com.alibaba.fastjson.JSON;
import fm.util.HuanXinSdkUtil;
import fm.entity.AccountBalance;
import fm.entityEnum.OrderEnum;
import fm.yichenet.mongo.service.SmsService;
import fm.service.UserService;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.util.CommonUtils;
import fm.util.Constant;
import fm.web.MediaTypes;
import fm.yichenet.mongo.service.EnginemenService;
import fm.yichenet.mongo.service.MerchantService;
import fm.yichenet.service.AccountBalanceService;
import fm.yichenet.service.OrderService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/14.
 */
@Controller
@RequestMapping("wxUser")
public class WxUserController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WxUserController.class);

    @Autowired
    UserService userService;
    @Autowired
    MerchantService merchantService;
    @Autowired
    EnginemenService enginemenService;

    @Autowired
    AccountBalanceService accountBalanceService;
    @Autowired
    OrderService orderService;

    @Autowired
    SmsService smsService;

    /**
     * 获取信息授权用户信息
     *
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "userInfo", method = RequestMethod.GET, produces = MediaTypes.JSON_UTF_8)
    public String wxIndex(ModelMap modelMap) throws Exception {
        Map<String, Object> data = new HashedMap();
        WxUser wxUser = (WxUser) getCurrentUser();
        wxUser = userService.getById(wxUser.getId());
        if (CommonUtils.isEmpty(wxUser)) {
            throw new BizException("用户不存在，无法获取用户信息");
        }
        Map<String, Object> userData = userObjectToDataMap(wxUser);
        AccountBalance accountBalance = accountBalanceService.addOrGetBalance(wxUser.getId());
        if (CommonUtils.isEmpty(accountBalance)) {
            userData.put("account", 0);
        } else {
            userData.put("account", accountBalance.getBalance());
        }

        data.put("wxUser", userData);


        //获取认证资料
        Map param = new HashMap();
        param.put("id", wxUser.getId());
        if (wxUser.getUserType() == 3 || wxUser.getUserType() == 4) {
            Map authInfo = merchantService.getMerchantAuthInfo(param);
            data.put("authInfo", authInfo);
        } else if (wxUser.getUserType() == 2) {
            Map authInfo = enginemenService.getEnginemenAuthInfo(param);
            data.put("authInfo", authInfo);
        }


        modelMap.put("data", data);
        this.success(modelMap);
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "login", produces = MediaTypes.JSON_UTF_8)
    public String login(ModelMap modelMap) throws Exception {
        Map<String, Object> data = new HashedMap();
        WxUser wxUser = (WxUser) getCurrentUser();
        Map<String, Object> userData = userObjectToDataMap(wxUser);
        AccountBalance accountBalance = accountBalanceService.addOrGetBalance(wxUser.getId());
        if (CommonUtils.isEmpty(accountBalance)) {
            userData.put("account", 0);
        } else {
            userData.put("account", accountBalance.getBalance());
        }
        data.put("wxUser", userData);

        //获取认证资料
        Map param = new HashMap();
        param.put("id", wxUser.getId());
        if (wxUser.getUserType() == 3 || wxUser.getUserType() == 4) {
            Map authInfo = merchantService.getMerchantAuthInfo(param);
            data.put("authInfo", authInfo);
        } else if (wxUser.getUserType() == 2) {
            Map authInfo = enginemenService.getEnginemenAuthInfo(param);
            data.put("authInfo", authInfo);
        }
        modelMap.put("data", data);
        this.success(modelMap);
        return JSON.toJSONString(modelMap);
    }

    public Map<String, Object> userObjectToDataMap(WxUser wxUser) {
        Map<String, Object> userData = new HashedMap();
        userData.put("userId", wxUser.getId());
        userData.put("nickname", CommonUtils.isEmpty(wxUser.getNickname()) ? "" : wxUser.getNickname());
        userData.put("headimgurl", CommonUtils.isEmpty(wxUser.getHeadimgurl()) ? "" : wxUser.getHeadimgurl());
        userData.put("country", CommonUtils.isEmpty(wxUser.getCountry()) ? "" : wxUser.getCountry());
        userData.put("city", CommonUtils.isEmpty(wxUser.getCity()) ? "" : wxUser.getCity());
        userData.put("province", CommonUtils.isEmpty(wxUser.getProvince()) ? "" : wxUser.getProvince());
        userData.put("remark", CommonUtils.isEmpty(wxUser.getRemark()) ? "" : wxUser.getRemark());
        userData.put("subscribed", CommonUtils.isEmpty(wxUser.getSubscribed()) ? "" : wxUser.getSubscribed() + "");
        userData.put("telephone", CommonUtils.isEmpty(wxUser.getTelephone()) ? "" : wxUser.getTelephone());
        userData.put("openid", CommonUtils.isEmpty(wxUser.getOpenid()) ? "" : wxUser.getOpenid());
        userData.put("iosOpenid", CommonUtils.isEmpty(wxUser.getIosOpenid()) ? "" : wxUser.getIosOpenid());
        userData.put("androidOpenid", CommonUtils.isEmpty(wxUser.getAndroidOpenid()) ? "" : wxUser.getAndroidOpenid());
        userData.put("unionid", CommonUtils.isEmpty(wxUser.getUnionId()) ? "" : wxUser.getUnionId());
        userData.put("registerType", CommonUtils.isEmpty(wxUser.getRegisterType()) ? "" : wxUser.getRegisterType());
        userData.put("firstTime", CommonUtils.isEmpty(wxUser.getFirstTime()) ? "" : wxUser.getFirstTime().getTime() + "");
        userData.put("firstShopId", CommonUtils.isEmpty(wxUser.getFirstShopId()) ? "" : wxUser.getFirstShopId() + "");
        userData.put("invalid", CommonUtils.isEmpty(wxUser.getInvalid()) ? "" : wxUser.getInvalid() + "");
        userData.put("userType", CommonUtils.isEmpty(wxUser.getUserType()) ? "" : wxUser.getUserType() + "");
        userData.put("reviewStatus", CommonUtils.isEmpty(wxUser.getReviewStatus()) ? "" : wxUser.getReviewStatus() + "");
        userData.put("reviewNotes", CommonUtils.isEmpty(wxUser.getReviewNotes()) ? "" : wxUser.getReviewNotes());
        userData.put("hxUsername", CommonUtils.isEmpty(wxUser.getHxUsername()) ? "" : wxUser.getHxUsername());
        userData.put("hxPassword", CommonUtils.isEmpty(wxUser.getHxPassword()) ? "" : wxUser.getHxPassword());
        userData.put("token", CommonUtils.isEmpty(wxUser.getToken()) ? "" : wxUser.getToken());
        if (wxUser.getUserType() == 3 || wxUser.getUserType() == 4) {
            long checkOrderAmount = orderService.countShopOrder(OrderEnum.WAIT_EDIT_PRICE, wxUser.getId());
            long payedOrderAmount = orderService.countShopOrder(OrderEnum.PAYMENT_SUCCESS, wxUser.getId());
            userData.put("orderAmount", checkOrderAmount + payedOrderAmount);
            userData.put("checkOrderAmount", checkOrderAmount);
            userData.put("payedOrderAmount", payedOrderAmount);
        }
        return userData;
    }

    /**
     * 获取用户基础信息:头像 昵称 认证类型
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/userBasicInfo")
    public Map getUserBasicInfo(String userId) {
        Map res = new HashMap();
        try {
            if (userId == null) {
                throw new BizException("参数缺失");
            }

            if (StringUtils.isNotEmpty(userId)) {
                HashMap data = new HashMap();

                if (StringUtils.equals(userId, "admin")) {

                    data.put("nickName", "系统管理员");
                    data.put("headImgUrl", "http://wx.qlogo.cn/mmopen/vi_32/Wb43ViaHebfWYxlz8icN5OP7SPWXe6bbHxnic6e1WxDFic6upf4u4gHPpHXLVufAUWT88qibJeKykEtVIQjqlK6zDeQ/0");
                    data.put("userId", 0L);
                    data.put("hxId", "admin");
                    res.put("data",data);
                    this.success(res);
                    return res;

                }

                if (StringUtils.equals(userId, "system")) {
                    data.put("nickName", "系统消息");
                    data.put("headImgUrl", "http://wx.qlogo.cn/mmopen/vi_32/Wb43ViaHebfWYxlz8icN5OP7SPWXe6bbHxnic6e1WxDFic6upf4u4gHPpHXLVufAUWT88qibJeKykEtVIQjqlK6zDeQ/0");
                    data.put("userId", 0L);
                    data.put("hxId", "system");
                    res.put("data",data);
                    this.success(res);
                    return res;
                }

                if (StringUtils.equals(userId, "forum")) {
                    data.put("nickName", "社区助手");
                    data.put("headImgUrl", "http://www.cdmates.com/resources/upload/image/1508333373870.png");
                    data.put("userId", 0L);
                    data.put("hxId", "forum");
                    res.put("data",data);
                    this.success(res);
                    return res;
                }
            }

            WxUser user = userService.getByHxUserName("wx_" + userId);

            if (user == null) {
                throw new BizException("未找到相关用户信息，错误代码:" + userId);
            }
            String nickName = user.getNickname();
            String headImgUrl = user.getHeadimgurl();

            Map data = new HashMap();

            if (StringUtils.isEmpty(user.getHxUsername())) {
                try {
                    String hxUsername = "wx_" + user.getId();
                    String hxPassword = Base64.encodeToString(String.valueOf(user.getId()).getBytes());
                    HuanXinSdkUtil.registUser(hxUsername, hxPassword, user.getNickname());

                    user.setHxUsername(hxUsername);
                    user.setHxPassword(hxPassword);
                    userService.saveOrUpdate(user);

                    LOGGER.info("获取用户基础信息发现未注册环信用户:{}", user.getId());
                } catch (Exception ex) {
                    LOGGER.error("注册环信用户失败:%o", ex);
                }
            }

            data.put("nickName", nickName);
            data.put("headImgUrl", headImgUrl);
            data.put("userId", user.getId());
            data.put("hxId", user.getHxUsername());
            res.put("data", data);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "updateUserInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map updateUserInfo(@RequestBody Map<String, Object> userInfo) throws Exception {
        Map<String, Object> res = new HashedMap();
        try {
            WxUser wxUser = (WxUser) getCurrentUser();
            wxUser = userService.updateUserInfo(userInfo, wxUser);
            Session session = SecurityUtils.getSubject().getSession();
            session.setAttribute(Constant.SESSION_LOGIN_USER, wxUser);
            this.success(res);
        } catch (BizException ex) {
            this.failed(res, ex.getMessage());
        }
        return res;
    }


    /**
     * 发送短信
     *
     * @param phone
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "sendAuthSms", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map sendAuthSms(@RequestParam(value = "phone", required = true) String phone) throws Exception {
        Map<String, Object> res = new HashedMap();
        try {
            smsService.businessSmsSend(phone, "REGISTER_VERIFY");
            this.success(res);
        } catch (BizException ex) {
            this.failed(res, ex);
        }
        return res;
    }

    /**
     * 手机信息验证入库（找回旧用户信息）
     *
     * @param phone
     * @param code
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "authUserInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map authUserInfo(@RequestParam(value = "phone", required = true) String phone,
                            @RequestParam(value = "code", required = true) String code,
                            String state,
                            @RequestParam(value = "recoverOld", required = false, defaultValue = "false") Boolean recoverOld) throws Exception {
        Map<String, Object> res = new HashedMap();

        try {
            if (StringUtils.isEmpty(state)) {
                throw new BizException("未知的客户端，无法完成绑定!");
            }
            WxUser wxUser = (WxUser) getCurrentUser();
            smsService.checkAuthUserSms(phone, code);
            if (recoverOld) {
                wxUser = userService.updateOldUserInfo(wxUser, phone, state);
                Session session = SecurityUtils.getSubject().getSession();
                session.setAttribute(Constant.SESSION_LOGIN_USER, wxUser);
            }

            this.success(res);
        } catch (BizException ex) {
            this.failed(res, ex);
        }
        return res;
    }
}


