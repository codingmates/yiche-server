package fm.cheyiwang.quartz;

import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryResult;
import com.github.binarywang.wxpay.service.WxPayService;
import fm.cache.GoodClassCache;
import fm.entity.OrderTransaction;
import fm.entityEnum.PayStatusEnum;
import fm.yichenet.service.OrderService;
import me.chanjar.weixin.common.exception.WxErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by CM on 2017-09-01.
 */
@Component
public class GoodClassCacheReloadQuartz {

    private static final Logger log = LoggerFactory.getLogger(GoodClassCacheReloadQuartz.class);

    @Autowired
    OrderService orderService;

    @Autowired
    WxPayService wxPayService;

    public void run() {

        try {
            log.info("商品类别缓存重置");
            GoodClassCache.loadCache();
        } catch (Exception ex) {
            log.error("商品类别缓存重置发生错误:", ex);
        }

    }
}
