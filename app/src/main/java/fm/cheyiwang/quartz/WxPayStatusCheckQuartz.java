package fm.cheyiwang.quartz;

import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryResult;
import com.github.binarywang.wxpay.service.WxPayService;
import fm.entity.OrderTransaction;
import fm.entityEnum.OrderEnum;
import fm.entityEnum.PayStatusEnum;
import fm.yichenet.service.OrderService;
import me.chanjar.weixin.common.exception.WxErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by 宏炜 on 2017-07-01.
 */
@Component
public class WxPayStatusCheckQuartz {

    private static final Logger log = LoggerFactory.getLogger(WxPayStatusCheckQuartz.class);

    @Autowired
    OrderService orderService;

    @Autowired
    WxPayService wxPayService;

    public void run() {

        try {
            log.info("微信支付订单状态扫描定时器启动");
            List<OrderTransaction> orderTransactionList = orderService.getOrderTransactionPayingList();
            for(OrderTransaction orderTransaction : orderTransactionList){
                try{
                    WxPayOrderQueryResult wxPayOrderQueryResult = wxPayService.queryOrder(null,orderTransaction.getId());
                    if(wxPayOrderQueryResult.getTradeState().equals("SUCCESS")){
                        orderService.updateOrderTransactionStatus(orderTransaction.getId(), PayStatusEnum.PAID);
                        log.info("user openid :{} ,pay succeed update OrderTransaction id {} ,wxOrderId: {}",wxPayOrderQueryResult.getOpenid(),orderTransaction.getId(),wxPayOrderQueryResult.getTransactionId());
//                    orderService.updateOrderGoodStatus(orderTransaction.getId(), OrderEnum.PAYMENT_SUCCESS);
                        log.info("user openid :{} ,pay succeed update OrderGood by OrderTransaction id {} ,wxOrderId: {}",wxPayOrderQueryResult.getOpenid(),orderTransaction.getId(),wxPayOrderQueryResult.getTransactionId());
                    }else if(wxPayOrderQueryResult.getTradeState().equals("CLOSED") || wxPayOrderQueryResult.getTradeState().equals("PAYERROR")){
                        orderService.updateOrderTransactionStatus(orderTransaction.getId(),PayStatusEnum.PAY_FAILED);
                        log.info("user openid :{} ,pay failed update OrderTransaction id {} ,wxOrderId: {}",wxPayOrderQueryResult.getOpenid(),orderTransaction.getId(),wxPayOrderQueryResult.getTransactionId());
                    }else{
                        orderService.updateOrderTransactionStatus(orderTransaction.getId(),PayStatusEnum.PAY_TIMEOUT);
                        log.info("user openid :{} ,pay timeout update OrderTransaction id {} ,wxOrderId: {}",wxPayOrderQueryResult.getOpenid(),orderTransaction.getId(),wxPayOrderQueryResult.getTransactionId());
                    }
                }catch (WxErrorException e){
                    orderService.updateOrderTransactionStatus(orderTransaction.getId(), PayStatusEnum.PAY_FAILED);
                    log.error("微信订单查询错误：{}",e.getError().toString());
                }

            }

        } catch (Exception ex) {
            log.error("微信支付订单状态扫描定时器异常:", ex);
        }

    }
}
