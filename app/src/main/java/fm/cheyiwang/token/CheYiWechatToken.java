package fm.cheyiwang.token;

import fm.entity.WxUser;
import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

/**
 * Created by 63267 on 2017/5/14.
 */
public class CheYiWechatToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
    private String devType;
    private String openId;
    private String host;
    private WxUser wxUser;


    public CheYiWechatToken(String devType, String openId,WxUser wxUser) {
        this(devType, openId,wxUser, null);
    }

    public CheYiWechatToken(String devType, String openId,WxUser wxUser, String host) {
        this.devType = devType;
        this.openId = openId;
        this.wxUser = wxUser;
        this.host = host;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public boolean isRememberMe() {
        return true;
    }

    @Override
    public Object getPrincipal() {
        return this.openId;
    }

    @Override
    public Object getCredentials() {
        return this.devType;
    }

    public void clear() {
        this.devType = null;
        this.host = null;
        this.openId = null;
    }

    public String getDevType() {
        return devType;
    }

    public void setDevType(String devType) {
        this.devType = devType;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public WxUser getWxUser() {
        return wxUser;
    }

    public void setWxUser(WxUser wxUser) {
        this.wxUser = wxUser;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "CheYiWechatToken{" +
                "devType='" + devType + '\'' +
                ", openId='" + openId + '\'' +
                ", host='" + host + '\'' +
                '}';
    }
}
