package fm.cheyiwang.filter;

import com.alibaba.fastjson.JSON;
import com.tencent.Main;
import fm.cheyiwang.token.CheYiWechatToken;
import fm.config.MainConfig;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.UserService;
import fm.util.CommonUtils;
import fm.util.Constant;
import fm.yichenet.service.AdminUserService;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/14.
 */
public class WechatPermissionsAuthorizationFilter extends AuthorizationFilter {

    @Autowired
    protected WxMpConfigStorage configStorage;
    @Autowired
    protected WxMpService wxMpService;
    @Autowired
    HttpSession session;

    @Autowired
    UserService userService;

    @Autowired
    MainConfig mainConfig;

    @Autowired
    SessionDAO sessionDAO;

    @Autowired
    AdminUserService adminUserService;


    private static final Logger LOGGER = LoggerFactory.getLogger(WechatPermissionsAuthorizationFilter.class);

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {

        try {
            if (isLogined()) {
                LOGGER.info("已登录用户访问");
                String redirectUrl = servletRequest.getParameterMap().containsKey("redirectUrl") ? servletRequest.getParameter("redirectUrl") : null;
                if (!CommonUtils.isEmpty(redirectUrl) && redirectUrl != null) {
                    LOGGER.info("重定向URL：{}", URLDecoder.decode(redirectUrl, "UTF-8"));
                    WebUtils.issueRedirect(servletRequest, servletResponse, getRedirectUrl(URLDecoder.decode(redirectUrl, "UTF-8")));
                }
                return true;
            } else {
                LOGGER.info("未登录用户访问");
                String state = servletRequest.getParameter("state");
                if (CommonUtils.isEmpty(state)) {
                    writeExceptionMsg(servletRequest, servletResponse, "state状态标识不能为空");
                    return false;
                }

                if (CommonUtils.isEmpty(state)) {
                    writeExceptionMsg(servletRequest, servletResponse, "state状态标识错误,无法授权");
                    return false;
                }

                if (MainConfig.weChatApp.equals(state)) {
                    return weChatAppLogin(servletRequest, servletResponse);
                } else if (MainConfig.iosApp.equals(state)) {
                    return iosAppLogin(servletRequest, servletResponse);
                } else if (MainConfig.androidApp.equals(state)) {
                    return androidAppLogin(servletRequest, servletResponse);
                } else if (MainConfig.appPhone.equals(state)) {
                    return phoneAppLogin(servletRequest, servletResponse);
                }
            }
        } catch (BizException ex) {
            LOGGER.error("微信授权登陆失败", ex);
            writeExceptionMsg(servletRequest, servletResponse, "用户验证异常");
            return false;
        }
        return false;
    }

    public String getRedirectUrl(String redirectUrl) {
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        WxUser user = (WxUser) session.getAttribute(Constant.SESSION_LOGIN_USER);
        String[] redirectUrlArray = redirectUrl.split("\\?");
        redirectUrl = redirectUrlArray[0];
        if (redirectUrlArray.length > 1) {
            LOGGER.info("重定向参数信息:{}", redirectUrlArray[1]);
            String realParams;
            if (redirectUrlArray[1].indexOf("#/") >= 0) {
                String[] paramsArray = redirectUrlArray[1].split("#");
                realParams = "#" + paramsArray[1] + "?" + paramsArray[0];
            } else {
                realParams = "?" + redirectUrlArray[1];
            }
            redirectUrl = redirectUrl + realParams + "&token=" + user.getToken();
        } else {
            redirectUrl = redirectUrl + "?token=" + user.getToken();
        }
        return redirectUrl;
    }

    public String getRedirectUrl(String redirectUrl, String token) {

        String[] redirectUrlArray = redirectUrl.split("\\?");
        redirectUrl = redirectUrlArray[0];
        if (redirectUrlArray.length > 1) {
            LOGGER.info("重定向参数信息:{}", redirectUrlArray[1]);
            String realParams;
            if (redirectUrlArray[1].indexOf("#/") >= 0) {
                String[] paramsArray = redirectUrlArray[1].split("#");
                realParams = "#" + paramsArray[1] + "?" + paramsArray[0];
            } else {
                realParams = "?" + redirectUrlArray[1];
            }
            redirectUrl = redirectUrl + realParams + "&token=" + token;
        } else {
            redirectUrl = redirectUrl + "?token=" + token;
        }
        return redirectUrl;
    }

    public void setSelToken(WxUser wxUser, String openid) {
        String timeFormat = "00000000000000000000";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        DecimalFormat decimalFormat = new DecimalFormat(timeFormat);
        String timeStr = decimalFormat.format(timestamp.getTime());
        String tokenStr = timeStr + openid;
        AesCipherService aesCipherService = new AesCipherService();
        byte[] encodeData = aesCipherService.encrypt(tokenStr.getBytes(), Base64.decode(MainConfig.tokenAesKey.getBytes())).getBytes();
        tokenStr = Base64.encodeToString(encodeData);
        tokenStr = tokenStr.replaceAll("\\+", "-").replaceAll("/", "*");
        wxUser.setToken(tokenStr);
    }

    /**
     * 判断是否用户已经登录
     *
     * @return
     */
    private boolean isLogined() {
        Subject subject = SecurityUtils.getSubject();
        String openid = String.valueOf(subject.getPrincipal());
        if (subject.isAuthenticated()) {
            LOGGER.info("user authenticated before :{}", openid);
            if ("null".equals(openid) || StringUtils.isBlank(openid)) {
                sessionDAO.delete(subject.getSession());
                LOGGER.info("user Principal is not exit");
                return false;
            }
            WxUser wxUser = userService.getByOpenId(openid);
            if (CommonUtils.isEmpty(wxUser)) {
                sessionDAO.delete(subject.getSession());
                LOGGER.info("user data is not exit");
                return false;
            }
            subject.getSession().setAttribute(Constant.SESSION_LOGIN_USER, wxUser);
            return true;
        } /*else if (subject.isRemembered()) {
            LOGGER.info("user was remembered ：{}", openid);
            if("null".equals(openid) || StringUtils.isBlank(openid)){
                sessionDAO.delete(subject.getSession());
                LOGGER.info("user Principal is not exit");
                return false;
            }
            WxUser wxUser = userService.getByOpenId(openid);
            if(CommonUtils.isEmpty(wxUser)){
                sessionDAO.delete(subject.getSession());
                LOGGER.info("user data is not exit");
                return false;
            }
            subject.getSession().setAttribute(Constant.SESSION_LOGIN_USER, wxUser);
            return true;
        } */ else {
            return false;
        }
    }

    private boolean weChatAppLogin(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, WxErrorException {
        String code = servletRequest.getParameter("code");
        String lang = servletRequest.getParameter("lang");
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();

        LOGGER.info("微信公众号登录: code :{} lang: {}", code, lang);
        if (CommonUtils.isEmpty(code)) {
            String redirectUrl = servletRequest.getParameter("redirectUrl");
            session.setAttribute("redirectUrl", redirectUrl);
            LOGGER.info("重定向到微信服务器获取用户信息:{}", redirectUrl);
            WebUtils.issueRedirect(servletRequest, servletResponse, wxMpService.oauth2buildAuthorizationUrl(mainConfig.getDomain() + "/wxUser/login", "snsapi_userinfo", MainConfig.weChatApp));
            LOGGER.info("用户授权方式:snsapi_userinfo");
            return false;
        } else {
            LOGGER.info("获取access更新用户信息:{}", code);
            WxMpOAuth2AccessToken accessToken = this.wxMpService.oauth2getAccessToken(code);
            LOGGER.info("用户accessToken信息:{}", accessToken.getAccessToken());
            WxMpUser wxMpUser = this.wxMpService.oauth2getUserInfo(accessToken, null);
            WxUser wxUser = userService.getUserByWechatOpenid(wxMpUser.getOpenId());
            if (CommonUtils.isEmpty(wxUser)) {
                wxUser = new WxUser();
                wxUser.setOpenid(wxMpUser.getOpenId());
                wxUser.setNickname(wxMpUser.getNickname());
                wxUser.setProvince(wxMpUser.getProvince());
                wxUser.setCountry(wxMpUser.getCountry());
                wxUser.setCity(wxMpUser.getCity());
                wxUser.setHeadimgurl(wxMpUser.getHeadImgUrl());
                wxUser.setRegisterType(0);
                wxUser.setUserType(1);
                wxUser.setFirstTime(new Timestamp(System.currentTimeMillis()));
                wxUser.setInvalid(0);
                wxUser.setReviewStatus(0);
                //wxUser.setSubscribed(wxMpUser.getSubscribe() == true ? 1 : 0);
                wxUser.setRemark(wxMpUser.getRemark());
            } else {
                if (wxUser.getUserType() < 3) {
                    wxUser.setNickname(wxMpUser.getNickname());
                    userService.saveOrUpdate(wxUser);
                }
            }
            LOGGER.info("weChatApp tokenLogin openid {}", wxUser.getOpenid());
            setSelToken(wxUser, wxUser.getOpenid());
            tokenLogin(MainConfig.weChatApp, wxUser.getOpenid(), wxUser, servletRequest, servletResponse);


            String redirectUrl = String.valueOf(session.getAttribute("redirectUrl"));

            if (!CommonUtils.isEmpty(redirectUrl) && !redirectUrl.equals("null")) {
                redirectUrl = getRedirectUrl(redirectUrl, wxUser.getToken());
                LOGGER.info("重定向到微信前端:{}", redirectUrl);
                WebUtils.issueRedirect(servletRequest, servletResponse, redirectUrl);
            }
            return true;
        }
    }

    private boolean iosAppLogin(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String openId = httpServletRequest.getParameter("openId");
        if (CommonUtils.isEmpty(openId)) {
            writeExceptionMsg(servletRequest, servletResponse, "用户openid尚未获取,无法授权");
            return false;
        }

        String nickName = httpServletRequest.getParameter("nickName");
        String province = httpServletRequest.getParameter("province");
        String country = httpServletRequest.getParameter("country");
        String city = httpServletRequest.getParameter("city");
        String headImageUrl = httpServletRequest.getParameter("headImageUrl");
        String subscribed = httpServletRequest.getParameter("subscribed");
        String remark = httpServletRequest.getParameter("remark");

        WxUser wxUser = userService.getUserByIosOpenid(openId);

        if (CommonUtils.isEmpty(wxUser)) {
            wxUser = new WxUser();
            wxUser.setNickname(nickName);
            wxUser.setProvince(province);
            wxUser.setCountry(country);
            wxUser.setCity(city);
            wxUser.setHeadimgurl(headImageUrl);
            wxUser.setRegisterType(2);
            wxUser.setUserType(1);
            wxUser.setFirstTime(new Timestamp(System.currentTimeMillis()));
            wxUser.setInvalid(0);
            wxUser.setReviewStatus(0);
            if (!CommonUtils.isEmpty(subscribed)) {
                wxUser.setSubscribed(Integer.parseInt(subscribed));
            }
            wxUser.setRemark(remark);

        } else {
            if (wxUser.getUserType() < 3) {
                wxUser.setNickname(nickName);
                userService.saveOrUpdate(wxUser);
            }
        }
        wxUser.setIosOpenid(openId);
        setSelToken(wxUser, openId);
        return tokenLogin(MainConfig.iosApp, wxUser.getIosOpenid(), wxUser, servletRequest, servletResponse);
    }

    private boolean androidAppLogin(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String openId = httpServletRequest.getParameter("openId");
        if (CommonUtils.isEmpty(openId)) {
            writeExceptionMsg(servletRequest, servletResponse, "用户openid尚未获取,无法授权");
            return false;
        }

        String nickName = httpServletRequest.getParameter("nickName");
        String province = httpServletRequest.getParameter("province");
        String country = httpServletRequest.getParameter("country");
        String city = httpServletRequest.getParameter("city");
        String headImageUrl = httpServletRequest.getParameter("headImageUrl");
        String subscribed = httpServletRequest.getParameter("subscribed");
        String remark = httpServletRequest.getParameter("remark");

        WxUser wxUser = userService.getUserByAndroidOpenid(openId);

        if (CommonUtils.isEmpty(wxUser)) {
            wxUser = new WxUser();
            wxUser.setNickname(nickName);
            wxUser.setProvince(province);
            wxUser.setCountry(country);
            wxUser.setCity(city);
            wxUser.setHeadimgurl(headImageUrl);
            wxUser.setRegisterType(1);
            wxUser.setUserType(1);
            wxUser.setFirstTime(new Timestamp(System.currentTimeMillis()));
            wxUser.setInvalid(0);
            wxUser.setReviewStatus(0);
            if (!CommonUtils.isEmpty(subscribed)) {
                wxUser.setSubscribed(Integer.parseInt(subscribed));
            }
            wxUser.setRemark(remark);
        } else {
            if (wxUser.getUserType() < 3) {
                wxUser.setNickname(nickName);
                userService.saveOrUpdate(wxUser);
            }
        }
        wxUser.setAndroidOpenid(openId);
        setSelToken(wxUser, openId);
        return tokenLogin(MainConfig.androidApp, wxUser.getAndroidOpenid(), wxUser, servletRequest, servletResponse);
    }

    private boolean phoneAppLogin(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String phone = httpServletRequest.getParameter("phone");
        String code = httpServletRequest.getParameter("code");
        if("18359219330".equals(phone) && "171028".equals(code)){
            WxUser wxUser = userService.getUserByPhone(phone);
            String state = null;
            String openid = null;

            if (!CommonUtils.isEmpty(wxUser.getOpenid())) {
                state = MainConfig.weChatApp;
                openid = wxUser.getOpenid();
            } else if (!CommonUtils.isEmpty(wxUser.getAndroidOpenid())) {
                state = MainConfig.androidApp;
                openid = wxUser.getAndroidOpenid();
            } else if (!CommonUtils.isEmpty(wxUser.getIosOpenid())) {
                state = MainConfig.iosApp;
                openid = wxUser.getIosOpenid();
            } else {
                writeExceptionMsg(servletRequest, servletResponse, "用户信息不存在");
                return false;
            }
            setSelToken(wxUser, openid);
            return tokenLogin(state, openid, wxUser, servletRequest, servletResponse);
        }
        if (CommonUtils.isEmpty(phone)) {
            writeExceptionMsg(servletRequest, servletResponse, "用户手机号码不能为空");
            return false;
        }
        if (CommonUtils.isEmpty(code)) {
            writeExceptionMsg(servletRequest, servletResponse, "短信验证码不能为空");
            return false;
        }
        WxUser wxUser = userService.getUserByPhone(phone);
        if (CommonUtils.isEmpty(wxUser)) {
            writeExceptionMsg(servletRequest, servletResponse, "该手机用户不存在");
            return false;
        }

        try {
            boolean ret = adminUserService.checkSms(phone, code);
            if (!ret) {
                writeExceptionMsg(servletRequest, servletResponse, "短信验证码错误");
                return false;
            }
        } catch (BizException e) {
            writeExceptionMsg(servletRequest, servletResponse, e.getMessage());
            return false;
        }
        String state = null;
        String openid = null;

        if (!CommonUtils.isEmpty(wxUser.getOpenid())) {
            state = MainConfig.weChatApp;
            openid = wxUser.getOpenid();
        } else if (!CommonUtils.isEmpty(wxUser.getAndroidOpenid())) {
            state = MainConfig.androidApp;
            openid = wxUser.getAndroidOpenid();
        } else if (!CommonUtils.isEmpty(wxUser.getIosOpenid())) {
            state = MainConfig.iosApp;
            openid = wxUser.getIosOpenid();
        } else {
            writeExceptionMsg(servletRequest, servletResponse, "用户信息不存在");
            return false;
        }
        setSelToken(wxUser, openid);
        return tokenLogin(state, openid, wxUser, servletRequest, servletResponse);
    }

    private boolean tokenLogin(String state, String openid, WxUser wxUser, ServletRequest servletRequest, ServletResponse servletResponse) {
        CheYiWechatToken token = new CheYiWechatToken(state, openid, wxUser);
        try {
            LOGGER.info("用户【{}】正在请求授权认证 ...:", openid);
            Subject subject = SecurityUtils.getSubject();
            subject.login(token);

            return true;
        } catch (AuthenticationException ex) {
            token.clear();
            throw ex;
        }
    }

    public void writeExceptionMsg(ServletRequest servletRequest, ServletResponse servletResponse, String msg) throws IOException {
        Map<String, Object> modelMap = new HashedMap();
        modelMap.put("result", -2);
        modelMap.put("msg", msg);
        servletResponse.setContentType("text/html;charset=UTF-8");
        servletResponse.getWriter().write(JSON.toJSONString(modelMap));
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {
        return false;
    }
}
