package fm.cheyiwang.filter;


import com.alibaba.fastjson.JSON;
import fm.cheyiwang.token.CheYiWechatToken;
import fm.config.MainConfig;
import fm.entity.WxUser;
import fm.service.UserService;
import fm.util.CommonUtils;
import fm.util.Constant;
import fm.web.CurrentRequest;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.DateUtils;
import org.apache.shiro.SecurityUtils;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.subject.Subject;

import org.apache.shiro.web.filter.AccessControlFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Map;

/**
 * Created by 宏炜 on 2016-10-18.
 */
public class TokenFilter extends AccessControlFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenFilter.class);

    @Autowired
    UserService userService;

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {


            return true;
        } else {
            String token = servletRequest.getParameter("token");
            LOGGER.info("用户提交token:{}", token);
            if (token == null || StringUtils.isBlank(token) || "undefined".equals(token)) {
                return true;
            }
            token = token.replaceAll("-", "+").replaceAll("\\*", "/");
            LOGGER.info("替换字符后token :{}", token);
            try {
                Timestamp tokenTime;
                AesCipherService aesCipherService = new AesCipherService();
                byte[] decodeData = aesCipherService.decrypt(Base64.decode(token), Base64.decode(MainConfig.tokenAesKey.getBytes())).getBytes();
                String tokenDecode = new String(decodeData);

                Long tokenLong = Long.parseLong(tokenDecode.substring(0, 20));
                tokenTime = new Timestamp(tokenLong);
//                Long errorLongDateTime = DateUtils.parseDate("2017-09-26 23:15:00").getTime();

//                if (tokenTime.getTime() < errorLongDateTime) {
//                    LOGGER.info("认证授权大于错误日期，修正授权登陆!");
//                    return false;
//                }
                Timestamp now = new Timestamp(System.currentTimeMillis());
                long expiredTime = 15 * 24 * 60 * 60 * 1000;
                if ((now.getTime() - tokenTime.getTime()) > expiredTime) {
                    return false;
                }
                String openid = tokenDecode.substring(20, tokenDecode.length());
                LOGGER.info("token解码后获得openid:{}", openid);
                WxUser wxUser = userService.getByOpenId(openid);
                if (CommonUtils.isEmpty(wxUser)) {
                    return false;
                }
                CurrentRequest.addSessionAttribute(Constant.SESSION_LOGIN_USER, wxUser);
                LOGGER.info("取得用户信息:{}", wxUser.getNickname());
                CheYiWechatToken cheYiWechatToken = new CheYiWechatToken(MainConfig.tokenState, openid, wxUser);
                subject.login(cheYiWechatToken);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        Map<String, Object> modelMap = new HashedMap();
        modelMap.put("result", -5);
        modelMap.put("msg", "验证失败，token信息失效或错误");
        servletResponse.setContentType("text/html;charset=UTF-8");
        servletResponse.getWriter().write(JSON.toJSONString(modelMap));
        return false;
    }


}
