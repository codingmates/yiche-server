package fm.cheyiwang.filter;

import com.alibaba.fastjson.JSON;
import fm.entity.WxUser;
import fm.service.UserService;
import fm.util.CommonUtils;
import fm.util.Constant;
import org.apache.commons.collections.map.HashedMap;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/21.
 */
public class UserLoggedInFilter extends AccessControlFilter {

    @Autowired
    UserService userService;

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        Subject subject = getSubject(servletRequest, servletResponse);
        if(subject.isAuthenticated()){
            Session session = subject.getSession();
            WxUser wxUser = userService.getByOpenId(String.valueOf(subject.getPrincipal()));
            if(CommonUtils.isEmpty(wxUser)){
                return false;
            }
            session.setAttribute(Constant.SESSION_LOGIN_USER,wxUser);
            return true;
        }else {
            return false;
        }
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        Map<String,Object> modelMap = new HashedMap();
        modelMap.put("result",-4);
        modelMap.put("msg","用户未登录或登录超时");
        servletResponse.setContentType("text/html;charset=UTF-8");
        servletResponse.getWriter().write(JSON.toJSONString(modelMap));
        return false;
    }
}
