<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/26
  Time: 10:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<style>
    @media(min-width:1024px ){
        .menu {
            display: block !important;
        }
    }
</style>
<header class="site-header">
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-8">
                    <div class="logo">
                        <h1><a href="${ctx}/">${WEB_TITLE}</a></h1>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->
                <div class="col-md-8 col-sm-6 col-xs-4">
                    <div class="main-menu">
                        <a href="#" class="toggle-menu">
                            <i class="fa fa-bars"></i>
                        </a>
                        <ul class="menu" >
                            <li><a href="${ctx}/shop/index">首页</a></li>
                            <li><a href="#">商品分类</a></li>
                            <li><a href="${ctx}/shop/pu">社区</a></li>
                            <li><a href="#">个人中心</a></li>
                            <li><a href="${ctx}/shop/about">关于我们</a></li>
                        </ul>
                    </div> <!-- /.main-menu -->
                </div> <!-- /.col-md-8 -->
            </div> <!-- /.row -->
        </div>
    </div>
    <div class="main-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="list-menu">
                        <ul>
                            <li><a href="index.html">一级类别1</a></li>
                            <li><a href="product-detail.html">一级类别2</a></li>
                            <li><a href="contact.html">一级类别3</a></li>
                        </ul>
                    </div> <!-- /.list-menu -->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-5">
                    <div class="notification">
                        <span>CM 致力于提供更好的一站式解决方案</span>
                    </div>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
</header>