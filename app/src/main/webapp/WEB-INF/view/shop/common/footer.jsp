<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/27
  Time: 22:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<footer class="site-footer">
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span>Copyright © 2017 <a href="#">${WEB_TITLE}</a></span>
                    <p>CM工作室 致力于更好的一站式解决方案，想要了解等多，请发送邮件到<a href="mailto:codingmates@gmail.com">CM@Email</a></p>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.bottom-footer -->
</footer>
