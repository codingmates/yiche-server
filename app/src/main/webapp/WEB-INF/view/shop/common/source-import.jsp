<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/27
  Time: 22:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<title>${WEB_TITLE}</title>
<meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=1.0,user-scalable=no;" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet">
<link rel="stylesheet" href="${ctx}/resources/koolStore/css/bootstrap.css">
<link rel="stylesheet" href="${ctx}/resources/koolStore/css/normalize.min.css">
<link rel="stylesheet" href="${ctx}/resources/koolStore/css/font-awesome.min.css">
<link rel="stylesheet" href="${ctx}/resources/koolStore/css/animate.css">
<link rel="stylesheet" href="${ctx}/resources/koolStore/css/templatemo-misc.css">
<link rel="stylesheet" href="${ctx}/resources/koolStore/css/templatemo-style.css">
<link rel="stylesheet" href="${ctx}/resources/style/common.css">
<script src="${ctx}/resources/koolStore/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="${ctx}/resources/script/jquery.js"></script>
<script>var ctx = "${ctx}";window.jQuery || document.write('<script src="${ctx}/resources/koolStore/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${ctx}/resources/koolStore/js/jquery.easing-1.3.js"></script>
<script src="${ctx}/resources/koolStore/js/bootstrap.js"></script>
<script src="${ctx}/resources/koolStore/js/plugins.js"></script>
<script src="${ctx}/resources/script/jquery.tmpl.min.js"></script>
<script src="${ctx}/resources/script/common.js" type="text/javascript"></script>

