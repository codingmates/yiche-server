<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/3/4
  Time: 18:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html class="">
<head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <jsp:include page="common/source-import.jsp"></jsp:include>
    <link rel="stylesheet" href="${ctx}/resources/koolStore/css/customer.css">
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WnFDBuXWOTsu9PUNK5w1LfMg"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve
    your experience.</p>
<![endif]-->
<jsp:include page="common/header.jsp"></jsp:include>

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="widget-title">
                    <h2>联系我们</h2>
                </div>
                <div class="contact-form">
                    <form id="contactform" name="contactform">
                        <p><input type="text" id="name" name="name" placeholder="您的姓名"/></p>
                        <p><input type="text" id="phone" name="phone" placeholder="手机号码"/></p>

                        <p><input type="email" id="email" name="email" placeholder="邮箱@email"/></p>
                        <p><input type="text" id="qq" name="qq" placeholder="QQ"/></p>
                        <p><textarea id="msg" name="msg" placeholder="留给我们的信息"></textarea></p>
                    </form>
                    <input type="button" class="btn btn-primary" onclick="saveContact()" value="确定"/>

                </div>

            </div>
            <div class="col-md-8">
                <div class="widget-title">
                    <h2>公司地址</h2>
                </div>

                <div class="map" id="map" style="width: 100%;height: 38rem;">

                </div>

            </div>

        </div>
        <div class="row">
            <div class="widget-title">
                <h2>公司简介</h2>
            </div>
            <div class="col-md-12">
                ${config.content}
            </div>
        </div>
    </div>
</div>

<jsp:include page="common/footer.jsp"></jsp:include>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap() {
        createMap();//创建地图
        setMapEvent();//设置地图事件
        addMapControl();//向地图添加控件
        addMapOverlay();//向地图添加覆盖物
    }
    function createMap() {
        map = new BMap.Map("map");
        map.centerAndZoom(new BMap.Point(parseFloat('${config.lng}'),parseFloat('${config.lat}')), 15);
    }
    function setMapEvent() {
        map.enableScrollWheelZoom();
        map.enableKeyboard();
        map.enableDragging();
        map.enableDoubleClickZoom()
    }
    function addClickHandler(target, window) {
        target.addEventListener("click", function () {
            target.openInfoWindow(window);
        });
    }
    function addMapOverlay() {
        var markers = [
            {content: "${config.desc}", title: "${config.tip}", imageOffset: {width: 0, height: 3}, position: {lat: parseFloat('${config.lat}'), lng: parseFloat('${config.lng}')}}
        ];
        for (var index = 0; index < markers.length; index++) {
            var point = new BMap.Point(markers[index].position.lng, markers[index].position.lat);
            var marker = new BMap.Marker(point, {
                icon: new BMap.Icon("http://api.map.baidu.com/lbsapi/createmap/images/icon.png", new BMap.Size(20, 25), {
                    imageOffset: new BMap.Size(markers[index].imageOffset.width, markers[index].imageOffset.height)
                })
            });
            var label = new BMap.Label(markers[index].title, {offset: new BMap.Size(25, 5)});
            var opts = {
                width: 200,
                title: markers[index].title,
                enableMessage: false
            };
            var infoWindow = new BMap.InfoWindow(markers[index].content, opts);
            marker.setLabel(label);
            addClickHandler(marker, infoWindow);
            map.addOverlay(marker);
        }
        ;
    }
    //向地图添加控件
    function addMapControl() {
        var scaleControl = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});
        scaleControl.setUnit(BMAP_UNIT_IMPERIAL);
        map.addControl(scaleControl);
        var navControl = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_LEFT, type: BMAP_NAVIGATION_CONTROL_LARGE});
        map.addControl(navControl);
        var overviewControl = new BMap.OverviewMapControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT, isOpen: true});
        map.addControl(overviewControl);
    }
    var map;
    $(function () {
        initMap();
    })

    function saveContact() {
        var data = {
            name: isEmpty($("#name").val()) ? "" : encodeURI($("#name").val()),
            phone: isEmpty($("#phone").val()) ? "" : encodeURI($("#phone").val()),
            qq: isEmpty($("#qq").val()) ? "" : encodeURI($("#qq").val()),
            email: isEmpty($("#email").val()) ? "" : encodeURI($("#email").val()),
            msg: isEmpty($("#msg").val()) ? "" : encodeURI($("#msg").val()),
        }

        asyncAjax({
            url: ctx + "/shop/about/contact",
            data: data,
            type: "post",
            success: function (res) {
                console.log("data:%o", res);
                if (res['result'] == 0) {
                    alert("您留下的信息已经收到了，我们会尽快联系您的！");
                    $("#contactform")[0].reset();
                } else {
                    alert(res["msg"]);
                }
            }
        })

    }
</script>
</body>
</html>
