<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/26
  Time: 10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <jsp:include page="common/source-import.jsp"></jsp:include>
</head>
<body>
<jsp:include page="common/header.jsp"></jsp:include>
<script src="${ctx}/resources/koolStore/js/main.js"></script>

<link rel="stylesheet" href="${ctx}/resources/koolStore/css/customer.css">
<div class="content-section">
    <div class="container">
        <%--首页推广--%>
        <div class="row" id="adBox">

        </div>
        <div class="row" id="adBox2">


        </div>
    </div>
</div>
<div class="content-section">
    <div class="container">
        <%--常规商品--%>
        <div class="row" id="newBox">
            <div class="col-md-12 section-title">
                <h2>新品推荐</h2>
            </div>

        </div>

    </div>
</div>

<div class="content-section">
    <div class="container">
        <%--热销商品--%>
        <div class="row" id="hotBox">
            <div class="col-md-12 section-title">
                <h2>热销商品</h2>
            </div>

        </div>
    </div>
</div>
<script type="text/html" id="newProTmpl">
    <div class="col-md-3 col-sm-6">
        <div class="product-item">
            <div class="product-thumb">
                <img src="${ctx}/resources/cheyiwang\${ProPic}" alt="\${ProName}">
            </div> <!-- /.product-thum -->
            <div class="product-content">
                <h5><a href="#">\${ProName}</a></h5>
                <span class="tagline"><i class="fa fa-map-marker"></i>\${Area1} \${Area2}(\${Auth})</span>
                <span class="price">￥\${SalePrice}</span>
            </div> <!-- /.product-content -->
        </div> <!-- /.product-item -->
    </div>
</script>
<script type="text/html" id="adBoxTmpl2">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="product-item-4">
            <div class="product-thumb">
                <img src="${ctx}/resources/cheyiwang\${ProPic}" alt="\${ProName}">
            </div> <!-- /.product-thumb -->
            <div class="product-content overlay">
                <h5><a href="#">\${ProName}</a></h5>
                <span class="tagline"><i class="fa fa-map-marker"></i>\${Area1} \${Area2}(\${Auth})</span>
                <span class="price">￥\${SalePrice}</span>
                {{if isEmpty(desc) ==false}}
                <p>\${desc}</p>
                {{else}}
                <p>新品仅需\${SalePrice}元，点击查看详情</p>
                {{/if}}
            </div> <!-- /.product-content -->
        </div> <!-- /.product-item-4 -->
    </div>
</script>
<script type="text/html" id="adBoxTmpl">
    <div class="col-md-3">
        <div class="product-item-1">
            <div class="product-thumb">
                <img src="${ctx}/resources/cheyiwang\${item[0].ProPic}" alt="\${item[0].ProName}">
            </div> <!-- /.product-thumb -->
            <div class="product-content">
                <h5><a href="#">\${item[0].ProName}</a></h5>
                <span class="tagline"><i class="fa fa-map-marker"></i>\${item[0].Area1} \${item[0].Area2}(\${item[0].Auth})</span>
                <span class="price">￥\${item[0].SalePrice}</span>
                {{if isEmpty(item[0].desc) ==false}}
                <p>\${item[0].desc}</p>
                {{else}}
                <p>新品仅需\${item[0].SalePrice}元，点击查看详情</p>
                {{/if}}
            </div> <!-- /.product-content -->
        </div> <!-- /.product-item -->
    </div> <!-- /.col-md-3 -->
    <div class="col-md-5">
        <div class="product-holder">
            <div class="product-item-2">
                <div class="product-thumb">
                    <img src="${ctx}/resources/cheyiwang\${item[1].ProPic}" alt="\${item[1].ProName}">
                </div> <!-- /.product-thumb -->
                <div class="product-content overlay">
                    <h5><a href="#">\${item[1].ProName}</a></h5>
                    <span class="tagline"><i class="fa fa-map-marker"></i>\${item[1].Area1} \${item[1].Area2}(\${item[1].Auth})</span>
                    <span class="price">￥\${item[1].SalePrice}</span>
                    {{if isEmpty(item[1].desc) ==false}}
                    <p>\${item[1].desc}</p>
                    {{else}}
                    <p>新品仅需\${item[1].SalePrice}元，点击查看详情</p>
                    {{/if}}
                </div> <!-- /.product-content -->
            </div> <!-- /.product-item-2 -->
            <div class="product-item-2">
                <div class="product-thumb">
                    <img src="${ctx}/resources/cheyiwang\${item[2].ProPic}" alt="\${item[2].ProName}">
                </div> <!-- /.product-thumb -->
                <div class="product-content overlay">
                    <h5><a href="#">Koolest Shirt</a></h5>
                    <span class="tagline">\${item[2].Area1} \${item[2].Area2}(\${item[2].Auth})</span>
                    <span class="price">￥\${item[2].SalePrice}</span>
                    {{if isEmpty(item[2].desc) ==false}}
                    <p>\${item[2].desc}</p>
                    {{else}}
                    <p>新品仅需\${item[2].SalePrice}元，点击查看详情</p>
                    {{/if}}
                </div> <!-- /.product-content -->
            </div> <!-- /.product-item-2 -->
            <div class="clearfix"></div>
        </div> <!-- /.product-holder -->
    </div> <!-- /.col-md-5 -->
    <div class="col-md-4">
        <div class="product-item-3">
            <div class="product-thumb">
                <img src="${ctx}/resources/cheyiwang\${item[3].ProPic}" alt="\${item[3].ProName}">
            </div> <!-- /.product-thumb -->
            <div class="product-content">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h5><a href="#">\${item[3].ProName}</a></h5>
                        <span class="tagline">\${item[3].Area1} \${item[3].Area2}(\${item[3].Auth})</span>
                        <span class="price">\${item[3].SalePrice}</span>
                    </div> <!-- /.col-md-6 -->
                    <div class="col-md-6 col-sm-6">
                        <div class="full-row">
                            <label for="cat">型号:</label>
                            <select name="cat" id="cat" class="postform">
                                <option value="-1">- Select -</option>
                                <option class="level-0" value="49">型号-1</option>
                                <option class="level-0" value="56">型号-2</option>
                            </select>
                        </div>
                        <div class="full-row">
                            <label for="cat1">数量:</label>
                            <select name="cat1" id="cat1" class="postform">
                                <option value="-1">- Select -</option>
                                <option class="level-0" value="49">10</option>
                                <option class="level-0" value="49">20</option>
                                <option class="level-0" value="56">50</option>
                                <option class="level-0" value="56">更多</option>
                            </select>
                        </div>
                    </div> <!-- /.col-md-6 -->
                    <div class="col-md-12 col-sm-12">
                        <div class="button-holder">
                            <a href="#" class="red-btn"><i class="fa fa-angle-down"></i></a>
                        </div> <!-- /.button-holder -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
            </div> <!-- /.product-content -->
        </div> <!-- /.product-item-3 -->
    </div> <!-- /.col-md-4 -->
</script>
<script type="text/html" id="hotBoxTmpl">
    <div class="col-md-3 col-sm-6">
        <div class="product-item-vote">
            <div class="product-thumb">
                <img src="${ctx}/resources/cheyiwang\${ProPic}" alt="\${ProName}">
            </div> <!-- /.product-thum -->
            <div class="product-content">
                <h5><a href="#">\${ProName}</a></h5>
                <span class="tagline"><i class="fa fa-map-marker"></i>\${Area1} \${Area2}(\${Auth})</span>
                <ul class="progess-bars">
                    <li>
                        <div class="progress">
                            <div class="progress-bar comments" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                            <span class="comments">600<i class="fa fa-heart"></i></span>
                        </div>
                    </li>
                </ul>
            </div> <!-- /.product-content -->
        </div> <!-- /.product-item-vote -->
    </div>
</script>
<jsp:include page="common/footer.jsp"></jsp:include>
</body>
</html>
