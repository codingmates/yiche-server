<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/28
  Time: 22:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <link rel="stylesheet" href="${ctx}/resources/koolStore/css/customer.css">
    <jsp:include page="common/source-import.jsp"></jsp:include>
    <link type="text/css" rel="stylesheet" href="${ctx}/resources/style/shop/purchase.css">

</head>
<body>

<jsp:include page="common/header.jsp"></jsp:include>
<script type="text/javascript" src="${ctx}/resources/script/shop/purchase.js"></script>
<div class="content-section">
    <div class="container" id="purchaseBox">

    </div>
    <div class="row">
        <p style="text-align: center; margin: 0.8rem auto;">上拉加载更多...</p>
    </div>
</div>
<style>
    #replyBox,#replyHover {
        position: fixed;
        top: 0;
        left: 0;
        display: none;
        width: 100%;
        height: 100%;
        background: #fff;
        line-height: 2.5rem;
        font-size: 1.4rem;
        z-index: 998;
    }
    #replyBox textarea{
        height:100%;
        width:80%;
    }
    #replyHover{
        z-index: 997;
        background: rgba(0,0,0,0.2);
    }
    @media (min-width: 800px) {
        #replyBox{
            width:50%;
            min-height:20rem;
            height:auto;
            margin-top:-10rem;
            top:50%;
            left:25%;
        }
        #replyBox textarea{
            height:18rem !important;
        }
    }
</style>
<div id="replyHover"></div>
<div class="content-section" id="replyBox">
    <div>
        <p style="text-indent: 1rem;">请输入回复内容:</p>
        <div style="padding: 0.4rem 0.8rem;margin: 0 auto;height: 100%;width: 95%;">
            <input type="hidden" id="replyPid" value=""/>
            <textarea placeholder="请输入回复内容" id="replyContent" ></textarea>
        </div>
        <div id="replyBtnBox" style="padding: .4rem 0;text-align: right;">
            <button class="btn btn-primary" onclick="javascript:replyContent();">确定</button>
            <button class="btn btn-primary" onclick="$('#replyBox,#replyHover').fadeOut();"
                    style="margin-right: 1rem;">取消</button>
        </div>
    </div>
</div>
<script type="text/html" id="commentTmpl">
    <div class="row" data-id="comments-box" purchase-id="\${pid}">
        <%--<div class="container">--%>
        <%--<div data-id="comments-title">--%>
        <%--<span>评论</span> | <span>Comments</span>--%>
        <%--</div>--%>
        <div class="col-md-12 msg-item">
            <div class="col-md-2 right-align avstar-box">
                <img src="${ctx}/shopData\${himg}.data" class="user-avstar"
                     onerror="this.src='${ctx}/resources/koolStore/images/featured/1.jpg'"/>
            </div>

            <div class="col-md-10 msg-content">
                <h2 class="widget-title">\${uname}</h2>

                <div class="row" data-id="msg-box">
                    <p>
                        \${pcontext}
                    </p>
                    <p class="pic-box">
                        {{if isEmpty(pic1) == false}}
                        <img src="${ctx}/shopData\${pic1}.data" class="user-avstar"
                             onerror="this.src='${ctx}/resources/koolStore/images/featured/1.jpg'"/>
                        {{/if}}
                        {{if isEmpty(pic2) == false}}
                        <img src="${ctx}/shopData\${pic2}.data" class="user-avstar"
                             onerror="this.src='${ctx}/resources/koolStore/images/featured/1.jpg'"/>
                        {{/if}}
                        {{if isEmpty(pic3) == false}}
                        <img src="${ctx}/shopData\${pic3}.data" class="user-avstar"
                             onerror="this.src='${ctx}/resources/koolStore/images/featured/1.jpg'"/>
                        {{/if}}
                        {{if isEmpty(pic4) == false}}
                        <img src="${ctx}/shopData\${pic4}.data" class="user-avstar"
                             onerror="this.src='${ctx}/resources/koolStore/images/featured/1.jpg'"/>
                        {{/if}}
                    </p>
                    <p class="right-align">
                        <a href="javascript:showReplyPage('\${pid}');">回复</a>|
                        <a href="javascript:reply('1','','\${pid}')">点赞</a>
                    </p>
                    <div class="row" data-id="reply-box">

                        <div class="row" data-id="like-title-box">
                            <div class="col-md-10" data-id="like-box">
                                {{each(i,reply) Reply}}
                                {{if reply.Type == 1}}
                                <img src="\${reply.Img}" class="user-avstar"
                                     onerror="this.src='${ctx}/resources/koolStore/images/featured/1.jpg'"/>
                                {{/if}}
                                {{/each}}
                            </div>
                            <div class="col-md-2 right-align" data-id="like-amount">
                                \${Reply.length}
                            </div>
                        </div>
                        <div class="row" style="min-height: 1rem;">
                            {{each(i,reply) Reply}}
                            {{if reply.Type == 0}}
                            <p>
                                {{if reply.My == 0 || reply.My == "0"}}
                                <strong>\${reply.Name}：</strong>
                                {{else}}
                                <strong>楼主：\${reply.My}</strong>
                                {{/if}}
                                <span>\${reply.Content}</span>
                            </p>
                            {{/if}}
                            {{/each}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <%--</div>--%>
    </div>
</script>
<jsp:include page="common/footer.jsp"></jsp:include>
</body>
</html>