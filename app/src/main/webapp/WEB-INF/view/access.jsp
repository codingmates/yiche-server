<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/1/5
  Time: 下午1:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>留言板审核</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/script/jquery-1.8.3.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/script/jquery.tmpl.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/jquery.json.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/common.js"></script>


    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html {
            /*font-size: 62.5%;*/
        }

        body {
            background: url("${pageContext.request.contextPath}/resources/img/invite-background.jpg") no-repeat #f00;
            background-size: 100% 100%;
        }

        .msg-title {
            background: #fbecad;
            height: 10rem;
            overflow: hidden;
            box-shadow: 0 5px 20px #ffbb6d;
            opacity: 0.9;
        }

        .msg-title span {
            background: url(${pageContext.request.contextPath}/resources/img/title-bg.png) 0.8rem -0.2rem no-repeat;
            background-size: 30rem auto;
            display: block;
            position: absolute;
            top: 0;
            height: 10rem;
            width: 10rem;
        }

        .title-bg-left {
            left: 0;
        }

        .title-bg-right {
            background-position: 0.4rem -0.4rem !important;;
            right: 0;
            transform: rotate(180deg);
        }

        .msg-title h2 {
            font-size: 3.8rem;
            text-align: center;
            line-height: 10rem;
            color: #af0000;
        }

        div.msg-item {
            font-size: 2.4rem;
            color: #fff;
            margin: 1.5rem auto;
            width: 80%;
            overflow: hidden;
            border: 1px solid #ff0;
            border-radius: 0.5rem;
            padding: 1rem;
            background: #fbecad;
            color: #af0000;
            opacity: 0.9;
        }

        .msg-item p {
            text-align: left;
            line-height: 2.8rem;
            display: inline-block;
            font-family: "Helvetica";
        }

        .msg-item p span.name {
            font-weight: bold;
            font-size: 2.8rem;
        }

        .msg-item .content {
            font-style: italic;
        }
    </style>
</head>
<body>
<div class="msg-title">
    <span class="title-bg-left"></span>

    <h2>幸福留言板</h2>
    <span class="title-bg-right"></span>
</div>
<c:forEach items="${data}" var="msgItem">
    <div class="msg-item">
        <p id="${msgItem.msg_id}">
            <span class="name">${msgItem.name}</span>
            <span>:</span>
            <span class="content">${msgItem.msg}</span>


        </p>
        <p>手机:<a href="tel:${msgItem.phone}">${msgItem.phone}</a></p>
        <p>到场人数:<c:if test="${msgItem.coun != null}">${msgItem.coun}人</c:if>
            <c:if test="${msgItem.del.trim() eq '0' }">
                <a href="javascript:accessMsg('${msgItem.msg_id}');">删除</a>
            </c:if>
            <c:if test="${msgItem.del.trim() eq '1' }">
                <code>已删除</code>
            </c:if>
        </p>

    </div>
</c:forEach>
<script>
    function accessMsg(msgId) {
        $.ajax({
            url: "${pageContext.request.contextPath}/invite/msg/del?msgId=" + msgId,
            type: "post",
            async: true,
            cache: false,
            success: function (res) {
                alert("删除成功!");
                location.reload();
            }
        })
    }

</script>
</body>
</html>
