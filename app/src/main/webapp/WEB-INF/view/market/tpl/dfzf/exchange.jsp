<%--
  Created by IntelliJ IDEA.
  User: 蔻丁同学
  Date: 2016/3/12
  Time: 13:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<html>
<head>
    <title>领取流量活动</title>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <script>var absoluteContextPath = "${pageContext.request.contextPath}";var sst="${sst}";</script>
    <script src="/app/exchange/js/common/js/jquery.js?t=${token}"></script>
    <script src="/app/exchange/js/common/js/underscore.js?t=${token}"></script>
    <script src="/app/exchange/js/common/js/backbone.js?t=${token}"></script>
    <script src="/app/exchange/js/common/js/Framework.js?t=${token}"></script>
    <script src="/app/exchange/js/common/js/template.js?t=${token}"></script>
    <script src="/app/exchange/js/common/js/swiper.min.js?t=${token}"></script>
    <script src="/app/exchange/js/common/js/viewport.js?t=${token}"></script>
    <!--page-->
    <link rel="stylesheet" href="/app/exchange/css/module/page/page.css?t=${token}"/>
    <script type='text/html' id='pageTpl'>
        <!--领取状态-->
        <div class="first-view">
            <div class="main-title">
                <div class="logo">
                    <img src="{{logo}}">
                </div>
            </div>
            <div class="warning-tips">
                流量兑换只有60分钟有效期，请尽快兑换!
            </div>
            <div class="exchange-container">
                <input id="js_tel">

                <div class="btn js_btn"></div>
                <div class="logo">
                    <img src="{{logo}}">
                </div>
            </div>
            <div class="help-box"><p>已经兑奖过了？  <a href="javascript:jumpToConfigPage();">设置自定义分享</a></p></div>
            <div class="rule-title">兑换规则</div>
            <div class="js_rule_container rule-container"></div>
        </div>
        <div class="exchanged-view">
            <div class="main-title">
                <div class="logo">
                    <img src="{{logo}}">
                </div>
            </div>
            <div class="exchanged">
                <div class="ticket-exchanged">
                    <div class="ticket-bg">
                        <img src="{{logo}}" class="ticket-logo">
                        <img src="/app/exchange/image/module/page/image/exchanged-success.png">

                        <p><span id="flowSize">{{flowSize}}</span>M流量</p>
                    </div>
                </div>
            </div>
            <div class="warning-tips">
                兑换成功，请注意查收短信和流量到账信息！
            </div>
            <div class="btn-box">
                <a class="button" onclick="showConfirmBox()">立即分享</a>
            </div>
        </div>
        <div class="confirm-over">

        </div>
        <div class="confirm-box">
            <span onclick="hideConfirmBox()">X</span>

            <p>分享属于你的活动，获取更多奖励？</p>
            <ul>
                <li onclick="jumpToConfigPage();">自定义设置</li>
                <li onclick="showShare()">直接分享</li>
            </ul>
        </div>
        <div class="share-div"></div>
    </script>

    <script src="/app/exchange/js/module/page/page.js?t=${token}"></script>
    <style type="text/css">
        body {
            width: 640px;
            margin: 0 auto;
            padding: 0;
            background-color: #43cdc0;
        }

        .page-container {
            width: 640px;
            margin: 0 auto;
        }

        .fleft {
            float: left;
        }

        .clearfix:after {
            visibility: hidden;
            display: block;
            font-size: 0;
            content: " ";
            clear: both;
            height: 0;
        }
    </style>
</head>
<body>
<div class="page-container">
    <div id="js_page_wrap">
    </div>
</div>
<div style="text-align: center;height: 2.5rem;;">
    <a href="http://www.huaxiawd.com" style="color: #333;text-decoration: none;">©2016 广州微摩网络科技有限公司</a>
</div>
</body>
</html>
<script>
    var page = new Page({
        el: "#js_page_wrap"
    });
    function showConfirmBox() {
        $(".confirm-over").show(50);
        $(".confirm-box").show(300);
    }
    function hideConfirmBox() {
        $(".confirm-over").hide();
        $(".confirm-box").hide(300);
    }
    function showShare() {
        $(".share-div").fadeIn(300);
        hideConfirmBox()
    }
    function jumpToConfigPage() {
        location.href = "${pageContext.request.contextPath}/activity/flow/share/config/${id}?uuid=${uuid}";
    }


</script>
