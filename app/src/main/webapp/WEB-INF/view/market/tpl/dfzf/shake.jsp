<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>${title}</title>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <script>var absoluteContextPath = "${pageContext.request.contextPath}";</script>
    <script src="/app/index/js/common/js/jquery.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/underscore.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/backbone.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/Framework.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/template.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/swiper.min.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/viewport.js?t=${toekn}"></script>
    <!--page-->
    <link rel="stylesheet" href="/app/index/css/module/shake/page.css?t=${toekn}"/>
    <script type='text/html' id='pageTpl'>
        <div class="shake-view">
            <div class="shake">
                <img src="${pageContext.request.contextPath}/app/index/image/module/shake/image/shake-2.png">
            </div>
            <div class='shake-title'></div>
            <div class="tel-title">您当前参加活动的手机号码</div>
            <div class="tel-number js_tel_number">
            </div>
            <audio id='js_music_effect' src="${pageContext.request.contextPath}/app/index/module/page/effect.wav" style='display:none'>
            </audio>
        </div>
    </script>
    <script src="/app/index/js/module/shake/page.js?t=${toekn}"></script>
    <style type="text/css">
        body {
            width: 640px;
            margin: 0 auto;
            padding: 0;
            background-color: #43cdc0;
        }

        .page-container {
            width: 640px;
            margin: 0 auto;
        }

        .fleft {
            float: left;
        }

        .clearfix:after {
            visibility: hidden;
            display: block;
            font-size: 0;
            content: " ";
            clear: both;
            height: 0;
        }
    </style>
</head>
<body>
<div class="page-container">
    <div id="js_page_wrap">
    </div>
</div>
<div style="position: absolute;z-index: 9;width: 100%;height: 100%;display: none;top:0;left: 0;" id="td-code">
    <img src="/app/index/image/td-code.jpg" style="width: 100%;height: 100%;" onclick="hideTd()">
</div>
<div style="text-align: center;height: 2.5rem;;">
    <a href="http://www.huaxiawd.com" style="color: #333;text-decoration: none;">©2016 广州微摩网络科技有限公司</a>
</div>
</body>
</html>
<script>
    var page = new Page({
        el: "#js_page_wrap"
    });
    function showTd() {
        $("#td-code").show(300);
    }
    function hideTd() {
        $("#td-code").hide(300);
    }
</script>
