<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<html>
<head>
    <title>${title}</title>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <script>var absoluteContextPath = "${pageContext.request.contextPath}";</script>
    <script src="/app/index/js/common/js/jquery.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/underscore.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/backbone.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/Framework.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/template.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/swiper.min.js?t=${toekn}"></script>
    <script src="/app/index/js/common/js/viewport.js?t=${toekn}"></script>
    <!--page-->
    <link rel="stylesheet" href="/app/index/css/module/page/page.css?t=${toekn}"/>
    <style>

    </style>
    <script type='text/html' id='pageTpl'>
        {{if !hasJoined}}
        <!--领取状态-->
        <div class="first-view">
            <div class="main-title">
                <div class="logo">
                    <img src="{{logo}}">
                </div>
            </div>
            <div class="red-bag">
                <div class="logo">
                    <img src="{{logo}}">
                </div>
                <input id="js_tel" type="tel" placeholder="请输入正确的手机号码">

                <div class="get-btn js_get_btn"></div>
                <div class="prize-list">
                    <ul class='prize-list-wrap'>
                        {{each list as value index}}
                        <div class="prize-cell clearfix">
                            <div class="fleft avatar">
                                <img src="{{value.avatar}}">
                            </div>
                            <div class="fleft user-info">
                                <div class="name">{{value.name}}</div>
                                <div class="tel">{{value.tel}}</div>
                            </div>
                            <div class="fleft prize-info">
                                <div class="prize">抢到<span class="yellow">{{value.prize}}</span>流量券</div>
                                <div class="time">{{value.time}}</div>
                            </div>
                        </div>
                        {{/each}}
                    </ul>
                </div>
            </div>
            <div class="rule-title">活动规则</div>
            <div class="js_rule_container rule-container"></div>
        </div>
        {{else}}
        <div class="confirm-over">

        </div>
        <div class="confirm-box">
            <span onclick="hideConfirmBox()">X</span>

            <p>是否进入公众号领取？</p>
            <ul>
                <li onclick="actionByShareUserConfig();">立即兑换</li>
                <li onclick="hideConfirmBox()">不用了</li>
            </ul>
        </div>
        <div class="show-prize">
            <div class="main-container">
                <div class="logo">
                    <img src="{{logo}}">
                </div>
                <div class="avatar">
                    <img src="{{avatar}}">
                </div>
                <div class="text">恭喜您抢到了流量券</div>
                <div class="prize-name">
                    {{prize}}
                </div>
            </div>

            <div class="count-down">
                流量券有效期:{{available}}分钟,剩余:<span class="js_count_down"></span>
            </div>
            <div class="exchange-btn" onclick="showConfirmBox();">
                立即兑换流量
            </div>

            <div class="rule-title-2">活动现场</div>
            <div class="prize-list">
                <ul class='prize-list-wrap'>
                    {{each list as value index}}
                    <div class="prize-cell clearfix">
                        <div class="fleft avatar">
                            <img src="{{value.avatar}}">
                        </div>
                        <div class="fleft user-info">
                            <div class="name">{{value.name}}</div>
                            <div class="tel">{{value.tel}}</div>
                        </div>
                        <div class="fleft prize-info">
                            <div class="prize">抢到{{value.prize}}流量券</div>
                            <div class="time">{{value.time}}</div>
                        </div>
                    </div>
                    {{/each}}
                </ul>
            </div>
            <div class="rule-title-2">活动规则</div>
            <div class="js_rule_container rule-container"></div>
        </div>
        {{/if}}
        <!--摇一摇-->
        <div class="shake-view" style="display: none">
            <div class="shake"></div>
            <div class="tel-title">您当前参加活动的手机号码</div>
            <div class="tel-number js_tel_number">
            </div>
            <audio id='js_music_effect' src="${pageContext.request.contextPath}/app/index/module/page/effect.wav"
                   style='display:none'>
            </audio>
        </div>
    </script>
    <script src="/app/index/js/module/page/page.js?t=${toekn}"></script>
    <style type="text/css">
        body {
            width: 640px;
            margin: 0 auto;
            padding: 0;
            background-color: #43cdc0;
        }

        .page-container {
            width: 640px;
            margin: 0 auto;
        }

        .fleft {
            float: left;
        }

        .clearfix:after {
            visibility: hidden;
            display: block;
            font-size: 0;
            content: " ";
            clear: both;
            height: 0;
        }
    </style>
</head>
<body>
<div class="page-container">
    <div id="js_page_wrap">
    </div>
</div>
<div style="text-align: center;height: 2.5rem;;">
    <a href="http://www.huaxiawd.com" style="color: #333;text-decoration: none;">©2016 广州微摩网络科技有限公司</a>
</div>
<div style="position: absolute;z-index: 9;width: 100%;height: 100%;display: none;top:0;left: 0;z-index:9;background:rgba(0,0,0,180);" id="td-code">
    <c:choose>
        <c:when test="${shareType eq 'TD_CODE'}">
            <%--二维码，没有图片--%>
            <c:if test="${(targetUrl == ''|| targetUrl == null)}">
                <img src="/app/index/image/td-code.jpg" style="width: 100%;height: 100%;" onclick="hideImg()">
            </c:if>
            <%--二维码，且配置上传了图片--%>
            <c:if test="${!(targetUrl == ''|| targetUrl == null)}">
                <img src="${targetUrl}" style="width: 100%;height: 100%;" onclick="hideImg()" onerror="this.src='/app/index/image/td-code.jpg'">
            </c:if>
        </c:when>
        <c:otherwise>
            <%--链接--%>
            <img src="/app/index/image/td-code.jpg" style="width: 100%;height: 100%;" onclick="hideImg()">
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
<script>
    var shareType = "${shareType}";
    var targetUrl = "${targetUrl}";
    var page = new Page({
        el: "#js_page_wrap"
    });
    function showConfirmBox() {
        $(".confirm-over").show(50);
        $(".confirm-box").show(300);
    }
    function hideConfirmBox() {
        $(".confirm-over").hide();
        $(".confirm-box").hide(300);
    }
    function actionByShareUserConfig() {
        hideConfirmBox();
        if (shareType === "LINK") {
            location.href = targetUrl;
        } else {
//            $("#td-code").find("img").attr("src", targetUrl);
            $("#td-code").show();
        }
    }
    function hideImg() {
        $("#td-code").fadeOut(300);
    }
</script>
