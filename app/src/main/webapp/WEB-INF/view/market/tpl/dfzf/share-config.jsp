<%--
  Created by IntelliJ IDEA.
  User: dengfs
  Date: 2016/3/26
  Time: 19:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>自定义分享设置</title>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,
user-scalable=no">
    <script type="text/javascript">var absoluteContextPath = "${pageContext.request.contextPath}", id = "${id}";</script>
    <script type="text/javascript" src="/resources/script/static/lib/jquery.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/underscore.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/backbone.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/Framework.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/template.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/swiper.min.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/viewport.js"></script>
    <script type="text/javascript" src="/resources/script/static/lib/ajaxfileupload.js"></script>
    <link rel="stylesheet" type="text/css" href="/share-config/css/widget/css/style.css?t=${token}">
</head>

<body>
<div class="content">
    <div class="first-box">
        <div class="big-icon-box bottom50">
            <p>设置专属二维码</p>
            <img src="/share-config/img/widget/img/fingerprint.png" class="default-img" id="td-code-active"
                 pop-target="uploadTD">
            <!-- <img src="../widget/img/fingerprint-active.png" class="active-img"/> -->
        </div>
        <div class="tip-bottom">
            <p>上传二维码，其他用户抽奖后，点击“立即兑换”按钮，将弹出属于你的推广二维码。（二维码可在公众号获取）</p>
        </div>
    </div>
    <div class="second-box">
        <div class="tip-top">
            <p>设置下载链接，其他用户抽奖后，点击“立即兑换”按钮，将跳转到属于你的App下载推广链接。（链接可在公众号中获取）</p>
        </div>
        <div class="big-icon-box top50">
            <img src="/share-config/img/widget/img/link.png" class="default-img" id="link-active" pop-target="setLink">
            <!-- <img src="../widget/img/link-active.png" class="active-img"/> -->
            <p>设置专属App下载链接</p>
        </div>
    </div>
</div>
<div class="pop-over"></div>
<div class="pop-box" id="uploadTD">
    <span class="close">关闭</span>

    <div class="pop-content">
        <img src="/share-config/img/widget/img/uplod-icon.png" class="upload-icon" id="td-preview">

        <p class="upload-tip process">点击上传图片</p>
        <input type="file" id="uploadFile" name="upload-file"
               onchange="uploadFile('uploadFile', 'td-preview', 'tdCodeUrl', 'image')"/>
        <input type="hidden" id="tdCodeUrl"/>
    </div>
    <div class="pop-btn-box">
        <a class="btn btn-no-border right" onclick='javascript:addConfig("TD_CODE")'>确定</a>
    </div>
</div>
<div class="pop-box" id="setLink">
    <span class="close">关闭</span>

    <div class="pop-content">
        <label>填入您的专属App下载链接</label>
    		<textarea>

    		</textarea>
    </div>
    <div class="pop-btn-box">
        <a class="btn btn-no-border right"  onclick='javascript:addConfig("LINK")'>确定</a>
    </div>
</div>
<script type="text/javascript">
    var tdCodeImg = document.getElementById("td-code-active");
    var linkImg = document.getElementById("link-active");
</script>
<script type="text/javascript" src="/share-config/js/widget/js/page.js"></script>
</body>

</html>
