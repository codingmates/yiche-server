<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/1/4
  Time: 下午10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>留言板</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/script/jquery-1.8.3.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/script/jquery.tmpl.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/jquery.json.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/common.js"></script>
    <style>
        * {
            margin: 0;
            padding: 0;
            border-radius:0 !important;
        }

        html {
            /*font-size: 62.5%;*/
        }

        body {
            background: url("${pageContext.request.contextPath}/resources/img/invite-background.jpg") no-repeat #f00;
            background-size: 100% 100%;
        }

        .msg-title {
            background: #fbecad;
            height: 10rem;
            overflow: hidden;
            box-shadow: 0 5px 20px #ffbb6d;
            opacity: 0.9;
        }

        .msg-title span {
            background: url(${pageContext.request.contextPath}/resources/img/title-bg.png) 0.8rem -0.2rem no-repeat;
            background-size: 30rem auto;
            display: block;
            position: absolute;
            top: 0;
            height: 10rem;
            width: 10rem;
        }

        .title-bg-left {
            left: 0;
        }

        .title-bg-right {
            background-position: 0.4rem -0.4rem !important;;
            right: 0;
            transform: rotate(180deg);
        }

        .msg-title h2 {
            font-size: 3.8rem;
            text-align: center;
            line-height: 10rem;
            color: #af0000;
        }

        div.msg-item {
            font-size: 2.4rem;
            color: #fff;
            margin: 1.5rem auto;
            width: 80%;
            overflow: hidden;
            padding: 1rem;

        }

        .msg-item p {
            border: 1px solid #ff0;
            background: #fbecad;
            color: #af0000;
            opacity: 0.9;
            text-align: left;
            line-height: 2.8rem;
            display: block;
            font-family: "Helvetica";
            padding: 0.2rem 0.4rem;

        }


        .msg-item .content {
            font-style: italic;
        }
        .msg-item .name{
            display:inline-block;
            border: 1px solid #ff0;
            border-radius: 0.5rem;
            background: #fbecad;
            color: #af0000;
            opacity: 0.9;
            padding: 0.2rem 0.4rem;
        }
    </style>
</head>
<body>
<div class="msg-title">
    <span class="title-bg-left"></span>

    <h2>幸福留言板</h2>
    <span class="title-bg-right"></span>
</div>
<c:forEach items="${data}" var="msgItem">
    <div class="msg-item">
        <span class="name">${msgItem.name}(到场${msgItem.coun}人)【<a href="tel:${msgItem.phone}">${msgItem.phone}</a>】</span>
        <p>
            <span>发来祝福:</span><span class="content">${msgItem.msg}</span>
        </p>
    </div>
</c:forEach>
<script type="text/html" id="msgTmpl">
    <div class="msg-item">
        <p id="\${id}">
            <span class="name">\${name}</span>
            <span>发来祝福:</span>
            <span class="content">\${msg}</span>
        </p>
    </div>
</script>
<script>
    $(document).ready(function () {
//        getNewMsg();
    })
    var maxId = "${maxId}";

    function getNewMsg() {
        if (!isEmpty(maxId)) {
            maxId = parseInt(maxId);
        }
        var data = {
            startId: maxId,
            access: "1"
        }
        getJson({
            url: "${pageContext.request.contextPath}/invite/msg/new",
            type: "post",
            data: data,
            async: true,
            success: function (res) {
                if (res.result == 0) {
                    var datas = res["data"];
                    if (!isEmpty(datas)) {
                        $("#msgTmpl").tmpl(datas).appendTo($("body"));
                        maxId = maxId + datas.length;
                    }
                }
                setTimeout(function () {
                    getNewMsg();
                }, 500);
            },
            error:function(){
            }
        })
    }
</script>
</body>
</html>
