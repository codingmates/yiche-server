var Page = PageView.extend({
    tplId: "pageTpl", hasPostByShake: false, initElView: function () {
        var t = this;
        Page.instance = this;
        var e = window.location.href, n = absoluteContextPath, i = (e.split("activity/flow/shake")[1]).split('?')[0].split("/")[1];
        this.activityId = i, $.ajax({
            method: "POST", url: n + "/market/flow/" + i, beforeSend: function (t) {
                t.setRequestHeader("X-Requested-With", "XMLHttpRequest")
            }, success: function (e) {
                t.$el.html(template("pageTpl", e.data)), document.title = e.data.title, t.$el.find(".js_rule_container").html(e.data.rule), t.bindGetBtn(), e.data.hasJoined && t.setCountDown(e.data.remainTime), t.bindScroll(e.data)
                //t.convertStyle($(".shake>img"));
                t.shake($(".shake>img"));
            }
        })
    }, bindGetBtn: function () {
        var t = this;
        t.checkTel(getQueryString("tel")) ? t.renderShakePage({tel: getQueryString("tel")}) : alert("请输入正确的手机号码")
    }, checkTel: function (t) {
        var e = /^1[34578]\d{9}$/;
        return e.test(t) ? !0 : !1
    }, renderShakePage: function (t) {
        //$('.tel-number').bind("click",function(){Page.instance.shakeEventDidOccur(t.tel)});
        this.$el.find(".first-view").hide(), this.$el.find(".shake-view").show(), this.$el.find(".js_tel_number").html(t.tel), this.bindShake(t.tel)
    }, bindShake: function (t) {
        function e(e) {
            var s = e.accelerationIncludingGravity, l = (new Date).getTime();
            if (l - i > 300) {
                var o = l - i;
                i = l, a = s.x, y = s.y, z = s.z;
                {
                    var c = Math.abs(a + y + z - last_x - last_y - last_z) / o * 1e4;
                    document.getElementById("status")
                }
                c > n && Page.instance.shakeEventDidOccur(t), last_x = a, last_y = y, last_z = z
            }
        }

        var n = 800, i = 0, a = y = z = last_x = last_y = last_z = 0;
        window.DeviceMotionEvent ? window.addEventListener("devicemotion", e, !1) : alert("本设备不支持devicemotion事件")
    }, shakeEventDidOccur: function (t) {
        var e = this;
        if (e.$el.find("#js_music_effect")[0].play(), !e.hasPostByShake) {
            e.hasPostByShake = true;
            var n = absoluteContextPath + "/market/flow/" + e.activityId + "/lottery?tel=" + t;
            var i = absoluteContextPath + "/activity/flow/" + e.activityId + "?uuid=" + getQueryString("uuid");
            $.ajax({
                method: "POST", url: n, beforeSend: function (t) {
                    t.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                }, success: function (t) {
                    0 == t.result ? (alert("恭喜您中了" + t.data.prize + "流量券"), window.location = i) : (e.hasPostByShake = false, alert(t.msg))
                }
            })
        }
    }, setCountDown: function (t) {
        var e, n = this;
        setInterval(function () {
            t > 0 ? (e = n.formatTime(t), n.$el.find(".js_count_down").html(e.minute + ":" + e.second), t--) : n.$el.find(".js_count_down").html("已过期")
        }, 1e3)
    }, formatTime: function (t) {
        var e = {};
        return e.minute = parseInt(t / 60), e.second = t % 60, e.second < 10 && (e.second = "0" + e.second), e
    }, bindScroll: function (t) {
        function e() {
            timer = setInterval(function () {
                s -= i, -a / 2 >= s && (s = 0), n.css("top", s + "px")
            }, 30)
        }

        if (_.size(t.list) > 3) {
            var n = $(".prize-list-wrap"), i = 1, a = n.height(), s = n.position().top;
            e()
        }
    }, shake: function (obj) {
        var posData = [obj.position().left, obj.position().top];
        console.info("posData:%o", posData);
        var i = 0;
        clearInterval(timer);
        var timer = setInterval(function () {
            i++;
            var left = posData[0] + ((i % 2) > 0 ? -5 : 5) + 'px';
            var top = posData[1] + ((i % 2) > 0 ? 5 : -5) + 'px';
            var rotate = ((i % 2) > 0 ? 10 : -10);
            if (i >= 3000) {
                clearInterval(timer);
                left = posData[0] + 'px';
                top = posData[1] + 'px';
                rotate = 0;
            }
            obj.animate({"left": left, "top": top}, 60);
            obj.css("transform", "rotate(" + rotate + "deg)");
        }, 80);
    }, convertStyle: function (obj) {
        if (obj.length) {
            for (var i = 0; i < obj.length; i++) {
                obj[i].style.left = obj[i].offsetLeft + 'px';
                obj[i].style.top = obj[i].offsetTop + 'px';
            }
            for (var i = 0; i < obj.length; i++) {
                obj[i].style.position = 'relative';
                obj[i].style.margin = 0;
            }
        }
        else {
            obj.style.left = obj.offsetLeft + 'px';
            obj.style.top = obj.offsetTop + 'px';
            obj.style.position = 'relative';
            obj.style.margin = 0;
        }
    }
});
Page.instance = null;
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}