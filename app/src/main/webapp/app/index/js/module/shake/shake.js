var Shake = PageView.extend({
    tplId: "shakeTpl",
    hasPostByShake: false,
    initElView: function () {
        var that = this;
        var url = window.location.href;
        Shake.instance = this;
        var params = (url.split("activity/shake")[1]).split('/')[0].split('?')[0].split('&')[0];
        this.activityId = params;
        //url = 'https://www.cdmates.com/market/flow/1/afuyfk';
        //这种url我tmd怎么处理
        var tel = that.getParamFromUrl('tel');
        that.$el.html(template('shakeTpl', {}));
        if (that.checkTel(tel)) {
            this.bindShake(tel);
        }
    },


    checkTel: function (tel) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(tel) ? true : false;
    },

    bindShake: function (tel) {
        var SHAKE_THRESHOLD = 800;
        var last_update = 0;
        var x = y = z = last_x = last_y = last_z = 0;

        if (window.DeviceMotionEvent) {
            window.addEventListener('devicemotion', deviceMotionHandler, false);
        } else {
            alert('本设备不支持devicemotion事件');
        }

        function deviceMotionHandler(eventData) {
            var acceleration = eventData.accelerationIncludingGravity;
            var curTime = new Date().getTime();

            if ((curTime - last_update) > 100) {
                var diffTime = curTime - last_update;
                last_update = curTime;
                x = acceleration.x;
                y = acceleration.y;
                z = acceleration.z;
                var speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;
                var status = document.getElementById("status");

                if (speed > SHAKE_THRESHOLD) {
                    Shake.instance.shakeEventDidOccur(tel);
                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }

        function doResult() {
            document.getElementById("result").className = "result";
            document.getElementById("loading").className = "loading loading-show";
            setTimeout(function () {
                //document.getElementById("hand").className = "hand";
                document.getElementById("result").className = "result result-show";
                document.getElementById("loading").className = "loading";
            }, 1000);
        }
    },

    shakeEventDidOccur: function (tel) {
        var that = this;
        that.$el.find("#js_music_effect")[0].play();
        if (!that.hasPostByShake) {
            that.hasPostByShake = true;
            var url = absoluteContextPaht + "/market/flow/" + that.activityId + "/lottery?tel=" + tel;
            $.ajax({
                method: "POST",
                url: url,
                beforeSend: function (request) {
                    request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                },
                success: function (res) {
                    if (res.result == 0) {
                        alert('恭喜您中了' + res.data.prize + "流量券");
                        // window.location.reload();
                        window.location.href = '/activity/flow/' + this.activityId + '?uuid=' + this.getParamFromUrl('uuid');

                    }
                    else {
                        that.hasPostByShake = false;
                        alert(res.msg);
                    }
                }
            });
        }
    },

    getParamFromUrl: function (key) {
        window.location.href得到URL
        var sHref = window.location.href;
        //取出不带＃的URL
        sHref = sHref.split('#')[0];
        var args = sHref.split("?");
        var retval = "";
        /*参数为空*/
        if (args[0] == sHref) {
            return retval;
            /*无需做任何处理*/
        }
        var str = args[1];
        args = str.split("&");
        for (var i = 0; i < args.length; i++) {
            str = args[i];
            var arg = str.split("=");
            if (arg.length <= 1) continue;
            if (arg[0] == key) retval = arg[1];
        }
        //if(!retval){
        //    //alert('url中的' + key + '参数为空');
        //    console.log('url中的' + key + '参数为空');
        //}
        return retval;

    }
});
Shake.instance = null;