var Page = PageView.extend({
    tplId: "pageTpl", initElView: function () {
        var e = window.location.href, t = e.split("/flow/exchange/")[1].split("/")[0].split("?")[0].split("&")[0], n = this;
        $.ajax({
            url: absoluteContextPath + "/market/flow/exchange/" + t, method: "POST", beforeSend: function (e) {
                e.setRequestHeader("X-Requested-With", "XMLHttpRequest")
            }, success: function (e) {
                console.info("data:%o", e);
                //e.data.logo = "https://avatar.tower.im/0c300743d9a64f3681f1efcb1b1b6bd1";
                0 == e.result ? (n.$el.html(template("pageTpl", e.data)), document.title = e.data.title, n.$el.find(".js_rule_container").html(e.data.rule), n.bindGetBtn(e.data.showShareTip)) : alert(e.msg)

            }
        })
    }, bindGetBtn: function (showShareTip) {
        var e = this;
        this.$el.find(".js_btn").bind("click", function () {
            e.checkTel(e.$el.find("#js_tel").val()) ? e.sendRequest() : alert("请输入正确的手机号码")
        });
        if (sst) {
            e.showSuccessView();
            showShare();
        }
    }, checkTel: function (e) {
        var t = /^1[34578]\d{9}$/;
        return t.test(e) ? !0 : !1
    }, sendRequest: function () {
        var a = window.location.href, ai = a.split("/flow/exchange/")[1].split("/")[0].split("?")[0].split("&")[0];
        var e = this, t = e.$el.find("#js_tel").val();
        var _t = e;
        $.ajax({
            url: absoluteContextPath + "/exchange/flow/" + ai + "?tel=" + t, method: "POST", beforeSend: function (e) {
                e.setRequestHeader("X-Requested-With", "XMLHttpRequest")
            }, success: function (e) {
                if (e.result == "0") {
                    $("#flowSize").html(e.flowSize);
                    _t.showSuccessView();
                } else {
                    alert(e.msg);
                }
            }
        })
    }, showSuccessView: function () {
        this.$el.find(".js_btn").unbind("click").hide();;
        this.$el.find(".first-view").slideUp(500);
        this.$el.find(".exchanged-view").slideDown(500);
    }
});