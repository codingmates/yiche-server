(function (window, $) {
    'use strict';

    asyncAjax({
        url: ctx + "/shopData/product/index",
        type: "post",
        success: function (res) {
            console.log("data:%o", res);
            var data = res["data"];
            if (!isEmpty(data["newPros"]) && data["adPros"].length >= 4) {
                $("#newProTmpl").tmpl(data["newPros"]).appendTo($("#newBox"));
            } else {
                $("#adBox").html("<div class=\"empty-tip\">模块尚未准备就绪哦</div>")
            }
            if (!isEmpty(data["adPros"]) && data["adPros"].length >= 4) {
                $("#adBoxTmpl").tmpl({"item": data["adPros"]}).appendTo($("#adBox"));
            } else {
                $("#adBox").html("<div class=\"empty-tip\">模块尚未准备就绪哦</div>")
            }
            if (!isEmpty(data["adPros2"]) && data["adPros2"].length >= 4) {
                $.each(data["adPros"], function (idx, item) {
                    item["desc"] = "";
                    $("#adBoxTmpl2").tmpl(item).appendTo($("#adBox2"));
                })

            } else {
                $("#adBox2").html("<div class=\"empty-tip\">模块尚未准备就绪哦</div>")
            }

            if (!isEmpty(data["hotPros"])) {

                $("#hotBoxTmpl").tmpl(data["hotPros"]).appendTo($("#hotBox"));
            } else {
                $("#adBox").html("<div class=\"empty-tip\">模块尚未准备就绪哦</div>")
            }
            $("img").each(function (idx, el) {
                var uri = "/resources/koolStore/";
                if (idx == 2) {
                    uri += "images/featured/2.jpg";
                } else if (idx == 1) {
                    uri += "images/featured/1.jpg"
                } else {
                    uri += "images/gallery-image-1.jpg";
                }

                $(this).attr("src", ctx + uri);
            })
        }
    })


})(window, jQuery);





