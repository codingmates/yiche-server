/**
 * Created by CM on 2017/2/28.
 */
var currentPage = 1;
$(function () {
    loadPurchaseData({});

    $(window).scroll(function () {
        var bot = 50; //bot是底部距离的高度
        if ((bot + $(window).scrollTop()) >= ($(document).height() - $(window).height())) {
            currentPage++;
            var data = {pageNum: currentPage};
            loadPurchaseData(data);
        }
    });
})


function loadPurchaseData(data) {
    asyncAjax({
        url: ctx + "/shopData/purchase/list?" + jQuery.param(data).replace(/(%5D|%5B)/g, ""),
        type: "post",
        success: function (res) {
            console.log("data:%o", res);
            //biz code here
            if (res.result === "0") {
                var data = res["data"];
                $("#commentTmpl").tmpl(data).appendTo($("#purchaseBox"))
            } else {
                alert(res.msg);
            }
        }
    })
}

function showReplyPage(pid) {
    $("#replyPid").val(pid);
    $("#replyBox,#replyHover").slideDown();
}

function replyContent(){
    var pid = $("#replyPid").val();
    var content = $("#replyContent").val();
    if(isEmpty(content)){
        alert("回复内容不能为空哦!");
        return;
    }
    reply(0,content,pid,function(){
        $("#replyBox,#replyHover").fadeOut();
    })
}

function reply(type, content, pid,_callbak) {
    var data = {
        type: type,
        content: content,
        pid: pid
    }

    asyncAjax({
        url: ctx + "/shopData/pu/reply?" + $.param(data).replace(/(%5B|%5D)/g, ""),
        type: "post",
        success: function (res) {
            console.log("reply data:%o", res);
            if (res["result"] == "0") {
               var html  = $("#commentTmpl").tmpl(res["data"]).html();
               $("div[purchase-id=\""+pid+"\"]").html(html);
                if(!isEmpty(_callbak)&& typeof _callbak =="function"){
                    _callbak();
                }
            } else {
                alert(res["msg"]);
            }

        }
    })

}