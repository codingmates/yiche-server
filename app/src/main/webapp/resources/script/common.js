/**
 * Created by CM on 16/9/12.
 */
String.prototype.toChartArray = function () {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        arr.push(this.charAt(i))
    }
    return arr;
}

$(function () {
    $('a.toggle-menu').unbind().bind("click", function () {
        $('ul.menu').fadeToggle("slow");
    });


    var owl = $("#owl-demo");

    owl.owlCarousel({
        items: 6
    });

    // Custom Navigation Events
    $(".next").click(function () {
        owl.trigger('owl.next');
    })
    $(".prev").click(function () {
        owl.trigger('owl.prev');
    })

    windowInit();
})

function windowInit() {
    var htmlFontSize = window.outerWidth >= 1600 ? "62.5%" : "31.25%";
    console.log("window initial ,window width:%o", window.outerWidth);
    $("html").css("font-size", htmlFontSize);
    $(window).resize(function () {
        var htmlFontSize = window.outerWidth >= 1600 ? "62.5%" : "31.25%";
        console.log("window resize ,window width:%o", window.outerWidth);
        $("html").css("font-size", htmlFontSize);
    })
}

function getJson(options) {
    console.log("[post:json] options:%o", options);
    if (isEmpty(options.url)) {
        console.error("url must be config!");
        return;
    }
    if (isEmpty(options.data)) {
        options.data = {};
    }
    $.ajax({
        url: options.url + "?" + $.param(options.data).replace(/%5D/g, "").replace(/%5B/g, ""),
        type: "post",
        dataType: "json",
        success: function (res) {
            console.log("res:%o", res);
            options.success(res);
        },
        error: function (err, sta) {
            var resTxt = err.responseText;
            if (!isEmpty(resTxt)) {
                var res = JSON.parse(resTxt);
                alert(res.msg);
            }
            console.log("请求发生错误:%o", err);
        }
    })
}

function isEmpty(obj) {
    if (typeof obj == "object") {
        for (var i in obj) {
            return false;
        }
    } else if (typeof obj == "string") {
        return obj == "" || obj == undefined || obj == "null" || obj == null;
    } else if (typeof obj == "undefined") {
        return true;
    }
}

function getContextPath() {
    return ctx;
}

function loadingShow() {
    $(".loading-hover,.loading-img").remove();
    $("body").append("<div class=\"loading-hover\"></div>").append("<div class=\"loading-img\"></div>");
}

function loadingHide() {
    $(".loading-hover,.loading-img").remove();
}


var asyncAjax = function (opts) {
    if (isEmpty(opts)) {
        console.error("async options empty!");
        return -1;
    }
    if (isEmpty(opts.url)) {
        console.error("async options url empty!");
        return -1;
    }
    $.ajax({
        url: opts.url,
        type: (isEmpty(opts.type) ? "post" : opts.type),
        async: true,
        data: (isEmpty(opts.data) ? {} : opts.data),
        beforeSend: function (req) {
            loadingShow();
            if (!isEmpty(opts.beforeSend) && typeof opts.beforeSend == "function") {
                opts.beforeSend(req);
            }
        },
        success: function (res) {
            if (!isEmpty(opts.success) && typeof opts.success == "function") {
                opts.success(res);
            }
        },
        error: function (e) {
            if (!isEmpty(opts.error) && typeof opts.error == "function") {
                opts.error(e);
            }
        },
        complete: function () {
            loadingHide();
            if (!isEmpty(opts.complete) && typeof opts.complete == "function") {
                opts.complete();
            }
        }
    })
}