window.viewerConfig = {
    "jsonHost": "//res.maka.im/",
    "apiHost": "//api.maka.im/",
    "imgHost": "//img1.maka.im/",
    "musicHost": "http://res3.maka.im/",
    "resHost": "//res.maka.im/",
    "fontHost": "//font.maka.im/",
    "env": "production"
}

window.projectVersion ={
    "id": "T_Q8HT70Q3",
    "title": "高贵典雅婚礼邀请函请柬",
    "uid": "4446328",
    "content": "棕色的欧式线条加韩式的婚纱照，让整体风格显得高贵典雅别具一格，适合韩式古典婚纱照。",
    "p_version": "27",
    "status": "notpass",
    "thumb": "background_0.jpg",
    "is_deleted": "false",
    "versionsource": "api",
    "vip_config": {
        "loading_page_custom": false,
        "viewer_domain_custom": false,
        "enable_bottom_menu": false,
        "enable_relay": false,
        "enable_lottery": false,
        "enable_form": true,
        "enable_vote": true,
        "enable_anti_fake_vote": true,
        "is_vip": false,
        "show_makalogo": true
    },
    "origin_thumb": "background_0.jpg",
    "wx_thumb": "background_0.jpg",
    "isnodeproxy": false,
    "showad": false,
    "isTempOffline": false,
    "pcViewerUrl": "高贵典雅婚礼邀请函请柬.htm",
    "canonical": "高贵典雅婚礼邀请函请柬.htm"
}
