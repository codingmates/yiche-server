/**
 * Created by john on 2016/3/12.
 */
$(function () {
    var page = {
        init: function () {
            var that = this;
            $.ajax({
                url: absoluteContextPath + "/activity/activityList",
                type: 'POST',
                data: {
                    page: '1',
                    pageSize: '2'
                },
                beforeSend: function (request) {
                    request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                },
                success: function (res) {
                    _.each(res.data.list, function (value, index) {
                        res.data.list[index].startTime = that.formatTimeStamp(value.startTime / 1000);
                        res.data.list[index].endTime = that.formatTimeStamp(value.endTime / 1000);
                    });
                    $("#js_page").html(template("pageTpl", res.data));
                    that.bindDetailClick();
                    that.bindExchangeClick();
                    that.bindCloseBtn();
                    that.bindPagePagination();
                }
            });

        },

        formatTimeStamp: function (nS) {
            return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/, ' ');
        },


        bindDetailClick: function () {
            var that = this;
            $(".js_participate_detail").bind('click', function () {
                var actId = $(this).attr('data-act-id');
                $.ajax({
                    url: absoluteContextPath + "/activity/participationList",
                    type: 'POST',
                    data: {
                        activityId: actId,
                        page: '0',
                        size: '10'
                    },
                    beforeSend: function (request) {
                        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    },
                    success: function (res) {
                        _.each(res.data.list, function (value, index) {
                            res.data.list[index].creatTime = that.formatTimeStamp(value.creatTime / 1000);
                        });

                        $("body").append("<div class='modal-backdrop fade in'></div>");
                        $("#modal-join-detail").removeClass('hide')
                            .removeClass('fade');
                        $('#join-detail-container').html(template('joinDetailTpl', res.data));
                        that.initPagination({
                            ajaxParams: {
                                activityId: actId,
                                size: '10'
                            },
                            url: absoluteContextPath + "/activity/participationList",
                            container: "#join-detail-container",
                            tpl: "joinDetailTpl",
                            pagination: '#pagination'
                        });
                    }
                })
            });
        },

        bindCloseBtn: function () {
            $(".close").bind('click', function () {
                $(this).parent('div').parent('.modal').addClass('fade').addClass('hide');
                $(".modal-backdrop").remove();
            });
        },

        bindExchangeClick: function () {
            var that = this;
            $(".js_exchange_detail").bind('click', function () {
                var actId = $(this).attr('data-act-id');
                $.ajax({
                    url: absoluteContextPath + "/activity/exchangedList",
                    type: 'POST',
                    data: {
                        activityId: actId,
                        page: '0',
                        size: '10'
                    },
                    beforeSend: function (request) {
                        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    },
                    success: function (res) {
                        _.each(res.data.list, function (value, index) {
                            res.data.list[index].createTime = that.formatTimeStamp(value.createTime / 1000);
                            res.data.list[index].exchangeTime = that.formatTimeStamp(value.exchangeTime / 1000);
                        });

                        $("body").append("<div class='modal-backdrop fade in'></div>");
                        $("#modal-exchange-detail").removeClass('hide')
                            .removeClass('fade');
                        $('#exchange_detail_container').html(template('exchangeDetailTpl', res.data))
                        that.initPagination({
                            ajaxParams: {
                                activityId: actId,
                                size: '10'
                            },
                            url: absoluteContextPath + "/activity/exchangedList",
                            container: "#exchange_detail_container",
                            tpl: "exchangeDetailTpl",
                            pagination: '#pagination2'
                        });
                    }
                })
            });
        },

        initPagination: function (params) {
            var options = {
                currentPage: 1,
                totalPages: 10,
                numberOfPages: 5,
                itemTexts: function (type, page, current) {
                    switch (type) {
                        case "first":
                            return "第一页";
                        case "prev":
                            return "上一页";
                        case "next":
                            return "下一页";
                        case "last":
                            return "最后一页";
                        case "page":
                            return page;
                    }
                },
                onPageClicked: function (event, originalEvent, type, page) { //异步换页
                    $.ajax({
                        url: params.url,
                        type: 'POST',
                        data: _.extend(params.ajaxParams, {
                            page: page
                        }),
                        beforeSend: function (request) {
                            request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                        },
                        success: function (res) {
                            $(params.container).html(template(params.tpl, res.data));
                        }
                    })
                }
            };
            $(params.pagination).bootstrapPaginator(options);
        },

        bindPagePagination: function () {
            var that = this;
            var options = {
                currentPage: 1,
                totalPages: 10,
                numberOfPages: 5,
                itemTexts: function (type, page, current) {
                    switch (type) {
                        case "first":
                            return "第一页";
                        case "prev":
                            return "上一页";
                        case "next":
                            return "下一页";
                        case "last":
                            return "最后一页";
                        case "page":
                            return page;
                    }
                },
                onPageClicked: function (event, originalEvent, type, page) { //异步换页
                    $.ajax({
                        url: absoluteContextPath + "/activity/activityList",
                        type: 'POST',
                        data: {
                            page: page,
                            pageSize: '2'
                        },
                        beforeSend: function (request) {
                            request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                        },
                        success: function (res) {
                            _.each(res.data.list, function (value, index) {
                                res.data.list[index].startTime = that.formatTimeStamp(value.startTime / 1000);
                                res.data.list[index].endTime = that.formatTimeStamp(value.endTime / 1000);
                            });
                            $("#js_page").html(template("pageTpl", res.data));
                            that.bindDetailClick();
                            that.bindExchangeClick();
                            that.bindCloseBtn();
                        }
                    })
                }
            };
            $('#pagination3').bootstrapPaginator(options);
        }
    };

    page.init();
});
