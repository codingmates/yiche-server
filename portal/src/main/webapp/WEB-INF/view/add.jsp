<%--
  Created by IntelliJ IDEA.
  User: 蔻丁同学
  Date: 2016/3/13
  Time: 11:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加活动</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="title" content="CM管理后台">
    <meta name="description" content="CM管理后台">
    <meta name="keywords" content="可视化,布局,系统">
    <link href="/app/css/bootstrap-combined.min.css?_v=${token}" rel="stylesheet">
    <link href="/app/css/layoutit.css?_v=${token}" rel="stylesheet">
    <link href="/app/css/datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/app/simditor/styles/simditor.css?_v=${token}"/>
    <link rel="stylesheet" type="text/css" href="/app/css/style.css?_v=${token}"/>
    <script>var absoluteContextPath = "${pageContext.request.contextPath}"</script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js?_v=${token}"></script>
    <![endif]-->
    <script type="text/javascript" src="/app/js/jquery-2.0.0.min.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/js/bootstrap.min.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/js/jquery-ui.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/js/jquery.ui.touch-punch.min.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/js/jquery.htmlClean.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/js/bootstrap-datetimepicker.min.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/js/bootstrap-wysiwyg.js?_v=${token}"></script>

    <script type="text/javascript" src="/app/simditor/scripts/module.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/simditor/scripts/hotkeys.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/simditor/scripts/uploader.js?_v=${token}"></script>
    <script type="text/javascript" src="/app/simditor/scripts/simditor.js?_v=${token}"></script>
    <script src="/app/js/template.js?_v=${token}"></script>
    <script src="/app/js/add.js?_v=${token}"></script>
    <script src="/app/js/underscore.js?_v=${token}"></script>
    <script src="/app/js/ajaxfileupload.js?_v${token}"></script>
    <style>
        .navbar {
            border: none;
            background: transparent;
        }

        .navbar-inner {
            border: none;
            background: transparent;
            box-shadow: none;
            border-bottom: 1px solid #ddd;
            padding: 0 !important;
        }

        a.brand {
            display: none !important;
        }

        .container-fluid {
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
</head>
<body>
<div id="js_page" style="max-width: 1320px;display: block;margin: 0 auto;">

</div>
<script type="text/html" id="pageTpl">
    <div class="container-fluid">
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand" href="#">CM管理后台</a>

                    <div class="nav-collapse collapse navbar-responsive-collapse">
                        <ul class="nav">
                            <li>
                                <a href="/activity">活动列表</a>
                            </li>
                            <li class="active">
                                <a href="/activity/add">创建活动</a>
                            </li>
                        </ul>
                        <ul class="nav pull-right">
                            <%--  <li>
                                  <a href="#">退出</a>
                              </li>
                              <li class="divider-vertical"></li>
                              <li class="dropdown">
                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">设置</a>
                                  <ul class="dropdown-menu">
                                      <li>
                                          <a href="#">信息设置</a>
                                      </li>
                                      <li>
                                          <a href="#">关于我们</a>
                                      </li>
                                  </ul>
                              </li>--%>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul id="stepNav" class="nav nav-tabs">
                    <li class="active">
                        <a href="#">基础信息</a>
                    </li>
                    <li>
                        <a href="#">流量包配置</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <form class="form-horizontal" id="baseForm">
        <div class="row-fluid">
            <div class="span10" style="margin: 0 auto;float: none;">
                <div class="alert alert-danger">
                    <strong>
                        提示：
                    </strong> <span>为了对账的准确性，进行中的活动只能修改活动名称、规则、公司logo</span></div>
            </div>
        </div>
        <div class="row-fluid" id="linkBox">
            <div class="span10" style="margin: 0 auto;float: none;">
                <div class="span6">
                    <div class="alert alert-success">
                        <strong>
                            活动连接：
                        </strong> <span id="activityLink"></span></div>
                </div>
                <div class="span6">
                    <div class="alert alert-info"><strong>
                        兑奖连接：
                    </strong><span id="exchangeLink"></span></div>
                </div>
            </div>

        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="title">活动名称</label>

                    <div class="controls">
                        <input id="title" name="title" type="text" value="{{!isNewAct?activity.title:''}}"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="count">奖品数量</label>

                    <div class="controls">
                        <input id="count" name="count" type="text" value="{{!isNewAct?activity.count:''}}"/>
                    </div>
                </div>
                <div class="control-group">

                    <label class="control-label" for="logo">Logo</label>

                    <div class="controls">
                        <%--进度条--%>
                        <div class="circleProgress_wrapper">
                            <span class="progress_text">0.00%</span>

                            <div class="wrapper right">
                                <div class="circleProgress rightcircle"></div>
                            </div>
                            <div class="wrapper left">
                                <div class="circleProgress leftcircle"></div>
                            </div>
                        </div>
                        <img id="logo_img" class="img_upload_preview" for="logo" data-target="logoUrl" src=""
                             onclick="uploadFileEvent()"
                             onerror="this.src='/app/img/user.jpg'"/>
                        <input id="logo" type="file" name="file_upload"
                               onchange="uploadFile('logo','logo_img','logoUrl','images')"/>
                        <%--<span class="upload_progress_tip"></span>--%>

                        <input name="logoUrl" id="logoUrl" type="hidden"></div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="startTime">开始时间</label>

                    <div class="controls">
                        <input class="form_datetime" id="startTime" placeholder="点击选择时间"
                               name="startTime" type="text" readonly="readonly"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="endTime">结束时间</label>

                    <div class="controls">
                        <input class="form_datetime" id="endTime" placeholder="点击选择时间"
                               name="endTime" type="text" readonly="readonly"/>
                    </div>
                </div>
                <div class="control-group" style="display:none;">
                    <label class="control-label" for="times">用户可参与次数</label>

                    <div class="controls">
                        <input id="times" name="times" type="text"/>
                    </div>
                </div>
            </div>

        </div>
        <div class="row-fluid">
            <div class="control-group">
                <label class="control-label" for="rule">活动规则</label>

                <div class="controls">
                <textarea id="rule" placeholder="请填入活动规则" autofocus>
                </textarea>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button class="btn btn-primary btn-block js_next_step_btn" type="button">下一步</button>
            </div>
        </div>
    </form>
    <form id="flowForm" class="form-horizontal">
        <div class="row-fluid">
            <div class="span12">
                <table class="table table-bordered" contenteditable="true">
                    <thead>
                    <tr>
                        <th width="120">移动</th>
                        <th width="120">联通</th>
                        <th width="120">电信</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width:300px;">
                            <ul class="flow-list inline">
                            </ul>
                            <div>
                                <input type="button" value="添加" data-what="YIDONG"
                                       class="btn btn-primary btn-block js_add_btn"></div>
                        </td>
                        <td style="width:300px;">
                            <ul class="flow-list inline">
                            </ul>
                            <div>
                                <input type="button" value="添加" data-what="LIANTONG"
                                       class="btn btn-primary btn-block js_add_btn"></div>
                        </td>
                        <td style="width:300px;">
                            <ul class="flow-list inline">
                            </ul>
                            <div>
                                <input type="button" value="添加" data-what="DIANXIN"
                                       class="btn btn-primary btn-block js_add_btn"></div>
                        </td>
                    </tr>
                    <tr>
                        <td data-what="YIDONG">
                            <ul>
                                <li data-tpl="true" class="pb-item" data-array="YIDONG"><span>100M</span><input
                                        type="text"/>%<input
                                        type="hidden" value="" name="flow-id"></li>
                            </ul>
                        </td>
                        <td data-what="LIANTONG">
                            <ul>
                                <li data-tpl="true" class="pb-item" data-array="LIANTONG"><span>100M</span><input
                                        type="text"/>%<input
                                        type="hidden" value="" name="flow-id"></li>
                            </ul>
                        </td>
                        <td data-what="DIANXIN">
                            <ul>
                                <li data-tpl="true" class="pb-item" data-array="DIANXIN"><span>100M</span><input
                                        type="text"/>%<input
                                        type="hidden" value="" name="flow-id"></li>
                            </ul>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <button class="btn btn-warn btn-block" type="button" onclick="preStep();">上一步</button>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <button class="btn btn-success btn-block js_create_btn" data-toggle="modal" href="#"
                            type="button">创建活动
                    </button>
                </div>
            </div>
        </div>

    </form>

    <div id="modal-container-936389" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close js_close_btn" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">创建成功</h3>
        </div>
        <div class="modal-body">
            <label>活动链接：</label>
            <a id="act_id_text">https://www.cdmates.com/market/flow/{id}?uuid={uuid}</a>
            <label>兑奖链接：</label>
            <a id="exLind_text">https://www.cdmates.com/market/flow/{id}?uuid={uuid}</a>
        </div>
    </div>
    <div id="modal-container-flow-list" class="modal hide fade" role="dialog" aria-labelledby="boxTitle"
         aria-hidden="true">
        <div class="modal-header">
            <button id="modal-container-flow-list-close" type="button" class="close js_close_btn" data-dismiss="modal"
                    aria-hidden="true">×
            </button>
            <h3 id="boxTitle">选择流量包</h3>
        </div>
        <div class="modal-body">
            <ul class="flow-list">

            </ul>
            <div>
                <input type="button" value="确定" class="btn btn-block btn-primary"/>
            </div>
        </div>
    </div>
</script>
<script type="text/javascript">

    function uploadFileEvent() {
        $("#logo").click();
    }

    function preStep() {
        $("#baseForm").show(500);
        $("#flowForm").hide(500);
        $("#stepNav").find('li').eq(1).removeClass('active');
        $("#stepNav").find('li').eq(0).addClass('active');
    }
</script>
</body>
</html>