<%@ page import="fm.cache.ConfigCache" %><%--
  Created by IntelliJ IDEA.
  User: 蔻丁同学
  Date: 2017/2/23
  Time: 22:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<jsp:include page="common/header.jsp"></jsp:include>

<%--顶部导航开始--%>
<nav class="navbar navbar-default top-navbar" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse"
                data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a target="_self" class="navbar-brand waves-effect waves-dark" href="index.html"><i
                class="large material-icons">insert_chart</i> <strong>${WEB_TITLE}</strong></a>

        <div id="sideNav" href="" class=""><i class="material-icons dp48">toc</i></div>
    </div>

    <ul class="nav navbar-top-links navbar-right">
        <%-- <li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown4"><i
                 class="fa fa-envelope fa-fw"></i> <i class="material-icons right">arrow_drop_down</i></a>
             <ul id="dropdown4" class="dropdown-content dropdown-tasks w250"
                 style="width: 73px; position: absolute; top: 0px; left: 0px; display: none; opacity: 1;">
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <strong>${username}</strong>
                             <span class="pull-right text-muted">
                                         <em>Today</em>
                                     </span>
                         </div>
                         <div>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <strong>John Smith</strong>
                             <span class="pull-right text-muted">
                                         <em>Yesterday</em>
                                     </span>
                         </div>
                         <div>Lorem Ipsum has been the industry's standard dummy text ever since an kwilnw...</div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <strong>John Smith</strong>
                             <span class="pull-right text-muted">
                                         <em>Yesterday</em>
                                     </span>
                         </div>
                         <div>Lorem Ipsum has been the industry's standard dummy text ever since the...</div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" class="text-center" href="#">
                         <strong>Read All Messages</strong>
                         <i class="fa fa-angle-right"></i>
                     </a>
                 </li>
             </ul>
         </li>
         <li><a target="mainFrame" class="dropdown-button waves-effect waves-dark" href="#!"
                data-activates="dropdown3"><i
                 class="fa fa-tasks fa-fw"></i> <i class="material-icons right">arrow_drop_down</i></a>
             <ul id="dropdown3" class="dropdown-content dropdown-tasks w250"
                 style="width: 73px; position: absolute; top: 0px; left: 0px; display: none; opacity: 1;">
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <p>
                                 <strong>Task 1</strong>
                                 <span class="pull-right text-muted">60% Complete</span>
                             </p>
                             <div class="progress progress-striped active">
                                 <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                      aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                     <span class="sr-only">60% Complete (success)</span>
                                 </div>
                             </div>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <p>
                                 <strong>Task 2</strong>
                                 <span class="pull-right text-muted">28% Complete</span>
                             </p>
                             <div class="progress progress-striped active">
                                 <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="28"
                                      aria-valuemin="0" aria-valuemax="100" style="width: 28%">
                                     <span class="sr-only">28% Complete</span>
                                 </div>
                             </div>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <p>
                                 <strong>Task 3</strong>
                                 <span class="pull-right text-muted">60% Complete</span>
                             </p>
                             <div class="progress progress-striped active">
                                 <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60"
                                      aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                     <span class="sr-only">60% Complete (warning)</span>
                                 </div>
                             </div>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <p>
                                 <strong>Task 4</strong>
                                 <span class="pull-right text-muted">85% Complete</span>
                             </p>
                             <div class="progress progress-striped active">
                                 <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="85"
                                      aria-valuemin="0" aria-valuemax="100" style="width: 85%">
                                     <span class="sr-only">85% Complete (danger)</span>
                                 </div>
                             </div>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                 </li>
             </ul>
         </li>
         <li><a target="mainFrame" class="dropdown-button waves-effect waves-dark" href="#!"
                data-activates="dropdown2"><i
                 class="fa fa-bell fa-fw"></i> <i class="material-icons right">arrow_drop_down</i></a>
             <ul id="dropdown2" class="dropdown-content w250"
                 style="width: 73px; position: absolute; top: 0px; left: -182px; display: none; opacity: 1;">
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <i class="fa fa-comment fa-fw"></i> New Comment
                             <span class="pull-right text-muted small">4 min</span>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                             <span class="pull-right text-muted small">12 min</span>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <i class="fa fa-envelope fa-fw"></i> Message Sent
                             <span class="pull-right text-muted small">4 min</span>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <i class="fa fa-tasks fa-fw"></i> New Task
                             <span class="pull-right text-muted small">4 min</span>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" href="#">
                         <div>
                             <i class="fa fa-upload fa-fw"></i> Server Rebooted
                             <span class="pull-right text-muted small">4 min</span>
                         </div>
                     </a>
                 </li>
                 <li class="divider"></li>
                 <li>
                     <a target="mainFrame" class="text-center" href="#">
                         <strong>See All Alerts</strong>
                         <i class="fa fa-angle-right"></i>
                     </a>
                 </li>
             </ul>
         </li>--%>
        <li><a target="mainFrame" class="dropdown-button waves-effect waves-dark"
               data-activates="dropdown1"><i
                class="fa fa-user fa-fw"></i> <b>${username}</b> <i class="material-icons right">arrow_drop_down</i></a>
            <ul id="dropdown1" class="dropdown-content"
                style="width: 146px; position: absolute; top: 0px; left: -19px; display: none; opacity: 1;">
                <%--<li><a target="mainFrame" href="#"><i class="fa fa-user fa-fw"></i>个人资料</a>--%>
                <%--</li>--%>
                <%--<li><a target="mainFrame" href="#"><i class="fa fa-gear fa-fw"></i> 设置</a>--%>
                <%--</li>--%>
                <li><a target="_self" href="${ctx}/admin/login/out"><i class="fa fa-sign-out fa-fw"></i> 退出</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<%--顶部导航结束--%>

<%--左侧导航开始--%>
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">

            <%--<li>--%>
            <%--<a target="mainFrame" class="active-menu waves-effect waves-dark" href="${ctx}/admin/home"><i--%>
            <%--class="fa fa-dashboard"></i>--%>
            <%--首页</a>--%>
            <%--</li>--%>
            <shiro:hasRole name="all">
                <li>
                    <a target="mainFrame" class="waves-effect waves-dark" href="${ctx}/portal/config/index"><i
                            class="fa fa-home"></i>平台首页配置</a>
                </li>
            </shiro:hasRole>
            <shiro:hasAnyRoles name="ACTIVITY">
                <li>
                    <a target="mainFrame" href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i>
                        营销管理<span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a target="mainFrame" href="${ctx}/activity">流量兑换活动</a>
                        </li>
                        <li>
                            <a target="mainFrame" href="#">通用营销活动<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse">
                                <li>
                                    <a target="mainFrame" href="#">大转盘送流量</a>
                                </li>
                                <li>
                                    <a target="mainFrame" href="#">石头剪刀布赢流量</a>
                                </li>
                                <li>
                                    <a target="mainFrame" href="#">关注送流量</a>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </li>
            </shiro:hasAnyRoles>
            <%--<shiro:hasAnyRoles name="all,HOME_MGR">--%>
            <%--<li>--%>
            <%--<a target="mainFrame" href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i>--%>
            <%--店铺管理<span--%>
            <%--class="fa arrow"></span></a>--%>
            <%--<ul class="nav nav-second-level collapse">--%>
            <%--<li>--%>
            <%--<a target="mainFrame" href="${ctx}/shop/home">首页配置</a>--%>

            <%--</li>--%>

            <%--</ul>--%>
            <%--</li>--%>
            <%--</shiro:hasAnyRoles>--%>
            <shiro:hasAnyRoles name="all,USER_MGR">
                <li>
                    <a target="mainFrame" href="#" class="waves-effect waves-dark"><i class="fa fa-users"></i> 用户管理<span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a target="mainFrame" href="${ctx}/user/mgr/index/1">普通用户管理</a>
                        </li>
                        <li>
                            <a target="mainFrame" href="${ctx}/user/mgr/index/2">维修技师管理</a>
                        </li>
                        <shiro:hasRole name="all">
                            <li>
                                <a target="mainFrame" href="${ctx}/user/mgr/index/3">商户用户管理</a>
                            </li>
                        </shiro:hasRole>
                    </ul>
                </li>
            </shiro:hasAnyRoles>
            <li>
                <a target="mainFrame" href="#" class="waves-effect waves-dark"><i class="fa fa-cny"></i> 商城管理<span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a target="mainFrame" href="${ctx}/goodMgr/index">商品管理</a>
                    </li>
                    <shiro:hasRole name="all">
                    <li>
                        <a href="${ctx}/shop/good/type" target="mainFrame">商品类别管理</a>
                    </li>
                    </shiro:hasRole>
                    <li>
                        <a href="${ctx}/order/index" target="mainFrame">订单管理</a>
                    </li>
                    <li>
                        <a>交易记录管理</a>
                    </li>
                    <shiro:hasRole name="all">
                        <li>
                            <a href="${ctx}/cash/application/mgr/index" target="mainFrame">提现管理</a>
                        </li>
                    </shiro:hasRole>
                   <shiro:hasRole name="merchant">
                    <li>
                    <a target="mainFrame" href="${ctx}/recruit/mgr/index">招聘管理</a>
                    </li>
                   </shiro:hasRole>
                    <li>
                        <a href="${ctx}/express/mgr/index" target="mainFrame">物流管理</a>
                    </li>
                </ul>
            </li>

            <li>
                <a target="mainFrame" href="#" class="waves-effect waves-dark">
                    <i class="fa fa-comments"></i>
                    社区管理
                    <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <shiro:hasRole name="all">
                        <li>
                            <a target="mainFrame" href="${ctx}/forum/carousel">轮播图配置</a>
                        </li>
                    </shiro:hasRole>
                    <li>
                        <a target="mainFrame" href="${ctx}/forum/article">文章管理</a>
                    </li>
                    <shiro:hasRole name="all">
                        <li>
                            <a target="mainFrame" href="${ctx}/forum/picArticle">图文管理</a>
                        </li>
                        <li>
                            <a target="mainFrame" href="${ctx}/sectionMgr/index">版块管理</a>
                        </li>
                    </shiro:hasRole>
                </ul>
            </li>
            <shiro:hasRole name="merchant">
                <li>
                    <a target="mainFrame" href="#" class="waves-effect waves-dark">
                        <i class="fa fa-home"></i>
                        店铺管理
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a target="mainFrame" href="${ctx}/per-shop/portal">基础配置</a>
                        </li>
                        <li>
                            <a target="mainFrame" href="${ctx}/per-shop/index">轮播图配置</a>
                        </li>
                        <%--<li>--%>
                            <%--<a target="mainFrame" href="${ctx}/per-shop/aut">认证资料管理</a>--%>
                        <%--</li>--%>

                    </ul>
                </li>
            </shiro:hasRole>
            </li>
            <shiro:hasRole name="all">
                <li>
                    <a target="mainFrame" href="#" class="waves-effect waves-dark">
                        <i class="fa fa-ambulance"></i>
                        维修管理
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a target="mainFrame" href="javascript:bootbox.alert('功能尚在开发中，敬请期待~');">维修订单管理</a>
                        </li>
                    </ul>
                </li>
            </shiro:hasRole>
            <shiro:hasRole name="admin">
                <li>
                    <a class="waves-effect waves-dark"><i class="fa fa-fw fa-cog"></i> 系统设置</a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a target="mainFrame" href="${ctx}/admin/config">基础配置</a>
                        </li>
                        <li>
                            <a target="mainFrame" href="${ctx}/admin/wechat/config">微信配置</a>
                        </li>
                        <li>
                            <a target="mainFrame" href="#">其他配置</a>
                        </li>

                    </ul>
                </li>
            </shiro:hasRole>
        </ul>

    </div>

</nav>
<script>
    $(function () {
        $("#main-menu>li>a").bind("click", function () {
            $("#main-menu>li>a.active-menu").removeClass("active-menu");
            $(this).addClass("active-menu");
        })
    })
</script>
<%--左侧导航结束--%>