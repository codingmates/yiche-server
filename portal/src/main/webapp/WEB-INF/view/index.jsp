<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <title>活动列表页面</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="title" content="CM管理后台">
    <meta name="description" content="CM管理后台">
    <meta name="keywords" content="可视化,布局,系统">
    <link href="/app/css/bootstrap-combined.min.css?_v=${token}" rel="stylesheet">
    <link href="/app/css/layoutit.css?_v=${token}" rel="stylesheet">
    <script>
        var absoluteContextPath = "${pageContext.request.contextPath}";
    </script>
    <style>
        .navbar {
            border: none;
            background: transparent;
        }

        .navbar-inner {
            border: none;
            background: transparent;
            box-shadow: none;
            border-bottom: 1px solid #ddd;
            padding: 0 !important;
        }

        a.brand {
            display: none !important;
        }

        .container-fluid {
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
</head>
<!--<script type="text/javascript" src="js/jquery-2.0.0.min.js"></script>-->
<!--[if lt IE 9]>
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>-->
<!--<![endif]&ndash;&gt;-->
<script type="text/javascript" src="/app/js/jquery.js?_v=${token}"></script>
<script type="text/javascript" src="/app/js/bootstrap.min.js?_v=${token}"></script>
<script type="text/javascript" src="/app/js/jquery-ui.js?_v=${token}"></script>
<script type="text/javascript" src="/app/js/jquery.ui.touch-punch.min.js?_v=${token}"></script>
<script type="text/javascript" src="/app/js/jquery.htmlClean.js?_v=${token}"></script>
<script src="/app/js/template.js?_v=${token}"></script>
<script src="/app/js/page.js?_v=${token}"></script>
<script src="/app/js/underscore.js?_v=${token}"></script>
<script src="/app/js/paginator.js?_v=${token}"></script>
<body>
<div id="js_page" style="max-width: 1320px;display: block;margin: 0 auto;">

</div>
<div class="pagination pagination-right pagination-small" id="pagination3"
     style="max-width: 1280px;display: block;margin: 0 auto;">
</div>
<script type="text/html" id="pageTpl">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-target=".navbar-responsive-collapse" data-toggle="collapse"></a>
                <a class="brand" href="#">CM管理后台</a>

                <div class="nav-collapse collapse navbar-responsive-collapse">
                    <ul class="nav">
                        <li class="active">
                            <a href="/activity">活动列表</a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/activity/add">创建活动</a>
                        </li>
                    </ul>
                    <ul class="nav pull-right">
                        <%--<li>--%>
                        <%--<a href="#">退出</a>--%>
                        <%--</li>--%>
                        <%--<li class="divider-vertical"></li>--%>
                        <%--<li class="dropdown">--%>
                        <%--<a class="dropdown-toggle" data-toggle="dropdown" href="#">设置</a>--%>
                        <%--<ul class="dropdown-menu">--%>
                        <%--<li>--%>
                        <%--<a href="#">信息设置</a>--%>
                        <%--</li>--%>
                        <%--<li>--%>
                        <%--<a href="#">关于我们</a>--%>
                        <%--</li>--%>
                        <%--</ul>--%>
                        <%--</li>--%>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div style="margin:80px 0 0 0;">
        <table class="table table-striped  table-bordered  table-condensed">
            <thead>
            <tr>
                <th width="60">编号</th>
                <th width="120">活动名称</th>
                <th width="120">开始时间</th>
                <th width="120">结束时间</th>
                <th width="100">状态</th>
                <th width="100">奖品数量(剩余/总量)</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>
            {{each list as value index}}
            <tr>
                <td>{{index}}</td>
                <td>{{value.title}}</td>
                <td>{{value.startTime}}</td>
                <td>{{value.endTime}}</td>
                <td>{{value.status}}</td>
                <td>{{value.remainCount}}/{{value.count}}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/activity/add?activityId={{value.id}}"
                       data-act-id="{{value.id}}">修改</a>
                    <span>/</span>
                    <a class=" js_participate_detail" href="#" data-act-id="{{value.id}}">参与明细</a>
                    <span>/</span>
                    <a href="#" class=" js_exchange_detail" data-toggle="modal" data-act-id="{{value.id}}">兑奖明细</a>
                </td>
            </tr>
            {{/each}}
            </tbody>
        </table>

    </div>

    <div id="modal-join-detail" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
         style="width:1024px;margin-left:-512px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>参与明细</h3>
        </div>
        <div class="modal-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>兑奖手机号码</th>
                    <th>微信openId</th>
                    <th>昵称</th>
                    <th>参与时间</th>
                    <th>是否中奖</th>
                    <th>获得的奖品</th>
                </tr>
                </thead>
                <tbody id="join-detail-container">
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-small" id="pagination">
            </div>
        </div>
    </div>

    <div id="modal-exchange-detail" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="width:1024px;margin-left:-512px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">兑奖明细</h3>
        </div>
        <div class="modal-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="60">兑换券发放时间</th>
                    <th width="60">兑换券兑换时间</th>
                    <th width="60">兑换用户手机</th>
                </tr>
                </thead>
                <tbody id="exchange_detail_container">

                </tbody>
            </table>
            <div class="pagination pagination-right pagination-small" id="pagination2">
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="joinDetailTpl">
    {{each list as value index}}
    <tr>
        <td>{{value.telephone}}</td>
        <td>{{value.openid}}</td>
        <td>{{value.nickName}}</td>
        <td>{{value.creatTime}}</td>
        <td>{{value.winning?'中奖':'未中奖'}}</td>
        <td>{{value.flowName}}</td>
    </tr>
    {{/each}}
</script>
<script type="text/html" id="exchangeDetailTpl">
    {{each list as value index}}
    <tr>
        <td>{{value.createTime}}</td>
        <td>{{value.exchangeTime}}</td>
        <td>{{value.telephone}}</td>
    </tr>
    {{/each}}
</script>
</body>
</html>