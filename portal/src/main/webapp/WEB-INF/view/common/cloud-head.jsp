<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>CM管理平台</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/css/cloud-admin.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/css/themes/default.css" id="skin-switcher">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/css/responsive.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger.min.css">
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger-spinner.min.css">
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger-theme-air.min.css">
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger-theme-block.min.css">
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger-theme-flat.min.css">
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger-theme-future.min.css">
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/hubspot-messenger/css/messenger-theme-ice.min.css">

<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/js/fuelux-tree/fuelux.min.css"/>

<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/js/datepicker/themes/default.date.min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/js/datepicker/themes/default.min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/js/datepicker/themes/default.time.min.css"/>

<!-- JQUERY UI-->
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css"/>
<!-- DATE RANGE PICKER -->
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/bootstrap-daterangepicker/daterangepicker-bs3.css"/>

<link rel="stylesheet" type="text/css" href="${ctx}/resources/cloudadmin/js/bootstrap-wizard/wizard.css"/>
<link href="${ctx}/resources/cloudadmin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="${ctx}/app/css/datetimepicker.css" rel="stylesheet"/>
<%--<link href="${ctx}/resources/chosen/chosen.min.css" rel="stylesheet" type="text/css"/>--%>
<link href="${ctx}/resources/chosen/chosen.css" rel="stylesheet" type="text/css"/>

<script>var ctx = "${ctx}";</script>

<script src="${ctx}/resources/cloudadmin/js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script src="${ctx}/resources/cloudadmin/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- BOOTSTRAP -->
<script src="${ctx}/resources/cloudadmin/bootstrap-dist/js/bootstrap.min.js"></script>


<!-- DATE RANGE PICKER -->
<script src="${ctx}/resources/cloudadmin/js/datepicker/legacy.js"></script>

<script src="${ctx}/resources/cloudadmin/js/datepicker/picker.js"></script>
<script src="${ctx}/resources/cloudadmin/js/datepicker/picker.date.js"></script>
<script src="${ctx}/resources/cloudadmin/js/datepicker/picker.time.js"></script>

<script src="${ctx}/resources/cloudadmin/js/bootstrap-daterangepicker/moment.min.js"></script>

<script src="${ctx}/resources/cloudadmin/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>

<script src="${ctx}/app/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<!-- SLIMSCROLL -->
<script type="text/javascript"
        src="${ctx}/resources/cloudadmin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
        src="${ctx}/resources/cloudadmin/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<!-- BLOCK UI -->
<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
<!-- NESTABLE LISTS -->
<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/nestable/jquery.nestable.min.js"></script>
<!-- COOKIE -->
<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/jQuery-Cookie/jquery.cookie.min.js"></script>

<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/hubspot-messenger/js/messenger.min.js"></script>
<script type="text/javascript"
        src="${ctx}/resources/cloudadmin/js/hubspot-messenger/js/messenger-theme-future.js"></script>
<script type="text/javascript"
        src="${ctx}/resources/cloudadmin/js/hubspot-messenger/js/messenger-theme-flat.js"></script>
<script type="text/javascript" src="${ctx}/resources/script/jquery.tmpl.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="${ctx}/resources/cloudadmin/js/script.js"></script>

<script src="${ctx}/resources/script/ajaxfileupload.js"></script>
<!-- WIZARD -->
<script src="${ctx}/resources/cloudadmin/js/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<!-- WIZARD -->
<script src="${ctx}/resources/cloudadmin/js/jquery-validate/jquery.validate.min.js"></script>
<script src="${ctx}/resources/cloudadmin/js/jquery-validate/additional-methods.min.js"></script>
<!-- BOOTBOX -->
<script src="${ctx}/resources/cloudadmin/js/bootstrap-wizard/form-wizard.js"></script>
<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/bootbox/bootbox.min.js"></script>

<!-- FUELUX TREE -->
<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/fuelux-tree/fuelux.tree-sampledata.js"></script>
<script type="text/javascript" src="${ctx}/resources/cloudadmin/js/fuelux-tree/fuelux.tree.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/script/jquery.json.js"></script>

<script type="text/javascript" src="${ctx}/resources/chosen/chosen.jquery.js"></script>
<script type="text/javascript" src="${ctx}/resources/chosen/chosen.proto.js"></script>


<script>
    //这段代码可以作为参考 不同插件需要初始化的App参数不一样，所以要在页面初始化的时候调用一下
    //    jQuery(document).ready(function() {
    //        App.setPage("nestable_lists");  //Set current page
    //        App.init(); //Initialise plugins and elements
    //    });
    $(function () {
        UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
        UE.Editor.prototype.getActionUrl = function (action) {
            if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadvideo') {
                return '${ctx}/common/file/upload/ue/' + action.replace(/upload/g, "");
            } else {
                return this._bkGetActionUrl.call(this, action);
            }
        }
        $(".auto-table").attr("class", "datatable table table-striped table-bordered table-hover dataTable auto-table");
    })
</script>
<style>
    .row {
        max-width: 98%;
        display: block;
        margin: 0 auto;
        padding: 0.8rem 0;
    }

    select {
        display: inline-block;
    }

    .tree .tree-folder .tree-folder-header, .tree .tree-item {
        height: auto !important;
    }

    .pagination {
        text-align: right !important;
        display: block !important;
        width: 100%;
    }

    .pagination * {
        display: inline-block !important;
        position: relative !important;
        width: auto !important;
        height: auto !important;;
        vertical-align: top !important;
        line-height: 1.8rem !important;
        float: none !important;
        margin: 0 0.4rem;
    }

    .pagination .jump-btn {
        background: none !important;
        color: #0aa !important;
        text-decoration: none !important;
    }

    .pagination input {
        width: 2rem !important;
        border: 1px solid #ccc;
        padding: 0 !important;
    }

    .clear {
        display: block;
        min-height:1px;
        width:100%;
        clear: both;
    }

    .form-group.normal input {
        display: inline-block !important;
        width: auto;
        min-width: 188px;
        float: none;
        line-height: 1.8rem !important;
        padding: 0 !important;
        margin: 0 0.8rem;
        vertical-align: middle;
        height: 2.4rem;
        font-size: 1rem;
        border: 1px solid #ccc;
    }

    .range_inputs *:not(button) {
        display: block !important;
        float: none !important;
        margin: 0 !important;
        text-align: left;
        padding: 0;
    }

    .range_inputs input {
        width: 90% !important;
    }

    .daterangepicker_end_input {
        padding: 0 !important;
    }

    .range_inputs button {
        margin: 0.4rem 0.8rem;
    }

     .modal-dialog {
        width: 100%;
        height: 100%;
        overflow: hidden;
        padding: 0;
        margin: 0;
    }

     .modal-content {
        padding: 0;
    }

    .modal .modal-footer .btn, .modal .modal-footer .btn-large, .modal .modal-footer .btn-flat {
        float: none;
    }

    .modal-header .close {
        position: absolute;
        top: 1rem;
        right: 1rem;
        color: #505e5f;
        opacity: 1;
    }
    .modal-footer button {
        vertical-align: middle;
        display: inline-block;
        margin: 0 0.8rem !important;
        min-width: 6rem;
    }

    a.chosen-single {
        position:relative !important;
        top:0 !important;
        min-width:8rem;
        height:3rem !important;
        display:inline-block;
        line-height:3rem !important;
        border-radius:3px !important;
        background:transparent !important;
        border-color:rgba(235,235,230,.6) !important;
        box-shadow:none !important;
        overflow:hidden !important;;
        width:100%;
    }
    a.chosen-single b{
        line-height:3rem !important;
        top:1.5rem !important;
        background-position-y: 8px !important;
    }



    .chosen-drop {
        border-color: rgba(235,235,230,.6) !important;
        padding:0 !important;
        background: #fff !important;
        padding-top:2.7rem !important;
        top:3.1rem !important;
        left:.8rem !important;
    }

    .chosen-drop *{
        background:#fff;
        margin-left:0 !important;
        margin-right:0 !important;

    }
    .chosen-drop em,.chosen-drop em:hover{
        background:transparent !important;
    }
    .chosen-container-single .chosen-search input[type="text"] {
        display:block;
        width:98% !important;
        border-color:rgba(185,185,185,.6) !important;
        border-bottom:1px solid rgba(235,235,235,.6) !important;
        margin:0  !important;
        background-image:none  !important;
        background:transparent !important;
        box-shadow:-2px 1px 2px #ccc;
    }

    .chosen-search {
        border-color:rgba(235,235,235,.6) !important;
        position:absolute !important;
        top:0;
        left:0;
        background:#fff  !important;
        padding:0 !important;
        margin:0 !important;

    }

    .chosen-search {
        background-color: #fff !important;
        border-color: rgba(235,235,235,.6) !important;
    }
</style>
<script>
    $(function () {


        if ($("#timeRange").length > 0) {
            $("#timeRange").daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                timePicker12Hour: false,
                locale: {
                    format: 'yyyy-MM-dd hh:mm:ss'
                },
                ranges: {
                    //'最近1小时': [moment().subtract('hours',1), moment()],
                    '今日': [moment().startOf('day'), moment()],
                    '昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],
                    '最近7日': [moment().subtract('days', 6), moment()],
                    '最近30日': [moment().subtract('days', 29), moment()]
                },
                opens: 'right', //日期选择框的弹出位置
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary blue',
                cancelClass: 'btn-small',
                format: 'YYYY-MM-DD HH:mm:ss', //控件中from和to 显示的日期格式
                separator: ' to ',
                locale: {
                    applyLabel: '确定',
                    cancelLabel: '取消',
                    fromLabel: '起始时间',
                    toLabel: '结束时间',
                    customRangeLabel: '自定义',
                    daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'],
                    firstDay: 1
                }
            }, function (start, end, label) {//格式化日期显示框
                $(this).html(start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss'));
            })
        }

        if($("#dateRange").length >0){
            $("#dateRange").daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                timePicker12Hour: false,
                locale: {
                    format: 'yyyy-MM-dd'
                },
                ranges: {
                    //'最近1小时': [moment().subtract('hours',1), moment()],
                    '今日': [moment().startOf('day'), moment()],
                    '昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],
                    '最近7日': [moment().subtract('days', 6), moment()],
                    '最近30日': [moment().subtract('days', 29), moment()]
                },
                opens: 'right', //日期选择框的弹出位置
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary blue',
                cancelClass: 'btn-small',
                format: 'YYYY-MM-DD', //控件中from和to 显示的日期格式
                separator: ' to ',
                locale: {
                    applyLabel: '确定',
                    cancelLabel: '取消',
                    fromLabel: '起始时间',
                    toLabel: '结束时间',
                    customRangeLabel: '自定义',
                    daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'],
                    firstDay: 1
                }
            }, function (start, end, label) {//格式化日期显示框
                $(this).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            })
        }

    })
</script>