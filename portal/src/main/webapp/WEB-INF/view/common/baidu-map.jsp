<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/6/12
  Time: 下午11:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WnFDBuXWOTsu9PUNK5w1LfMg"></script>
<script>
    var map = null;
    var currentCity = null;
    var defaultPoint = "${defaultPoint}" || "116.404, 39.915";
    var mk = null;
    var geoCoder = null;

    $.baidu_map = {
        init: function (id) {
            // 百度地图API功能
            map = new BMap.Map(id);
            geoCoder = new BMap.Geocoder();
            //根据浏览器定位
            map.centerAndZoom(new BMap.Point(parseFloat(defaultPoint.split(",")[0]), parseFloat(defaultPoint.split(",")[1])), 12);
            var geolocation = new BMap.Geolocation();
            loadingShow();
            geolocation.getCurrentPosition(function (r) {
                if (this.getStatus() == BMAP_STATUS_SUCCESS) {
                    console.log("point :%o", r);
                    mk = new BMap.Marker(r.point);
                    currentCity = r.address["province"] + "-" + r.address["city"];
                    var address = r.address;
                    if (!isEmpty(address)) {
                        $("#address").val(address.province + "-" + address.city + "-" + address.district + "-" + address.street + "-" + address.street_number);
                    }
                    map.addOverlay(mk);
                    map.panTo(r.point);
                    loadingHide();
                }
                else {
                    console.error('map init failed' + this.getStatus());
                }
                map.enableDragging(true);
                map.enableScrollWheelZoom(true);
            }, {enableHighAccuracy: true})
            //根据当前ip
//            var myCity = new BMap.LocalCity();
//            myCity.get(function(result){
//                currentCity = result.name;
//                console.log("current city:%o",result);
//                map.setCenter(result.name);
//            });

        },

        click_listener: function (_func) {
            if (typeof _func == "function") {
                map.addEventListener("click", _func);
            }
        }
    }
</script>