<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/25
  Time: 22:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<script>var ctx = "${ctx}";</script>
<link rel="stylesheet" href="${ctx}/resources/assets/materialize/css/materialize.min.css" media="screen,projection">
<!-- Bootstrap Styles-->
<link href="${ctx}/resources/assets/css/bootstrap.css" rel="stylesheet">
<!-- FontAwesome Styles-->
<link href="${ctx}/resources/assets/css/font-awesome.css" rel="stylesheet">
<!-- Morris Chart Styles-->
<link href="${ctx}/resources/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet">
<!-- Custom Styles-->
<link href="${ctx}/resources/assets/css/custom-styles.css" rel="stylesheet">
<!-- Google Fonts-->
<link rel="stylesheet" href="${ctx}/resources/assets/js/Lightweight-Chart/cssCharts.css">
<link rel="stylesheet" href="${ctx}/resources/style/common.css" type="text/css">


<script src="${ctx}/resources/assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="${ctx}/resources/assets/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/assets/materialize/js/materialize.min.js"></script>
<!-- Metis Menu Js -->
<script src="${ctx}/resources/assets/js/jquery.metisMenu.js"></script>
<!-- Morris Chart Js -->
<script src="${ctx}/resources/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="${ctx}/resources/assets/js/morris/morris.js"></script>
<script src="${ctx}/resources/assets/js/easypiechart.js"></script>
<%--<script src="${ctx}/resources/assets/js/easypiechart-data.js"></script>--%>
<script src="${ctx}/resources/assets/js/Lightweight-Chart/jquery.chart.js"></script>
<script src="${ctx}/resources/script/jquery.tmpl.min.js"></script>
<!-- Custom Js -->
<script src="${ctx}/resources/assets/js/custom.js"></script>
<script src="${ctx}/resources/assets/js/custom-scripts.js"></script>

<script src="${ctx}/resources/script/common.js?v=${token}"></script>

<!-- 配置文件 -->
<script type="text/javascript" src="${ctx}/resources/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="${ctx}/resources/ueditor/ueditor.all.js?version=123"></script>
<script type="text/javascript" src="${ctx}/resources/ueditor/ueditor.parse.js"></script>
