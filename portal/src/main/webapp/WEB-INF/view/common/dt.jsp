<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/cloudadmin/js/datatables/media/assets/css/datatables.min.css"/>
<link rel="stylesheet" type="text/css"
      href="${ctx}/resources/tabletools/css/dataTables.tableTools.min.css"/>
<!-- DATA TABLES -->
<script type="text/javascript"
        src="${ctx}/resources/cloudadmin/js/datatables/media/assets/js/datatables.min.js"></script>
<script type="text/javascript"
        src="${ctx}/resources/tabletools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript"
        src="${ctx}/resources/cloudadmin/js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>
<script>

    $.fn.dataTable.defaults.aButtons = ["copy", "csv", "xls"];

    $.fn.dataTable.defaults.oLanguage = {
        "oAria": {
            "sSortAscending": ": 升序排列",
            "sSortDescending": ": 降序排列"
        },
        "oPaginate": {
            "sFirst": "首页",
            "sLast": "末页",
            "sNext": "下页",
            "sPrevious": "上页"
        },
        "sEmptyTable": "没有找到符合条件的数据",
        "sInfo": "第 _START_ 到 _END_ 条记录，共 _TOTAL_ 条",
        "sInfoEmpty": "第 0 到 0 条记录，共 0 条",
        "sInfoFiltered": "(从 _MAX_ 条记录中检索)",
        "sInfoPostFix": "",
        "sDecimal": "",
        "sInfoThousands": ",",
        "sLengthMenu": "每页显示条数: _MENU_",
        "sLoadingRecords": "正在载入...",
        "sProcessing": "正在载入...",
        "sSearch": "搜索:",
        "sSearchPlaceholder": "",
        "sUrl": "",
        "sZeroRecords": "没有相关记录"
    };

    jQuery.fn.dataTable.defaults.iDisplayLength = 10;

    if (typeof drawCallBack != "undefined") {
        jQuery.fn.dataTable.defaults.fnDrawCallback = drawCallBack;
    }

</script>
<style>
    .dataTables_filter input {
        display: inline-block;
    }

    .dataTables_length select {
        display: inline-block;
    }
</style>