<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/6/29
  Time: 下午10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提现申请列表</title>
    <link href="${ctx}/resources/pagination/style/pagination.css" rel="stylesheet" type="text/css">
    <link href="${ctx}/resources/pagination/style/common.css" rel="stylesheet" type="text/css">
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <script type="text/javascript" src="${ctx}/resources/pagination/script/jquery.pagination.js"></script>
    <style>
        .bootbox-form input {
            float: none;
        }
    </style>
</head>
<body>
<div class="card">
    <div class="card-action">
        查询条件
    </div>
    <div class="card-content">
        <div class="col-md-12">
            <%--<div class="form-group col-md-4">--%>
                <%--<label>申请时间</label>--%>
                <%--<input type="datetime" placeholder="点击选择开始时间" id="timeRange" name="timeRange"/>--%>
            <%--</div>--%>
            <div class="form-group col-md-4">
                <label>提现用户</label>
                <select id="userId" name="userId">
                    <option value="">全部</option>

                </select>


            </div>

            <div class="form-group col-md-4">
                <label>审批状态</label>
                <select id="status" name="status">
                    <option value="" selected>全部</option>
                    <option value="WAIT_CHECK">等待审核</option>
                    <option value="SUCCESS">通过</option>
                    <option value="FAILED">驳回</option>
                </select>
                <input type="hidden" value="1" name="pageNum" id="pageNum"/>
                <input type="hidden" value="5" name="pageSize" id="pageSize"/>
                <input type="hidden" value="" name="total" id="total"/>
            </div>
            <div class="form-group col-md-4">
                <button class="btn btn-primary" onclick="loadData()"><i class="fa fa-search"></i> 筛选</button>
            </div>
        </div>
        <div class="clear">

        </div>
    </div>
</div>


<div class="card">
    <div class="card-action">
        提现申请列表
    </div>
    <div class="card-content">
        <table id="dataTable">
            <thead>
            <tr>
                <td>申请单号</td>
                <td>金额/账户余额</td>
                <td>申请人</td>
                <td>申请时间</td>
                <td>审批状态</td>
                <td>审批状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="7">
                    <div class="alert alert-warning"><i class="fa fa-spinner"></i> Loading...</div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/html" id="dataTmpl">
    <tr>
        <td>
            \${cp.id}
        </td>
        <td>
            \${cp.amount}/\${ab.balance}
        </td>
        <td>
            \${us.nickname}
        </td>
        <td>
            \${formatDate(cp.applicationTime)}
        </td>
        <td>
            {{if cp.status == "WAIT_CHECK"}}
            等待审核
            {{else cp.status == "SUCCESS"}}
            通过
            {{else cp.status == "FAILED"}}
            驳回
            {{else}}
            <code>异常状态</code>
            {{/if}}
        </td>
        <td>
            \${cp.remark}
        </td>
        <td>
            {{if cp.status == "WAIT_CHECK"}}
            <button class="btn btn-primary" onclick="passApplication(\${cp.id})">通过</button>
            <button class="btn btn-warning" onclick="refuseApplication(\${cp.id})">驳回</button>
            {{/if}}
        </td>
    </tr>
</script>
<script>
    $(function () {
        loadShop();
        loadData();
    })

    function loadData() {
        var data = {
            phone: $("#userId").val() || "",
            startTime: $("#timeRange").val()?$("#timeRange").val().split(" to ")[0]:"",
            endTime: $("#timeRange").val()?$("#timeRange").val().split(" to ")[1]:"",
            status: $("#status").val() || "",
            pageNum: $("#pageNum").val() || 1,
            pageSize: $("#pageSize").val() || 10
        };
        asyncAjax({
            url: ctx + "/cash/application/mgr/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $("#dataTable tbody").html("");
                    $("#dataTmpl").tmpl(res["data"]).appendTo($("#dataTable tbody"));
                    $("#total").val(res["total"]);
                    pageLoad();
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }


    function pageLoad() {
        var pageCount = Math.ceil((parseInt(($('#total').val() || 0)) / parseInt($("#pageSize").val() || 10)));
        $("#pagination").pagination({
            pageCount: pageCount,
            pageSize: parseInt($("#pageSize").val() || 10),
            totalCount: parseInt($("#total").val()),
            current: parseInt($("#pageNum").val()),
            jump: true,
            coping: true,
            homePage: '首页',
            endPage: '末页',
            prevContent: '上页',
            nextContent: '下页',
            callback: function (that) {
                console.log("that:%o", that);
                $("#pageNum").val(that.getCurrent());
                loadData();
                that.setCurrentPage()
            }
        })
    }


    function loadShop() {
        var data = {reviewStatus: 2};
        asyncAjax({
            url: ctx + "/user/mgr/list/3?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $.each(res["data"], function (idx, shopUser) {
                        $("#userId").append("<option value='" + shopUser.id + "'>" + shopUser.nickname + "</option>");
                    })
                    $("#userId").chosen({width: 380});
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }


    function passApplication(id) {
        bootbox.prompt({
            title: '请填写审批备注',
            buttons: {
                confirm: {
                    label: '确定'
                },
                cancel: {
                    label: '取消'
                }
            },
            callback: function (res) {
                if (res != null) {
                    if (isEmpty(res)) {
                        bootbox.alert("操作失败，请务必填写申请审批备注");
                    } else {
                        var data = {
                            applicationId: id,
                            remark: res
                        }
                        asyncAjax({
                            url: ctx + "/cash/application/mgr/apply?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                            success: function (res) {
                                console.log("res:%o", res);
                                if (res["result"] == "0") {
                                    bootbox.alert("操作成功");
                                    loadData();
                                } else {
                                    bootbox.alert(res["msg"]);
                                }
                            }
                        })
                    }
                }
                console.log("operation:%o", res);
            }
        })
    }
    function refuseApplication(id) {
        bootbox.prompt({
            title: '请填写驳回原因',
            buttons: {
                confirm: {
                    label: '确定'
                },
                cancel: {
                    label: '取消'
                }
            },
            callback: function (res) {
                if (res != null) {
                    if (isEmpty(res)) {
                        bootbox.alert("操作失败，请务必填写驳回原因");
                    } else {
                        var data = {
                            applicationId: id,
                            remark: res
                        }
                        asyncAjax({
                            url: ctx + "/cash/application/mgr/refuse?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                            success: function (res) {
                                console.log("res:%o", res);
                                if (res["result"] == "0") {
                                    bootbox.alert("操作成功");
                                    loadData();
                                } else {
                                    bootbox.alert(res["msg"]);
                                }
                            }
                        })
                    }
                }
                console.log("operation:%o", res);
            }
        })
    }
</script>
</body>
</html>
