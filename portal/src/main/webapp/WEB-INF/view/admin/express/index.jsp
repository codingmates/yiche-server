<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/6/25
  Time: 下午11:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>商品订单管理</title>
    <link href="${ctx}/resources/pagination/style/pagination.css" rel="stylesheet" type="text/css">
    <link href="${ctx}/resources/pagination/style/common.css" rel="stylesheet" type="text/css">
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <script type="text/javascript" src="${ctx}/resources/pagination/script/jquery.pagination.js"></script>
</head>
<body>
<div class="card">
    <div class="card-action">
        条件查询
        <button class="btn btn-primary" onclick="$('#addDialog').modal('show')">添加物流</button>
    </div>
    <div class="card-content">
        <div class="col-md-12" style="text-align:right;">
            <button class="btn btn-primary" onclick="loadData()" style="margin-right:2rem;"><i
                    class="fa fa-search"></i>搜索
            </button>
            <input type="hidden" value="1" name="pageNum" id="pageNum"/>
            <input type="hidden" value="5" name="pageSize" id="pageSize"/>
            <input type="hidden" value="" name="total" id="total"/>
        </div>
        <div class="clear"></div>
        <div class="col-md-12">
            <div class="col-md-4">
                <label>物流名称</label>
                <input type="text" name="keyword" id="keyword"/>
            </div>
            <shiro:hasAnyRoles name="all">
                <div class="col-md-4">
                    <label>店铺</label>

                    <select class="chosen" data-placeholder="选择店铺" id="shop" name="shop">
                        <option>全部</option>
                    </select>
                </div>
            </shiro:hasAnyRoles>

            <div class="col-md-4">
                <label>区域</label>
                <select class="chosen" data-placeholder="选择区域" id="area" name="area">
                    <option value="">全部</option>
                    <option value="default">全国默认</option>

                    <c:forEach items="${citys}" var="city">
                        <option value="${city.addressId}">${city.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-action">
            订单列表(Tip:金额栏数据格式表示：实际支付金额／订单总金额)
        </div>
        <div class="card-content">
            <div>
                <table id="mainTable" cellpadding="0" cellspacing="0" border="0" class="auto-table">
                    <thead>
                    <tr>
                        <%--<th>订单编号</th>--%>
                        <th>方案名称</th>
                        <th>目标区域</th>
                        <th>物流价格</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="addDialog" aria-hidden="true" class="modal"
     style="background: transparent; box-shadow: none">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4>添加物流方案</h4>
        </div>
        <div class="modal-content">
            <div class="col-md-12">
                <div class="form-group col-md-6">
                    <label>方案名称</label>
                    <input type="text" name="name" id="name" placeholder="请输入方案名称"/>
                </div>
                <div class="form-group col-md-6">
                    <label>费用</label>
                    <input type="number" name="fee" id="fee" placeholder="请输入费用#0.00#"/>
                </div>

            </div>
            <div class="clear"></div>
            <div class="form-group">
                <label>覆盖区域</label>
                <select id="province" name="province">
                    <option value="default">全国默认</option>
                    <c:forEach items="${citys}" var="city">
                        <option value="${city.addressId}">${city.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="clear"></div>
            <div class="modal-footer">
                <button class="btn btn-primary" onclick="addExpress()">添加</button>
                <button class="btn btn-cancel" onclick="$('#addDialog').modal('hide')">取消</button>
            </div>
        </div>

    </div>
</div>
<script type="text/html" id="operationTmpl">
    <div>
        <button class="btn btn-primary" click="delete('\${express_id}')">删除</button>
        <%--<button class="btn btn-warning" click="update('\${express_id}')">修改</button>--%>
    </div>
</script>
<script>
    var dt;
    $(function () {
        <shiro:hasAnyRoles name="all">
        loadShop();
        </shiro:hasAnyRoles>


        dt = $("#mainTable").dataTable();


        $("#province,#area").chosen({
            width: 200,
            height: 33,
            "no_results_text": "未找到对应的区域"
        });


        loadData();

    })

    function loadData() {
        var data = {
            keyword: $("#keyword").val() || "",
            area: $("#area").val() || "",
            shopId: $("#shopId").val() || "",

        };
        asyncAjax({
            url: ctx + "/express/mgr/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("列表数据:%o", res);
                if (res["result"] == "0") {
                    dt.fnClearTable()
                    var dataArray = [];
                    $.each(res["data"], function (idx, item) {
                        dataArray.push([item.name, item.province_desc || "错误的区域", item.fee,
                            '<button class="btn btn-error" onclick="deleteData(\'' + item["express_id"] + '\')">删除</button>']);
                    })
                    dt.fnAddData(dataArray);
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function addExpress() {
        var data = {
            name: $("#name").val() || "",
            fee: $("#fee").val() || "",
            area: $("#province").val() || ""
        };
        asyncAjax({
            url: ctx + "/express/mgr/add?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    var item = res["data"];
                    dt.fnAddData([[item.name, item.province_desc || "错误区域", item.fee,
                        '<button class="btn btn-error" onclick="deleteData(\'' + item["express_id"] + '\')">删除</button>']]);
                    $("#addDialog").modal('hide');
                } else {
                    bootbox.alert(res.msg);

                }
            }
        })
    }

    function deleteData(id) {
        bootbox.confirm({
            title: '操作确认',
            message: '确定删除?',
            buttons: {
                confirm: {
                    label: '确定'
                },
                cancel: {
                    label: '取消'
                }
            },
            callback: function (res) {
                if (res == true) {
                    var data = {expressId: id};
                    asyncAjax({
                        url: ctx + "/express/mgr/delete?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                        success: function (res) {
                            console.log("res:%o", res);
                            if (res["result"] == "0") {
                                bootbox.alert("删除成功");
                                loadData();
                            } else {
                                bootbox.alert(res.msg);
                            }
                        }
                    })
                }
            }

        })
    }


    <shiro:hasAnyRoles name="all">
    function loadShop() {
        var data = {reviewStatus: 2};
        asyncAjax({
            url: ctx + "/user/mgr/list/3?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $.each(res["data"], function (idx, shopUser) {
                        $("#shop").append("<option value='" + shopUser.id + "'>" + shopUser.nickname + "</option>");
                    })
                    $("#shop").chosen({width: 380});
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }
    </shiro:hasAnyRoles>
</script>
</body>
</html>
