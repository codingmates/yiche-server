<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/6/2
  Time: 上午12:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>图文管理</title>
    <link href="${ctx}/resources/pagination/style/pagination.css" rel="stylesheet" type="text/css">
    <link href="${ctx}/resources/pagination/style/common.css" rel="stylesheet" type="text/css">
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <script type="text/javascript" src="${ctx}/resources/pagination/script/jquery.pagination.js"></script>
    <style>
        #mainTable label.control-label.col-md-3 {
            min-width: 8rem;
        }
    </style>
</head>
<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-action">
            <label>图文配置</label>
            <button class="btn btn-primary" data-toggle="modal" data-target="addDialog"
                    onclick="initArticle();$('#addDialog').modal('show')">添加图文
            </button>
        </div>
        <div class="card-action">
            <div class="form-group normal col-md-3">
                <label>查询关键字:</label>
                <input type="text" value="" id="keyword" name="keyword"/>

            </div>

            <button class="btn btn-primary" onclick="loadData();">查询
            </button>
        </div>
        <div class="card-content">
            <table id="mainTable" cellpadding="0" cellspacing="0" border="0" class="auto-table">
                <thead>
                <tr>
                    <th>图文标题</th>
                    <td>配图</td>
                    <td>发布时间</td>
                    <td>发布人</td>
                    <td>发布板块</td>
                    <td>是否置顶</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <input type="hidden" name="pageNum" id="pageNum" value="1"/>
            <input type="hidden" name="pageSize" id="pageSize" value="10"/>
            <input type="hidden" name="total" id="total" value=""/>

            <div class="pagination pagination-right pagination-small" id="pagination">

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="addDialog" tabindex="-1" role="dialog" aria-labelledby="articleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="articleLabel">
                    添加图文
                </h4>
            </div>
            <div class="modal-body" style="height: 430px;overflow-y: scroll;">
                <input type="hidden" id="article_id" name="article_id">
                <div class="form-group">
                    <label>图文内容</label>
                    <input type="text" value="" placeholder="请输入图文内容" name="article_content" id="article_content"/>
                </div>
                <div class="form-group">
                    <label>图文配图（最多9张）</label>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview1" name="img-preview" class="img_upload_preview"
                                 for="img-file1" data-target="img-val" src=""
                                 onclick="$('#img-file1').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file1" type="file" name="img-file1" style="display: none;"
                                   onchange="uploadFile('img-file1','img-preview1','pic1','image')"/>
                            <input name="pic1" id="pic1" type="hidden">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview2" name="img-preview2" class="img_upload_preview"
                                 for="img-file2" data-target="img-val" src=""
                                 onclick="$('#img-file2').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file2" type="file" name="img-file2" style="display: none;"
                                   onchange="uploadFile('img-file2','img-preview2','pic2','image')"/>
                            <input name="pic2" id="pic2" type="hidden">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview3" name="img-preview3" class="img_upload_preview"
                                 for="img-file3" data-target="img-val" src=""
                                 onclick="$('#img-file3').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file3" type="file" name="img-file3" style="display: none;"
                                   onchange="uploadFile('img-file3','img-preview3','pic3','image')"/>
                            <input name="pic3" id="pic3" type="hidden">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview4" name="img-preview4" class="img_upload_preview"
                                 for="img-file4" data-target="img-val" src=""
                                 onclick="$('#img-file4').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file4" type="file" name="img-file4" style="display: none;"
                                   onchange="uploadFile('img-file4','img-preview4','pic4','image')"/>
                            <input name="pic4" id="pic4" type="hidden">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview5" name="img-preview5" class="img_upload_preview"
                                 for="img-file5" data-target="img-val" src=""
                                 onclick="$('#img-file5').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file5" type="file" name="img-file5" style="display: none;"
                                   onchange="uploadFile('img-file5','img-preview5','pic5','image')"/>
                            <input name="pic5" id="pic5" type="hidden">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview6" name="img-preview6" class="img_upload_preview"
                                 for="img-file6" data-target="img-val" src=""
                                 onclick="$('#img-file6').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file6" type="file" name="img-file6" style="display: none;"
                                   onchange="uploadFile('img-file6','img-preview6','pic6','image')"/>
                            <input name="pic6" id="pic6" type="hidden">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview7" name="img-preview7" class="img_upload_preview"
                                 for="img-file7" data-target="img-val" src=""
                                 onclick="$('#img-file7').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file7" type="file" name="img-file7" style="display: none;"
                                   onchange="uploadFile('img-file7','img-preview7','pic7','image')"/>
                            <input name="pic7" id="pic7" type="hidden">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview8" name="img-preview8" class="img_upload_preview"
                                 for="img-file8" data-target="img-val" src=""
                                 onclick="$('#img-file8').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file8" type="file" name="img-file8" style="display: none;"
                                   onchange="uploadFile('img-file8','img-preview8','pic8','image')"/>
                            <input name="pic8" id="pic8" type="hidden">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <img id="img-preview9" name="img-preview9" class="img_upload_preview"
                                 for="img-file9" data-target="img-val" src=""
                                 onclick="$('#img-file9').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="img-file9" type="file" name="img-file9" style="display: none;"
                                   onchange="uploadFile('img-file9','img-preview9','pic9','image')"/>
                            <input name="pic9" id="pic9" type="hidden">
                        </div>
                    </div>
                </div>

                <div class="form-group" id="areaSelectBox">
                    <label>发布版块</label>
                    <select name="section_id" id="section_id" class="chosen-select">
                        <c:forEach items="${sectionList}" var="section">
                            <option value="${section.id}">${section.sectionName}</option>
                        </c:forEach>
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="button" class="btn btn-primary" onclick="addArticle()">
                    提交
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/html" id="dataTmpl">
    <tr data-id="\${id}">
        <td>
            <p>\${content}</p>
        </td>
        <td>
            {{each(i,pic) pics}}
                <div class="col-md-4" style="height: 60px;overflow: hidden;padding: 5px;">
                    <img style="width: 50px;" src="\${pic}">
                </div>

            {{/each}}
        </td>
        <td>
            <p>
                \${formatDate(create_time)}
            </p>
        </td>
        <td>
            <p>
                \${user_nickname}
            </p>
        </td>
        <td>
            \${sectionName}
        </td>
        <td>
            <p>
                {{if is_top == "1"}}
                <font color='#20b2aa'>已置顶</font>
                {{else}}
                <font color='#orange'>未置顶</font>
                {{/if}}
            </p>
        </td>
        <td>
            <div>
                <div style="min-width:16rem;">
                    {{if is_top == "1"}}
                    <a href="javascript:untopArticle('\${id}');" class="btn btn-warning">取消置顶</a>
                    {{else}}
                    <a href="javascript:topArticle('\${id}');" class="btn btn-success">置顶</a>
                    {{/if}}
                    <a href="javascript:editArticle('\${id}');" class="btn btn-warning">修改</a>
                    <a href="javascript:delArticle('\${id}');" class="btn btn-danger">删除</a>
                </div>
            </div>
        </td>
    </tr>
</script>
<jsp:include page="../shop/content/common-tmpl.jsp"></jsp:include>
<script src="${ctx}/resources/script/common.js" type="text/javascript"></script>

<script>
    var sectionSelect = {
    };
    <c:forEach items="${sectionList}" var="section">
    sectionSelect["${section.id}"] = "${section.sectionName}";
    </c:forEach>



    function initArticle() {
        $("#article_id").val("");
        $("#article_content").val("");
        $("#section_id").val(0);
        var i = 0;
        while(i < 9){
            $("#pic" + (i + 1)).val("");
            $("#img-preview" + (i + 1)).attr("src","");
            i++;
        }
    }
    $(function () {
        loadData();
    })

    function pageLoad() {
        var pageCount = Math.ceil((parseInt(($('#total').val() || 0)) / parseInt($("#pageSize").val() || 10)));
        $("#pagination").pagination({
            pageCount: pageCount,
            pageSize: parseInt($("#pageSize").val() || 10),
            totalCount: parseInt($("#total").val()),
            current: parseInt($("#pageNum").val()),
            jump: true,
            coping: true,
            homePage: '首页',
            endPage: '末页',
            prevContent: '上页',
            nextContent: '下页',
            callback: function (that) {
                console.log("that:%o", that);
                $("#pageNum").val(that.getCurrent());
                loadData();
                that.setCurrentPage()
            }
        })
    }

    function loadData() {
        var data = {
            pageNum: $("#pageNum").val(),
            pageSize: $("#pageSize").val(),
            type:0
        };
        if (!isEmpty($("#keyword").val())) {
            data["keyword"] = encodeURI($("#keyword").val());
        }

        asyncAjax({
            url: ctx + "/article/getArticleList?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            type: "post",
            beforeSend: function (req) {
                $("#mainTable tbody").html("<tr><td colspan=\"7\"><div class=\"alert alert-warning\">正在请求数据...</div></td></tr>");
            },
            success: function (res) {
                if (res["result"] == "0") {
                    if (res["data"].length <= 0) {
                        $("#mainTable tbody").html("<tr><td colspan=\"7\"><div class=\"alert alert-warning\">没有找到符合条件...</div></td></tr>");
                    } else {
                        $("#total").val(res["total"] || 0);
                        $("#mainTable tbody").html("");
                        for(var i in res["data"]){
                            res["data"][i]["sectionName"] = sectionSelect[res["data"][i]["section_id"]];
                        }
                        $("#dataTmpl").tmpl(res["data"]).appendTo("#mainTable tbody");
                        pageLoad();
                    }
                } else {

                }
            }
        })
    }

    function addArticle() {

        var pics = [];
        var i = 0;
        while(i < 9){
            if(!isEmpty($("#pic" + (i + 1)).val())){
                pics.push($("#pic" + (i + 1)).val());
            }
            i++;
        }

        var data = {
            id:$("#article_id").val() || "",
            section_id: $("#section_id").val() || "",
            content:$("#article_content").val(),
            pics:pics,
            type:0
        }
        if (isEmpty(data["content"])) {
            bootbox.alert("请输入图文内容!");
            return;
        }
        if (isEmpty(data["section_id"])) {
            bootbox.alert("版块不能为空!");
            return;
        }
        asyncAjax({
            url: ctx + "/article/mergeArticle",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("操作成功");
                    $("#addDialog").modal("hide");

                    loadData();
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }
    function editArticle(id){
        var params = {
            "article_id":id
        }
        $.post(ctx + "/article/getArticleInfo",params,function(res){
            if(res.result == 0){
                $("#article_id").val(res.data.id);
                $("#article_content").val(res.data.content);
                $("#section_id").val(res.data.section_id);

                var i = 0;
                while(i < 9){
                    if(!isEmpty(res.data.pics[i])){
                        $("#pic" + (i + 1)).val(res.data.pics[i]);
                        $("#img-preview" + (i + 1)).attr("src",res.data.pics[i]);
                    }else{
                        $("#pic" + (i + 1)).val("");
                        $("#img-preview" + (i + 1)).attr("src","");
                    }
                    i++;
                }
                $('#addDialog').modal('show');

            }else{
                alert(res.msg)
            }
        });

    }
    function untopArticle(id) {
        var params = {
            "id":id
        }
        $.post(ctx + "/article/unTopArticle",params,function(res){
            if(res.result == 0){
                loadData();
            }else{
                alert(res.msg)
            }
        });
    }
    function topArticle(id) {
        var params = {
            "id":id
        }
        $.post(ctx + "/article/topArticle",params,function(res){
            if(res.result == 0){
                loadData();
            }else{
                alert(res.msg)
            }
        });
    }
    function delArticle(id){
        if (!confirm("确认要删除？")) {
            return;
        }
        var params = {
            "id":id
        }
        $.post(ctx + "/article/delArticle",params,function(res){
            if(res.result == 0){
                loadData();
            }else{
                alert(res.msg)
            }
        });
    }

</script>
</body>
</html>
