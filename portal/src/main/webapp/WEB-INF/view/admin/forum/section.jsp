<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/6/2
  Time: 上午12:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>社区版块配置</title>
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
</head>
<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-action">
            <label>版块配置</label>
            <button class="btn btn-primary" data-toggle="modal" data-target="addDialog"
                    onclick="$('#addDialog').modal('show')">添加版块
            </button>
        </div>
        <div class="card-content">
            <table class="autoTable" id="mainTable">
                <thead>
                <tr>
                    <th>版块名称</th>
                    <td>版块LOGO</td>
                    <td>是否启用</td>
                    <td>操作</td>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="addDialog" tabindex="-1" role="dialog" aria-labelledby="sectionLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="sectionLabel">
                    添加版块
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>版块名称</label>
                    <input type="text" value="" placeholder="请输入版块名称" name="sectionName" id="sectionName"/>
                </div>
                <div class="form-group">
                    <label>版块LOGO</label>
                    <img id="img-preview" name="img-preview" class="img_upload_preview"
                         for="img-file" data-target="img-val" src=""
                         onclick="$('#img-file').click()"
                         onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                    <input id="img-file" type="file" name="img-file" style="display: none;"
                           onchange="uploadFile('img-file','img-preview','sectionImage','image')"/>
                    <input name="sectionImage" id="sectionImage" type="hidden">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="button" class="btn btn-primary" onclick="addSection()">
                    提交
                </button>
            </div>
        </div>
    </div>

</div>
<jsp:include page="../shop/content/common-tmpl.jsp"></jsp:include>
<script src="${ctx}/resources/script/common.js" type="text/javascript"></script>

<script>
    var dt;
    $(function () {
        dt = $("#mainTable").dataTable({
            "fnDrawCallback": drawCallBack,
            "columnDefs": [
                {
                    targets: [0],
                    render: function (td, cellData, rowData, col) {
                        return '<span class="edit_able" data-name="sectionName" data-type="text" ' +
                            'data-id="' + rowData[3] + '" >' + rowData[0] + '</span>'
                    }
                },
                {
                    targets: [1],
                    render: function (td, cellData, rowData, row, col) {
                        return '<img class="edit_able" data-name="sectionImage" data-type="img" ' +
                            'data-id="' + rowData[3] + '" src="' + rowData[1] + '" style="max-width:15rem;"/>'
                    }
                },
                {
                    targets: [2],
                    render: function (td, cellData, rowData, col) {
                        return rowData[2] == "1" ? "<font color='#20b2aa'>启用</font>" : "<font color='orange'>禁用</font>";
                    }
                },
                {
                    targets: [3],
                    render: function (td, cellData, rowData, col) {
                        if (rowData[2] == 1) {
                            return "<button class='btn btn-warning' onclick=\"updateSection(" + rowData[3] + ",\'status\',0)\">禁用</button>";
                        } else {
                            return "<button class='btn btn-primary' onclick=\"updateSection(" + rowData[3] + ",\'status\',1)\">启用</button>";
                        }
                    }
                }
            ]
        });

        loadSection();
    })

    function updateSection(id, key, val) {
        var data = CURRENT_DATA_CACHE[id];
        data[key] = val;
        if (isEmpty(key) || isEmpty(val)) {
            bootbox.alert("参数缺失！");
            return;
        }
        asyncAjax({
            url: ctx + "/sectionMgr/mergeSection",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("修改成功");
                    loadSection();
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function addSection() {
        var data = {
            sectionName: $("#sectionName").val() || "",
            sectionImage: $("#sectionImage").val() || ""
        }
        if (isEmpty(data["sectionImage"])) {
            bootbox.alert("请上传版块LOGO!");
            return;
        }
        asyncAjax({
            url: ctx + "/sectionMgr/addSection",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("添加成功");
                    var section = res["data"];
                    CURRENT_DATA_CACHE[section.id] = section;
                    dt.fnAddData([[section.sectionName, section.sectionImage, section.status, section.id]]);
                    $("#addDialog").modal("hide");
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    var CURRENT_DATA_CACHE = {};
    function loadSection() {
        var data = {};
        asyncAjax({
            url: ctx + "/sectionMgr/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            beforeSend: function () {
                dt.fnClearTable();
                CURRENT_DATA_CACHE = {};
            },
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    if (!isEmpty(res["data"])) {
                        var datas = [];
                        $.each(res["data"], function (idx, section) {
                            CURRENT_DATA_CACHE[section.id] = section;
                            datas.push([section.sectionName, section.sectionImage, section.status, section.id]);
                        })
                        console.log("show datas:%o", datas);
                        dt.fnAddData(datas);
                    }

                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }


    var drawCallBack = function () {
        $(".edit_able").clickEditVal(function (data, el) {
            updateSection(parseInt(data["id"]), data["name"], data["value"]);
        });
    }
</script>
</body>
</html>
