<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/25
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/header.jsp"></jsp:include>
<body style="background: #efefef;">
<div class="col-lg-6">
    <div class="card">
        <div class="card-action">
            微信参数配置
        </div>
        <div class="card-content">
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" id="appid" name="appid" class="validate" value="${pn.appid}">
                    <label for="appid">微信公众号App ID</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" id="appsecret" name="appsecret" class="validate" value="${pn.appsecret}">
                    <label for="appsecret">微信公众号App Secret</label>
                </div>

                <div class="input-field col s6">
                    <input type="text" id="uuid" name="uuid" class="validate" value="${pn.uuid}">
                    <label for="uuid">微信公众号ID（UUID）</label>
                </div>

                <div class="input-field col s6">
                    <input type="text" id="token" name="token" class="validate" value="${pn.token}">
                    <label for="token">微信公众号token</label>
                </div>
            </div>
            <div class="row right-align">
                <button class="btn btn-primary" id="wechatConfigUpdateBtn"><i class="material-icons dp48">保存</i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6" id="wechatFuncConfigBox">
    <div class="card">
        <div class="card-action">
            微信功能配置
        </div>
        <div class="card-content">
            <div class="row">
                <div class="switch">
                    <label>
                        <input id="share_app" type="checkbox">
                        <span class="lever"></span>
                        允许分享给朋友（显示分享给朋友按钮）
                    </label>
                </div>
                <div class="switch">
                    <label>
                        <input id="share_timeline" type="checkbox">
                        <span class="lever"></span>
                        允许分享到朋友圈（显示分享到朋友圈按钮）
                    </label>
                </div>
            </div>

            <div class="row right-align">
                <button class="btn btn-primary" id="wechatFuncConfigBtn"><i class="material-icons dp48">保存</i>
                </button>
            </div>
        </div>
    </div>
</div>
</body>