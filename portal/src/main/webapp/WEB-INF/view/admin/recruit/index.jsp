<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/6/30
  Time: 下午11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>招聘管理</title>
    <link href="${ctx}/resources/pagination/style/pagination.css" rel="stylesheet" type="text/css">
    <link href="${ctx}/resources/pagination/style/common.css" rel="stylesheet" type="text/css">
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <script type="text/javascript" src="${ctx}/resources/pagination/script/jquery.pagination.js"></script>
</head>
<body>
<div class="card">
    <div class="card-action">
        <label>招聘管理</label>
        <button class="btn" onclick="add()">发布招聘</button>
    </div>
    <div class="card-content">
        <div class="form-group col-md-4">
            <label>招聘职位</label>
            <input type="text" id="keyword" name="keyword"/>
        </div>
        <div class="col-md-4">
            <button class="btn btn-primary" onclick="loadData()">筛选</button>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="card">
    <div class="card-action">
        <label>招聘列表</label>
    </div>
    <div class="card-action">
        <table class="autoTable" id="dataTable">
            <thead>
            <tr>
                <th>招聘标题</th>
                <th>薪资描述</th>
                <th>工作时间描述</th>
                <th>工作内容描述</th>
                <td>操作</td>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script id="addTmpl" type="text/html">
    <div>
        <div class="form-group col-md-4">
            <label>招聘岗位</label>
            <input type="text" name="title" id="title"/>
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label>工作时间描述</label>
                <input type="text" name="worktime" id="worktime"/>
            </div>
            <div class="form-group col-md-6">
                <label>薪资范围</label>
                <input type="text" name="salary" id="salary"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label>工作内容描述 </label>
            <input type="text" name="rcontent" id="rcontent"/>
        </div>
        <div class="clear"></div>
    </div>
</script>
<script>
    var dt = null;
    $(function () {
        dt = $("#dataTable").DataTable();
        loadData();
    })

    function loadData() {
        var data = {
            keyword: $("#keyword").val() || "",
            shopId:'${uid}'
        };
        asyncAjax({
            url: ctx + "/recruit/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    dt.clear();
                    var dataArray = [];
                    $.each(res["data"], function (idx, item) {
                        dataArray.push([item.title, item.salary, item.worktime, item.content,
                            '<button class="btn btn-warning" onclick="delRecruit(\'' + item._id + '\')">删除</button>'])
                    })
                    dt.rows.add(dataArray).draw();
                } else {
                    bootbox.alert(res.msg);
                }
            }
        })
    }

    function add() {
        bootbox.confirm({
            title: "发布招聘",
            message: $("#addTmpl").tmpl({}).html(),
            buttons:{
                confirm:{
                    label:'添加'
                },
                cancel:{label:'取消'}
            },
            callback:function(res){
                if(res == true){
                    var data = {
                        title:$("#title").val()||"",
                        salary:$("#salary").val()||"",
                        content:$("#rcontent").val()||"",
                        worktime:$("#worktime").val()||""
                    }

                    asyncAjax({
                        url: ctx + "/recruit/mgr/add?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                        success: function (res) {
                            console.log("res:%o", res);
                            if (res["result"] == "0") {
                                bootbox.alert("添加成功！");
                                loadData();
                            } else {

                            }
                        }
                    })
                }
            }
        })
    }

    function delRecruit(id) {
        var data = {id: id};
        asyncAjax({
            url: ctx + "/recruit/mgr/del?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("操作成功");
                    loadData();
                } else {
                    bootbox.alert(res.msg);
                }
            }
        })
    }
</script>
</body>
</html>
