<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/5/8
  Time: 下午9:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>CM创合管理平台</title>
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <link href="${ctx}/resources/style/login/style.css" rel="stylesheet">
    <link href="${ctx}/resources/style/jquery-ui-1.11.2.css" rel="stylesheet prefetch">
    <style>
        .tab-box {
            line-height: 2rem;
            border-bottom: 1px solid rgba(0, 200, 180, .8);
            margin: 0.5rem 0;
            padding: 0 !important;
            word-spacing: 0 !important;
            width: 90%;

        }

        .tab-item {
            display: inline-block;
            line-height: 2.8rem;
            padding: 0.4rem 0.8rem;
            border-bottom: none;
            vertical-align: bottom;
            margin: 0 !important;
            background-position: 100% 0;
            transition: all ease .8s;
            cursor: pointer;
        }

        .tab-item.active {
            background: rgba(0, 200, 180, 0.8);
            color: #fff;
        }

        #pass {
            display: none;
        }

        .vcrd-box {
            display: block;
            text-align: right;
            margin-bottom: .5rem;
        }

        .vcrd-box input:first-child {
            text-align: left;
        }

        .tip-box {
            display: block;
            position: relative;
        }

        .tip-box::before {
            content: ' ';
            width: 2px;
            display: block;
            border-left: 2px solid #efefef;
            height: 28rem;
            position: absolute;
            left: -.8rem;
            top: 4rem;
        }

        .tip-box h3 {
            font-size: 1.4rem;
            margin: 2rem auto 4rem auto;
            color: #353535;
        }

        .tip-box ul li {
            list-style: decimal;
            margin-left: 2rem;

            color: #686868;
        }

        .tip-box ul li p {
            font-size: 1.2rem;
            position: relative;
            color: #686868;
        }

        .tip-box ul li p a{
            color: #f8555a;
            display:inline-block;
            margin:0 0.8rem;
            text-decoration: solid;
        }

        .passed {
            font-size: 1.2rem;
            line-height: 2rem;
            text-align: center;
            width: 2rem;
            height: 2rem;
            float: right;
            background: #0aa;
            border-radius: 50% !important;
            color: #fff;
        }

        .un-passed {
            font-size: 1.2rem;
            line-height: 2rem;
            text-align: center;
            width: 2rem;
            height: 2rem;
            float: right;
            background: #7c0a1a;
            border-radius: 50% !important;
            color: #fff;
        }
    </style>
</head>
<body>
<div class="login-card">
    <div class="row">
        <div class="col-md-6">
            <h1>管理平台登录</h1>
            <div class="tab-box">
                <div class="tab-item active">商户登录</div>
                <div class="tab-item">管理员登录</div>
            </div>
            <form action="${ctx}/admin/login/check" method="post" onsubmit="return checkForm(this)">
                <input type="text" name="phone" id="phone" placeholder="手机号码" value="">
                <input type="password" name="pass" id="pass" onfocus="this.type='password'" placeholder="密码" value="">
                <div class="vcrd-box">
                    <div style="margin: 0.4rem auto;text-align: left;">
                        <input type="text" name="validateCode" id="validateCode" placeholder="输入验证码"
                               style="width: 9rem;height: 30px;vertical-align: top;"/>
                        <img src="" id="codePreview" style="width:160px;height:30px;" onclick="reloadValidateCode()"/>
                    </div>
                    <input type="text" name="vrcd" placeholder="短信验证码"/>
                    <input type="button" value="短信验证码" id="sendBtn" onclick="sendSms()" class="login login-submit"/>

                </div>
                <input type="submit" name="登录" class="login login-submit"/>
            </form>

            <div class="login-help">
                <a href="#">技术支持</a> • <a href="#">CM创合</a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tip-box">
                <h3>使用本系统，请确保您的计算机符合以下条件：</h3>
                <ul>
                    <li><p><span>系统版本为window 7 以上或者Mac OS 10.x以上</span></p></li>
                    <li><p><span>使用Chrome浏览器45以上版本。
                        <br>
                        <a href="https://www.baidu.com/link?url=MkAK-KyAQNLCdmsO6l1bW43XIdwqS-4Tqi9mL9ndX6_dZenVWqjFSeUcqFmN87s5Dog3SwFjbCC33lmOBW4asocqwc_fHKWPKzIKtJb9gZ7&wd=&eqid=a0a1d81300002d0f000000065a311a16" target="_blank">Mac点击下载</a>

                        <a href="https://www.baidu.com/link?url=AmTJR6uS3k37xKfmMcsoO08oB5mCpiQo5BVB0A9xkWOyCdgfYTSnFllF-ioFaBKNr804uoeEZAYwE5deDtH_rmoky2y2AwfGN5CqQy_H0lG&wd=&eqid=a0a1d81300002d0f000000065a311a16" target="_blank">window点击下载</a>
                    </span>
                        <i id="browerCheck" class="fa"></i></p></li>
                    <li><p><span>显示器分辨率再1366*768以上</span><i id="screenCheck" class="fa"></i></p></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="${ctx}/resources/script/jquery-ui-1.11.2.js"></script>
<script>
    $(function () {
        if (self.frameElement && self.frameElement.tagName == "IFRAME") {
            parent.location = window.location;
        }

        reloadValidateCode();

        $(".tab-item").unbind().bind("click", function () {
            if ($(this).hasClass("active")) {
                return;
            }
            $(".tab-item").toggleClass("active");
            $("#pass ,.vcrd-box").fadeToggle();
        })

        $(window).bind("load resize", function () {
            if (navigator) {
                if (navigator.userAgent.indexOf("Chrome") == -1) {
                    $("#browerCheck").attr("class", "fa fa-times un-passed");
                } else {
                    var version = navigator.userAgent.substr(navigator.userAgent.indexOf("Chrome") + ("Chrome".length) + 1).split(".")[0];
                    if (parseInt(version) > 45) {
                        $("#browerCheck").attr("class", "fa fa-check passed");
                    } else {
                        $("#browerCheck").attr("class", "fa fa-times un-passed");
                    }
                }
            }

            if (window.outerWidth > 1300 && window.outerHeight > 680) {
                $("#screenCheck").attr("class", "fa fa-check passed");
            } else {
                $("#screenCheck").attr("class", "fa fa-times un-passed");

            }
        })

    })

    function sendSms() {
        if (CURRENT_VALIDATE.toLowerCase() != $("#validateCode").val().toLowerCase()) {
            alert("验证码错误，请重试!");
            return;
        }
        var phone = $("#phone").val();
        if (/^1[3|4|5|7|8][0-9]{9}$/.test(phone) == false) {
            alert("请输入合法的手机号码!");
            return;
        }
        var data = {
            phone: phone,
            action: "LOGIN_VERIFY"
        }

        asyncAjax({
            url: ctx + "/sms/send",
            data: data,
            success: function (res) {
                if (res["result"] == 0) {
                    alert("验证码已经发送，请注意查收！");
                    $("#sendBtn").val("60s后重新发送").removeAttr("onclick").removeClass("login-submit");
                    var count = 60;
                    var timeInterval = window.setInterval(function () {
                        $("#sendBtn").val(--count + "后重新发送");
                    }, 1000)
                    setTimeout(function () {
                        $("#sendBtn").val("短信验证码").attr("onclick", "sendSms()").addClass("login-submit");
                        window.clearInterval(timeInterval);
                    }, 60000)
                } else {
                    alert(res["msg"]);
                }
            }
        })
    }

    var CURRENT_VALIDATE = 0;

    function reloadValidateCode() {
        var data = {};
        asyncAjax({
            url: ctx + "/sms/validate",
            type: "post",
            data: data,
            success: function (res) {
                console.log("data:%o", res);
                if (res["result"] == 0) {
                    $("#codePreview").attr("src", ctx + "/resources/upload/validateCode/" + res["data"] + ".png");
                    CURRENT_VALIDATE = res["data"];
                } else {
                    alert(res["msg"]);
                }
            }
        })
    }

    function checkForm(e) {
        var tabIndex = $(".tab-item.active").index();
        if (isEmpty($("#phone").val())) {
            alert("请输入手机号!");
            return false;
        }
        if (tabIndex == 0) {
            if (isEmpty($("#validateCode").val())) {
                alert("请输入短信验证码!");
                return false;
            }
        } else if (tabIndex == 1) {
            if (isEmpty($("#pass").val())) {
                alert("请输入密码!");
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
</script>
</body>
</html>
