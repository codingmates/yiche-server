<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/5/21
  Time: 下午7:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>平台首页管理</title>
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <style>
        #areaSelectBox > div {
            display: inline-block;
            width: 166px;
        }

        #dateRange {
            display: inline-block;
            width: 15rem;
        }

        #linkInputBox {
            display: block;
        }

        .card-content {
            min-height: 83px;
        }

        #articleSelectBox, #goodSelectBox {
            display: none;
        }

        span.good-item, span.shop-item, span.article-item {
            width: 10rem;
            display: inline-block;
            vertical-align: top;
            line-height: 2rem;
            overflow: hidden;
            max-height: 5.6rem;
            text-overflow: ellipsis;
            word-break: break-all;
            word-wrap: break-word;
            border: 1px solid rgba(200, 10, 10, .8);
            border-radius: 3px;
            padding: 0.4rem 0.8rem !important;
            cursor: pointer;
            overflow: hidden;
            padding-right: 3rem;
        }

        span.good-item code, span.shop-item code,

        ,
        span.article-item code {
            display: block;
            margin: 0.2rem auto;
        }

        span.good-item, span.shop-item, span.article-item {
            margin: 0.4rem 0.8rem;
            background: #0aa;
            color: #fff;
            border-color: #0aa;
            text-align: center;
            position: relative;

        }

        .good-item em, .shop-item em,

        ,
        span.article-item em {
            font-family: Microsoft Yahei, Helvetica;
            font-style: normal !important;
        }

        .good-item i {
            color: red;
            position: absolute;
            top: 0rem;
            line-height: 2rem;
            right: 0rem;
            display: block;
            width: 2rem;
            height: 2rem;
            font-size: 1.6rem;
        }

        span.good-item:hover, span.shop-item:hover,

        ,
        span.article-item:hover {
            background: rgba(230, 50, 50, .8);
            border-color: #af8080;
            box-shadow: 0 0 3px #f00;
        }

        #newBox, #hotBox, #infoBox, #shopBox {
            height: 20rem;
            overflow-y: auto;
        }
    </style>
</head>
<body>
<div class="col-md-12">

    <div class="col-md-6">
        <div class="card" data-id="NEW_GOOD">
            <div class="card-action">
                <label>新品推荐</label>
                <button class="btn btn-primary" onclick="showGoodSelect('NEW_GOOD')">选择商品</button>
                <select name="filterCity" class="chosen-select">
                    <option value="">全部</option>
                    <option value="default">默认全国</option>
                    <c:forEach items="${citys}" var="city">
                        <option value="${city.addressId}">${city.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="card-content" id="newBox">

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card" data-id="HOT_GOOD">
            <div class="card-action">
                <label>热门商品</label>
                <button class="btn btn-primary" onclick="showGoodSelect('HOT_GOOD')">选择商品</button>
                <select name="filterCity" class="chosen-select">

                    <option value="">全部</option>
                    <option value="default">默认全国</option>
                    <c:forEach items="${citys}" var="city">
                        <option value="${city.addressId}">${city.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="card-content" id="hotBox">

            </div>
        </div>
    </div>

</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="card">
            <div class="card-action">
                <label>资讯配置</label>
                <button class="btn btn-primary" onclick="$('#articleSelectDialog').modal('show');">添加文章</button>

            </div>
            <div class="card-content" id="infoBox">

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card" id="flagShopBox">
            <div class="card-action">
                <label>首页店铺配置</label>
                <button class="btn btn-primary" onclick="$('#shopSelectDialog').modal('show');">添加店铺</button>
                <select name="filterCity" class="chosen-select">
                    <option value="">全部</option>
                    <option value="default">默认全国</option>
                    <c:forEach items="${citys}" var="city">
                        <option value="${city.addressId}">${city.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="card-content" id="shopBox">

            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-action">
            <label>轮播图配置</label>
            <button class="btn btn-primary" data-toggle="modal" onclick="$('#addDialog').modal('show')">添加轮播图
            </button>
        </div>
        <div class="card-content">
            <table id="mainTable" cellpadding="0" cellspacing="0" border="0" class="auto-table">
                <thead>
                <tr>
                    <th>图片</th>
                    <th>投放时间</th>
                    <th>投放内容</th>
                    <th>投放区域</th>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="addDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    添加轮播图
                </h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="form-group col-md-6">
                        <label>有效时间</label>
                        <input type="text" value="" placeholder="点击选择时间范围" name="dateRange" id="dateRange"/>
                    </div>
                    <div>
                        <div class="form-group" id="areaSelectBox">
                            <label>投放区域</label>
                            <select name="city" id="city" class="chosen-select">
                                <option value="default">默认全国</option>
                                <c:forEach items="${citys}" var="city">
                                    <option value="${city.addressId}">${city.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6">
                            <label>响应类型</label>
                            <select name="type" id="type" style="display: inline-block;width:10rem;"
                                    onchange="changeValueShow()">
                                <option value="LINK" selected>链接</option>
                                <option value="GOOD">商品</option>
                                <option value="ARTICLE">文章</option>
                            </select>
                        </div>
                        <div class="col-md-6" id="linkInputBox">
                            <input placeholder="请输入合法的链接" id="linkInput" type="text"/>
                        </div>
                        <div class="col-md-6" id="articleSelectBox">
                            <select id="articleSelect">

                            </select>
                        </div>
                        <div class="col-md-6" id="goodSelectBox">
                            <select id="carouselShopSelect" data-target="goodSelect" onchange="showShopGood(this)">
                                <option value="0">请选择</option>
                            </select>
                            <select id="goodSelect">
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
                <div>
                    <div class="form-group upload-img-box">
                        <label>选择图片</label>
                        <img id="preveiw1" name="preveiw1" class="img_upload_preview" for="upfile" data-target="img1"
                             src=""
                             onclick="$('#upfile').click()"
                             onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                        <input id="upfile" type="file" name="upfile" style="display: none;"
                               onchange="uploadFile('upfile','preveiw1','uri','image')"/>
                        <input name="uri" id="uri" type="hidden">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="button" class="btn btn-primary" onclick="addCarousel()">
                    提交
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shopSelectDialog" tabindex="-1" role="dialog" aria-labelledby="shopLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="shopLabel">
                    选择首页推广店铺
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>认证店铺</label>
                    <select id="shopSelect">
                    </select>
                </div>
                <div class="form-group">
                    <label>投放区域</label>
                    <select name="city" id="shopAreaSelect" class="chosen-select">
                        <option value="default">默认全国</option>
                        <c:forEach items="${citys}" var="city">
                            <option value="${city.addressId}">${city.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="button" class="btn btn-primary" onclick="flagShop('ADD')">
                    提交
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="articleSelectDialog" tabindex="-1" role="dialog" aria-labelledby="articleLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="articleLabel">
                    选择文章
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>选择文章</label>
                    <select id="articleSelector">
                    </select>
                </div>
                <div class="form-group">
                    <label>投放区域</label>
                    <select name="city" id="articleAreaSelector" class="chosen-select">
                        <option value="default">默认全国</option>
                        <c:forEach items="${citys}" var="city">
                            <option value="${city.addressId}">${city.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="button" class="btn btn-primary" onclick="flagArticle('ADD')">
                    提交
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="goodSelectDialog" tabindex="-1" role="dialog" aria-labelledby="hotGoodLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="hotGoodLabel">
                    选择商品
                </h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>商家</label>
                            <select id="goodMerchantSelector" onchange="showShopGood(this)" data-target="addGoodSelect">
                                <option value="0">请选择</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>商品</label>
                            <select id="addGoodSelect">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="form-group">
                    <label>投放区域</label>
                    <select name="city" id="goodAreaSelect" class="chosen-select">
                        <option value="default">默认全国</option>
                        <c:forEach items="${citys}" var="city">
                            <option value="${city.addressId}">${city.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="button" class="btn btn-primary"
                        onclick="addGood($('#addGoodSelect').val()||'','ADD',$('#goodAreaSelect').val())">
                    提交
                </button>
            </div>
        </div>
    </div>

</div>


<script type="text/html" id="carouselTmpl">
    <tr>
        <td>
            <img data-src="\${thumb_url}" onerror="this.src='\${url}'" style="width:12rem;max-height:8rem;"/>
        </td>
        <td>
            <p><code>开始时间</code> \${formatDate(startTime)}</p>
            <p><code>结束时间</code> \${formatDate(endTime)}</p>
        </td>
        <td>
            <span>\${type_desc}</span>
            {{if type == "LINK"}}
            <a href="\${value}">\${value}</a>
            {{else}}
            <div class="target-value" data-value="\${value}" data-type="\${type}">
                <i class="fa fa-spin fa-spinner"></i>Loading
            </div>
            {{/if}}
        </td>
        <td>
            \${city_desc}
        </td>
        <td>
            <button class="btn btn-danger" onclick="delCarousel('\${cid}')">删除</button>
        </td>
    </tr>
</script>

<script type="text/html" id="goodItemTmpl">
    {{each(i,fg) flags}}
    {{if fg.name == target}}
    <span class="good-item" data-id="\${good_id}" title="\${goodName}|点击删除" data-city="\${fg.city}"
          onclick="delGood('\${good_id}','DELETE',this);">
        <nobr><em>\${goodName}</em></nobr>
        <code>
            \${fg.city_desc}
        </code>
    </span>
    {{/if}}
    {{/each}}
</script>

<script type="text/html" id="shopItemTmpl">
    <span class="shop-item" data-id="\${id}" title="\${nickname}|点击删除" data-city="\${flag_city}"
          onclick="flagShop('DELETE','\${id}');">
        <nobr><em>\${nickname}</em></nobr>
        <code>\${city_desc}</code>
</span>
</script>

<script type="text/html" id="articleItemTmpl">
    <span class="article-item" data-id="\${id}" title="\${main_title}|点击删除" data-city="\${area}"
          onclick="flagArticle('DELETE','\${id}','\${area}');">
        <nobr><em>\${main_title}</em></nobr>
        <code>\${area_desc}</code>
</span>
</script>

<script>
    var dt;
    $(function () {
        dt = $("#mainTable").dataTable({
            fnDrawCallback: function () {
                $("img[data-src]").each(function () {
                    $(this).attr("src", $(this).attr("data-src"));
                })
                loadGoodInfo();
            }
        });
        loadFlagGood("NEW_GOOD");
        loadFlagGood("HOT_GOOD");
        loadShop();
        loadCarouselList();
        loadGoodChosen();
        loadFlagShop();
        loadArticle();
        loadInfoList();
        $("#city,#goodAreaSelect,#shopAreaSelect,select[name='filterCity']").chosen({
            width: 200,
            height: 33,
            "no_results_text": "未找到对应的区域"
        });
        filterContentByCity();
    })

    function addCarousel() {
        var data = {
            uri: $("#uri").val() || "",
            startTime: $("#dateRange").val() ? $("#dateRange").val().split(" to ")[0] : "",
            endTime: $("#dateRange").val() ? $("#dateRange").val().split(" to ")[1] : "",
            city: $("#city").val() || "",
            type: $("#type").val() || ""
        };

        if (data["type"] == "LINK") {
            data["typeValue"] = $("#linkInput").val()
        } else if (data["type"] == "GOOD") {
            data["typeValue"] = $("#goodSelect").val()
        } else if (data["type"] == "ARTICLE") {
            data["typeValue"] = $("#articleSelect").val()
        }

        if (isEmpty(data["uri"])) {
            bootbox.alert("轮播图片不能为空!");
            return -1;
        }
        if (isEmpty(data["startTime"]) || isEmpty(data["endTime"])) {
            bootbox.alert("请选择时间区间!");
            return -1;
        }

        if (isEmpty(data["city"])) {
            bootbox.alert("请选择投放城市!");
            return -1;
        }
        var scope = "PORTAL";
        asyncAjax({
            url: ctx + "/portal/config/carousel/" + scope + "/add?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $("#addDialog").modal("hide");
                    var htmlArray = [];
                    var tds = $("#carouselTmpl").tmpl(res["data"]).find("td");
                    $.each(tds, function (idx, el) {
                        htmlArray.push($(el).html());
                    })
                    dt.fnAddData([htmlArray]);
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function loadCarouselList() {
        var data = {};
        asyncAjax({
            url: ctx + "/portal/config/carousel/list/PORTAL?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            beforeSend: function (req) {
                dt.fnClearTable();
            },
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    var dataArray = [];
                    $.each(res["data"], function (idx, carousel) {
                        var htmlArray = [];
                        var tds = $("#carouselTmpl").tmpl(carousel).find("td");
                        $.each(tds, function (idx, el) {
                            htmlArray.push($(el).html());
                        })
                        dataArray.push(htmlArray);
                    })

                    dt.fnAddData(dataArray);

                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function delCarousel(cid) {
        bootbox.confirm({
            title: '提示',
            message: '确定删除此轮播图么？',
            buttons: {
                confirm: {
                    label: "确定"
                },
                cancel: {
                    label: '取消'
                }
            },
            callback: function (res) {
                if (res == true) {
                    var data = {cid: cid};
                    asyncAjax({
                        url: ctx + "/portal/config/carousel/del?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                        success: function (res) {
                            console.log("res:%o", res);
                            if (res["result"] == "0") {
                                loadCarouselList();
                            } else {
                                bootbox.alert(res["msg"]);
                            }
                        }
                    })
                }
            }
        })

    }

    function changeValueShow() {
        var type = $("#type").val();
        switch (type) {
            case "LINK":
                $("#goodSelectBox,#articleSelectBox").hide("slow", function () {
                    $("#linkInputBox").show("slow");
                });
                break;
            case "GOOD":
                $("#linkInputBox,#articleSelectBox").hide("slow", function () {
                    $("#goodSelectBox").show("slow");
                });
                break;

            case "ARTICLE":
                $("#goodSelectBox,#linkInputBox").hide("slow", function () {
                    $("#articleSelectBox").show("slow");
                });
                break;

        }
    }

    var GOOD_CACHE = {};
    function loadGoodChosen() {
        var data = {
            status: 1,
            pageNum:1,
            pageSize:50000
        };
        asyncAjax({
            url: ctx + "/goodMgr/good/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                $("#goodSelect,#addGoodSelect").html("");
                if (res["result"] == "0") {
                    $.each(res["data"]["goods"], function (id, good) {
                        GOOD_CACHE[good["good_id"]] = good;
                        if (!isEmpty(SHOP_GOOD_CACHE[good["wx_user_id"]])) {
                            SHOP_GOOD_CACHE[good["wx_user_id"]].push(good);
                        } else {
                            SHOP_GOOD_CACHE[good["wx_user_id"]] = [good];
                        }
//                        $("#goodSelect").append('<option value=\"' + good["good_id"] + '\">' + good['goodName'] + '</option>')
                    })
//                    $("#goodSelect").chosen({width: 240})
                    console.log("good cache:%o",SHOP_GOOD_CACHE);
                    $("#addGoodSelect").chosen({width: 380});
                    loadGoodInfo();
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function loadGoodInfo() {
        $(".target-value[data-type=\"GOOD\"]").each(function (idx, el) {
            var good = GOOD_CACHE[$(this).attr("data-value")];

            $(this).html((good ? good["goodName"] : "<code>关联商品已经失效</code>"));
        })
    }

    var CURRENT_TARGET_GOOD_LIST;
    function showGoodSelect(type) {
        CURRENT_TARGET_GOOD_LIST = type;
        $("#goodSelectDialog").modal("show");
    }

    function delGood(goodId, action, el) {
        var flag = $(el).parents(".card").attr("data-id");
        var city = $(el).attr("data-city");
        CURRENT_TARGET_GOOD_LIST = flag;
        bootbox.confirm({
            title: "移除商品",
            message: "确认将商品移除么?",
            buttons: {
                confirm: {label: "确定"},
                cancel: {label: "取消"}
            },
            callback: function (res) {
                if (res == true) {
                    addGood(goodId, action, city);
                }
            }
        })
    }

    function addGood(goodId, action, city) {
        var data = {goodId: goodId, action: action, goodFlag: CURRENT_TARGET_GOOD_LIST, city: city};
        if (isEmpty(data["goodId"])) {
            bootbox.alert("请选择商品!");
            return -1;
        }

        asyncAjax({
            url: ctx + "/portal/config/mark/good/?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("操作成功");
                    $("#goodSelectDialog").modal('hide');
                    loadFlagGood();
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }
    var SHOP_GOOD_CACHE = {};
    function loadFlagGood(flag) {
        var data = {};
        flag = isEmpty(flag) ? CURRENT_TARGET_GOOD_LIST : flag;
        asyncAjax({
            url: ctx + "/portal/good/flag/list/" + flag + "?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("load good res:%o", res);
                if (res["result"] == "0") {
                    if (isEmpty(res["data"]) || res["data"].length <= 0) {

                        $("div[data-id=\"" + flag + "\"]").find(".card-content").html("<label>尚未配置商品</label>");
                        return -1;
                    } else {
                        $("div[data-id=\"" + flag + "\"]").find(".card-content").html("");
                        $.each(res["data"], function (idx, good) {

                            good["target"] = flag;
                        })
                        $("#goodItemTmpl").tmpl(res["data"]).appendTo($("div[data-id=\"" + flag + "\"]").find(".card-content"));
                    }

                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function loadShop() {
        var data = {reviewStatus: 2};
        asyncAjax({
            url: ctx + "/user/mgr/list/3?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $.each(res["data"], function (idx, shopUser) {
                        $("#shopSelect,#goodMerchantSelector,#carouselShopSelect").append("<option value='" + shopUser.id + "'>" + shopUser.nickname + "</option>");
                    })
                    $("#shopSelect,#goodMerchantSelector,#carouselShopSelect").chosen({width: 380});
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function flagShop(action, shopId) {
        shopId = shopId || $("#shopSelect").val();

        if ("DELETE" == action) {
            bootbox.confirm({
                title: "移除商铺",
                message: "确定将此商铺从首页推广列表中移除么？",
                buttons: {
                    confirm: {
                        label: "确定"
                    },
                    cancel: {
                        label: "取消"
                    }
                },
                callback: function (re) {
                    if (re == true) {
                        doFlag(shopId, action);
                    }
                }
            })
        } else {
            doFlag(shopId, action);
        }

    }

    function doFlag(shopId, action) {
        var data = {shopId: shopId, action: action, city: $("#shopAreaSelect").val()};
        asyncAjax({
            url: ctx + "/portal/config/shop/flag?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    loadFlagShop();
                    $("#shopSelectDialog").modal("hide");
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }

    function loadFlagShop() {
        var data = {};
        asyncAjax({
            url: ctx + "/portal/shop/flag/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $("#flagShopBox .card-content").html("");
                    if (!isEmpty(res["data"])) {
                        $("#shopItemTmpl").tmpl(res["data"]).appendTo($("#flagShopBox .card-content"));

                    } else {
                        $("#flagShopBox .card-content").html("<label>尚未配置首页推广商铺</label>");
                    }
                } else {
                    bootbox.alert(res["msg"]);

                }
            }
        })
    }

    function filterContentByCity() {
        $("select[name=\"filterCity\"]").on('change', function (e, param) {
                var value = $(this).val();
                if (isEmpty(value)) {
                    $(this).parents(".card").find(".good-item,.shop-item").show();
                    return -1;
                }
                $(this).parents(".card").find(".good-item,.shop-item").css("display", "none");
                $(this).parents(".card").find(".good-item[data-city=\"" + value + "\"],.shop-item[data-city=\"" + value + "\"]").show("fast");
                console.log("filter :%o ,target find:%o", value, $(this).parents(".card").find(".card-content .good-item[data-city=\"" + value + "\"],.shop-item[data-city=\"" + value + "\"]"));
            }
        )
    }

    function loadArticle() {
        var data = {
            type: 1,
            uuid: 0
        };
        asyncAjax({
            url: ctx + "/article/getArticleList?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    if (!isEmpty(res["data"])) {
                        $.each(res["data"], function (idx, item) {
                            $("#articleSelector").append("<option value=\"" + item["id"] + "\">" + item["main_title"] + "</option>");
                        })

                        $("#articleSelect").chosen({width: 240});
                    } else {
                        bootbox.alert("没有文章信息，如果需要配置资讯请先配置文章内容;");
                    }
                } else {

                }
            }
        })
    }


    function flagArticle(action, id, city) {
        var data = {};
        if (action == "DELETE") {
            if (isEmpty(id)) {
                bootbox.alert("请选择要移除的文章");
                return;
            }
            data["articleId"] = id;
            data["area"] = city;
        } else {
            data["articleId"] = $("#articleSelector").val();
            data["area"] = $("#articleAreaSelector").val();
        }
        if (isEmpty(data["articleId"])) {
            bootbox.alert("请选择文章！");
            return;
        }
        if (isEmpty(data["articleId"])) {
            bootbox.alert("请选择区域！");
            return;
        }
        asyncAjax({
            url: ctx + "/portal/config/info/" + action + "?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    loadInfoList();
                    $("#articleSelectDialog").modal("hide");
                } else {
                    bootbox.alert(res.msg);
                }
            }
        })
    }

    function loadInfoList() {
        var data = {};
        asyncAjax({
            url: ctx + "/portal/info/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    if (!isEmpty(res["data"])) {
                        $("#infoBox").html("");
                        $("#articleItemTmpl").tmpl(res["data"]).appendTo($("#infoBox"))
                    } else {
                        $("#infoBox").html("尚未配置文章信息!");
                    }
                } else {
                    bootbox.alert(res.msg);
                }
            }
        })
    }
    function showShopGood(el) {
        if ($(el).val() == "0") {
            return -1;
        }
        var target = $(el).attr("data-target");
        $("#" + target).html("<option value=\"\">正在载入</option>");
        $("#"+target).trigger("chosen:updated");

        var data ={
            wx_user_id:$(el).val() || "",
            pageSize:10000,
            pageNum:1,
            status:"1"
        };
        asyncAjax({
            url: ctx + "/goodMgr/good/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    var goods = res["data"]["goods"]
                    if (isEmpty(goods)) {
                        bootbox.alert("该商店没有商品!");
                        return;
                    } else {
                        $("#" + target).html("");
                        $.each(goods, function (idx, good) {
                            $("#" + target).append('<option value=\"' + good["good_id"] + '\">' + good['goodName'] + '</option>')
                        })
                        $("#"+target).trigger("chosen:updated");
                        $("#" + target).chosen();
                    }
                } else {
                    bootbox.alert(res.msg);
                }
            }
        })


    }
</script>
</body>
</html>
