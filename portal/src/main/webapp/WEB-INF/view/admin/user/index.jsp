<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/5/12
  Time: 下午10:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<html>
<head>
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <script type="text/javascript" src="${ctx}/resources/pagination/script/jquery.pagination.js"></script>
    <link href="${ctx}/resources/pagination/style/pagination.css" rel="stylesheet" type="text/css">
    <link href="${ctx}/resources/pagination/style/common.css" rel="stylesheet" type="text/css">


    <style>
        .page-content {
            padding: 1rem 2rem;
            overflow: hidden;
            position: relative;
        }

        .info-box {
            min-width: 20rem;
        }

        .info-box img {
            display: inline-block;
            width: 6rem;
            height: 6rem;
            vertical-align: top;
            margin-left: 2rem;
        }

        .info-box .name-box {
            display: inline-block;
            vertical-align: top;
            margin-left: 1rem;
        }

        .name-box p {
            line-height: 3rem;
            margin: 0;
            padding: 0;
        }

        .operator-box {
            min-width: 20rem;
        }

        .operator-box .btn {
            margin: 0 0.8rem;
        }

        .auth-dialog .modal-header {
            padding: 0;
        }

        .auth-dialog .auto-info-box {
            height: 10rem;
            display: block;
            overflow-y: auto;
        }

        .auth-dialog .form-group img {
            display: inline-block;
            width: 8rem !important;
            height: auto;
        }

        .auth-dialog .form-group img:first-child {
            margin-left: 4rem;
        }

        .auth-dialog .modal-content {
            height: 30rem;
        }

        .auth-info-box {
            height: 20rem;
            max-height: 20rem;
            overflow: auto;
            padding: 3rem 2rem;
        }
    </style>
</head>
<body>


<div class="page-content">
    <div class="col-md-12">
        <div class="card">
            <div class="card-action">
                条件查询
            </div>
            <div class="card-content">
                <div class="col-md-12">
                    <div class="form-group normal col-md-3">
                        <label>关键字</label>
                        <input type="text" value="" name="keyword" id="keyword" placeholder="输入姓名或者手机号码过滤"/>
                    </div>
                    <div class="form-group normal col-md-4">
                        <label>注册时间</label>
                        <input type="text" value="" name="timeRange" id="timeRange" placeholder="点击选择"
                               style="width:24rem;"/>
                    </div>
                    <shiro:hasRole name="all">
                        <c:choose>
                            <c:when test="${type == 1 || type == 2}">
                                <div class="form-group normal col-md-3">
                                    <label>关联商户</label>
                                    <select style="display: inline-block;float: none;width:12rem;" id="shopId"
                                            name="shopId">
                                        <option value="" selected>全部</option>
                                        <c:forEach items="${shopUsers}" var="sp">
                                            <option value="${sp.id}">${sp.nickname}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <c:if test="${type == 2}">
                                    <div class="form-group col-md-3">
                                        <label>审核状态</label>
                                        <select name="reviewStatus" id="reviewStatus" style="width:10rem;">
                                            <option value="" selected>全部</option>
                                            <option value="0">未提交资料</option>
                                            <option value="1">待审核</option>
                                            <option value="2">审核通过</option>
                                            <option value="3">审核不通过</option>
                                        </select>
                                    </div>
                                </c:if>
                            </c:when>
                            <c:when test="${ type == 3 || type == 4}">
                                <div class="form-group col-md-3">
                                    <label>审核状态</label>
                                    <select name="reviewStatus" id="reviewStatus" style="width:10rem;">
                                        <option value="" selected>全部</option>
                                        <option value="0">未提交资料</option>
                                        <option value="1">待审核</option>
                                        <option value="2">审核通过</option>
                                        <option value="3">审核不通过</option>
                                    </select>
                                </div>
                            </c:when>
                        </c:choose>
                    </shiro:hasRole>
                    <button class="btn btn-primary" onclick="reSearch();">
                        <i class="fa fa-search"></i>查询
                    </button>
                </div>
                <div class="clear"></div>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-content">
                <div class="col-md-12">
                    <table id="mainTable" cellpadding="0" cellspacing="0" border="0" class="auto-table">
                        <thead>
                        <tr>
                            <td>用户信息</td>
                            <td>手机号码</td>
                            <td>注册时间</td>
                            <td>注册类型</td>
                            <td>用户类型</td>
                            <shiro:hasRole name="all">
                                <c:choose>
                                    <c:when test="${type == 1 || type == 2}">
                                        <td>关联商户</td>
                                        <c:if test="${type == 2}">
                                            <td>审核状态</td>
                                            <td>操作</td>
                                        </c:if>
                                    </c:when>
                                    <c:when test="${type == 3 || type == 4}">
                                        <td>审核状态</td>
                                        <td>操作</td>
                                    </c:when>
                                </c:choose>
                            </shiro:hasRole>
                            <%--<td>最后访问时间</td>--%>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <input type="hidden" name="pageNum" id="pageNum" value="1"/>
                    <input type="hidden" name="pageSize" id="pageSize" value="10"/>
                    <input type="hidden" name="total" id="total" value=""/>

                    <div class="pagination pagination-right pagination-small" id="pagination">

                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

</div>
<div aria-hidden="true" class="modal auth-dialog" style="background: transparent; box-shadow: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>认证资料</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                        onclick="$('.auth-dialog').modal('hide')">&times;
                </button>
            </div>
            <div class="auth-info-box">

            </div>
            <%--<div class="modal-footer">--%>
            <%--<div class="col-md-12">--%>
            <%--<div class="col-md-offset-3 col-md-9">--%>
            <%--<a href="javascript:$('.auth-dialog').modal('hide');" class="btn btn-success submitBtn" style="display: none;">--%>
            <%--确定 <i class="fa fa-arrow-circle-right"></i>--%>
            <%--</a>--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--</div>--%>
        </div>

    </div>
</div>
<script type="text/html" id="dataTmpl">
    <tr data-id="\${id}">
        <td>
            <div class="info-box">
                <img src="\${headimgurl}"/>
                <div class="name-box">
                    <p><strong>\${nickname}</strong></p>
                    <p>\${remark}</p>
                </div>
            </div>
        </td>
        <td>
            <p class="phone-box">\${telephone}</p>
        </td>
        <td>
            <p class="register-time">
                \${formatDate(firstTime)}
            </p>
        </td>
        <td>
            <p class="register-type">
                {{if registerType}}
                {{if registerType == 0}}
                公众号用户
                {{else registerType == 1}}
                安卓用户
                {{else registerType == 2}}
                iOS用户
                {{/if}}
                {{/if}}
            </p>
        </td>
        <td>
            {{if userType == 1}}
            普通用户
            {{else userType == 2}}
            维修技师
            {{else userType == 3}}
            个人商户
            {{else userType == 4}}
            企业商户
            {{/if}}
        </td>
        <shiro:hasRole name="all">
            <c:choose>
                <c:when test="${type == 1 || type ==2}">
                    <td>
                        {{if firstShopId}}
                        <p class="shop-user" data-id="\${firstShopId}"><i class="fa fa-spinner fa-spin"></i>载入中...</p>
                        {{else}}
                        --
                        {{/if}}
                    </td>
                    <c:if test="${type == 2}">
                        <td>
                            {{if reviewStatus == 0}}
                            <p class="review-status" style="color:#666;">未提交资料</p>
                            {{else reviewStatus == 1}}
                            <p class="review-status" style="color:orange;">待审核</p>
                            <a href="javascript:showAuthInfo(\${id});">查看资料</a>
                            {{else reviewStatus == 2}}
                            <p class="review-status" style="color:green;">审核通过</p>
                            <a href="javascript:showAuthInfo(\${id});">查看资料</a>
                            {{else reviewStatus == 3}}
                            <p class="review-status" style="color:#a33;">审核不通过</p>
                            <a href="javascript:showAuthInfo(\${id});">查看资料</a>
                            <p><strong>备注：</strong>\${reviewNotes}</p>
                            {{else}}
                            <p class="review-status" style="color:#a33;">未知状态</p>
                            {{/if}}
                        </td>
                    </c:if>
                </c:when>
                <c:when test="${type == 2 || type == 3 || type == 4}">
                    <td>
                        {{if reviewStatus == 0}}
                        <p class="review-status" style="color:#666;">未提交资料</p>
                        {{else reviewStatus == 1}}
                        <p class="review-status" style="color:orange;">待审核</p>
                        <a href="javascript:showAuthInfo(\${id});">查看资料</a>
                        {{else reviewStatus == 2}}
                        <p class="review-status" style="color:green;">审核通过</p>
                        <a href="javascript:showAuthInfo(\${id});">查看资料</a>
                        <p><strong>备注：</strong>\${reviewNotes}</p>
                        {{else reviewStatus == 3}}
                        <p class="review-status" style="color:#a33;">审核不通过</p>
                        <a href="javascript:showAuthInfo(\${id});">查看资料</a>
                        <p><strong>备注：</strong>\${reviewNotes}</p>
                        {{else}}
                        <p class="review-status" style="color:#a33;">未知状态</p>
                        {{/if}}
                    </td>
                </c:when>
            </c:choose>

            <c:if test="${type == 2 || type == 3 || type == 4}">
                <td>
                    {{if reviewStatus == 1}}
                    <div class="operator-box">
                        <button class="btn btn-primary" onclick="review(\${id},true,${type})">审核通过</button>
                        <button class="btn btn-danger" onclick="review(\${id},false,${type})">审核不通过</button>
                    </div>
                    {{else reviewStatus == 2}}
                    <div class="operator-box">
                        <button class="btn btn-danger" onclick="review(\${id},false,${type})">审核不通过</button>
                    </div>
                    {{else reviewStatus == 0}}
                    <div>
                        --
                    </div>
                    {{else reviewStatus == 3}}
                    <div class="operator-box">
                        <button class="btn btn-warning" onclick="review(\${id},true,${type})">重新审核通过</button>
                    </div>
                    {{/if}}
                </td>
            </c:if>
        </shiro:hasRole>
        <%--<td>--%>
        <%------%>
        <%--</td>--%>
    </tr>


</script>
<script type="text/html" id="authInfoTmpl">
    {{if real_name}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>真实姓名</label>
        <span>\${real_name}</span>
    </div>
    {{/if}}
    {{if company_name}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>公司名称</label>
        <span>\${company_name}</span>
    </div>
    {{/if}}
    {{if charge_person_name}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>公司法人</label>
        <span>\${charge_person_name}</span>
    </div>
    {{/if}}
    {{if address}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>公司地址</label>
        <span>\${address}</span>
    </div>
    {{/if}}
    {{if telephone}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>公司电话</label>
        <span>\${telephone}</span>
    </div>
    {{/if}}
    {{if cret_card}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>银行卡号</label>
        <span>\${cret_card}</span>
    </div>
    {{/if}}
    {{if phone_number}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>手机号码</label>
        <span>\${phone_number}</span>
    </div>
    {{/if}}
    {{if desc}}
    <div class="form-group">
        <label>主营业务</label>
        <span>\${desc}</span>
    </div>
    <hr>
    {{/if}}
    {{if business_license_pic}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>营业执照</label>
        <div><img class="decode-img" data-src="\${business_license_pic}" title="点击查看大图"
                  style="cursor: pointer;"/></div>
    </div>
    {{/if}}

    {{if idcard_pic}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>法人身份证</label>
        <span><img class="decode-img" data-src="\${idcard_pic}" title="点击查看大图"
                   style="cursor: pointer;"/></span>
    </div>
    {{/if}}


    {{if pics}}
    <hr>
    <div class="form-group">
        <label>场景照片</label>
        <div>
            {{each(i,img) pics}}
            <img class="decode-img" data-src="\${img}" class="pic-item" title="点击查看大图" style="cursor: pointer;"/>
            {{/each}}
        </div>
    </div>
    {{/if}}


    {{if idcard}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>身份证号</label>
        <span>\${idcard}</span>
    </div>
    {{/if}}

    {{if employ_expes}}
    <div class="form-group">
        <label>工作经验</label>
        <span>\${employ_expes}</span>
    </div>
    {{/if}}
    {{if skills}}
    <div class="form-group">
        <label>维修技能</label>
        <span>\${skills}</span>
    </div>
    {{/if}}

    {{if idcard_img1}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>身份证正面</label>
        <div><img data-src="\${idcard_img1}" class="idcard-img decode-img" title="点击查看大图" style="cursor: pointer;"/>
        </div>
    </div>
    {{/if}}
    {{if idcard_img2}}
    <div class="form-group" style="display: inline-block;width:40%;vertical-align: top">
        <label>身份证反面</label>
        <div><img data-src="\${idcard_img2}" class="idcard-img decode-img" title="点击查看大图" style="cursor: pointer;"/>
        </div>
    </div>
    {{/if}}
</script>
<script>
    $(function () {
        loadData();
    })

    function pageLoad() {
        var pageCount = Math.ceil((parseInt(($('#total').val() || 0)) / parseInt($("#pageSize").val() || 10)));
        $("#pagination").pagination({
            pageCount: pageCount,
            pageSize: parseInt($("#pageSize").val() || 10),
            totalCount: parseInt($("#total").val()),
            current: parseInt($("#pageNum").val()),
            jump: true,
            coping: true,
            homePage: '首页',
            endPage: '末页',
            prevContent: '上页',
            nextContent: '下页',
            callback: function (that) {
                console.log("that:%o", that);
                $("#pageNum").val(that.getCurrent());
                loadData();
                that.setCurrentPage()
            }
        })
    }

    function loadData() {
        var data = {
            pageNum: $("#pageNum").val(),
            pageSize: $("#pageSize").val(),
            startTime: $("#timeRange").val().split(" to ")[0],
            endTime: $("#timeRange").val().split(" to ")[1]
        };
        if (!isEmpty($("#keyword").val())) {
            data["keyword"] = encodeURI($("#keyword").val());
        }

        if ($("#reviewStatus").length >= 1) {
            var reviewStatus = $("#reviewStatus").val();
            data["reviewStatus"] = reviewStatus;
        }

        if ($("#shopId").length >= 1) {
            data["shopId"] = $("#shopId").val();
        }

        asyncAjax({
            url: ctx + "/user/mgr/list/${type}?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            type: "post",
            beforeSend: function (req) {
                $("#mainTable tbody").html("<tr><td colspan=\"7\"><div class=\"alert alert-warning\">正在请求数据...</div></td></tr>");
            },
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    if (res["data"].length <= 0) {
                        $("#mainTable tbody").html("<tr><td colspan=\"7\"><div class=\"alert alert-warning\">没有找到符合条件...</div></td></tr>");
                    } else {
                        $("#total").val(res["total"] || 0);
                        $("#mainTable tbody").html("");
                        $("#dataTmpl").tmpl(res["data"]).appendTo("#mainTable tbody");
                        $(".shop-user").each(function () {
                            getShopUserInfo($(this).attr("data-id"), $(this));
                        })
                        pageLoad();
                    }
                } else {

                }
            }
        })
    }


    function getShopUserInfo(id, el) {
        var data = {};
        asyncAjax({
            url: ctx + "/user/mgr/user/" + id + "?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            data: data,
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    el.html(res["data"]["nickname"]);
                } else {
                    alert("未找到商户信息!");
                }
            }
        })
    }


    function showAuthInfo(id) {
        var data = {userId: id};
        asyncAjax({
            url: ctx + "/user/mgr/authinfo?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            data: data,
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    $(".auth-info-box").html("");
                    $("#authInfoTmpl").tmpl(res["data"]).appendTo($(".auth-info-box"));
                    $(".decode-img").each(function () {
                        $(this).attr("src", decodeURIComponent(decodeURIComponent($(this).attr("data-src"))));
                        console.log("data-src:%o", decodeURIComponent(decodeURIComponent($(this).attr("data-src"))))
                    })
                    $(".auth-dialog img").unbind().bind("click", function () {
                        bootbox.alert("<img src=\"" + $(this).attr("src") + "\" style=\"max-height:480px;\"/>")
                    })

                    $('.auth-dialog').modal('show');
                } else {
                    bootbox.alert("发生错误:" + res["msg"]);
                }
            }
        })
    }

    function review(id, status, type) {
        if (status == true) {
            bootbox.confirm({
                message: "确定通过该用户的认证申请？",
                callback: function (result) {
                    console.log("success:%o", result);
                    if (result == true) {
                        if (type == 3 || type == 4) {
                            bootbox.confirm({
                                message: "请选择商户类型",
                                buttons: {
                                    confirm: {
                                        label: '经销商',
                                        className: 'btn-success',
                                    },
                                    cancel: {
                                        label: '生产商',
                                        className: 'btn-warning'
                                    }
                                },
                                callback: function (isMerchant) {
                                    console.log("merchant type :%o", isMerchant);

                                    var data = {
                                        reviewStatus: 2,
                                        reviewNotes: "审核通过",
                                        userId: id
                                    };
                                    if (isMerchant == true) {
                                        data["reviewNotes"] = "经销商";
                                    } else {
                                        data["reviewNotes"] = "生产商";
                                    }
                                    asyncAjax({
                                        url: ctx + "/user/mgr/authinfo/review?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                                        success: function (res) {
                                            console.log("res:%o", res);
                                            if (res["result"] == "0") {
                                                $("tr[data-id=\"" + id + "\"]").html($("#dataTmpl").tmpl(res["data"]).html());
                                                $(".shop-user").each(function () {
                                                    getShopUserInfo($(this).attr("data-id"), $(this));
                                                })
                                                bootbox.alert("操作成功");
                                            } else {
                                                bootbox.alert("发生错误:" + res["msg"]);
                                            }
                                        }
                                    })
                                }
                            });
                        } else {
                            var data = {
                                reviewStatus: 2,
                                reviewNotes: "审核通过",
                                userId: id
                            };
                            asyncAjax({
                                url: ctx + "/user/mgr/authinfo/review?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                                success: function (res) {
                                    console.log("res:%o", res);
                                    if (res["result"] == "0") {
                                        $("tr[data-id=\"" + id + "\"]").html($("#dataTmpl").tmpl(res["data"]).html());
                                        $(".shop-user").each(function () {
                                            getShopUserInfo($(this).attr("data-id"), $(this));
                                        })
                                        bootbox.alert("操作成功");
                                    } else {
                                        bootbox.alert("发生错误:" + res["msg"]);
                                    }
                                }
                            })
                        }


                    }
                },
                buttons: {
                    confirm: {
                        label: '是',
                        className: 'btn-success',
                    },
                    cancel: {
                        label: '否',
                        className: 'btn-danger'
                    }
                },
            })
        } else {
            bootbox.confirm({
                message: "确定驳回该用户的认证申请？",
                callback: function (confirm) {
                    if (confirm == true) {
                        bootbox.prompt({
                            title: "请填写驳回原因",
                            callback: function (reviewNotes) {
                                if (reviewNotes == null) {
                                    bootbox.alert("取消操作成功！");
                                    return -1;
                                }
                                var data = {
                                    reviewStatus: 3,
                                    reviewNotes: reviewNotes || "无驳回原因，请联系客服人员咨询!",
                                    userId: id
                                };
                                asyncAjax({
                                    url: ctx + "/user/mgr/authinfo/review?" + $.param(data).replace(/(%5D|%5B)/g, ""),
                                    success: function (res) {

                                        if (res["result"] == "0") {
                                            $("tr[data-id=\"" + id + "\"]").html($("#dataTmpl").tmpl(res["data"]).html());
                                            $(".shop-user").each(function () {
                                                getShopUserInfo($(this).attr("data-id"), $(this));
                                            })
                                            bootbox.alert("操作成功");

                                        } else {
                                            bootbox.alert("发生错误:" + res["msg"]);
                                        }
                                    }
                                })
                            },
                            buttons: {
                                confirm: {
                                    label: '确定',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-danger"
                                }
                            }
                        })
                    }
                },
                buttons: {
                    confirm: {
                        label: '是',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '否',
                        className: 'btn-danger'
                    }
                },
            })
        }
    }

</script>
</body>
</html>
