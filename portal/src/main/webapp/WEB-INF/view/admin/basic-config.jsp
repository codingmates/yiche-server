<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/25
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/header.jsp"></jsp:include>
<body style="background: #efefef;">
<div class="col-lg-6">
    <div class="card">
        <div class="card-action">
            基础配置
        </div>
        <div class="card-content">
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" id="appid" name="appid" class="validate" value="${pn.organizationName}">
                    <label for="appid">系统标题</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" id="domain" name="domain" class="validate" value="">
                    <label for="domain">系统域名</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" id="domain_no" name="domain_no" class="validate" value="">
                    <label for="domain_no">域名备案号</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" id="telephone_no" name="telephone_no" class="validate" value="">
                    <label for="telephone_no">联系电话</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" id="address" name="address" class="validate" value="">
                    <label for="address">联系地址</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" id="description" name="description" class="validate">
                    <label for="description">其他描述</label>
                </div>
            </div>
            <div class="row right-align">
                <button class="btn btn-primary" id="basicConfigUpdateBtn"><i class="material-icons dp48">保存</i>
                </button>
            </div>
        </div>
    </div>
</div>
</body>