<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 17/5/31
  Time: 下午9:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>店招配置</title>
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>
    <jsp:include page="../../common/dt.jsp"></jsp:include>
    <jsp:include page="../../common/baidu-map.jsp"></jsp:include>
    <style>
        .map-container {
            display: block;
            width: 100%;
            height: 20rem;
        }
    </style>

</head>
<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-action">
            <label>当前店招图片</label>
        </div>
        <div class="card-content">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <img id="preveiw4" name="preveiw1" style="max-width:20rem;" for="upfile"
                             data-target="img1" src="${portal_img}"
                             onclick="$('#upfile4').click()"
                             onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                        <input id="upfile4" type="file" name="upfile" style="display: none;"
                               onchange="uploadFile('upfile4','preveiw4','portalImgUrl','image')"/>
                        <input name="portalImgUrl" id="portalImgUrl" type="hidden">
                        <input type="hidden" name="userId" value="${id}" id="userId"/>
                        <button class="btn btn-success" onclick="updatePortalImg()">保存</button>
                    </div>

                    <div>
                        <p><i class="fa fa-info-o"></i> 提示：图片尺寸请尽量裁剪为640*320。</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>当前地址</label>
                        <p>${authInfo.address}</p>
                    </div>
                    <div class="form-group">
                        <label>当前经纬度</label>
                        <p>${authInfo.loc}</p>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-action">
            <label>修改店铺地址</label>
        </div>
        <div class="card-content">
            <div class="col-md-6">
                <div class="form-group">
                    <label>店铺地址</label>
                    <input type="text" value="" id="address" name="address" placeholder="请点击地图选择或者手动输入"/>
                    <input type="hidden" id="province" name="province"/>
                    <input type="hidden" id="city" name="city"/>
                    <input type="hidden" id="district" name="district"/>
                    <input type="hidden" id="street" name="street"/>
                    <input type="hidden" id="streetNumber" name="streetNumber"/>
                </div>
                <div class="form-group">
                    <label>经纬度</label>
                    <input type="text" disabled id="locLabel" name="locLabel" value="" placeholder="请点击地图选择"/>
                    <input type="hidden" value="" id="loc" name="loc" placeholder="请点击地图选择"/>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" onclick="updateAddress()">保存</button>
                </div>
            </div>
            <div class="col-md-6">
                <div id="map-container" class="map-container">

                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>
<script>
    $(function () {
        initMap()
    })

    function updatePortalImg() {
        var data = {
            userId: $("#userId").val(),
            portalImgUrl: $("#portalImgUrl").val()
        };
        if (isEmpty(data["userId"])) {
            bootbox.alert("用户信息丢失");
            return;
        }
        if (isEmpty(data["portalImgUrl"])) {
            bootbox.alert("请上传店招图片!");
            return;
        }
        asyncAjax({
            url: ctx + "/per-shop/updatePortalImg?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("更新成功!");
                } else {
                    bootbox.alert(res.msg);
                }
            }
        })
    }


    function initMap() {
        $.baidu_map.init('map-container');
        $.baidu_map.click_listener(getLoc);
        $("#address").val(currentCity);
    }


    function getLoc(e) {
        mk.setPosition(e.point);

        if (!isEmpty(e.point.lat) && !isEmpty(e.point.lng)) {
            geoCoder.getLocation(e.point, function (res) {
                console.log("get address:%o", res);
                if (!isEmpty(res)) {
                    if (!isEmpty(res.address)) {
                        $("#address").val(res.address);
                    }
                    if (!isEmpty(res.addressComponents)) {
                        for (var i in res.addressComponents) {
                            $("#" + i).val(res.addressComponents[i]);
                        }
                    }
                    if (!isEmpty(res.point)) {
                        $("#loc,#locLabel").val(res.point.lat + "," + res.point.lng);
                    }
                }
            })
        }
    }


    function updateAddress() {
        var data = {
            province: $("#province").val() || "",
            city: $("#city").val() || "",
            district: $("#district").val() || "",
            street: $("#street").val() || "",
            street_number: $("#streetNumber").val() || "",
            loc: $("#loc").val() || "",
            address: $("#address").val() || ""
        };
        if (isEmpty(data["province"]) || isEmpty(data["city"])) {
            bootbox.alert("未能定位省份、区县地址，无法更新地址信息!");
            return;
        }
        if (isEmpty(data["loc"])) {
            bootbox.alert("未能在地图上找到对应的坐标，无法更新地址信息");
            return;
        }
        console.log("update address:%o", data);
        asyncAjax({
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            url: ctx + "/per-shop/update/merchant/address",
            success: function (res) {
                console.log("res:%o", res);
                if (res["result"] == "0") {
                    bootbox.alert("修改成功!");
                    location.reload();
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        })
    }
</script>
</body>
</html>
