<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/4/13
  Time: 21:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="addDialog" aria-hidden="true" class="modal" style="background: transparent; box-shadow: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="addForm" action="#" class="form-horizontal" novalidate="novalidate">
                <div class="modal-header">
                    <ul class="nav nav-pills nav-justified steps">
                        <li class="active">
                            <a href="#account" data-toggle="tab" class="wiz-step">
                                <span class="step-number">1</span>
                                <span class="step-name"><i class="fa fa-check"></i> 厂商及商品图片配置 </span>
                            </a>
                        </li>

                        <li>
                            <a href="#payment" data-toggle="tab" class="wiz-step active">
                                <span class="step-number">2</span>
                                <span class="step-name"><i class="fa fa-check"></i> 商品信息配置 </span>
                            </a>
                        </li>
                        <li>
                            <a href="#confirm" data-toggle="tab" class="wiz-step">
                                <span class="step-number">3</span>
                                <span class="step-name"><i class="fa fa-check"></i> 信息核对与提交 </span>
                            </a>
                        </li>
                    </ul>
                    <div id="bar" class="progress progress-striped progress-sm active" role="progressbar">
                        <div class="progress-bar progress-bar-warning" style="width: 33.3333%;"></div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height:320px;overflow-y:auto;">
                    <div class="wizard-form">
                        <div class="wizard-content">

                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <a class="close" aria-hidden="true" href="#" data-dismiss="alert">×</a>
                                    表单有不符合要求的输入项，请检验后再提交
                                </div>
                                <div class="alert alert-success display-none">
                                    <a class="close" aria-hidden="true" href="#" data-dismiss="alert">×</a>
                                    表单校验成功
                                </div>
                                <div class="tab-pane active" id="account">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>厂商/供应商名称</label>
                                            <input type="text" value="" name="product" id="product"/>
                                        </div>
                                        <div class="form-group  col-md-6">
                                            <label>出厂日期</label>
                                            <input type="text" value="" name="productTime" id="productTime"/>
                                        </div>
                                        <div class="form-group  col-md-6">
                                            <label>其他描述</label>
                                            <input type="text" value="" name="productRemark" id="productRemark"/>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="form-group size-box">
                                            <label>规格信息(长*宽*高)</label>
                                            <input class="col-md-2" type="number" value="" name="length" id="length"
                                                   placeholder="长/cm"/> *
                                            <input class="col-md-2" type="number" value="" name="width" id="width"
                                                   placeholder="宽/cm"/> *
                                            <input class="col-md-2" type="number" value="" name="height" id="height"
                                                   placeholder="高/cm"/>
                                        </div>
                                    </div>
                                    <hr>
                                    <label class="block">商品图片（最多四张）</label>
                                    <p>提示:商品图片将按照顺序展示在商品页面中，首张图片将作为封面，上传图片请尽量使用正方形图片，分辨率为480*480最佳</p>
                                    <div style="text-align: center;">
                                        <div class="form-group upload-img-box">
                                            <img id="preveiw1" name="preveiw1" class="img_upload_preview" for="upfile"
                                                 data-target="img1" src=""
                                                 onclick="$('#upfile').click()"
                                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                                            <input id="upfile" type="file" name="upfile" style="display: none;"
                                                   onchange="uploadFile('upfile','preveiw1','img1','image')"/>
                                            <input name="img" id="img1" type="hidden">
                                        </div>
                                        <div class="form-group upload-img-box">
                                            <img id="preveiw2" name="preveiw1" class="img_upload_preview" for="upfile"
                                                 data-target="img1" src=""
                                                 onclick="$('#upfile2').click()"
                                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                                            <input id="upfile2" type="file" name="upfile" style="display: none;"
                                                   onchange="uploadFile('upfile2','preveiw2','img2','image')"/>
                                            <input name="img" id="img2" type="hidden">
                                        </div>
                                        <div class="form-group upload-img-box">
                                            <img id="preveiw3" name="preveiw1" class="img_upload_preview" for="upfile"
                                                 data-target="img1" src=""
                                                 onclick="$('#upfile3').click()"
                                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                                            <input id="upfile3" type="file" name="upfile" style="display: none;"
                                                   onchange="uploadFile('upfile3','preveiw3','img3','image')"/>
                                            <input name="img" id="img3" type="hidden">
                                        </div>
                                        <div class="form-group upload-img-box">
                                            <img id="preveiw4" name="preveiw1" class="img_upload_preview" for="upfile"
                                                 data-target="img1" src=""
                                                 onclick="$('#upfile4').click()"
                                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                                            <input id="upfile4" type="file" name="upfile" style="display: none;"
                                                   onchange="uploadFile('upfile4','preveiw4','img4','image')"/>
                                            <input name="img" id="img4" type="hidden">
                                        </div>
                                        <p class="upload_progress_tip" style="text-align: center;"></p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="payment">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="col-md-3">商品名称</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="goodName"
                                                           id="goodName" placeholder="请输入商品名称">
                                                    <span class="error-span"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">商品备注</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="goodRemr"
                                                           id="goodRemr" placeholder="商品备注信息，可以为空">
                                                    <span class="error-span"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">商品库存</label>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" name="amount" id="amount"
                                                           placeholder="商品数量，只能为数字">
                                                    <span class="error-span"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">零售价</label>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" name="source_price"
                                                           id="source_price" placeholder="商品数量，只能为数字">
                                                    <span class="error-span"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">批发价</label>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" name="sale_price"
                                                           id="sale_price" placeholder="商品数量，只能为数字">
                                                    <span class="error-span"></span>
                                                </div>
                                            </div>
                                            <%--<div class="form-group">--%>
                                                <%--<label class="col-md-3">展示优先级</label>--%>
                                                <%--<div class="col-md-9">--%>
                                                    <input type="hidden" name="priority"
                                                           id="priority" placeholder="数值越大，商品显示越靠前" value="0">
                                                    <%--<span class="error-span"></span>--%>
                                                <%--</div>--%>
                                            <%--</div>--%>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-12 good-class-box">
                                                <label>商品类别</label>
                                                <div id="goodClassTree" class="tree">
                                                </div>
                                                <input id="goodClass" name="goodClass" value="" id="goodClass"
                                                       type="hidden"/>
                                            </div>
                                            <div class="col-md-12">
                                                <label>商品类型</label>
                                                <select name="bigType" id="bigType">
                                                    <option value="新品">新品</option>
                                                    <option value="二手">二手</option>
                                                    <option value="配件">配件</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <label>商品物流</label>
                                                <select name="goodExpress" id="goodExpress" multiple="multiple" data-placeholder="选择物流">

                                                </select>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <hr>
                                        <label>图文详情</label>
                                        <script id="container" name="content" type="text/plain"></script>
                                        <script type="text/javascript">
                                            var ue = UE.getEditor('container', {
                                                autoHeight: false,
                                                maxFrameHeight: 350,
                                                initialFrameHeight: 350,
                                                initialFrameWidth: 700,

                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="tab-pane" id="confirm">
                                    <h4 class="form-section">厂商规格+图片</h4>
                                    <div class="well">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">厂商/供应商名称:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="product"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">出厂日期:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="productTime"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">规格信息(长*宽*高):</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="length,width,height"
                                                   data-split="*"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品图片:</label>
                                            <div style="clear:both;"></div>
                                            <br>
                                            <div class="form-control-static" data-display="img1,img2,img3,img4"
                                                 data-type="img">

                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="form-section">商品基础信息</h4>
                                    <div class="well">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品名称:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="goodName"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品备注:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="goodRemr"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品类别:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" id="confirmGoodClass"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品物流:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" id="confirmGoodExpress"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品库存:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="amount"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">零售价:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="source_price"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">批发价:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="sale_price"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品展示优先级:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="priority"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">商品图文详情:</label>
                                            <div class="col-md-12" id="confirmContent">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="javascript:;" class="btn btn-default prevBtn disabled" style="display: none;">
                                <i class="fa fa-arrow-circle-left"></i> 上一步
                            </a>
                            <a href="javascript:;" class="btn btn-primary nextBtn">
                                下一步 <i class="fa fa-arrow-circle-right"></i>
                            </a>
                            <a href="javascript:;" class="btn btn-success submitBtn" style="display: none;">
                                提交 <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
