<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
    <title>关于我们</title>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <jsp:include page="../../common/cloud-head.jsp"></jsp:include>

    <style>
        #aboutContent {
            min-height: 280px;
        }

        .item-config-box {
            width: 100%;
            display: block;
            min-height: 10rem;
            border-top: 1px solid #cccccc;
        }

        .item-config-box:first-child {
            border: none;
        }

        .item-config-box h2 {
            font-size: 1.2rem;
            font-weight: bold;
            color: #666;
            line-height: 2rem;
            display: block;
            /* z-index: 999; */
        }

        .select-item {
            text-align: center;
            margin: 0.4rem auto;
        }

        .select-item p * {
            cursor: pointer;
            vertical-align: top;
            line-height: 1.6rem;
        }

        .select-item p {
            cursor: pointer;
            background: #efefef;
            display: inline-block;
            padding: 0 0.8rem;
            line-height: 1.6rem;
            border-radius: 3px;
            transition: all ease 0.8s;
        }

        .select-item p label {
            color: #333;
            display: block;
        }

        .select-item p:hover {
            background: #ffffff;
            box-shadow: 0 3px 10px #ccc;
        }

        .select-item {
            position: relative;
            width: auto;
        }

        .select-item p i.fa {
            position: absolute;
            top: -1rem;
            right: -.2rem;
            width: 2rem;
            height: 2rem;
            color: #a00;
            background: #ccc;
            display: none;
            z-index: 3;
            border-radius: 50%;
            box-shadow: 0 0 13px #dcdcdc;
            line-height: 2rem;
        }

        .select-item p:hover i.fa {
            display: block;
        }
    </style>
</head>
<body style="background: #efefef;">

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-action">
                        首页展示商品配置
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="item-config-box">
                                <h2>首页广告商品</h2>
                                <div class="rows" id="HOME_AD">
                                    <div class="col-xs-12 col-sm-6 col-md-2 select-item">
                                        <button class="btn btn-primary" onclick="loadGoods('HOME_AD');"><i class="fa fa-plus" style="margin: 0 1rem;"></i>添加商品</button>
                                    </div>
                                    <c:forEach items="${adGoods}" var="gd">
                                        <div class="col-xs-12 col-sm-6 col-md-2 select-item" data-id="${gd.good_id}">
                                            <p>
                                                <label>${gd.goodName}</label>
                                                <code>￥${gd.sale_price}</code>
                                                <i class="fa fa-trash-o" title="删除" data-id="${gd.good_id}"></i>
                                            </p>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="item-config-box">
                                <h2>新品推荐商品</h2>
                                <div class="rows" id="HOME_NEW">
                                    <div class="col-xs-12 col-sm-6 col-md-2 select-item">
                                        <button class="btn btn-primary" onclick="loadGoods('HOME_NEW');"><i class="fa fa-plus" style="margin: 0 1rem;"></i>添加商品</button>
                                    </div>
                                    <c:forEach items="${newGoods}" var="gd">
                                        <div class="col-xs-12 col-sm-6 col-md-2 select-item" data-id="${gd.good_id}">
                                            <p>
                                                <label>${gd.goodName}</label>
                                                <code>￥${gd.sale_price}</code>
                                                <i class="fa fa-trash-o" title="删除" data-id="${gd.good_id}"></i>
                                            </p>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="item-config-box">
                                <h2>热销商品</h2>
                                <div class="rows" id="HOME_HOT">
                                    <div class="col-xs-12 col-sm-6 col-md-2 select-item">
                                        <button class="btn btn-primary" onclick="loadGoods('HOME_HOT');"><i class="fa fa-plus" style="margin: 0 1rem;"></i>添加商品</button>
                                    </div>
                                    <c:forEach items="${hotGoods}" var="gd">
                                        <div class="col-xs-12 col-sm-6 col-md-2 select-item" data-id="${gd.good_id}">
                                            <p>
                                                <label>${gd.goodName}</label>
                                                <code>￥${gd.sale_price}</code>
                                                <i class="fa fa-trash-o" title="删除" data-id="${gd.good_id}"></i>
                                            </p>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="row right-align">
                            <button class="btn btn-primary" id="mapConfigSaveBtn" onclick="saveMapConfig()"><i class="material-icons dp48">保存</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="goodSelectedBox" aria-hidden="true" class="modal" style="background: transparent; box-shadow: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>选择商品</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#eidt-common-box-pop').remove()">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        <label>商品</label>
                        <select name="good" id="goodSelector" data-placeholder="请选择商品..."  class="chosen-select">

                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-success submitBtn">
                    提交 <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<script id="itemTmpl" type="text/html">
    <div class="col-xs-12 col-sm-6 col-md-2 select-item" data-id="\${good_id}">
        <p>
            <label>\${goodName}</label>
            <code>￥\${sale_price}</code>
            <i class="fa fa-trash-o" title="删除" data-id="\${good_id}"></i>
        </p>
    </div>
</script>
<script>
    $(function () {
        $(".fa-trash-o").unbind("click").bind("click", function () {
            var id = $(this).attr("data-id");
            var flag = $(this).parents(".rows").attr("id");
            removeFlat(id, flag, $(this).parents(".select-item"));
        })
    })

    var CURRENT_GOOD_CACHE = {}
    function loadGoods(type) {
        asyncAjax({
            url: "${ctx}/goodMgr/good/list",
            beforeSend: function (req) {
                $("#goodSelector").empty();
                CURRENT_GOOD_CACHE = {};
            },
            success: function (res) {
                console.log("res:%o", res);
                if (parseInt(res["result"]) == 0 || res["state"] == "SUCCESS") {
                    $.each(res["data"], function (idx, item) {
                        CURRENT_GOOD_CACHE[item["good_id"]] = item;
                        $("#goodSelector").append(
                            '<option value="' + item["good_id"] + '">' + item["goodName"] + '</option>'
                        );
                    })
                    $('#goodSelectedBox .submitBtn').attr("href", "javascript:selectGood('" + type + "');");
                    $('#goodSelectedBox').modal('show');
                    $(".chosen-select").chosen({no_results_text: "没有找到符合条件的数据!"})

                } else {
                    Messenger().run({
                        errorMessage: '请求商品数据发生错误:' + res.msg,
                        showCloseButton: true,
                        action: function (opts) {
                            return opts.error();
                        }
                    });
                }
            }
        })
    }

    function selectGood(type) {
        var data = {
            goodId: $("#goodSelector").val(),
            flag: type
        }
        asyncAjax({
            url: ctx + "/goodMgr/good/updateFlag?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            type: "post",
            success: function (res) {
                console.log("data:%o", res);
                $('#goodSelectedBox').modal('hide');
                if (res["result"] == "0" || res["state"] == "SUCCESS") {
                    Messenger().run({
                        successMessage: '操作成功',
                        showCloseButton: true,
                        action: function (opts) {
                            return opts.success();
                        }
                    });
                    console.log("selected good :%o,target type:%o", CURRENT_GOOD_CACHE[data["goodId"]], type)
                    $("#itemTmpl").tmpl(CURRENT_GOOD_CACHE[data["goodId"]]).appendTo($("#" + type));
                    $(".fa-trash-o").unbind("click").bind("click", function () {
                        var id = $(this).attr("data-id");
                        var flag = $(this).parents(".rows").attr("id");
                        removeFlat(id, flag, $(this).parents(".select-item"));
                    })
                } else {
                    Messenger().run({
                        errorMessage: '操作发生错误:' + res.msg,
                        showCloseButton: true,
                        action: function (opts) {
                            return opts.error();
                        }
                    });
                }
            }
        })

    }

    function removeFlat(id, flag, el) {
        var data = {
            goodId: id,
            flag: flag
        }
        asyncAjax({
            url: ctx + "/goodMgr/good/removeFlag?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            type: "post",
            success: function (res) {
                console.log("data:%o", res);
                if (res["result"] == "0" || res["state"] == "SUCCESS") {
                    Messenger().run({
                        successMessage: '操作成功',
                        showCloseButton: true,
                        action: function (opts) {
                            return opts.success();
                        }
                    });
                    $(el).remove();
                } else {
                    Messenger().run({
                        errorMessage: '操作发生错误:' + res.msg,
                        showCloseButton: true,
                        action: function (opts) {
                            return opts.error();
                        }
                    });
                }
            }
        })

    }
</script>
</body>
</html>