<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/4/13
  Time: 22:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/html" id="edit-common-box">
    <div id="edit-common-box-pop" aria-hidden="true" class="modal" style="background: transparent; box-shadow: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>修改内容</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#eidt-common-box-pop').remove()">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">
                        {{if type== "text" || type == "time"}}
                        <div class="form-group">
                            <label>修改：</label>
                            <input type="text" name="edit-common-val" id="edit-common-val" value="" placeholder="\${src}"/>
                        </div>
                        {{else type == "img"}}
                        <div class="form-group upload-img-box">
                            <img id="edit-common-img-preview" name="edit-common-img-preview" class="img_upload_preview" for="edit-common-img-file" data-target="edit-common-img-val" src=""
                                 onclick="$('#edit-common-img-file').click()"
                                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"/>
                            <input id="edit-common-img-file" type="file" name="edit-common-img-file" style="display: none;"
                                   onchange="uploadFile('edit-common-img-file','edit-common-img-preview','edit-common-val','image')"/>
                            <input name="edit-common-val" id="edit-common-val" type="hidden">
                        </div>
                        {{/if}}
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-success submitBtn">
                        提交 <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</script>
