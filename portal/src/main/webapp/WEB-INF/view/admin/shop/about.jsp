<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
    <title>关于我们</title>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=640,minimum-scale=0.5,maximum-scale=0.5,initial-scale=0.5">
    <jsp:include page="../../common/header.jsp"></jsp:include>
    <style>
        #aboutContent {
            min-height: 280px;
        }
    </style>
</head>
<body style="background: #efefef;">

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-action">
                        地图配置
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field col s6">
                                <input type="text" id="lng" name="lng" class="validate" value="${config.lng}">
                                <label for="lng">经度</label>
                            </div>
                            <div class="input-field col s6">
                                <input type="text" id="lat" name="lat" class="validate" value="${config.lat}">
                                <label for="lat">纬度</label>
                            </div>
                            <div class="input-field col s6">
                                <label for="desc">路线提示</label>
                                <input type="text" name="desc" id="desc" value="${config.desc}" placeholder="乘车路线提示" class="validate"/>
                            </div>
                            <div class="input-field col s6">
                                <input type="text" name="tip" id="tip" value="${config.tip}" placeholder="地图标记提示" class="validate"/>
                                <label for="tip">地图标记提示</label>
                            </div>
                        </div>
                        <div class="row right-align">
                            <button class="btn btn-primary" id="mapConfigSaveBtn" onclick="saveMapConfig()"><i class="material-icons dp48">保存</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-action">
                        内容编辑
                    </div>
                    <div class="card-content">
                        <textarea id="aboutContent" polaceholder="公司介绍、团队描述等">${config.content}</textarea>
                        <div class="row right-align" style="margin-top:1rem;">
                            <button class="btn btn-primary" id="aboutContentSaveBtn"><i class="material-icons dp48" onclick="saveAboutContent()">保存</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function saveMapConfig() {
        var data = {
            desc: isEmpty($("#desc").val) ? "" : encodeURI($("#desc").val()),
            tip: isEmpty($("#tip").val) ? "" : encodeURI($("#tip").val()),
            lat: isEmpty($("#lat").val) ? "" : encodeURI($("#lat").val()),
            lng: isEmpty($("#lng").val) ? "" : encodeURI($("#lng").val())
        }
        asyncAjax({
            url: ctx + "/shop/map/config/save",
            type: "post",
            data: data,
            success: function (res) {
                console.log("data:%o", res);
                if (res["result"] == 0) {
                    alert("修改成功!");
                } else {
                    alert(res["msg"]);
                }
            }
        })

    }

    function saveAboutContent() {
        var data = {
            content: isEmpty($("#aboutContent").val) ? "" : encodeURI($("#aboutContent").val()),
        }
        asyncAjax({
            url: ctx + "/shop/about/content/save",
            type: "post",
            data: data,
            success: function (res) {
                console.log("data:%o", res);
                if (res["result"] == 0) {
                    alert("修改成功!");
                } else {
                    alert(res["msg"]);
                }
            }
        })
    }
</script>
</body>
</html>