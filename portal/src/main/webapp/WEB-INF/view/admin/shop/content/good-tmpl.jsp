<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/4/13
  Time: 21:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script id="tableTmpl" type="text/html">
    <tr>
        <td>
            <div class="well">
                <div class="img-box" style="border: none;width:auto;display: block;">
                    {{each(i,iu) img}}
                    <img src="\${iu}" data-src="\${iu}"
                         onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"
                         class="data-preview-img edit_able" data-type="img" data-name="img" data-id="\${good_id}"/>
                    {{/each}}
                </div>
                <div class="good-info-box" style="display: block;border:none;border-top:1px solid #ccc;">
                    <div class="form-group">
                        <label class="control-label ">商品名称:</label>
                        <span class="edit_able" data-name="goodName" data-id="\${good_id}">\${goodName}</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label ">商品原价:</label>
                        <span class="edit_able" data-name="source_price" data-id="\${good_id}">\${parseFloat(source_price).toFixed(2)}</span>元
                    </div>
                    <div class="form-group">
                        <label class="control-label ">商品售价:</label>
                        <span class="edit_able" data-name="sale_price" data-id="\${good_id}">\${parseFloat(sale_price).toFixed(2)}</span>元
                    </div>
                    <div class="form-group">
                        <label class="control-label ">商品备注:</label>
                        <span class="edit_able" data-name="goodRemr" data-id="\${good_id}">\${goodRemr || '--'}</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label ">商品类别:</label>
                        <div>
                            <span class="gc-box edit_able" data-type="good-class"
                                  data-name="good_class_name"
                                  data-id="\${good_id}"
                                  style="margin: 0 1rem;display: inline-block;vertical-align: top;">\${good_class_name}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label ">商品物流:</label>
                        <div>
                            {{if good_express}}
                            <a href="javascript:void(0)" onclick="editExpress(this);return false;" class="gc-box" data-name="good_express" data-id="\${good_id}"
                               style="margin: 0 1rem;display: inline-block;vertical-align: top;">\${express_text}</a>
                            {{else}}
                            <a href="javascript:void(0)" onclick="editExpress(this);return false;" class="gc-box" data-name="good_express" data-id="\${good_id}"
                               style="margin: 0 1rem;display: inline-block;vertical-align: top;">无</a>
                            {{/if}}
                        </div>
                    </div>
                </div>


            </div>
        </td>
        <td>
            <div class="row">
                <div class="form-group">
                    <label class="control-label ">厂商名称</label><span class="edit_able" data-name="product"
                                                                    data-id="\${good_id}">\${product || '--'}</span>
                </div>
                <div class="form-group">
                    <label class="control-label ">生产日期</label><span class="edit_able" data-name="productTime"
                                                                    data-type="time"
                                                                    data-id="\${good_id}">\${productTime || '--'}</span>
                </div>
                <div class="form-group">
                    <label class="control-label ">备注</label><span class="edit_able" data-name="productRemark"
                                                                  data-id="\${good_id}">\${productRemark || '--'}</span>
                </div>
                <div class="form-group">
                    <label class="control-label ">规格</label>
                    <span class="edit_able" data-name="length" data-id="\${good_id}">\${length || '0'}</span>cm *
                    <span class="edit_able" data-name="width" data-id="\${good_id}">\${width || '0'}</span>cm *
                    <span class="edit_able" data-name="height" data-id="\${good_id}">\${height || '0'}</span>cm
                </div>
                <div class="form-group">
                    <label class="control-label ">商品展示优先级</label>

                    <span <shiro:hasRole name="all">class="edit_able"</shiro:hasRole> data-name="priority" data-id="\${good_id}">\${priority}</span>
                    <p>提示：优先级越高，在平台的商品列表上显示越靠前，如需推广商品，请联系平台客服咨询。</p>
                </div>
            </div>
        </td>
        <td>
            {{if status == "1"}}
            <span class="status-active" data-id="\${good_id}">已上架</span>
            {{else}}
            <span class="status-disabled" data-id="\${good_id}">已下架</span>
            {{/if}}
            <p><strong>发布时间：</strong>\${formatDate(latm)}</p>
        </td>
        <td>
            <span class="edit_able" data-name="amount" data-id="\${good_id}">\${amount}</span>
        </td>
        <td>
            \${ bigType || "--" }
        </td>
        <shiro:hasRole name="all">
            <td>
                <div class="shop-user" data-id="\${wx_user_id}"></div>
            </td>
        </shiro:hasRole>
        <td>
            <div>
                <div style="text-align:center;min-width:16rem;">
                    {{if status == "1"}}
                    <a href="javascript:offShelf('\${good_id}');" class="btn btn-warning">下架</a>
                    {{else}}
                    <a href="javascript:onShelf('\${good_id}');" class="btn btn-success">上架</a>
                    {{/if}}

                    <a href="javascript:showDetail('\${good_id}');" class="btn btn-primary">图文详情</a>

                </div>
            </div>
        </td>
    </tr>
</script>

<%--<script type="text/html" id="baseInfoTmpl">
    <div class="well" style="min-width:45rem;">
        <div class="img-box" style="border: none;width:auto;display: block;">
            {{each(i,iu) img}}
            <img src="\${iu}" data-src="\${iu}"
                 onerror="this.src='${ctx}/resources/ueditor/dialogs/image/images/image.png'"
                 class="data-preview-img edit_able" data-type="img" data-name="img" data-id="\${good_id}"/>
            {{/each}}
        </div>
        <div class="good-info-box" style="display: block;border:none;border-top:1px solid #ccc;">
            <div class="form-group">
                <label class="control-label col-md-3">商品名称:</label>
                <span class="edit_able" data-name="goodName" data-id="\${good_id}">\${goodName}</span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">商品原价:</label>
                <span class="edit_able" data-name="source_price" data-id="\${good_id}">\${parseFloat(source_price).toFixed(2)}</span>元
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">商品售价:</label>
                <span class="edit_able" data-name="sale_price" data-id="\${good_id}">\${parseFloat(sale_price).toFixed(2)}</span>元
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">商品备注:</label>
                <span class="edit_able" data-name="goodRemr" data-id="\${good_id}">\${goodRemr}</span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">商品类别:</label>
                <div>
                    {{each(i,gc) goodClass}}
                    <span class="gc-box edit_able" data-type="good-class" data-name="goodClass" data-id="\${good_id}"
                          style="margin: 0 1rem;display: inline-block;vertical-align: top;">\${gc.name}</span>
                    {{/each}}
                </div>
            </div>
        </div>


    </div>
</script>
<script type="text/html" id="productInfoTmpl">
    <div class="row">
        <div class="form-group">
            <label class="control-label col-md-3">厂商名称</label><span class="edit_able" data-name="product"
                                                                    data-id="\${good_id}">\${product}</span>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">生产日期</label><span class="edit_able" data-name="productTime"
                                                                    data-type="time"
                                                                    data-id="\${good_id}">\${productTime}</span>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">备注</label><span class="edit_able" data-name="productRemark"
                                                                  data-id="\${good_id}">\${productRemark}</span>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">规格</label>
            <span class="edit_able" data-name="length" data-id="\${good_id}">\${length}</span>cm *
            <span class="edit_able" data-name="width" data-id="\${good_id}">\${width}</span>cm *
            <span class="edit_able" data-name="height" data-id="\${good_id}">\${height}</span>cm
        </div>
    </div>
</script>
<script type="text/html" id="operatorTmpl">
    <div>
        <div style="text-align:center;min-width:16rem;">
            {{if status == "1"}}
            <a href="javascript:offShelf('\${good_id}');" class="btn btn-warning">下架</a>
            {{else}}
            <a href="javascript:onShelf('\${good_id}');" class="btn btn-success">上架</a>
            {{/if}}

            <a href="javascript:showDetail('\${good_id}');" class="btn btn-primary">图文详情</a>

        </div>
    </div>
</script>--%>

<script>
    function offShelf(id, el) {
        var data = {
            good_id: id,
            status: "0"
        }
        updateGood(data, function () {
            $(el).removeClass("btn-warning").addClass("btn-success")
                .text("上架").attr("href", "javascript:onShelf('" + id + "');");
            $(".status-active[data-id=\"" + id + "\"]").removeClass("status-active")
                .addClass("status-disabled").text("已下架");
            loadData();
        });
    }
    function onShelf(id, el) {
        var data = {
            good_id: id,
            status: "1"
        }
        updateGood(data, function () {
            $(el).removeClass("btn-success").addClass("btn-warning")
                .text("下架").attr("href", "javascript:offShelf('" + id + "');");
            $(".status-disabled[data-id=\"" + id + "\"]").removeClass("status-disabled")
                .addClass("status-active").text("已上架");
            loadData();
        });
    }

    function updateField(data, el) {
        var param = {
            good_id: data["id"]
        };
        param[data["name"]] = data["value"];
        if (data["type"] == "img") {
            param["src"] = data["src"];
        }
        updateGood(param, function () {
            if (data["type"] == "text" || data["type"] == "time") {
                $(el).text(data["value"]);
            } else if (data["type"] == "img") {
                $(el).attr("src", data["value"]);
            } else if (data["type"] == "good-class") {
                console.log("update class:%o,el:%o",data,el);
                if (el) {
                    $(el).html(data["value"]);
                }
            }
        })
    }


    function updateGood(data, _callback) {
        asyncAjax({
            url: ctx + "/goodMgr/good/update",
            data: JSON.stringify(data),
            type: "post",
            dataType: "json",
            contentType: "application/json;charset:UTF-8",
            success: function (res) {
                console.log("update good res:%o", res);
                if (res.result == "0" || res.state == "SUCCESS") {
                    bootbox.alert("修改成功");
                    if (typeof _callback == "function") {
                        _callback();
                    }
                }
            }
        })

    }

    function showDetail(id) {
        var good = CURRENT_DATA_CACHE[id];
        var content = good["content"];
        var showContent = '<div class="detail-box" style="max-height:28rem;overflow-y:auto;">' + content + '</div>';
        bootbox.confirm({
            message: showContent,
            buttons: {
                confirm: {
                    label: '<i class="fa fa-edit"></i>修改',
                    className: "btn-warning"
                }
                ,
                cancel: {
                    label: "确定",
                    className: "btn-success"
                }
            },
            callback: function (res) {
                if (res == true) {
                    var updateEditor = null;
                    bootbox.confirm({
                        title: "修改图文详情",
                        width: 800,
                        height: 480,
                        buttons: {
                            confirm: {
                                label: "确认"
                            },
                            cancel: {
                                label: "取消"
                            }
                        },
                        message: '<div style="width:100%;height:400px;margin:1rem auto;display:block;overflow-y:auto;"><script id="modifyDetail" data-id="' + id + '" type="text/plain" style="width:95%;height:280px;margin:1rem auto;display:block;">' + content + '<\/script></div>',
                        callback: function (mres) {
                            var updateResult = updateEditor.getContent();
                            if (mres == true) {
                                if (isEmpty(updateResult)) {
                                    bootbox.confirm({
                                        message: "修改内容为空，是被否保存修改!", buttons: {
                                            confirm: {
                                                label: "确定"
                                            },
                                            cancel: {
                                                label: "取消"
                                            }
                                        },
                                        callback: function (cres) {
                                            if (cres == true) {
                                                bootbox.alert("保存成功");
                                            } else {
                                                return -1;
                                            }
                                        }
                                    })
                                } else {
                                    updateGood({
                                        good_id: id,
                                        content: updateResult
                                    }, function () {
                                        bootbox.alert("操作成功!");
                                        CURRENT_DATA_CACHE[id]["content"] = updateResult;
                                    })
                                }

                            }
                            UE.delEditor("modifyDetail");
                        }
                    });
                    updateEditor = UE.getEditor("modifyDetail");


                }
            }
        });
    }
    function editExpress(obj){
        var good = CURRENT_DATA_CACHE[$(obj).attr("data-id")];

        var data = {
            keyword: "",
            area: "",
            shopId: parseInt("${$_LoginUser.id}")

        };
        var select = '<select id="editGoodExpress" multiple="multiple" data-placeholder="选择物流">';
        asyncAjax({
            url: ctx + "/express/mgr/list?" + $.param(data).replace(/(%5D|%5B)/g, ""),
            success: function (res) {
                console.log("物流数据:%o", res);
                if (res["result"] == "0") {

                    for(var i in res.data){
                        select += '<option value="' + res.data[i].express_id + '">' + res.data[i].name + '</option>';
                    }
                    select += '</select>';

                    bootbox.confirm({
                        title: "修改商品物流",
                        width: 800,
                        height: 480,
                        message: select,
                        buttons: {
                            confirm: {
                                label: '确定'
                            }
                            ,
                            cancel: {
                                label: "取消"
                            }
                        },
                        callback: function (mres) {
                            if (mres == true) {
                                updateGood({
                                    good_id: good["good_id"],
                                    good_express: $("#editGoodExpress").val()
                                }, function () {
                                    CURRENT_DATA_CACHE[good["good_id"]]["good_express"] = $("#editGoodExpress").val();
                                    var selectedText = "";
                                    var i = 0;
                                    $("#editGoodExpress option:selected").each(function () {
                                        if(i == 0){
                                            selectedText += $(this).text();
                                        }else{
                                            selectedText += "," + $(this).text();
                                        }
                                        i++;
                                    });
                                    $(obj).html(selectedText);
                                })
                            }
                        }
                    });

                    $("#editGoodExpress").val(good["good_express"]);
                    $("#editGoodExpress").chosen({
                        width:"100%"
                    });
                } else {
                    bootbox.alert(res["msg"]);
                }
            }
        });

    }

    function updateDetail(id) {

    }
</script>