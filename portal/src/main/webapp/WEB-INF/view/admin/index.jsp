<%--
  Created by IntelliJ IDEA.
  User: 蔻丁同学
  Date: 2017/2/23
  Time: 22:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<c:set value="${pageContext.request.contextPath}" var="ctx"></c:set>
<html>
<head>
    <title>${WEB_TITLE}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<div class="wrapper">
    <jsp:include page="../page-nav.jsp"></jsp:include>
    <div id="page-wrapper">
        <shiro:hasRole name="all">
            <iframe id="mainFrame" name="mainFrame" class="content-main-frame" src="${ctx}/portal/config/index"/>
        </shiro:hasRole>
        <shiro:hasRole name="merchant">
            <iframe id="mainFrame" name="mainFrame" class="content-main-frame" src="${ctx}/user/mgr/index/1"/>
        </shiro:hasRole>
    </div>
</div>

</body>
</html>
