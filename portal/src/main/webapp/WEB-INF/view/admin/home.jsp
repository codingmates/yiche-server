<%--
  Created by IntelliJ IDEA.
  User: CM
  Date: 2017/2/25
  Time: 22:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<jsp:include page="../common/header.jsp"></jsp:include>
<body style="background: #efefef;padding: 0.4rem 1rem;width: 100%;display: block;overflow: hidden;">
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-3">

        <div class="card horizontal cardIcon waves-effect waves-dark">
            <div class="card-image red">
                <i class="fa fa-eye fa-5x"></i>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h3><fmt:formatNumber value="${visitorCount}"></fmt:formatNumber></h3>
                </div>
                <div class="card-action">
                    <strong> 今日访问量</strong>
                </div>
            </div>
        </div>

    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">

        <div class="card horizontal cardIcon waves-effect waves-dark">
            <div class="card-image orange">
                <i class="fa fa-shopping-cart fa-5x"></i>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h3>25,550</h3>
                </div>
                <div class="card-action">
                    <strong> 今日销售额</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">

        <div class="card horizontal cardIcon waves-effect waves-dark">
            <div class="card-image blue">
                <i class="fa fa-comments fa-5x"></i>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h3>11,225</h3>
                </div>
                <div class="card-action">
                    <strong> 待回复消息</strong>
                </div>
            </div>
        </div>

    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">

        <div class="card horizontal cardIcon waves-effect waves-dark">
            <div class="card-image">
                <i class="fa fa-files-o fa-5x"></i>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h3>72,525</h3>
                </div>
                <div class="card-action">
                    <strong> 待处理订单</strong>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
