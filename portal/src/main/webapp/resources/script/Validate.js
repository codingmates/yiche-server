var absoluteContextPath = getContextPath() ? getContextPath() : "";
var validateRegExp = {
    decmal: "^([+-]?)\\d*\\.\\d+$", // 浮点数
    decmal1: "^[1-9]\\d*.\\d*|0.\\d*[1-9]\\d*$", // 正浮点数
    decmal2: "^-([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*)$", // 负浮点数
    decmal3: "^-?([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*|0?.0+|0)$", // 浮点数
    decmal4: "^[1-9]\\d*.\\d*|0.\\d*[1-9]\\d*|0?.0+|0$", // 非负浮点数（正浮点数 + 0）
    decmal5: "^(-([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*))|0?.0+|0$", // 非正浮点数（负浮点数 +0）
    intege: "^-?[1-9]\\d*$", // 整数
    intege1: "^[1-9]\\d*$", // 正整数
    intege2: "^-[1-9]\\d*$", // 负整数
    num: "^([+-]?)\\d*\\.?\\d+$", // 数字
    num1: "^[1-9]\\d*|0$", // 正数（正整数 + 0）
    num2: "^-[1-9]\\d*|0$", // 负数（负整数 + 0）
    ascii: "^[\\x00-\\xFF]+$", // 仅ACSII字符
    chinese: "^[\\u4e00-\\u9fa5]+$", // 仅中文
    notchinese: "^[^\u4e00-\u9fa5]+$", // 非中文
    color: "^[a-fA-F0-9]{6}$", // 颜色
    date: "^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$", // 日期
    email: "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$", // 邮件
    idcard: "^[1-9]([0-9]{14}|[0-9]{17})$", // 身份证
    ip4: "^(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)$", // ip地址
    letter: "^[A-Za-z]+$", // 字母
    letter_l: "^[a-z]+$", // 小写字母
    letter_u: "^[A-Z]+$", // 大写字母
    mobile: "^0?(13|15|18)[0-9]{9}$", // 手机
    notempty: "^\\S+$", // 非空
    password: "^.*[A-Za-z0-9\\w_-]+.*$", // 密码
    fullNumber: "^[0-9]+$", // 数字
    picture: "(.*)\\.(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)$", // 图片
    qq: "^[1-9]*[1-9][0-9]*$", // QQ号码
    rar: "(.*)\\.(rar|zip|7zip|tgz)$", // 压缩文件
    tel: "^[0-9\-()（）]{7,18}$", // 电话号码的函数(包括验证国内区号,国际区号,分机号)
    url: "^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+(:\\d+)?([\\w-./?%&=]*)?$", // url
    username: "^[A-Za-z0-9_\\-\\u4e00-\\u9fa5]+$", // 用户名
    deptname: "^[A-Za-z0-9_()（）\\-\\u4e00-\\u9fa5]+$", // 单位名
    zipcode: "^\\d{6}$", // 邮编
    realname: "^[A-Za-z\\u4e00-\\u9fa5]+$", // 真实姓名
    companyname: "^[A-Za-z0-9_()（）\\-\\u4e00-\\u9fa5]+$",
    companyaddr: "^[A-Za-z0-9_()（）\\#\\-\\u4e00-\\u9fa5]+$",
    companysite: "^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+([\\w-./?%&#=]*)?$",
    website: "^(?=^.{3,255}$)[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\\.?$",
    SChar: "^[^'\"<>&/\\s]*$",
    ipList: "^((?:(?:25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))\\.){3}(?:25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)));?){0,10}$"
};

// 配置
var validateSettings = {
    onFocus: {
        state: null,
        container: "_error",
        style: "focus",
        run: function (option, str) {
            if (!validateRules.checkType(option.element)) {
                option.element.removeClass(validateSettings.INPUT_style2).addClass(validateSettings.INPUT_style1);
            }
            option.onFocusEle.removeClass().addClass(validateSettings.onFocus.style).html(str);
        }
    },
    isNull: {
        state: 0,
        container: "_error",
        style: "null",
        run: function (option, str) {
            option.element.attr("sta", validateSettings.isNull.state);
            if (!validateRules.checkType(option.element)) {
                if (str != "") {
                    option.element.removeClass(validateSettings.INPUT_style1).addClass(validateSettings.INPUT_style2);
                } else {
                    option.element.removeClass(validateSettings.INPUT_style2).removeClass(validateSettings.INPUT_style1);
                }
            }
            option.succeedEle.removeClass(validateSettings.succeed.style);
            option.isNullEle.removeClass().addClass(validateSettings.isNull.style).html(str);
        }
    },
    error: {
        state: 1,
        container: "_error",
        style: "error",
        run: function (option, str) {
            option.element.attr("sta", validateSettings.error.state);
            if (!validateRules.checkType(option.element)) {
                option.element.removeClass(validateSettings.INPUT_style1).addClass(validateSettings.INPUT_style2);
            }
            option.succeedEle.removeClass(validateSettings.succeed.style);
            option.errorEle.removeClass().addClass(validateSettings.error.style).html(str);
        }
    },
    succeed: {
        state: 2,
        container: "_error",
        style: "succeed",
        run: function (option, str) {
            option.element.attr("sta", validateSettings.succeed.state);
            option.errorEle.empty();
            if (!validateRules.checkType(option.element)) {
                option.element.removeClass(validateSettings.INPUT_style1).removeClass(validateSettings.INPUT_style2);
            }
            option.succeedEle.removeClass().addClass(validateSettings.succeed.style).html(str);
        }
    },
    INPUT_style1: "highlight1",
    INPUT_style2: "highlight2"

};

// 验证规则
var validateRules = {
    isNull: function (str) {
        return (str == "" || typeof str != "string");
    },
    betweenLength: function (str, _min, _max) {
        return (str.length >= _min && str.length <= _max);
    },
    maxLength: function (str, _max) {
        return str.length <= _max;
    },
    minLength: function (str, _min) {
        return str.length >= _min;
    },
    length: function (str, length) {
        return str.length == length;
    },
    maxValue: function (value, _max) {
        return parseInt(value) <= parseInt(_max);
    },
    minValue: function (value, _min) {
        return parseInt(value) >= parseInt(_min);
    },
    isWebsite: function (str) {
        return new RegExp(validateRegExp.website).test(trim(str));
    },
    isIP4: function (str) {
        return new RegExp(validateRegExp.ip4).test(trim(str));
    },
    isUid: function (str) {
        return new RegExp(validateRegExp.username).test(str);
    },
    fullNumberName: function (str) {
        return new RegExp(validateRegExp.fullNumber).test(str);
    },
    isPwd: function (str) {
        return /^.*([\W_a-zA-z0-9-])+.*$/i.test(str);
    },
    isPwd2: function (str1, str2) {
        return (str1 == str2);
    },
    isEmail: function (str) {
        return new RegExp(validateRegExp.email).test(str);
    },
    isTel: function (str) {
        return new RegExp(validateRegExp.tel).test(str);
    },
    isMobile: function (str) {
        return new RegExp(validateRegExp.mobile).test(str);
    },
    isIPList: function (str) {
        return new RegExp(validateRegExp.ipList).test(str);
    },
    checkType: function (element) {
        return (element.attr("type") == "checkbox" || element.attr("type") == "radio" || element.attr("rel") == "select");
    },
    isChinese: function (str) {
        return new RegExp(validateRegExp.chinese).test(str);
    },
    isRealName: function (str) {
        return new RegExp(validateRegExp.realname).test(str);
    },
    isDeptname: function (str) {
        return new RegExp(validateRegExp.deptname).test(str);
    },
    isCompanyname: function (str) {
        return new RegExp(validateRegExp.companyname).test(str);
    },
    isCompanyaddr: function (str) {
        return new RegExp(validateRegExp.companyaddr).test(str);
    },
    isCompanysite: function (str) {
        return new RegExp(validateRegExp.companysite).test(str);
    },
    isQq: function (str) {
        return new RegExp(validateRegExp.qq).test(str);
    },
    isInteger: function (str) {
        return new RegExp(validateRegExp.intege).test(str);
    },
    isInteger1: function (str) {
        return new RegExp(validateRegExp.intege1).test(str);
    },
    isNumber: function (str) {
        return new RegExp(validateRegExp.num).test(str);
    },
    isNumber1: function (str) {
        return new RegExp(validateRegExp.num1).test(str);
    },
    isUrl: function (str) {
        return new RegExp(validateRegExp.url).test(str);
    },
    isSChar: function (str) {
        return new RegExp(validateRegExp.SChar).test(str);
    },
    isLetter: function (str) {
        return new RegExp(validateRegExp.letter).test(str);
    },
    isNotChinese: function (str) {
        return new RegExp(validateRegExp.notchinese).test(str);
    },
    isRealname: function (str) {
        return new RegExp(validateRegExp.realname).test(str);
    }
};
var validate = {
    showFocus: function (option, ele) {
        var id = $(ele).attr("id");
        var _onFocus = $("#" + id + validateSettings.onFocus.container);
        var _succeed = $("#" + id + validateSettings.succeed.container);
        validateSettings.onFocus.run({
            prompts: option,
            element: $(ele),
            onFocusEle: _onFocus,
            succeedEle: _succeed
        }, option.onFocus);
    },
    initStatus: function (elements) {
        for (var i = 0, len = elements.length; i < len; i++) {
            $(elements[i]).attr("sta", 3);
        }
    }
}
// 验证文本
var validatePrompt = {
    r_username: {
        url: absoluteContextPath + "/dev/user/findByUserName",//ajax验证是否存在的url，若为空，则无需进行ajax验证
        onFocus: "4-20位字符，可由中文、英文、数字及“_”、“-”组成",//聚焦时显示的文本
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",//验证成功时显示的内容
        isNull: "请输入用户名",//输入为空时显示的文本
        error: {//错误信息
            beUsed: "该用户名已被使用，请使用其它用户名注册",
            badLength: "用户名长度只能在4-20位字符之间",
            badFormat: "用户名只能由中文、英文、数字及“_”、“-”组成"
        }
    },
    r_pwd: {
        onFocus: "6-20位字符，可由英文、数字及符号组成",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "请输入密码",
        error: {
            badLength: "密码长度只能在6-20位字符之间",
            badFormat: "密码只能由英文、数字及符号组成"
        }
    },
    r_pwd2: {
        onFocus: "请再次输入密码",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "确认密码不能为空",
        error: {
            badLength: "密码长度只能在6-20位字符之间",
            badFormat2: "两次输入密码不一致",
            badFormat1: "密码只能由英文、数字及符号组成"
        }
    },
    r_mail: {
        url: absoluteContextPath + '/bo/user/checkEmail',
        onFocus: "请输入常用的邮箱，将用来找回密码、登录等",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "",
        error: {
            beUsed: "该邮箱已被使用，请更换其它邮箱",
            badFormat: "邮箱格式不正确",
            badLength: "您填写的邮箱过长，邮件地址只能在50个字符以内"
        }
    },
    mobile: {
        onFocus: "",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "",
        error: {
            badLength: "移动号为11位数字",
            badFormat: "无效的手机号码"
        }
    },
    ipList: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: {
            badLength: "IP列表长度错误",
            badFormat: "IP列表格式错误"
        }
    },
    qq: {
        onFocus: "",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "",
        error: "qq号格式不正确"
    },
    phone: {
        onFocus: "",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "",
        error: "正确格式例如：0592-6666666"
    },
    areaPhone: {
        onFocus: "",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "",
        error: "正确格式例如：0592-6666666"
    },
    authcode: {
        onFocus: "请输入图片中的字符，不区分大小写",
        succeed: "<img src='" + absoluteContextPath + "/image/spt.jpg'/>",
        isNull: "请输入验证码",
        error: "验证码错误"
    },
    int: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "只能是非0的整数"
    },
    int1: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "只能是正整数"
    },
    number: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "只能是数字"
    },
    number1: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "只能是正数"
    },
    email: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "邮箱格式不正确"
    },
    url: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "不合法的url"
    },
    notNull: {
        onFocus: "*",
        succeed: "&nbsp;",
        isNull: "*",
        error: "不能为空"
    },
    length: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "只能是{1}个字符"
    },
    maxLength: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "不能多于{1}个字符"
    },
    minLength: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "不能少于{1}个字符"
    },
    maxValue: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "输入的值不能大于{1}"
    },
    minValue: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "输入的值不能小于{1}"
    },
    IPAdd: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "输入的服务地址格式不正确，请输入正确的域名或ip"
    },
    host: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "输入的服务地址格式不正确，请输入正确的域名"
    },
    sChar: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "不能含空白字符及以下字符: \'\"<>\&/"
    },
    letter: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "只能为英文"
    },
    notchinese: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "不能含中文"
    },
    website: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "请输入正确域名"
    },
    realname: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "请输入中文姓名或英文姓名"
    },
    isIPV4: {
        onFocus: "",
        succeed: "",
        isNull: "",
        error: "请输入合法的IP地址"
    }
};

var nameold, emailold;
var namestate = false, emailstate = false;
// 回调函数
var validateFunction = {
    username: function (option) {
        if (validateRules.isNull(option.value)) {
            validateSettings.error.run(option, option.prompts.isNull);
        } else {
            var format = validateRules.isUid(option.value);
            var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 4, 20);
            if (!length && format) {
                validateSettings.error.run(option, option.prompts.error.badLength);
            } else if (!length && !format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else if (length && !format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else if (option.prompts.url != "") {
                if (!namestate || nameold != option.value) {
                    if (nameold != option.value) {
                        nameold = option.value;
                        option.errorEle.html("<span style='color:#999'>检验中……</span>");
                        $.ajax({
                            type: "POST",
                            async: false,
                            url: option.prompts.url,
                            data: {userName: nameold},
                            dataType: "text"
                        }).done(function (msg) {
                            if (msg) {
                                validateSettings.error.run(option, option.prompts.error.beUsed);
                                namestate = false;
                            } else {
                                namestate = true;
                            }
                        });
                    } else {
                        validateSettings.error.run(option, option.prompts.error.beUsed);
                        namestate = false;
                    }
                }
            }
        }
    },
    pwd: function (option) {
        if (validateRules.isNull(option.value)) {
            validateSettings.error.run(option, option.prompts.isNull);
        } else {
            var str1 = option.value;
            var str2 = $("#pwd2").val();
            var format = validateRules.isPwd(option.value);
            var length = validateRules.betweenLength(option.value, 6, 20);
            if (!length && format) {
                validateSettings.error.run(option, option.prompts.error.badLength);
            } else if (!length && !format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else if (length && !format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            }
            if (str2 == str1) {
                $("#pwd2").focus();
            }
        }
    },
    pwd2: function (option) {
        if (validateRules.isNull(option.value)) {
            validateSettings.error.run(option, option.prompts.isNull);
        } else {
            var str1 = option.value;
            var str2 = $("#pwd").val();
            var length = validateRules.betweenLength(option.value, 6, 20);
            var format2 = validateRules.isPwd2(str1, str2);
            var format1 = validateRules.isPwd(str1);
            if (!length) {
                validateSettings.error.run(option, option.prompts.error.badLength);
            } else {
                if (!format1) {
                    validateSettings.error.run(option, option.prompts.error.badFormat1);
                } else {
                    if (!format2) {
                        validateSettings.error.run(option, option.prompts.error.badFormat2);
                    }
                }
            }
        }
    },
    mail: function (option) {
        var format = validateRules.isEmail(option.value);
        var format2 = validateRules.betweenLength(option.value, 0, 50);
        if (!format) {
            validateSettings.error.run(option, option.prompts.error.badFormat);
        } else {
            if (!format2) {
                validateSettings.error.run(option, option.prompts.error.badLength);
            } else if (option.prompts.url != "") {
                if (!emailstate || emailold != option.value) {
                    if (emailold != option.value) {
                        emailold = option.value;
                        option.errorEle.html("<span style='color:#999'>检验中……</span>");
                        $.ajax({
                            type: "POST",
                            async: false,
                            url: option.prompts.url,
                            data: {
                                'email': emailold
                            },
                            dataType: "json"
                        }).done(function (msg) {
                            //var json = JSON.parse(msg);
                            if (msg.exist) {
                                validateSettings.error.run(option, option.prompts.error.beUsed);
                                emailstate = false;
                            } else {
                                emailstate = true;
                            }
                        });
                    } else {
                        validateSettings.error.run(option, option.prompts.error.beUsed);
                        emailstate = false;
                    }
                }
            }
        }
    },
    IPAdd: function (option) {
        if (/[A-Za-z_-]/.test(option.value)) {
            if (option.value.indexOf(" ") >= 0) {
                option.value = option.value.replace(/ /g, "");
            }
            if (!/^([\w-]+\.)+((com)|(net)|(org)|(gov\.cn)|(info)|(cc)|(com\.cn)|(net\.cn)|(org\.cn)|(name)|(biz)|(tv)|(cn)|(mobi)|(name)|(sh)|(ac)|(io)|(tw)|(com\.tw)|(hk)|(com\.hk)|(ws)|(travel)|(us)|(tm)|(la)|(me\.uk)|(org\.uk)|(ltd\.uk)|(plc\.uk)|(in)|(eu)|(it)|(jp))$/.test(option.value)) {
                validateSettings.error.run(option, option.prompts.error);
            }
        } else {
            var ipArray, j;
            ipArray = value.split(".");
            j = ipArray.length
            if (j != 4) {
                validateSettings.error.run(option, option.prompts.error);
            } else {
                for (var i = 0; i < 4; i++) {
                    if (ipArray[i].length == 0 || ipArray[i] > 255) {
                        validateSettings.error.run(option, option.prompts.error);
                        break;
                    }
                }
            }
        }
    },
    host: function (option) {
        if (/[A-Za-z_-]/.test(option.value)) {
            if (option.value.indexOf(" ") >= 0) {
                option.value = option.value.replace(/ /g, "");
            }
            if (!/^([\w-]+\.)+((com)|(net)|(org)|(gov\.cn)|(info)|(cc)|(com\.cn)|(net\.cn)|(org\.cn)|(name)|(biz)|(tv)|(cn)|(mobi)|(name)|(sh)|(ac)|(io)|(tw)|(com\.tw)|(hk)|(com\.hk)|(ws)|(travel)|(us)|(tm)|(la)|(me\.uk)|(org\.uk)|(ltd\.uk)|(plc\.uk)|(in)|(eu)|(it)|(jp))$/.test(option.value)) {
                validateSettings.error.run(option, option.prompts.error);
            }
        }
        validateSettings.succeed.run(option, option.prompts.succeed);
    },
    FORM_submit: function (elements) {
        var bool = true;
        for (var i = 0, len = elements.length; i < len; i++) {
            if (!$(elements[i]).attr("sta") || $(elements[i]).attr("sta") == 3) {
                $(elements[i]).verify($(elements[i]).attr('verify'));
            }
            var sta = $(elements[i]).attr("sta");
            if (sta == 2 || sta == 0) {
                bool = true;
            } else {
                bool = false;
                break;
            }
        }
        return bool;
    }

};
(function ($) {
    $.fn.verify = function (verify) {
        if (!verify || verify == '') return;
        var ele = this;
        var Features = verify.split("\&\&");
        var id = ele.attr("id");
        var type = ele.attr("type");
        var rel = ele.attr("rel");
        var value = ele.val();
        var defaultValue = ele[0].defaultValue;
        var _onFocus = $("#" + id + validateSettings.onFocus.container);
        var _succeed = $("#" + id + validateSettings.succeed.container);
        var _isNull = $("#" + id + validateSettings.isNull.container);
        var _error = $("#" + id + validateSettings.error.container);
        var op = {
            element: ele,
            value: value,
            errorEle: _error,
            isNullEle: _isNull,
            succeedEle: _succeed
        };
        var option;
        $(this).attr("sta", 2);
        var notNullTip = "";
        for (var i = 0; i < Features.length && ele.attr("sta") != 1; i++) {
            var f = Features[i].split(":");
            var fName = f[0];
            var fValue = null;
            if (f.length > 1) {
                fValue = f[1];
            }
            if (fName == "notNull") {
                option = $.extend({}, {prompts: validatePrompt.notNull}, op);
                if (value == null || trim(value) == "") {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "number") {
                option = $.extend({}, {prompts: validatePrompt.number}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isNumber(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "number1") {
                option = $.extend({}, {prompts: validatePrompt.number1}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isNumber1(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "int") {
                option = $.extend({}, {prompts: validatePrompt.int}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isInteger(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "int1") {
                option = $.extend({}, {prompts: validatePrompt.int1}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isInteger1(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "email") {
                option = $.extend({}, {prompts: validatePrompt.email}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isEmail(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "url") {
                option = $.extend({}, {prompts: validatePrompt.url}, op);
                if (value == null || value == "") {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isUrl(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "length") {
                option = $.extend({}, {prompts: validatePrompt.length}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (isNaN(fValue)) {
                    alert("校验规则错误，Length后面必须是数字");
                } else if (!validateRules.length(value, fValue)) {
                    validateSettings.error.run(option, option.prompts.error.replace("{1}", fValue));
                }
            } else if (fName == "maxLength") {
                option = $.extend({}, {prompts: validatePrompt.maxLength}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (isNaN(fValue)) {
                    alert("校验规则错误，maxLength后面必须是数字");
                } else if (!validateRules.maxLength(value, fValue)) {
                    validateSettings.error.run(option, option.prompts.error.replace("{1}", fValue));
                }
            } else if (fName == "minLength") {
                option = $.extend({}, {prompts: validatePrompt.minLength}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (isNaN(fValue)) {
                    alert("校验规则错误，minLength后面必须是数字");
                } else if (!validateRules.minLength(value, fValue)) {
                    validateSettings.error.run(option, option.prompts.error.replace("{1}", fValue));
                }
            } else if (fName == "IPAdd") {
                option = $.extend({}, {prompts: validatePrompt.IPAdd}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                validateFunction.IPAdd(option);
            } else if (fName == "host") {
                option = $.extend({}, {prompts: validatePrompt.host}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                validateFunction.host(option);
            } else if (fName == "sChar") {
                option = $.extend({}, {prompts: validatePrompt.sChar}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isSChar(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "letter") {
                option = $.extend({}, {prompts: validatePrompt.letter}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isLetter(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "notchinese") {
                option = $.extend({}, {prompts: validatePrompt.notchinese}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isNotChinese(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "maxValue") {
                option = $.extend({}, {prompts: validatePrompt.maxValue}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (isNaN(fValue)) {
                    alert("校验规则错误，maxValue后面必须是数字");
                } else if (!validateRules.maxValue(value, fValue)) {
                    validateSettings.error.run(option, option.prompts.error.replace("{1}", fValue));
                }
            } else if (fName == "minValue") {
                option = $.extend({}, {prompts: validatePrompt.minValue}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (isNaN(fValue)) {
                    alert("校验规则错误，minValue后面必须是数字");
                } else if (!validateRules.minValue(value, fValue)) {
                    validateSettings.error.run(option, option.prompts.error.replace("{1}", fValue));
                }
            } else if (fName == "phone") {
                option = $.extend({}, {prompts: validatePrompt.phone}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isTel(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "areaPhone") {
                option = $.extend({}, {prompts: validatePrompt.areaPhone}, op);
                option.errorEle = $("#areaPhone_error");
                option.isNullEle = $("#areaPhone_error");
                option.succeedEle = $("#areaPhone_error");
                var tphone = $('#telephone').val();
                var tphoneDefault = $('#telephone')[0].defaultValue;
                var area_code = $('#area_code').val();
                var area_codeDefault = $("#area_code")[0].defaultValue;
                if (((tphone == null || tphone == "") && (area_code == null || area_code == "")) || (tphoneDefault === tphone && area_codeDefault === area_code)) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                var area_reg = /^\d{3,5}$/;
                var tphone_reg = /^\d{7,8}$/;
                var result_code = area_reg.exec(area_code);
                var result = tphone_reg.exec(tphone);
                if (id == 'area_code') {
                    if (tphone.length == 0) {
                        $('#telephone').attr("sta", 3);
                    }
                    if (area_code.length == 0 || result_code == null) {
                        validateSettings.error.run(option, option.prompts.error);
                    }
                }
                if (id == 'telephone') {
                    if (area_code.length == 0) {
                        $('#area_code').attr("sta", 3);
                    }
                    if (tphone.length == 0 || result == null) {
                        validateSettings.error.run(option, option.prompts.error);
                    }
                }
            } else if (fName == "mobile") {
                option = $.extend({}, {prompts: validatePrompt.mobile}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (value.length < 11) {
                    validateSettings.error.run(option, option.prompts.error.badLength);
                } else if (!validateRules.isMobile(value)) {
                    validateSettings.error.run(option, option.prompts.error.badFormat);
                }
            } else if (fName == "ipList") {
                option = $.extend({}, {prompts: validatePrompt.ipList}, op);
                if (!validateRules.isIPList(value)) {
                    validateSettings.error.run(option, option.prompts.error.badFormat);
                }
            }
            else if (fName == "qq") {
                option = $.extend({}, {prompts: validatePrompt.qq}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isQq(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "realname") {
                option = $.extend({}, {prompts: validatePrompt.realname}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isRealname(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            } else if (fName == "r_username") {
                option = $.extend({}, {prompts: validatePrompt.r_username}, op);
                validateFunction.username(option);
            } else if (fName == "r_pwd") {
                option = $.extend({}, {prompts: validatePrompt.r_pwd}, op);
                validateFunction.pwd(option);
            } else if (fName == "r_pwd2") {
                option = $.extend({}, {prompts: validatePrompt.r_pwd2}, op);
                validateFunction.pwd2(option);
            } else if (fName == "r_mail") {
                option = $.extend({}, {prompts: validatePrompt.r_mail}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                validateFunction.mail(option);
            } else if (fName == "isIPV4") {
                option = $.extend({}, {prompts: validatePrompt.isIPV4}, op);
                if (value == null || value == "" || defaultValue === value) {
                    validateSettings.isNull.run(option, option.prompts.isNull + notNullTip);
                    continue;
                }
                if (!validateRules.isIP4(value)) {
                    validateSettings.error.run(option, option.prompts.error);
                }
            }
        }
        if (id == 'area_code' || id == 'telephone') {
            if ($('#telephone').attr("sta") == 2 && $('#area_code').attr("sta") == 2) {
                validateSettings.succeed.run(option, option.prompts.succeed);
            }
        } else if (ele.attr("sta") == 2) {
            validateSettings.succeed.run(option, option.prompts.succeed);
        }
    };
    $.fn.isValid = function () {
        var rs = true;
        if ($(this).attr('verify')) {
            rs = rs && $(this).verify($(this).attr('verify'));
        }
        rs = rs && validateFunction.FORM_submit($(this).find('$[verify]'));
        return rs;
    };
    $(function () {
        $('$[verify]').attr("sta", 3).live("blur", function () {
            if ($(this).is(":visible")) {
                $(this).verify($(this).attr('verify'));
            }
        });
        $('form').each(function () {
            this.onsubmit = function () {
                //return false;
                return $(this).isValid();
            }
        });
    });
})(jQuery);

function trim(str) { //删除左右两端的空格　　
    return str.replace(/(^\s*)|(\s*$)/g, "");
}