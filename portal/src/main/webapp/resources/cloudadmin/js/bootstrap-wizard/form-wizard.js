var FormWizard = function () {
    return {
        init: function (opts) {
            var target = opts.target;
            var confirm = opts.confirm;
            var callback = opts.submit;
            if (!jQuery().bootstrapWizard) {
                return;
            }


            var wizform = $('#' + target);
            var alert_success = $('.alert-success', wizform);
            var alert_error = $('.alert-danger', wizform);

            /*-----------------------------------------------------------------------------------*/
            /*	Validate the form elements
             /*-----------------------------------------------------------------------------------*/
            wizform.validate({
                doNotHideMessage: true,
                errorClass: 'error-span',
                errorElement: 'span',
                rules: {
                    /* Create Account */
                    img1: {
                        required: true
                    },
                    img2: {
                        required: false
                    },
                    img3: {
                        required: false
                    },
                    img4: {
                        required: false
                    }
                },

                invalidHandler: function (event, validator) {
                    alert_success.hide();
                    alert_error.show();
                },

                highlight: function (element) {
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error');
                },

                unhighlight: function (element) {
                    $(element)
                        .closest('.form-group').removeClass('has-error');
                },

                success: function (label) {
                    if (label.attr("for") == "gender") {
                        label.closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove();
                    } else {
                        label.addClass('valid')
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                }
            });

            /*-----------------------------------------------------------------------------------*/
            /*	Initialize Bootstrap Wizard
             /*-----------------------------------------------------------------------------------*/
            $('#' + target).bootstrapWizard({
                'nextSelector': '.nextBtn',
                'previousSelector': '.prevBtn',
                onNext: function (tab, navigation, index) {
                    alert_success.hide();
                    alert_error.hide();
                    if (wizform.valid() == false) {
                        return false;
                    }
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    $('.stepHeader', $('#' + target)).text('Step ' + (index + 1) + ' of ' + total);
                    jQuery('li', $('#' + target)).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }
                    if (current == 1) {
                        $('#' + target).find('.prevBtn').hide();
                    } else {
                        $('#' + target).find('.prevBtn').show();
                    }
                    if (current >= total) {
                        $('#' + target).find('.nextBtn').hide();
                        $('#' + target).find('.submitBtn').show();
                        //最后一步
                        if (!isEmpty(confirm) && (typeof confirm == "function")) {
                            confirm();
                        }
                    } else {
                        $('#' + target).find('.nextBtn').show();
                        $('#' + target).find('.submitBtn').hide();
                    }
                },
                onPrevious: function (tab, navigation, index) {
                    alert_success.hide();
                    alert_error.hide();
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    $('.stepHeader', $('#' + target)).text('Step ' + (index + 1) + ' of ' + total);
                    jQuery('li', $('#' + target)).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }
                    if (current == 1) {
                        $('#' + target).find('.prevBtn').hide();
                    } else {
                        $('#' + target).find('.prevBtn').show();
                    }
                    if (current >= total) {
                        $('#' + target).find('.nextBtn').hide();
                        $('#' + target).find('.submitBtn').show();

                        //最后一步
                        if (!isEmpty(confirm) && (typeof confirm == "function")) {
                            confirm();
                        }
                    } else {
                        $('#' + target).find('.nextBtn').show();
                        $('#' + target).find('.submitBtn').hide();
                    }
                },
                onTabClick: function (tab, navigation, index) {
                    tab.show();
                    navigation.find("li.done").removeClass("done");
                    $.each(navigation.find("li"), function (idx, el) {
                        if (idx - index < 0) {
                            $(el).addClass("done");
                        }
                    })
                    return true;
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#' + target).find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#' + target).find('.prevBtn').hide();
            $('#' + target + ' .submitBtn').click(function () {
                if (!isEmpty(callback) && (typeof callback == "function")) {
                    callback();
                }
            }).hide();
        }
    };
}();