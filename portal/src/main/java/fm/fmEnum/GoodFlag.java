package fm.fmEnum;

/**
 * @修改人：CM
 * @修改时间：2017/4/15 17:58
 */
public enum GoodFlag {
    HOME_AD("首页广告商品"),HOME_NEW("首页新品推荐"),HOME_HOT("首页热销商品");

    private String desc;

    GoodFlag(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
