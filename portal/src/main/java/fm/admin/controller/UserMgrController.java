package fm.admin.controller;

import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.dto.UserAuthInfo;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.UserService;
import fm.yichenet.mongo.service.MerchantService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.security.SecurityUtil;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CM on 17/5/12.
 */
@Controller
@RequestMapping("/user/mgr")
public class UserMgrController extends BaseController {
    private static Logger LOGGER = LoggerFactory.getLogger(UserMgrController.class);
    @Autowired
    UserService userService;

    @Autowired
    MerchantService merchantService;

    @RequestMapping("/index/{registerType}")
    public String index(ModelMap modelMap, @PathVariable Integer registerType
    ) {

        modelMap.put("type", registerType);
        modelMap.put("shopUsers", userService.getByTypeAndUserId(null, null,
                3, null, null, null, null, null));
        return "/admin/user/index";
    }


    @ResponseBody
    @RequestMapping("/user/{id}")
    public Map getUserById(@PathVariable Long id) {
        Map res = new HashMap();
        try {
            if (id == null) {
                throw new BizException("参数缺失!");
            }
            WxUser user = userService.getById(id);
            res.put("data", user);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/list/{userType}")
    public Map userList(Integer pageSize, Integer pageNum,
                        Long shopId, Integer reviewStatus, String keyword,
                        String startTime, String endTime, @PathVariable(value = "userType") Integer userType) {
        Map res = new HashMap();
        try {
            if (!isAdmin()) {
                shopId = getCurrentUserId();
            }
            long now = System.currentTimeMillis();
            if (StringUtils.isNotEmpty(keyword)) {
                keyword = URLDecoder.decode(keyword, "UTF-8");
            }
            List<WxUser> userList = userService.getByTypeAndUserId(keyword, reviewStatus, userType, shopId, startTime, endTime, pageSize, pageNum);
            long total = userService.countUser(keyword, reviewStatus, userType, shopId, startTime, endTime, pageSize, pageNum);
            res.put("total", total);
            LOGGER.info("cost time : {}ms", System.currentTimeMillis() - now);
            res.put("data", userList);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/authinfo")
    public Map getUserAuthInfo(Long userId) {
        Map res = new HashMap();
        try {
            if (userId == null) {
                throw new BizException("参数缺失！");
            }
            res.put("data", userService.getUserAuthInfo(userId));
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping("/user/authinfo/updateOrAdd/{targetUserType}")
    public Map userAuthUpdate(UserAuthInfo userAuthInfo, @PathVariable Integer targetUserType) {
        Map res = new HashMap();
        try {
            if (userAuthInfo == null || targetUserType == null) {
                throw new BizException("参数缺失");
            }
            WxUser user = userService.getById(userAuthInfo.getUser_id());
            if (user == null) {
                throw new BizException("未找到相关用户信息，操作失败！");
            }
            DBObject authInfo = null;
            switch (targetUserType) {
                case 2:
                    authInfo = userAuthInfo.getEngineerAuthInfo();
                    break;
                case 3:
                case 4:
                    authInfo = userAuthInfo.getMerchantAuthInfo();
                    break;
                default:
                    throw new BizException("错误的认证类型");
            }
            userService.insertUserAuthInfo(userAuthInfo.getUser_id(), authInfo, targetUserType);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/authinfo/review")
    public Map reviewAuthInfo(Long userId, Integer reviewStatus, String reviewNotes) {
        Map res = new HashMap();
        try {
            WxUser user = userService.getById(userId);
            if (user == null) {
                throw new BizException("未找到相关用户信息，审核失败!");
            }
            Map<String,Object> query = new HashedMap();
            query.put("id",user.getId());
            String companyName = merchantService.getMerchantAuthName(query);
            user.setNickname(companyName);
            user.setReviewStatus(reviewStatus);
            user.setReviewNotes(reviewNotes);
            userService.saveOrUpdate(user);
            res.put("data",user);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


}
