package fm.admin.controller;

import com.mongodb.DBObject;
import fm.cache.AreaCache;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.service.UserService;
import fm.web.CurrentRequest;
import fm.yichenet.mongo.service.MerchantUserService;
import fm.yichenet.service.AdminUserService;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by CM on 17/5/30.
 */
@Controller
@RequestMapping("/per-shop")
public class PersonShopMgrController extends BaseController {
    @Autowired
    UserService userService;
    @Autowired
    AdminUserService adminUserService;
    @Autowired
    MerchantUserService merchantUserService;

    @RequestMapping("/index")
    public String index(ModelMap modelMap) throws Exception {
        modelMap.put("citys", AreaCache.getAllProvince());
        modelMap.put("uid", CurrentRequest.getCurrentUserId());
        return "/admin/per-shop/index";
    }


    @RequestMapping("/portal")
    public String portalConfig(ModelMap modelMap) throws Exception {
        WxUser user = (WxUser) CurrentRequest.getCurrentUser();
        modelMap.put("portal_img", user.getPortalImgUrl());
        modelMap.put("id", user.getId());

        DBObject authInfo = merchantUserService.getAuthInfoByUserIdAndType(user.getId(), user.getUserType());
        if (authInfo != null) {
            modelMap.put("authInfo", authInfo);
            modelMap.put("defaultPoint", authInfo.get("loc"));

        } else {
            modelMap.put("defaultPoint", user.getLatitude() + "," + user.getLongitude());
        }

        return "/admin/per-shop/portal";
    }

    @RequestMapping("/aut")
    public String aut(ModelMap modelMap) {
        return "/admin/per-shop/aut";
    }


}
