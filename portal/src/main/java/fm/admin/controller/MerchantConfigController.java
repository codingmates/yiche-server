package fm.admin.controller;

import fm.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 店铺配置控制器
 * Created by CM on 2017/6/9.
 */
@Controller
@RequestMapping("/merchant")
public class MerchantConfigController extends BaseController {


    @RequestMapping("/index")
    public String index(ModelMap modelMap) {

        return "/admin/merchant";
    }
}
