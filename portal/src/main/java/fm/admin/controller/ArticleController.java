package fm.admin.controller;

import com.alibaba.fastjson.JSON;
import com.mongodb.DBObject;
import fm.cache.ConfigCache;
import fm.controller.BaseController;
import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.util.CommonUtils;
import fm.web.CurrentRequest;
import fm.web.MediaTypes;
import fm.yichenet.dto.ArticleDto;
import fm.yichenet.mongo.service.ArticleService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by 宏炜 on 2017-05-26.
 */
@Controller
@RequestMapping("article")
public class ArticleController extends BaseController {

    @Autowired
    ArticleService articleService;

    @ResponseBody
    @RequestMapping(value = "mergeArticle", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String mergeArticle(@RequestBody ArticleDto articleDto, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            articleService.mergeArticle(articleDto);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "topArticle", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String topArticle(@RequestParam(value = "id", required = true) String id, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            articleService.topArticle(id);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "unTopArticle", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String unTopArticle(@RequestParam(value = "id", required = true) String id, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            articleService.unTopArticle(id);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "getArticleList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getArticleList(@RequestParam(value = "pageNum", required = false) Integer pageNum,
                                 @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                 @RequestParam(value = "type", required = true) Integer type,
                                 @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
                                 @RequestParam(value = "uuid", required = false, defaultValue = "0") Long uuid,
                                 ModelMap modelMap) throws Exception {
        try {
            keyword = URLDecoder.decode(keyword, "utf-8");
            Map<String, Object> params = new HashedMap();

            if (!"".equals(keyword) && !CommonUtils.isEmpty(keyword)) {
                if (type.equals(1)) {
                    MCondition mc = MCondition.create(MRel.regex).append("main_title", Pattern.compile(keyword));
                    params.putAll(mc.toDBObject().toMap());
                    MCondition mcSub = MCondition.create(MRel.regex).append("sub_title", Pattern.compile(keyword));
                    params.putAll(mcSub.toDBObject().toMap());
                } else {
                    MCondition mcCon = MCondition.create(MRel.regex).append("content", Pattern.compile(keyword));
                    params.putAll(mcCon.toDBObject().toMap());
                }
            }
            Object user = CurrentRequest.getCurrentLoginUser();
            if (user instanceof WxUser) {
                params.put("uuid", getCurrentUserId());
            } else {
                if (!uuid.equals(0L)) {
                    params.put("uuid", uuid);
                }
            }

            params.put("type", type);
            List<DBObject> articles = articleService.getArticleForPage(params, pageSize, pageNum);
            Long total = articleService.countArticles(params);
            modelMap.put("data", articles);
            modelMap.put("total", total);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 获取图文文章详情
     *
     * @param article_id
     * @param modelMap
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "getArticleInfo", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getArticleInfo(@RequestParam(value = "article_id", required = true) String article_id, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            DBObject articleInfo = articleService.getArticleInfo(article_id);
            articleInfo.removeField("likes");
            articleInfo.removeField("comments");
            modelMap.put("data", articleInfo);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    @ResponseBody
    @RequestMapping(value = "delArticle", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String delArticle(@RequestParam(value = "id", required = true) String id, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            articleService.deleteArticle(id);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }
}
