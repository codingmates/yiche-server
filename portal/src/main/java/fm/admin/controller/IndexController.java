package fm.admin.controller;

import fm.controller.BaseController;
import fm.entity.VisitorLog;
import fm.service.VisitorLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 文件描述：管理员界面控制器
 * 更新时间：2017/2/23
 * 更新人： codingmates@gmail.com .
 */
@Controller
@RequestMapping("/admin/")
public class IndexController extends BaseController {
    @Autowired
    private VisitorLogService visitorLogService;

    @RequestMapping("index")
    public String index(ModelMap modelMap) throws Exception {
        modelMap.put("user", super.getCurrentUser());
        return "admin/index";
    }

    @RequestMapping("home")
    public String home(ModelMap modelMap) throws Exception {
        modelMap.put("visitorCount", visitorLogService.todayVisitor());
        return "admin/home";
    }


    @RequestMapping("wechat/config")
    public String wechatConfig(ModelMap modelMap) throws Exception {
        modelMap.put("pn", getCurrentUser());
        return "admin/wx-config";
    }

    @RequestMapping("config")
    public String config(ModelMap modelMap) throws Exception {
        modelMap.put("pn", getCurrentUser());
        return "admin/basic-config";
    }

}
