package fm.admin.controller;

import com.alibaba.fastjson.JSON;
import fm.controller.BaseController;
import fm.entity.Section;
import fm.exception.BizException;
import fm.yichenet.service.SectionService;
import fm.web.MediaTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-05-29.
 */
@Controller
@RequestMapping("/sectionMgr")
public class SectionMgrController extends BaseController {


    @Autowired
    SectionService sectionService;

    /**
     * 新增更新板块信息
     *
     * @param section
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/mergeSection", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String mergeSection(@RequestBody Section section) {
        Map res = new HashMap();
        try {

            Section source = sectionService.getById(section.getId());
            source.setStatus(section.getStatus());
            source.setSectionDesc(section.getSectionDesc());
            source.setSectionName(section.getSectionName());
            source.setSectionImage(section.getSectionImage());
            sectionService.updateSection(section);
            this.success(res);
        } catch (BizException ex) {
            this.failed(res, ex);
        }
        return JSON.toJSONString(res);
    }


    @ResponseBody
    @RequestMapping(value = "/addSection", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map addSection(@RequestBody Section section) {
        Map res = new HashMap();
        try {
            section.setStatus(0);
            sectionService.addSection(section);
            res.put("data", section);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/list")
    public Map getSectionList() {
        Map res = new HashMap();
        try {
            List sections = sectionService.getAllSection(null);
            res.put("data", sections);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        return "/admin/forum/section";
    }
}
