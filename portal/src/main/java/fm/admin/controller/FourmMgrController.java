package fm.admin.controller;

import fm.cache.AreaCache;
import fm.controller.BaseController;
import fm.entity.Section;
import fm.service.UserService;
import fm.yichenet.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 社区管理 控制器
 * Created by CM on 17/5/30.
 */
@Controller
@RequestMapping("/forum")
public class FourmMgrController extends BaseController {

    @Autowired
    SectionService sectionService;

    @Autowired
    UserService userService;

    @RequestMapping("/carousel")
    public String index(ModelMap modelMap) {
        modelMap.put("citys", AreaCache.getAllProvince());

        return "/admin/forum/carousel";
    }

    @RequestMapping("/article")
    public String article(ModelMap modelMap) {
        List<Section> sectionList = sectionService.getAllSection(1);
        modelMap.put("sectionList",sectionList);

        List merchantList = userService.getAllMerchantUser();
        modelMap.put("merchantList",merchantList);
        return "/admin/forum/article";
    }

    @RequestMapping("/picArticle")
    public String picArticle(ModelMap modelMap) {
        List<Section> sectionList = sectionService.getAllSection(1);
        modelMap.put("sectionList",sectionList);
        return "/admin/forum/picArticle";
    }

    @RequestMapping("/section")
    public String section(ModelMap modelMap) {

        return "/admin/forum/section";
    }

}
