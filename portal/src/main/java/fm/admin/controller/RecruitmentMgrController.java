package fm.admin.controller;

import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.exception.BizException;
import fm.web.CurrentRequest;
import fm.yichenet.mongo.service.RecruitmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CM on 2017/6/30.
 */
@Controller
@RequestMapping("/recruit/mgr")
public class RecruitmentMgrController extends BaseController {

    @Autowired
    RecruitmentService recruitmentService;


    @RequestMapping("/index")
    public String index(ModelMap modelMap) throws Exception {
        modelMap.put("uid", CurrentRequest.getCurrentUserId());
        return "/admin/recruit/index";
    }


    @ResponseBody
    @RequestMapping("/add")
    public Map recruitmentAdd(String title, String salary, String worktime, String content) {
        Map res = new HashMap();
        try {

            DBObject data = recruitmentService.addRecurit(title, salary, worktime, content);
            res.put("data", data);

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/del")
    public Map recruitmentDelete(String id) {
        Map res = new HashMap();
        try {
            recruitmentService.delRecurit(id);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

}
