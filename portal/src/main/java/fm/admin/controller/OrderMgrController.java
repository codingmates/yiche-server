package fm.admin.controller;

import com.alibaba.fastjson.JSON;
import fm.controller.BaseController;
import fm.entity.OrderGood;
import fm.entity.WxUser;
import fm.entityEnum.OrderEnum;
import fm.exception.BizException;
import fm.util.DateUtils;
import fm.web.CurrentRequest;
import fm.yichenet.service.OrderService;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by CM on 2017/6/19.
 */
@Controller
@RequestMapping("/order")
public class OrderMgrController extends BaseController {
    @Autowired
    OrderService orderService;

    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        HashMap statusDesc = new HashMap();
        for (OrderEnum i : OrderEnum.values()) {
            statusDesc.put(i.toString(), i.getDesc());
        }

        modelMap.put("status", statusDesc);
        return "/admin/order/index";
    }

    @ResponseBody
    @RequestMapping("/list")
    public Map orderIndex(Double minAmount, Long shopId,
                          Double maxAmount, String startTime, String endTime, String phone,
                          Integer pageSize, Integer pageNum, Long targetShop, String status) {
        Map res = new HashMap();
        try {
            if (CurrentRequest.getCurrentUser() instanceof WxUser) {
                //商户权限
                shopId = CurrentRequest.getCurrentUserId();
            } else {
                if (targetShop != null) {
                    shopId = targetShop;
                }
            }
            Date startTimeDate = StringUtils.isEmpty(startTime) ? null : DateUtils.StringToDate(startTime, "yyyy-MM-dd HH:mm:ss");
            Date endTimeDate = StringUtils.isEmpty(endTime) ? null : DateUtils.StringToDate(endTime, "yyyy-MM-dd HH:mm:ss");

            HashMap statusDesc = new HashMap();
            for (OrderEnum i : OrderEnum.values()) {
                statusDesc.put(i.toString(), i.getDesc());
            }
            res.put("statusDesc", statusDesc);

            res.put("data", orderService.getShopOrder(pageSize, pageNum, shopId, minAmount, maxAmount,
                    startTimeDate,
                    endTimeDate, phone, status));
            res.put("total", orderService.countShopOrder(shopId, minAmount, maxAmount,
                    startTimeDate,
                    endTimeDate, phone, status));
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


}
