package fm.admin.controller;

import fm.controller.BaseController;
import fm.token.AdminToken;
import fm.token.MerchantToken;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;


/**
 * 管理端登陆控制器
 * <p>
 * Created by CM on 17/5/7.
 */
@Controller
@RequestMapping("/admin/login")
public class AdminLoginController extends BaseController {


    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(ModelMap modelMap, String err_msg) {
        if(SecurityUtils.getSubject().isAuthenticated()){
            return "redirect:/admin/index";
        }
        return "admin/login/login";
    }
    @RequestMapping("/out")
    public String index(ModelMap modelMap){
        SecurityUtils.getSubject().logout();
        return "admin/login/login";
    }


    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public String index(HttpServletRequest request, String phone, String vrcd, String pass) {
        try {
            if (StringUtils.isNotEmpty(vrcd)) {
                //登录验证码校验
                Subject subject = SecurityUtils.getSubject();
                subject.login(new MerchantToken(phone, vrcd));
            } else if (StringUtils.isNotEmpty(pass)) {
                //密码校验
                Subject subject = SecurityUtils.getSubject();
                subject.login(new AdminToken(phone, pass));
            } else {
                //参数缺失
                return "redirect:/admin/login?err_msg=parameter lost!";
            }
        } catch (UnknownAccountException ex) {
            return "redirect:/admin/login?err_msg=account/password invalid";
        } catch (Exception ex) {
            LOGGER.error("登录发生错误：", ex);
            return "redirect:/admin/login?err_msg=" + ex.getMessage();

        }

        return "redirect:/admin/index";
    }


}
