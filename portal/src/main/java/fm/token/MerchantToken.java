package fm.token;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

/**
 * Created by CM on 17/5/10.
 */
public class MerchantToken implements HostAuthenticationToken, RememberMeAuthenticationToken{
    private String phone;
    private String vrcd;

    public MerchantToken(String phone, String vrcd) {
        this.phone = phone;
        this.vrcd = vrcd;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public boolean isRememberMe() {
        return false;
    }

    @Override
    public Object getPrincipal() {
        return phone;
    }

    @Override
    public Object getCredentials() {
        return vrcd;
    }
}
