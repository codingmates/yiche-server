package fm.token;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

/**
 * Created by CM on 17/5/8.
 */
public class AdminToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
    private String phone;
    private String pass;

    public AdminToken(String phone, String pass) {
        this.phone = phone;
        this.pass = pass;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public boolean isRememberMe() {
        return false;
    }

    @Override
    public Object getPrincipal() {
        return phone;
    }

    @Override
    public Object getCredentials() {
        return pass;
    }
}
