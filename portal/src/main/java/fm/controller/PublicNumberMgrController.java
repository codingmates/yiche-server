package fm.controller;

import com.alibaba.fastjson.JSON;
import fm.entity.PublicNumber;
import fm.service.PublicNumberService;
import fm.web.MediaTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * 类名：fm.controller.PublicNumberMgrController
 * 创建者： CM .
 * 创建时间：2016/3/8
 */
@Controller
@RequestMapping("/mgr/public")
public class PublicNumberMgrController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PublicNumberMgrController.class);
    @Autowired
    PublicNumberService publicNumberService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST, headers = {"uuid"}, produces = MediaTypes.JSON_UTF_8)
    public String detail(ModelMap modelMap, @RequestHeader String uuid) {
        try {
            PublicNumber publicNumber = publicNumberService.getByUuid(uuid);
            HashMap<String, Object> data = new HashMap<>();
            data.put("detail", publicNumber);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (Exception ex) {
            LOGGER.error("public number detail error", ex);
            this.failed(modelMap, "server error!");
        }
        return JSON.toJSONString(modelMap);
    }
}
