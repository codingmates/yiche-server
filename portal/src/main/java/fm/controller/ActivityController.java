package fm.controller;

import com.alibaba.fastjson.JSON;
import fm.dto.ActivityAndFlows;
import fm.entity.Activity;
import fm.entityEnum.Carrier;
import fm.exception.BizException;
import fm.service.ActivityAndFlowsService;
import fm.service.FlowService;
import fm.service.RecorderParticipationService;
import fm.util.CommonUtils;
import fm.util.Constant;
import fm.web.MediaTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类名：fm.controller.ActivityController
 * 创建者： CM .
 * 创建时间：2016/3/6
 */
@Controller
@RequestMapping("/activity")
public class ActivityController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityController.class);
    @Autowired
    ActivityAndFlowsService activityAndFlowsService;

    @Autowired
    FlowService flowService;

    @Autowired
    RecorderParticipationService recorderParticipationService;


    @RequestMapping(value = "", method = RequestMethod.GET)
    public String indexPage(ModelMap modelMap) {

        return "/index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(ModelMap modelMap) {

        return "/add";
    }

    /**
     * 活动参与页面数据接口
     *
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/example", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String index(ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            Map<String, Object> data = new HashMap<>();
            //注入数据
            if (true) {
                throw new BizException("错误原因");
            }
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            LOGGER.error("new occur error", ex);
            this.failed(modelMap, ex);
        }
        //返回数据
        return JSON.toJSONString(modelMap);
    }


    /**
     * 新建活动以及奖品
     *
     * @param activityAndFlows
     * @param modelMap
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/newActivity", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String newActivity(@RequestBody ActivityAndFlows activityAndFlows, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            Map<String, Object> data = new HashMap<>();
            Map activity = activityAndFlowsService.addActivityAndFlows(activityAndFlows);
            data.put("activity", activity);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            LOGGER.error("new activity occur error", ex);
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 更新活动
     *
     * @param activityAndFlows
     * @param modelMap
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/updateActivity", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String updateActivity(@RequestBody ActivityAndFlows activityAndFlows, ModelMap modelMap) throws Exception {
        modelMap.clear();
        try {
            Map<String, Object> data = new HashMap<>();
            Map activity = activityAndFlowsService.updateActivityAndFlows(activityAndFlows);
            data.put("activity", activity);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            LOGGER.error("update activity occur error", ex);
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 删除活动
     *
     * @param id
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteActivity", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String deleteActivity(@RequestParam(value = "id", required = true) Long id, ModelMap modelMap) {
        modelMap.clear();
        try {
            activityAndFlowsService.deleteActivity(id);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 获取活动列表
     *
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/activityList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String activityList(@RequestParam(value = "page", required = true, defaultValue = "0") Integer page,
                               @RequestParam(value = "size", required = true, defaultValue = "10") Integer size,
                               ModelMap modelMap) {
        try {
            List<Activity> activityList = activityAndFlowsService.getActivityList(page, size);
            Long activityCount = activityAndFlowsService.getActivityCount();
            Map<String, Object> data = new HashMap<>();
            data.put("list", activityList);
            data.put("count", activityCount);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 活动数据和奖品数据
     *
     * @param activityId 活动id
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getActivityAndFlows", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getActivityAndFlows(@RequestParam(value = "activityId", required = true) Long activityId, ModelMap modelMap) {
        try {
            Activity activity = activityAndFlowsService.getActivity(activityId);
            Map<String, Object> result = new HashMap<>();
            result.put("title", activity.getTitle());
            result.put("status", activity.getStatus().getDesc());
            result.put("startTime", activity.getStartTime());
            result.put("endTime", activity.getEndTime());
            result.put("probability", activity.getProbability());
            result.put("count", activity.getCount());
            result.put("remainCount", activity.getRemainCount());
            result.put("times", activity.getTimes());
            result.put("description", activity.getDescription());
            result.put("link", activity.getLink());
            result.put("createTime", activity.getCreateTime());
            result.put("rule", activity.getRule());
            result.put("logoUrl", activity.getLogoUrl());
            result.put("id", activity.getId());
            result.put("version", activity.getVersion());
            result.put("exchangeLink", activity.getExchangeLink());
            List activityFlowList = activityAndFlowsService.getActivityFlowList(activityId);
            Long activityFlowCount = activityAndFlowsService.getActivityFlowCount(activityId);
            Map<String, Object> data = new HashMap<>();
            data.put("activity", result);
            data.put("list", activityFlowList);
            data.put("count", activityFlowCount);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 获取流量包列表
     *
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/flowList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String flowList(@RequestParam(value = "carrier", required = true) Carrier carrier, ModelMap modelMap) {
        try {
            List flowList = flowService.getFlowList(carrier);
            Map<String, Object> data = new HashMap<>();
            data.put("list", flowList);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }
        return JSON.toJSONString(modelMap);
    }

    /**
     * 用户参与明细
     *
     * @param activityId
     * @param page
     * @param size
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/participationList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String participationList(@RequestParam(value = "activityId", required = true) Long activityId,
                                    @RequestParam(value = "page", required = true) Integer page,
                                    @RequestParam(value = "size", required = true) Integer size, ModelMap modelMap) {
        try {
            if (CommonUtils.isEmpty(activityId)) {
                throw new BizException("活动不存在！");
            }
            List participationList = recorderParticipationService.getUserParticipationDetailList(page, size, activityId);
            Long participationCount = recorderParticipationService.getUserParticipationDetailCount(activityId);
            Map<String, Object> data = new HashMap<>();
            data.put("list", participationList);
            data.put("count", participationCount);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }

        return JSON.toJSONString(modelMap);
    }

    /**
     * 兑奖明细列表
     *
     * @param activityId
     * @param page
     * @param size
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/exchangedList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String exchangedList(@RequestParam(value = "activityId", required = true) Long activityId,
                                @RequestParam(value = "page", required = true) Integer page,
                                @RequestParam(value = "size", required = true) Integer size, ModelMap modelMap) {
        try {
            if (CommonUtils.isEmpty(activityId)) {
                throw new BizException("活动不存在！");
            }
            List exchangedList = recorderParticipationService.getUserAwardingDetailsList(page, size, activityId);
            Long exchangedCount = recorderParticipationService.getUserAwardingDetailsCount(activityId);
            Map<String, Object> data = new HashMap<>();
            data.put("list", exchangedList);
            data.put("count", exchangedCount);
            modelMap.put("data", data);
            this.success(modelMap);
        } catch (BizException ex) {
            this.failed(modelMap, ex);
        }

        return JSON.toJSONString(modelMap);
    }

    /**
     * 获取活动详情
     *
     * @param id
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getActivity", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getActivity(@RequestParam(value = "id", required = true) Long id, ModelMap modelMap) {
        try {
            Map<String, Object> data = new HashMap<>();
            Activity activity = activityAndFlowsService.getActivity(id);
            if (CommonUtils.isEmpty(activity)) {
                this.failed(modelMap, "请求的活动不存在");
            } else {
                data.put("activity", activity);
                modelMap.put("data", data);
                this.success(modelMap);
            }
        } catch (Exception ex) {
            this.failed(modelMap);
        }
        return JSON.toJSONString(modelMap);
    }

}
