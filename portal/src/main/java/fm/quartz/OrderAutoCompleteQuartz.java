package fm.quartz;

import fm.entity.OrderGood;
import fm.yichenet.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by 宏炜 on 2017-09-25.
 * 自动确认收货定时器
 */
@Component
public class OrderAutoCompleteQuartz {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAutoCompleteQuartz.class);

    @Autowired
    OrderService orderService;

    public void run() {
        LOGGER.debug("订单自动确认定时器启动 ...");
        try {
            List<OrderGood> orderGoods = orderService.getAutoCompleteOrderGoodList();
            for(OrderGood orderGood : orderGoods){
                orderService.updateAutoCompleteOrderGood(orderGood);
            }
        } catch (Exception ex) {
            LOGGER.error("订单自动确认定时器异常:", ex);
        }
        LOGGER.debug("订单自动确认定时器结束 ...");
    }
}
