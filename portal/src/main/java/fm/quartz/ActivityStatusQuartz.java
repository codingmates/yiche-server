package fm.quartz;

import fm.entity.Activity;
import fm.service.ActivityAndFlowsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ActivityStatusQuartz {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityStatusQuartz.class);

    public static boolean RUNNING = false;

    @Autowired
    ActivityAndFlowsService activityAndFlowsService;

    private static final long EXPIRE_TIME = 10 * 1000;

    public void run() {
        if(RUNNING){
            return;
        }
        LOGGER.debug("activity status auto expire task start working ...");
        RUNNING = true;
        try {
            List<Activity> activities = activityAndFlowsService.getAllActivity();
            for (Activity activity : activities) {
                activityAndFlowsService.updateActivityStatus(activity);
            }
        } catch (Exception ex) {
            LOGGER.error("activity status auto expire task occur error:", ex);
        }
        RUNNING = false;
        LOGGER.debug("activity status auto expire task finish working ...");
    }
}
