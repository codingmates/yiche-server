package fm.dto;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Created by CM on 17/5/15.
 */
public class UserAuthInfo {
    private Long user_id;
    private String company_name;
    private String address;
    private String charge_person_name;
    private String telephone;
    private String business_license_pic;
    private String idcard_pic;
    private String[] pics;
    private String desc;

    //以下为维修技师的认证资料
    private String real_name;
    private String[] employ_expes;
    private String[] skills;
    private String idcard;
    private String idcard_img1;
    private String idcard_img2;

    //公用字段
    private String phone_number;
    private String cret_card;


    public UserAuthInfo() {
    }


    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCharge_person_name() {
        return charge_person_name;
    }

    public void setCharge_person_name(String charge_person_name) {
        this.charge_person_name = charge_person_name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getBusiness_license_pic() {
        return business_license_pic;
    }

    public void setBusiness_license_pic(String business_license_pic) {
        this.business_license_pic = business_license_pic;
    }

    public String getIdcard_pic() {
        return idcard_pic;
    }

    public void setIdcard_pic(String idcard_pic) {
        this.idcard_pic = idcard_pic;
    }

    public String[] getPics() {
        return pics;
    }

    public void setPics(String[] pics) {
        this.pics = pics;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCret_card() {
        return cret_card;
    }

    public void setCret_card(String cret_card) {
        this.cret_card = cret_card;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String[] getEmploy_expes() {
        return employ_expes;
    }

    public void setEmploy_expes(String[] employ_expes) {
        this.employ_expes = employ_expes;
    }

    public String[] getSkills() {
        return skills;
    }

    public void setSkills(String[] skills) {
        this.skills = skills;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getIdcard_img1() {
        return idcard_img1;
    }

    public void setIdcard_img1(String idcard_img1) {
        this.idcard_img1 = idcard_img1;
    }

    public String getIdcard_img2() {
        return idcard_img2;
    }

    public void setIdcard_img2(String idcard_img2) {
        this.idcard_img2 = idcard_img2;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public DBObject getEngineerAuthInfo() {
        DBObject authInfo = new BasicDBObject("user_id", this.user_id);
        authInfo.put("real_name", real_name);

        authInfo.put("employ_expes", this.employ_expes);
        authInfo.put("skills", this.skills);
        authInfo.put("idcard", this.idcard);

        authInfo.put("idard_img1", this.idcard_img1);
        authInfo.put("idcard_img2", this.idcard_img2);
        authInfo.put("phone_number", this.phone_number);

        return authInfo;
    }


    public DBObject getMerchantAuthInfo() {
        DBObject authInfo = new BasicDBObject("user_id", this.user_id);
        authInfo.put("phone_number", this.phone_number);

        authInfo.put("company_name", this.company_name);
        authInfo.put("address", this.address);
        authInfo.put("telephone", this.telephone);

        authInfo.put("charge_person_name", this.charge_person_name);
        authInfo.put("cret_card", this.cret_card);
        authInfo.put("business_license_pic", this.business_license_pic);

        authInfo.put("idcard_pic", this.idcard_pic);
        authInfo.put("pics", this.pics);
        authInfo.put("desc", this.desc);

        return authInfo;
    }
}
