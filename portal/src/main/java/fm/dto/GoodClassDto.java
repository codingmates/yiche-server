package fm.dto;

import java.util.List;

/**
 * 文件描述：
 * 更新时间：2017/4/2
 * 更新人： codingmates@gmail.com .
 */
public class GoodClassDto {
    private String title;
    private long id;
    private String icon;
    private Boolean hidePrice;
    private List<GoodClassDto> children;

    public GoodClassDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<GoodClassDto> getChildren() {
        return children;
    }

    public void setChildren(List<GoodClassDto> children) {
        this.children = children;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getHidePrice() {
        return hidePrice;
    }

    public void setHidePrice(Boolean hidePrice) {
        this.hidePrice = hidePrice;
    }
}
