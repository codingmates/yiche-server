package fm.realm;

import com.alibaba.fastjson.JSON;
import fm.yichenet.service.AdminUserService;
import fm.cache.RoleCache;
import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.PublicNumberService;
import fm.token.AdminToken;
import fm.token.MerchantToken;
import fm.util.Constant;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * 类名： fm.realm.UserRealm
 * 创建人： CM
 * 创建时间： 2016/3/6.
 */
public class UserRealm extends AuthorizingRealm {

    private static Logger LOGGER = LoggerFactory.getLogger(UserRealm.class);

    @Autowired
    AdminUserService userService;
    @Autowired
    PublicNumberService publicNumberService;
    @Autowired
    HttpSession session;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String phone = (String) principals.getPrimaryPrincipal();
        List<String> roles = new ArrayList<>();
        if (principals.getRealmNames().contains("ADMIN")) {
            roles.addAll(RoleCache.getPermision("ADMIN"));
        } else if (principals.getRealmNames().contains("MERCHANT")) {
            roles.addAll(RoleCache.getPermision("MERCHANT"));
        }

        LOGGER.info("authorization finnish:{} ,user roles : {}", principals.getPrimaryPrincipal(), JSON.toJSONString(roles));

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRoles(roles);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        try {

            SimpleAuthenticationInfo info = null;
            if (token instanceof AdminToken) {
                AdminToken adminToken = (AdminToken) token;

                if (!userService.checkPass(adminToken.getPrincipal().toString(), adminToken.getCredentials().toString())) {
                    throw new UnknownAccountException("invalid account/password!");
                }
                PublicNumber user = publicNumberService.getByPhone(token.getPrincipal().toString());
                session.setAttribute(Constant.SESSION_LOGIN_USER, user);
                info = new SimpleAuthenticationInfo(adminToken.getPrincipal(), adminToken.getCredentials(), "ADMIN");

            } else if (token instanceof MerchantToken) {
                MerchantToken merchantToken = (MerchantToken) token;

                if (!userService.checkSms(merchantToken.getPrincipal().toString(), merchantToken.getCredentials().toString())) {
                    throw new UnknownAccountException("invalid sms code or account!");
                }
                WxUser user = userService.getMerchantByPhone(token.getPrincipal().toString());
                session.setAttribute(Constant.SESSION_LOGIN_USER, user);

                info = new SimpleAuthenticationInfo(merchantToken.getPrincipal(), merchantToken.getCredentials(), "MERCHANT");

            }

            return info;
        } catch (UnknownAccountException ex) {
            throw ex;
        } catch (BizException ex) {
            throw new UnknownAccountException(ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("安全校验发生错误：", ex);
            throw new RuntimeException("account check error!");
        }

    }


    @Override
    public boolean supports(AuthenticationToken token) {
        if (token instanceof AdminToken || token instanceof MerchantToken) {
            return true;
        } else {
            return super.supports(token);
        }
    }
}
