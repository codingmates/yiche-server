package fm.user;

import com.mongodb.DBObject;
import fm.cache.AreaCache;
import fm.dao.HibernateBaseDao;
import fm.dao.MongoBaseDao;
import fm.entity.Address;
import fm.entity.WxUser;
import fm.mongo.MongoTable;
import fm.service.UserService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-08-09.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "classpath:spring-data-mongo.xml"})
public class userTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(userTest.class);

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Autowired
    HibernateBaseDao hibernateBaseDao;
    @Autowired
    UserService userService;

    @Test
    public void testQuery() {
        try {
            List<DBObject> list = (List<DBObject>) mongoBaseDao.findAll(DBObject.class, MongoTable.merchant_auth_info);
            for (DBObject dbObject : list) {
                Long userId = Long.parseLong(String.valueOf(dbObject.get("id")));
                try {
                    String hql = "from WxUser where id = ?";
                    WxUser wxUser = (WxUser) hibernateBaseDao.getUnique(hql, userId);
                    if (wxUser == null) {
                        continue;
                    }
//                    if (new Long(2).equals(wxUser.getReviewStatus())) {
                    String companyName = (String) dbObject.get("company_name");

                    if (!StringUtils.isBlank(companyName)) {
                        wxUser.setNickname(companyName);
//                            hibernateBaseDao.update(wxUser);
                        userService.saveOrUpdate(wxUser);
                    }
                    LOGGER.info("用户:{}更新公司名:{}", userId, companyName);
//                    }
                } catch (Exception ex) {
                    LOGGER.error("用户:{}更新失败", userId, ex);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("发生错误:", e);
        }
    }

    @Test
    public void recoverUserName() {
        try {
            List<DBObject> list = (List<DBObject>) mongoBaseDao.findAll(DBObject.class, MongoTable.community_article);
            for (DBObject article : list) {
                Long uuid = (Long) article.get("uuid");
                String nickname = (String) article.get("user_nickname");
                try {
                    WxUser user = userService.getById(uuid);
                   if (user != null) {
                        user.setNickname(nickname);
                        userService.saveOrUpdate(user);
                    } else {
                        LOGGER.error("未找到用户:{}", uuid);
                    }

                } catch (Exception ex) {
                    LOGGER.error("用户更新失败:{}", uuid);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("发生错误:", ex);
        }
    }


    @Test
    public void updateUserProvinceInfo() {
        AreaCache.loadCache();


        List<WxUser> users = userService.getAllMerchantUserFull();

        for (WxUser user : users) {
            if (StringUtils.isNotEmpty(user.getCity()) && StringUtils.isEmpty(user.getProvince())) {
                LOGGER.info("更新用户:{}省份:{},", user.getNickname(), user.getProvince());

            }
        }
    }


}
