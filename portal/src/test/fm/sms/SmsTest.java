package fm.sms;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.entityEnum.ActionTypeEnum;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.util.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * Created by CM on 2017/8/8.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "classpath:spring-data-mongo.xml"})
public class SmsTest {

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Test
    public void testCheckSms() {
        try {
            MCondition mc = MCondition.create(MRel.and);
            mc.append("phone", "18359219330");
            mc.append("type", ActionTypeEnum.LOGIN_VERIFY.toString());
            mc.append("check_status", 0);
            mc.append(MCondition.create(MRel.lte).append("latm", new Date()));
            mc.append(MCondition.create(MRel.gte).append("latm", DateUtils.addMinute(new Date(), -50)));

            List<DBObject> items = (List<DBObject>) mongoBaseDao.getPageList(mc.toDBObject().toMap(), DBObject.class, 1, 1, MongoTable.sms_log, new BasicDBObject("latm", Sort.Direction.DESC.toString()));
            System.out.println("查询条件:{}" + mc.toDBObject().toString());
            if (items != null) {

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
