package fm.dao;

import com.mongodb.DBObject;
import fm.dto.GoodClassDto;
import fm.entity.WxUser;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.service.UserService;
import fm.util.CommonUtils;
import fm.yichenet.mongo.service.MerchantService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;
import java.util.regex.Pattern;

/**
 * 文件描述：
 * 更新时间：2017/4/3
 * 更新人： codingmates@gmail.com .
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "classpath:spring-data-mongo.xml"})
public class MongoOperationTest {
    @Autowired
    MongoBaseDao mongoBaseDao;

    @Test
    public void testQuery() {
        Map params = new HashMap();
        params.put("uuid", "wx190240352cbffde3");
        try {
            Collection<DBObject> data = mongoBaseDao.getList(params, DBObject.class, MongoTable.cyw_purchase);

            System.out.println("end");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInsert() {
        Map params = new HashMap();
        params.put("uuid", "wx190240352cbffde3");
        GoodClassDto goodClassDto = new GoodClassDto();
        goodClassDto.setId(1);
        goodClassDto.setTitle("測試");
        params.put("list", goodClassDto);
        try {
            mongoBaseDao.insert(params, MongoTable.goodClass);
            System.out.println("end");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void queryDao() {
        String keyword = null;
        Long timestamp = null;
        Long section_id = null;
        MCondition mc = MCondition.create(MRel.and);
        if (StringUtils.isNotEmpty(keyword)) {
            MCondition keywordMatchCondition = MCondition.create(MRel.or);
            keywordMatchCondition.append(MCondition.create(MRel.regex).append("main_title", Pattern.compile(keyword)));
            keywordMatchCondition.append(MCondition.create(MRel.regex).append("sub_title", Pattern.compile(keyword)));
            keywordMatchCondition.append(MCondition.create(MRel.regex).append("content", Pattern.compile(keyword)));
            mc.append(keywordMatchCondition);
        }
        if (timestamp != null) {
            mc.append(MCondition.create(MRel.lte).append("create_time", new Date(timestamp)));
        }

        if (!CommonUtils.isEmpty(section_id)) {
            mc.append("section_id", section_id);
        }

        System.out.println(mc.toDBObject().toString());
    }

    @Autowired
    UserService userService;
    @Autowired
    MerchantService merchantService;

    /**
     * 补充老平台商户用户地址信息
     */
    @Test
    public void shopAddressAsyncTest() {
        List<WxUser> merchants = userService.getAllMerchantUserFull();
        if (CollectionUtils.isNotEmpty(merchants)) {
            for (WxUser merchant : merchants) {
                try {
                    Map param = new HashMap();
                    param.put("id", merchant.getId());
                    Map merchantAuthInfo = merchantService.getMerchantAuthInfo(param);
                    String district = merchantAuthInfo.containsKey("district") ? (String) merchantAuthInfo.get("district") : null;
                    String city = merchantAuthInfo.containsKey("city") ? (String) merchantAuthInfo.get("city") : null;
                    if (district != null && city != null && StringUtils.isEmpty(merchant.getCity()) && StringUtils.isEmpty(merchant.getDistrict())) {
                        merchant.setCity(city);
                        merchant.setDistrict(district);
                        userService.saveOrUpdate(merchant);
                        System.out.println("更新用户信息:" + merchant.getNickname() + "|" + city + "-" + district);
                    }
                } catch (Exception ex) {
                    System.out.println("商户信息同步错误:" + merchant.getId());
                }

            }
        }
    }
}
