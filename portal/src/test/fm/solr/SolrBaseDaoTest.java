package fm.solr;

import fm.dao.SolrBaseDao;
import fm.util.CacheUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CM on 17/5/6.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "classpath:spring-data-mongo.xml"})
public class SolrBaseDaoTest {


    @Test
    public void testAnalysis(){
        CacheUtils.loadAllCache();

        String field = "Context";
        String value = "品质值得推广";
        try {
            SolrBaseDao.analysisQuery("buyerscomments",field,value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public  void testQuery(){
        CacheUtils.loadAllCache();

        try{
            Map param = new HashMap<>();
            param.put("Context","品质值得推荐");
            List<Map<String,Object>> result = SolrBaseDao.queryFromeSolrByPage("buyerscomments",param,null,null,10,0,null);


            assert result != null;
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
