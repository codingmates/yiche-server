package fm.huanxin;

import fm.cache.ConfigCache;
import fm.huanxin.api.MessageApi;
import fm.huanxin.task.ChatHistoryAsyncTask;
import fm.huanxin.utils.OrgInfo;
import io.swagger.client.ApiException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by CM on 17/6/3.
 */
public class ChatHistoryAsyncTaskTest {

    @Test
    public void testRun() {
        ConfigCache.loadProjectConfig();
        ChatHistoryAsyncTask task = new ChatHistoryAsyncTask();
        task.run();
    }

    @Test
    public void testSendMsg() {
        try {

        ConfigCache.loadProjectConfig();
        OrgInfo.init();
        MessageApi.sendForumMsg("新动态", "wx_63");
        }catch (Exception ex){
            ex.printStackTrace();
            throw ex;
        }

    }
}
