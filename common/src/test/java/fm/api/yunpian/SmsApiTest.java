package fm.api.yunpian;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * class name： fm.api.yunpian.SmsApiTest
 * author： CM
 * create time： 2016/3/6.
 */
public class SmsApiTest {

    /**
     * 测试流量订购通知短信
     */
    @Test
    public void testSendFlowBuySms() {
        JSONObject result = null;
        try {
            result = SmsApi.sendSms("18359219330", 500L);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(result.toJSONString());
    }

    /**
     * 测试流量订购通知短信
     */
    @Test
    public void customerSmsTest() {
        JSONObject result = null;
        try {
            result = SmsApi.sendSms("18359219330", "【CM工作室】由CM工作室提供的流量营销平台，为您充值了100M流量，请注意查收！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(result.toJSONString());
    }
}
