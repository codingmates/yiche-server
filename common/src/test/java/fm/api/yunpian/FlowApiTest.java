package fm.api.yunpian;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

/**
 * 类名：fm.api.yunpian.FlowApiTest
 * 创建者： CM .
 * 创建时间：2016/3/8
 */
public class FlowApiTest {


    @Test
    public void testFlowBuy() {
        //测试订购10M流量包
        FlowBuyMessage msg= null;
        try {
            msg = FlowApi.buyFlow("18359219330", "1008601", "");
            System.out.println("response :" + JSON.toJSONString(msg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
