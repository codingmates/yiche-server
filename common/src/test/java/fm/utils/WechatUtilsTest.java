package fm.utils;

import fm.model.LongToShortMsgModel;
import fm.util.WechatUtils;
import org.junit.Test;

/**
 * 类名：fm.utils.WechatUtilsTest
 * 创建者： CM .
 * 创建时间：2016/3/28
 */
public class WechatUtilsTest {

    @Test
    public void testLongToShortUrl() {
        try {
//            String accessToken = WechatUtils.getBaseAccessToken("wxd580621f8ac54849", "473e13641d350b391075ac15fbc7a478", Constant.GRANT_TYPE_CLIENT_CREDENTIAL, null);
            String accessToken = "tUsdeW10x4J78u7YQOrMHcGZKC5lA3oj1Zi19rG5bWgeKvmYijaVzF7qcF2zU64N8C1sYW5avffxWT0UjvMrB7E5AZGL-SckWvXB9iImR1aUcnAnVDfxQrKmSXJ6jMRgTBYhABAGOL";
            String longUrl = "https://www.cdmates.com/activity/flow/37?uuid=gh_5814f658b0f5&shareId=2";
            LongToShortMsgModel lts = WechatUtils.longToShortUrl(longUrl, accessToken);
            System.out.println("shortUrl:" + lts.getShort_url().replaceAll("\\\\\\\\",""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
