package fm.mongoService.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.mongoService.InviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;

@Service
public class InviteServiceImpl implements InviteService {

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Override
    public void addMsg(DBObject msg) throws Exception {
        mongoBaseDao.insert(msg, MongoTable.invite_msg);
    }

    @Override
    public Collection getList(Integer mgr) throws Exception {
        MCondition mc = MCondition.create(MRel.and);
        if (mgr != null) {
            return mongoBaseDao.findAll(DBObject.class, MongoTable.invite_msg);
        } else {
            mc.append("del", "0");
            return mongoBaseDao.getList(mc.toDBObject().toMap(), DBObject.class, MongoTable.invite_msg);
        }
    }

    @Override
    public void del(String msgId) throws Exception {
        mongoBaseDao.updateOne(new BasicDBObject("msg_id", msgId).toMap(), new BasicDBObject("del", "1").toMap(), MongoTable.invite_msg);
    }
}
