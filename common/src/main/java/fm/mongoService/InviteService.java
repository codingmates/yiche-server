package fm.mongoService;

import com.mongodb.DBObject;

import java.util.Collection;

public interface InviteService {

    void addMsg(DBObject msg) throws Exception;


    Collection getList(Integer mgr) throws Exception;

    void del(String msgId) throws Exception;
}
