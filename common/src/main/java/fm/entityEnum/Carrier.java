package fm.entityEnum;

/**
 * 类名：fm.entityEnum.Carrier
 * 创建者： CM .
 * 创建时间：2016/3/9
 */
public enum Carrier {
    YIDONG("中国移动"), DIANXIN("中国电信"), LIANTONG("中国联通");

    private String desc;

    Carrier(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
