package fm.entityEnum;

/**
 * Created by CM on 17/5/22.
 */
public enum CarouselType {

    LINK("链接"), GOOD("商品"),ARTICLE("文章");

    private String desc;

    CarouselType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
