package fm.entityEnum;

/**
 * 类名：fm.entityEnum.ActivityStatus
 * 创建者： CM .
 * 创建时间：2016/3/8
 */
public enum ActivityStatus {
    PROCESS("进行中"),DELETED("已删除"),OVERDUE("已结束"),NOSTARTED("未开始");
    private String desc;

    ActivityStatus(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
