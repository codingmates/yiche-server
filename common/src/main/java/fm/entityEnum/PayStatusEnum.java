package fm.entityEnum;

/**
 * Created by 宏炜 on 2017-06-19.
 */
public enum PayStatusEnum {
    UNPAID("未支付"),
    PAID("已支付"),
    PAYING("支付中"),
    PAY_TIMEOUT("支付超时"),
    PAY_FAILED("支付失败"),
    WRONG_FEE("金额错误");

    private String desc;

    PayStatusEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
