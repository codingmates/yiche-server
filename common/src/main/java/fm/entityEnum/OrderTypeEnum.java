package fm.entityEnum;

/**
 * Created by 宏炜 on 2017-06-19.
 */
public enum OrderTypeEnum {
    EDIT_ORDER("商户改价订单"),
    ONCE_ORDER("一口价订单");

    private String desc;

    OrderTypeEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
