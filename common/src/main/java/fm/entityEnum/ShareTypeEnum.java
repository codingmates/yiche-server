package fm.entityEnum;

/**
 * 类名：fm.entityEnum.ShareTypeEnum
 * 创建者： CM .
 * 创建时间：2016/3/26
 */
public enum ShareTypeEnum {
    LINK, TD_CODE;

    ShareTypeEnum() {
    }
}
