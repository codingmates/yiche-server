package fm.entityEnum;

/**
 * Created by 宏炜 on 2017-09-25.
 */
public enum CompleteEnum {
    USER_COMPLETE("用户确认"),
    AUTO_COMPLETE("自动确认");
    private String desc;

    CompleteEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
