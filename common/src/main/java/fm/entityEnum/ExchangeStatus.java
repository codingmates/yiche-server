package fm.entityEnum;

/**
 * Created by 宏炜 on 2016-03-09.
 */
public enum ExchangeStatus {
    EXCHANGING("兑奖中"),EXCHANGED("已兑奖"),NOEXCHANGE("未兑奖"),EXPIRED("已失效");
    private String desc;

    ExchangeStatus(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
