package fm.entityEnum;

/**
 * Created by CM on 17/5/22.
 */
public enum CarouselScope {

    PORTAL("平台首页"), SHOP("商铺首页"), FORUM("社区首页");


    private String desc;


    CarouselScope(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
