package fm.entityEnum;

/**
 * Created by 宏炜 on 2017-06-19.
 */
public enum OrderEnum {

    WAIT_EDIT_PRICE("提交待改价"),
    UNPAID("待支付"),
    PAYING("支付中"),
    PAYMENT_SUCCESS("支付成功"),
    PAYMENT_FAIL("支付失败"),
    PAYMENT_TIMEOUT("支付超时"),
    DELIVERED("已发货"),
    RECED_CONFIRMATION("已收货"),
    ORDER_CANCEL("订单取消"),
    ORDER_REFUND("申请退款"),
    ORDER_REFUNDED("退款申请已处理");

    private String desc;

    OrderEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }


}
