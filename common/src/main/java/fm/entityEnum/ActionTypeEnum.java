package fm.entityEnum;

/**
 * Created by CM on 17/5/7.
 */
public enum ActionTypeEnum {
    LOGIN_VERIFY("登录短信验证码"), REGISTER_VERIFY("注册短信验证码"), NORMAL("验证码"),
    TRADE_ORDER("商品交易订单通知"), TEC_SERVICE_ORDER("维修订单通知"),ORDER_POSTS("订单发货通知");
    private String desc;

    ActionTypeEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return this.desc;
    }
}
