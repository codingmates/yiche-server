package fm.entityEnum;

/**
 * Created by CM on 17/5/26.
 */
public enum  GoodFlagEnum {

    HOT_GOOD("热门商品",10),NEW_GOOD("新品推荐",8),RECOMMEND("猜你喜欢",6);
    private String desc;
    private int num;

    GoodFlagEnum(String desc,int num) {
        this.num = num;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public int getNum() {
        return num;
    }
}
