package fm.entityEnum;

/**
 * Created by 宏炜 on 2017-10-26.
 */
public enum RepairEnum {
    WITHOUT_CHECK("未接单"),
    WAIT_REPAIR("等待维修"),
    REPAIR_COMPLETE("维修完成"),
    PAYMENT_FAIL("支付失败"),
    PAYMENT_TIMEOUT("支付超时"),
    REPAIR_TIMEOUT("维修超时"),
    PAYMENT_SUCCESS("支付成功");

    private String desc;

    RepairEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
