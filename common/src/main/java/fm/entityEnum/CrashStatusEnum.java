package fm.entityEnum;

/**
 * Created by CM on 2017/6/29.
 */
public enum CrashStatusEnum {
    WAIT_CHECK("等待审核"), SUCCESS("审核通过"), FAILED("被驳回");
    private String desc;

    CrashStatusEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
