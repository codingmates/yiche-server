package fm.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by CM on 2017/7/30.
 */
@Configuration
public class MainConfig {

    public final static String weChatApp = "weChatApp";
    public final static String iosApp = "iosApp";
    public final static String androidApp = "androidApp";
    public final static String tokenState = "tokenState";
    public final static String appPhone = "appPhone";

    public final static String openIdAesKey = "HBxZFtSAi7HevhteoHBS+w==";
    public final static String tokenAesKey = "MukFqjzn1DwbB2qUnDzMVQ==";

    public final static String hxOrgName = "1120170507115452";

    public final static String hxAppName = "yiche";

    @Value("#{wxProperties.appid}")
    private String appid;

    @Value("#{wxProperties.appsecret}")
    private String appsecret;

    @Value("#{wxProperties.partener_id}")
    private String partenerId;

    @Value("#{wxProperties.partener_key}")
    private String partenerKey;

    @Value("#{wxProperties.domain}")
    private String domain;

    @Value("#{wxProperties.mobileAppid}")
    private String mobileAppid;

    @Value("#{wxProperties.mobileAppsecret}")
    private String mobileAppsecret;

    @Value("#{wxProperties.mobilePartenerId}")
    private String mobilePartenerId;

    @Value("#{wxProperties.mobilePartenerKey}")
    private String mobilePartenerKey;



    /**
     * 如果出现 org.springframework.beans.BeanInstantiationException
     * https://github.com/Wechat-Group/weixin-java-tools-springmvc/issues/7
     * 请添加以下默认无参构造函数
     */
    protected MainConfig(){}

//    /**
//     * 为了生成自定义菜单使用的构造函数，其他情况Spring框架可以直接注入
//     *
//     * @param appid
//     * @param appsecret
//     * @param token
//     * @param aesKey
//     */
//    protected MainConfig(String appid, String appsecret, String token, String aesKey) {
//        this.appid = appid;
//        this.appsecret = appsecret;
//        this.token = token;
//        this.aesKey = aesKey;
//    }

    @Bean
    public WxMpConfigStorage wxMpConfigStorage() {
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId(this.appid);
        configStorage.setSecret(this.appsecret);
        return configStorage;
    }

    @Bean
    public WxPayConfig payConfig() {
        WxPayConfig configStorage = new WxPayConfig();
        configStorage.setAppId(this.appid);
        configStorage.setMchId(this.partenerId);
        configStorage.setMchKey(this.partenerKey);
        return configStorage;
    }

    @Bean
    public WxMpService wxMpService() {
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }

    @Bean
    public WxPayService payService() {
        WxPayService payService = new WxPayServiceImpl();
        payService.setConfig(payConfig());
        return payService;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }

    public String getPartenerId() {
        return partenerId;
    }

    public void setPartenerId(String partenerId) {
        this.partenerId = partenerId;
    }

    public String getPartenerKey() {
        return partenerKey;
    }

    public void setPartenerKey(String partenerKey) {
        this.partenerKey = partenerKey;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getMobileAppid() {
        return mobileAppid;
    }

    public void setMobileAppid(String mobileAppid) {
        this.mobileAppid = mobileAppid;
    }

    public String getMobileAppsecret() {
        return mobileAppsecret;
    }

    public void setMobileAppsecret(String mobileAppsecret) {
        this.mobileAppsecret = mobileAppsecret;
    }

    public String getMobilePartenerId() {
        return mobilePartenerId;
    }

    public void setMobilePartenerId(String mobilePartenerId) {
        this.mobilePartenerId = mobilePartenerId;
    }

    public String getMobilePartenerKey() {
        return mobilePartenerKey;
    }

    public void setMobilePartenerKey(String mobilePartenerKey) {
        this.mobilePartenerKey = mobilePartenerKey;
    }
}

