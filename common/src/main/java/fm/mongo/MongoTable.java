package fm.mongo;

/**
 * @修改人：CM
 * @修改时间：2017/2/26 21:29
 */
public final class MongoTable {

    /**
     * 婚礼邀请函的留言表
     */
    public static final String invite_msg = "invite_msg";
    /**
     * 车衣网商品信息表
     */
    public static final String cyw_products = "cyw_products";
    /**
     * 车衣网社区数据
     */
    public static final String cyw_purchase = "cyw_purchase";
    /**
     * 微信用户表
     */
    public static final String wechatuser = "wechatuser";

    /**
     * 关于界面联系人信息
     */
    public static final String contact = "contact_info";
    /**
     * 关于界面地图配置
     */
    public static final String aboutConfig = "about_config";
    /**
     * 首页配置
     */
    public static final String homeConfig = "home_config";
    /**
     * 商品分类表
     */
    public static final String goodClass = "good_class";
    /**
     * 商品表
     */
    public static final String good = "shop_good";
    /**
     * 短信发送记录表
     */
    public static final String sms_log = "sms_log";

    /**
     * 商户认证资料
     */
    public static final String merchant_auth_info = "merchant_auth_info";

    /**
     * 维修技师认证资料
     */
    public static final String engineer_auth_info = "engineer_auth_info";

    /**
     * 轮播图存储表
     */
    public static final String carousel_img = "carousel_image";

    /**
     * 图文／文章表
     */
    public static final String community_article = "community_article";

    /**
     * 首页商铺
     */
    public static final String portal_shop = "portal_shop";
    /**
     * 图文文章评论表
     */
    public static final String article_comment = "article_comment";
    /**
     * 首页推广文章
     */
    public static final String portal_article = "info_article";
    /**
     * 维修技师技能
     */
    public static final String engineer_skill = "engineer_skill";
    /**
     * 维修技师工作经验
     */
    public static final String engineer_experience = "engineer_experience";
    /**
     * 商品评论
     */
    public static final String good_comment = "good_comment";
    /**
     * 购物车
     */
    public static final String shopping_cart = "shopping_cart";
    /**
     * 收货地址
     */
    public static final String receive_address = "receive_address";

    /**
     * 店铺收藏
     */
    public static final String shop_collection = "shop_collection";

    /**
     * 物流方式配置表
     */
    public static final String express = "express";
    /**
     * 店铺招聘列表页面
     */
    public static final String shopRecuritment = "shop_recuritment";

    /**
     * 环信信息推送表（系统、社区）
     */
    public static final String huanxin_message = "huanxin_message";

    /**
     * 商品浏览记录表，记录类别、搜索关键字、店铺之类的信息，用于商品的推荐
     * 数据来源于商品详情、订单下单等接口
     * 猜你喜欢接口的数据根据此类记录信息来检索商品，并返回给前端
     */
    public static final String good_visit_log = "good_visit_log";

    /**
     * 网站商务合作记录
     */
    public static final String business_log = "bussiness_log";

    /**
     * 维修订单图片表
     */
    public static final String order_repair_pic = "order_repair_pic";

    /**
     * 维修订单评价表
     */
    public static final String order_repair_comments = "order_repair_comments";
}
