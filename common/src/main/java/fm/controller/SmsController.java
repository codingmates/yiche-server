package fm.controller;

import fm.entity.WxUser;
import fm.exception.BizException;
import fm.yichenet.mongo.service.SmsService;
import fm.service.UserService;
import fm.util.ValidateCode;
import fm.web.MediaTypes;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by CM on 17/5/7.
 */
@Controller
@RequestMapping("/sms")
public class SmsController extends BaseController {

    @Autowired
    SmsService smsService;
    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping(value = "/send", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map sendLoginVerifyCode(String phone, String action) {
        Map res = new HashMap();
        try {
            if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(action)) {
                throw new BizException("参数缺失");
            }
            WxUser merchant = userService.getMerchantByPhone(phone);
            if(merchant == null){
                throw new BizException("该手机未在平台绑定过商户帐号，请先绑定后再登录!");
            }
            smsService.businessSmsSend(phone, action);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }

        return res;
    }

    @ResponseBody
    @RequestMapping(value = "/validate", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public Map validateCodeImage(HttpServletRequest request) {
        Map res = new HashMap();
        try {
            String path = request.getSession().getServletContext().getRealPath("/resources/upload/validateCode/");
            File parent = new File(path);
            if (!parent.exists()) {
                parent.mkdirs();
            }
            ValidateCode code = new ValidateCode(160, 30, 4, 10);
            code.write(path + "/" + code.getCode() + ".png");
            res.put("data", code.getCode());
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
