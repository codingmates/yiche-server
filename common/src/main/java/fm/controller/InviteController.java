package fm.controller;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.exception.BizException;
import fm.mongoService.InviteService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/invite")
public class InviteController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InviteController.class);
    @Autowired
    InviteService inviteService;

    @ResponseBody
    @RequestMapping("/msg/add")
    public Map sendLoginVerifyCode(String name, String coun, String phone, String msg) {
        Map res = new HashMap();
        try {

            DBObject param = new BasicDBObject();

            param.put("name", name == null ? "不知道是谁" : name);
            param.put("coun", coun == null ? "没填哦" : coun);
            param.put("phone", phone == null ? "--" : phone);
            param.put("msg", msg == null ? "--" : msg);
            param.put("msg_id", UUID.randomUUID().toString().replace("-", ""));
            param.put("latm", new Date());
            param.put("del", "0");
            inviteService.addMsg(param);

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @RequestMapping("/msg/mgr")
    public String index(ModelMap modelMap, Integer mgr) throws Exception {
        List<DBObject> msgs = (List<DBObject>) inviteService.getList(mgr);

        Collections.sort(msgs, new Comparator<DBObject>() {
            @Override
            public int compare(DBObject o1, DBObject o2) {
                Long latm1 = o1.containsField("latm") ? ((Date) o1.get("latm")).getTime() : 0L;

                Long latm2 = o2.containsField("latm") ? ((Date) o2.get("latm")).getTime() : 0L;
                return latm1 - latm2 > 0 ? -1 : 1;
            }
        });
        modelMap.put("data", msgs);
        if (mgr != null) {
            return "/access";
        }
        return "/msg";
    }

    @ResponseBody
    @RequestMapping("/msg/del")
    public Map sendLoginVerifyCode(String msgId) {
        Map res = new HashMap();
        try {
            if (StringUtils.isNotEmpty(msgId)) {
                inviteService.del(msgId);
            }
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


}
