package fm.controller;

import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.web.CurrentRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @修改人：CM
 * @修改时间： 2015/4/9 11:29.
 */
public class BaseController {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());


    protected Object getCurrentUser() throws Exception {
        return CurrentRequest.getCurrentLoginUser();
    }

    protected long getCurrentUserId() throws Exception {
        Object user = CurrentRequest.getCurrentLoginUser();
        if (user instanceof WxUser) {
            return ((WxUser) user).getId();
        } else if (user instanceof PublicNumber) {
            return ((PublicNumber) user).getId();
        } else {
            throw new BizException("错误的用户类型");
        }
    }

    protected boolean isAdmin() throws Exception {
        Object user = CurrentRequest.getCurrentLoginUser();
        if (user instanceof WxUser) {
            return false;
        } else if (user instanceof PublicNumber) {
            return true;
        } else {
            throw new BizException("错误的用户类型");
        }
    }


    /**
     * 成功
     *
     * @param map
     */
    public void success(Map map) {
        map.put("result", "0");
        map.put("state", "SUCCESS");
    }

    /**
     * 不可预期的错误
     *
     * @param map
     */
    public void failed(Map map) {
        map.put("result", "-1");
        map.put("msg", "服务器发生未知错误，请求失败!");
    }

    /**
     * 业务异常
     *
     * @param map
     * @param e
     */
    public void failed(Map map, BizException e) {
        map.put("result", "-3");
        map.put("msg", e.getMessage());
    }

    /**
     * 已知异常
     *
     * @param map
     * @param errorMsg
     */
    public void failed(Map map, String errorMsg) {
        map.put("result", "-2");
        map.put("msg", errorMsg);
    }

    protected Map getEmptyParamMap() {
        return new ConcurrentHashMap();
    }


}
