package fm.yichenet.dto;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.commons.collections.map.HashedMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-05-26.
 */
public class ArticleDto {

    private String id;
    private Integer type;
    private List<String> pics;
    private String main_title;
    private String sub_title;
    private String content;
    private String portal_img;
    private Date create_time;
    private Long uuid;
    private Long section_id;
    private Integer is_top;
    private String user_nickname;
    private String user_head_img_url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPortal_img() {
        return portal_img;
    }

    public void setPortal_img(String portal_img) {
        this.portal_img = portal_img;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Long getSection_id() {
        return section_id;
    }

    public void setSection_id(Long section_id) {
        this.section_id = section_id;
    }

    public Integer getIs_top() {
        return is_top;
    }

    public void setIs_top(Integer is_top) {
        this.is_top = is_top;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public String getUser_nickname() {
        return user_nickname;
    }

    public void setUser_nickname(String user_nickname) {
        this.user_nickname = user_nickname;
    }

    public String getUser_head_img_url() {
        return user_head_img_url;
    }

    public void setUser_head_img_url(String user_head_img_url) {
        this.user_head_img_url = user_head_img_url;
    }

    public DBObject toDBObject(){
        DBObject dbObject = new BasicDBObject();
        dbObject.put("id",this.id);
        dbObject.put("type",this.type);
        dbObject.put("pics",this.pics);
        dbObject.put("content",this.content);
        dbObject.put("portal_img",this.portal_img);
        dbObject.put("create_time",this.create_time);
        dbObject.put("uuid",this.uuid);
        dbObject.put("section_id",this.section_id);
        dbObject.put("is_top",this.is_top);
        dbObject.put("main_title",this.main_title);
        dbObject.put("sub_title",this.sub_title);
        dbObject.put("user_nickname",this.user_nickname);
        dbObject.put("user_head_img_url",this.user_head_img_url);
        return dbObject;
    }
}
