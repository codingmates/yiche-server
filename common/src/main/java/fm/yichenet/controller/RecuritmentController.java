package fm.yichenet.controller;

import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.exception.BizException;
import fm.yichenet.mongo.service.RecruitmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CM on 2017/7/1.
 */
@Controller
@RequestMapping("/recruit")
public class RecuritmentController extends BaseController {

    @Autowired
    RecruitmentService recruitmentService;

    @ResponseBody
    @RequestMapping("/list")
    public Map getList(String keyword, Long shopId,
                        Integer pageSize,
                       Integer pageNum) {
        Map res = new HashMap();
        try {

            List<DBObject> data = recruitmentService.recuritList(keyword, shopId, pageNum, pageSize);
            res.put("data", data);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
