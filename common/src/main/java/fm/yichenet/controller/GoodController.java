package fm.yichenet.controller;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.cache.ConfigCache;
import fm.cache.GoodClassCache;
import fm.controller.BaseController;
import fm.dao.SolrBaseDao;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.exception.TokenInvalidException;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.nio.SemaphoreExecutor;
import fm.service.UserService;
import fm.web.CurrentRequest;
import fm.yichenet.mongo.service.GoodMgrService;
import fm.yichenet.mongo.service.ShopMgrService;
import fm.yichenet.mongo.service.VisitHistoryService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * Created by CM on 17/5/31.
 */
@Controller
@RequestMapping("/good")
public class GoodController extends BaseController {
    private SemaphoreExecutor executor = new SemaphoreExecutor(30, "goodInfoQueryThread");
    @Autowired
    private GoodMgrService goodMgrService;
    @Autowired
    private ShopMgrService shopMgrService;
    @Autowired
    private UserService userService;
    @Autowired
    VisitHistoryService visitHistoryService;

    @ResponseBody
    @RequestMapping("/list")
    public Map goodList(String goodName, Integer status, Long shopId, String classId, String bigType,
                        @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                        @RequestParam(required = false, defaultValue = "10") Integer pageSize, String orderField,
                        @RequestParam(value = "orderType", required = false, defaultValue = "1") Integer orderType,
                        String loc, Double distance) {
        Map res = new HashMap();
        try {
            Map param = new HashMap();


            try {
                WxUser user = (WxUser) CurrentRequest.getCurrentUser();
                if (user != null) {
                    DBObject log = new BasicDBObject();
                    if (StringUtils.isNotEmpty(goodName)) {
                        log.put("keyword", goodName);
                    }
                    if (StringUtils.isNotEmpty(classId)) {
                        log.put("good_class_id", classId);
                    }
                    if (StringUtils.isNotEmpty(bigType)) {
                        log.put("bigType", bigType);
                    }
                    log.put("visit_time", new Date());
                    log.put("wx_user_id", user.getId());
                    visitHistoryService.addVisitLog(log);
                }
            } catch (TokenInvalidException ex) {
            } catch (Exception ex) {
                LOGGER.error("获取用户登陆信息失败:", ex);
            }


            if (status != null) {
                param.put("status", status + "");
            }

            if (shopId != null) {
                param.put("wx_user_id", shopId);
            }

            if (StringUtils.isNotEmpty(goodName)) {
                param.put("goodName", goodName);
            }

            if (StringUtils.isNotEmpty(classId)) {
                param.put("good_class_id", classId);
            }

            if (StringUtils.isNotEmpty(loc)) {
                param.put("loc", loc);
            }
            if (StringUtils.isNotEmpty(bigType)) {
                param.put("bigType", bigType);
            }


            Map<String, SolrQuery.ORDER> orderObject = null;
            if (StringUtils.isNotEmpty(orderField)) {
                orderObject = new LinkedHashMap<>();
                SolrQuery.ORDER order = SolrQuery.ORDER.asc;
                if (orderType == -1) {
                    order = SolrQuery.ORDER.desc;
                }
                orderObject.put(orderField, order);
            }
            if (distance != null) {
                param.put("distance", distance + "");
            }
            List<Map<String, String>> result = SolrBaseDao.queryFromeSolrByPage(MongoTable.good, param,
                    null, null, pageSize, (pageNum - 1) * pageSize, orderObject);

            if (CollectionUtils.isNotEmpty(result)) {

                final List<DBObject> goods = new ArrayList<>();
                final CountDownLatch cdl = new CountDownLatch(res.size());

                for (final Map<String, String> item : result) {
                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String goodId = item.get("good_id");


                                MCondition mc = MCondition.create(MRel.and);
                                mc.append("good_id", goodId);
                                DBObject object = goodMgrService.getGood(mc.toDBObject().toMap());

                                Long goodClassId = Long.parseLong(""+ object.get("good_class_id"));
                                //TODO 商品价格是否显示为0 要根据类别是否开启了隐藏价格 如果开启了此属性，该类商品的价格全部显示为零 ，订单流程也要按照待改价的商品来处理
                                if (GoodClassCache.isHidePrice(goodClassId)) {
                                    object.put("sale_price", 0);
                                    object.put("source_price", 0);
                                }


                                if (object != null) {

                                    Object id = object.get("wx_user_id");
                                    Long userId;

                                    if (id instanceof String) {
                                        userId = Long.parseLong((String) id);
                                    } else if (id instanceof Integer) {
                                        userId = Long.parseLong(id + "");
                                    } else {
                                        userId = (Long) id;
                                    }

                                    WxUser user = userService.getById(userId);
                                    String address = (String) object.get("address");
                                    if (StringUtils.isEmpty(address)) {
                                        if (user != null && StringUtils.isNotEmpty(user.getCity()) && StringUtils.isNotEmpty(user.getDistrict())) {
                                            address = "";
                                            address += (StringUtils.isEmpty(user.getCity()) ? "" : user.getCity());
                                            address += (StringUtils.isEmpty(user.getDistrict()) ? "" : "·" + user.getDistrict());
                                            BasicDBObject update = new BasicDBObject("address", address).append("good_id", goodId);
                                            if (user.getLatitude() != null && user.getLongitude() != null) {
                                                update.append("loc", user.getLatitude() + "," + user.getLongitude());
                                            }
                                            goodMgrService.updateGood(update.toMap());
                                            object.put("address", address);

                                        } else {
                                            object.put("address", "--");
                                        }
                                    }
                                    object.put("distance", item.get("distance"));
                                    if (!object.containsField("sale_amount")) {
                                        object.put("sale_amount", 0);
                                    }
                                    goods.add(object);
                                }
                            } catch (Exception ex) {
                                LOGGER.error("查询商品信息发生错误:", ex);
                            } finally {
                                cdl.countDown();
                            }
                        }
                    });
                }


                cdl.await();
                res.put("data", goods);

            } else {
                res.put("data", CollectionUtils.EMPTY_COLLECTION);
            }

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/class/list")
    public Map goodClassList() {
        Map res = new HashMap();
        try {
            Map param = new HashMap();

            param.put("uuid", 1L);
            DBObject all = shopMgrService.getGoodClassByUuid(param);

            res.put("data", all.get("list"));
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping("/like/list")
    public Map likeGoodList() {
        Map res = new HashMap();
        try {
            List goods = visitHistoryService.getLikeGoodListOfVisitLog();
            res.put("data", goods);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

}
