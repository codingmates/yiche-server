package fm.yichenet.controller;

import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.service.UserService;
import fm.web.CurrentRequest;
import fm.yichenet.mongo.service.MerchantUserService;
import fm.yichenet.service.AdminUserService;
import org.apache.commons.collections.MapUtils;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CM on 2017/6/9.
 */
@Controller
@RequestMapping("/per-shop/")
public class PersonShopController extends BaseController {

    @Autowired
    UserService userService;

    @Autowired
    AdminUserService adminUserService;

    @Autowired
    MerchantUserService merchantUserService;


    @ResponseBody
    @RequestMapping("/updatePortalImg")
    public Map updatePortalImg(String portalImgUrl, Long userId) {
        Map res = new HashMap();
        try {
            if (StringUtils.isEmpty(portalImgUrl) || userId == null) {
                throw new BizException("参数缺失");
            }

            WxUser user = userService.getById(userId);
            user.setPortalImgUrl(portalImgUrl);
            userService.saveOrUpdate(user);
            ((WxUser) CurrentRequest.getCurrentUser()).setPortalImgUrl(portalImgUrl);

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/update/merchant/address")
    public Map commitMerchantAddress(@RequestBody Map param) {
        Map res = new HashMap();
        try {
            if (MapUtils.isEmpty(param)) {
                throw new BizException("请填入地址");
            }
            merchantUserService.updateMerchantAuthInfo(param);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
