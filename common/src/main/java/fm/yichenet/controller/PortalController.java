package fm.yichenet.controller;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.controller.BaseController;
import fm.entityEnum.CarouselScope;
import fm.entityEnum.GoodFlagEnum;
import fm.exception.BizException;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.yichenet.mongo.service.ArticleService;
import fm.yichenet.mongo.service.CarouselService;
import fm.yichenet.mongo.service.GoodMgrService;
import fm.yichenet.mongo.service.ShopMgrService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by CM on 17/5/26.
 */
@Controller
@RequestMapping("/portal")
public class PortalController extends BaseController {


    @Autowired
    private GoodMgrService goodMgrService;
    @Autowired
    private ShopMgrService shopMgrService;
    @Autowired
    private CarouselService carouselService;
    @Autowired
    private ArticleService articleService;

    /**
     * 平台首页标记商品获取
     *
     * @param flag
     * @return
     */
    @ResponseBody
    @RequestMapping("/good/flag/list/{flag}")
    public Map getGoodList(@PathVariable GoodFlagEnum flag, String city) {
        Map res = new HashMap();
        try {
            MCondition mc = MCondition.create(MRel.and);
            mc.append("flags.name", flag.toString());
            if (StringUtils.isEmpty(city) == false) {
                mc.append("flags.city", city);
            }
            mc.append("status", "1");
            List<DBObject> goods = goodMgrService.getList(mc.toDBObject().toMap(), null, null);


            if (StringUtils.isNotEmpty(city)) {
                if (CollectionUtils.isEmpty(goods)) {
                    MCondition defaultCondition = MCondition.create(MRel.and);
                    defaultCondition.append("flags.name", flag.toString());
                    defaultCondition.append("flags.city", "default");
                    defaultCondition.append("status",1+"");
                    goods = goodMgrService.getList(defaultCondition.toDBObject().toMap(), null, null);

                }
            }


            res.put("data", goods);
            LOGGER.info("{} result:{}", flag.getDesc(), goods.size());
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/shop/flag/list")
    public Map getShopList(String city) {
        Map res = new HashMap();
        try {
            List<DBObject> flagShops = shopMgrService.getFlagShop(city);
            res.put("data", flagShops);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping("/carousel/list/{scope}")
    public Map getCarouselScopeList(@PathVariable CarouselScope scope, Long shopId, String city, String loc) {
        Map res = new HashMap();
        try {
            if (StringUtils.isEmpty(city)) {
                city = "default";
            }
            List<DBObject> object = carouselService.getList(null, scope, city, shopId, new Date());
            if (CollectionUtils.isEmpty(object)) {
                object = carouselService.getList(null, scope, "default", shopId, new Date());
            }
            res.put("data", object);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping("/info/list")
    public Map getInfoArticleList(String city, String loc) {
        Map res = new HashMap();
        try {
            DBObject condition = new BasicDBObject();
            if (StringUtils.isNotEmpty(city)) {
                condition.put("area", city);
            }

            List<DBObject> infos = articleService.getInfoArticleList(condition);
            if (CollectionUtils.isEmpty(infos)) {
                infos = articleService.getInfoArticleList(new BasicDBObject("area", "default"));
            }

            List<DBObject> articles = new ArrayList<>();
            if (!CollectionUtils.isEmpty(infos)) {
                for (DBObject obj : infos) {
                    try {
                        DBObject article = articleService.getArticleInfo((String) obj.get("article_id"));
                        if (article != null) {
                            article.putAll(obj);
                        }
                        articles.add(article);
                    } catch (BizException ex){
                        LOGGER.error("获取已经配置的文章信息发生错误:", ex.getMessage());
                        continue;
                    }catch (Exception ex) {
                        LOGGER.error("获取已经配置的文章信息发生错误:", ex);
                        continue;
                    }
                }
            }

            res.put("data", articles);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
