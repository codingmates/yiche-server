package fm.yichenet.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ConcurrentHashMultiset;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.cache.ConfigCache;
import fm.controller.BaseController;
import fm.dao.MongoBaseDao;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import fm.nio.SemaphoreExecutor;
import fm.service.UserService;
import fm.util.RequestHeader;
import fm.util.RequestUtils;
import fm.web.MediaTypes;
import fm.yichenet.mongo.service.GoodMgrService;
import fm.yichenet.mongo.service.MerchantService;
import fm.yichenet.mongo.service.ShopMgrService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

/**
 * @修改人：CM
 * @修改时间：2017/2/26 21:12
 */
@Controller
@RequestMapping("/cheyiwang/data/col")
public class DataCollectionController extends BaseController {
    private static Boolean COLLECTING = true;
    private static final Logger LOGGER = LoggerFactory.getLogger(DataCollectionController.class);
    @Autowired
    private MongoBaseDao mongoDao;
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/product", method = RequestMethod.POST, name = MediaTypes.JSON_UTF_8)
    private Map colProductData(ModelMap modelMap) {
        Map result = new HashMap();
        COLLECTING = true;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int page = 1;
                    int count = 0;
                    while (COLLECTING) {
                        try {
                            Map param = new HashMap();
                            param.put("p", page + "");
                            param.put("vtrade", "0");
                            param.put("action", "FrontGetList");
                            param.put("st", "2");
                            String url = "http://www.2016927.com/Admin/Base/Product.ashx";
                            Collection<DBObject> datas = colData(url, param, "Status", "ok", "ProList");
                            count += datas.size();
                            if (CollectionUtils.isEmpty(datas)) {
                                COLLECTING = false;
                            } else {

                                String startProId = (String) datas.iterator().next().get("ProId");
                                Map<String, Object> queryParam = new HashedMap();
                                queryParam.put("ProId", startProId);
                                if (mongoDao.findOne(queryParam, DBObject.class, MongoTable.cyw_products) != null) {
                                    COLLECTING = false;
                                } else {
                                    mongoDao.insertAll(datas, MongoTable.cyw_products);
                                    LOGGER.info("当前抓取第{}页 共 {}条", page + "", datas.size() + "");
                                }
                            }

                        } catch (Exception ex) {
                            COLLECTING = false;
                            LOGGER.error("抓取数据发生错误,当前已抓取数据{}页 共 {}条", page + "", count + "", ex);
                        } finally {
                            page++;
                        }
                    }
                    LOGGER.info("抓取数据任务停止,当前已抓取数据{}页 共 {}条", page + "", count + "");
                }
            }).start();
            this.success(result);
        } catch (Exception ex) {
            this.failed(result, ex.getMessage());
            LOGGER.error("抓取数据发生错误", ex);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/purchase", method = RequestMethod.POST, name = MediaTypes.JSON_UTF_8)
    private Map colPurchaseData(ModelMap modelMap) {
        Map result = new HashMap();
        COLLECTING = true;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int page = 131;
                    int count = 0;
                    while (COLLECTING) {
                        try {
                            Map param = new HashMap();
                            param.put("p", page + "");
                            param.put("k", "");
                            param.put("action", "FrontGetList");
                            String url = "http://www.2016927.com/Admin/Business/PurchaseInfo.ashx";
                            Collection<DBObject> datas = colData(url, param, "Status", "1", "List");
                            count += datas.size();
                            if (CollectionUtils.isEmpty(datas)) {
                                COLLECTING = false;
                            } else {

                                String startPID = (String) datas.iterator().next().get("pid");
                                Map<String, Object> queryParam = new HashedMap();
                                queryParam.put("pid", startPID);
                                if (mongoDao.findOne(queryParam, DBObject.class, MongoTable.cyw_purchase) != null) {
                                    COLLECTING = false;
                                } else {
                                    mongoDao.insertAll(datas, MongoTable.cyw_purchase);
                                    LOGGER.info("当前抓取第{}页 共 {}条", page + "", datas.size() + "");
                                }
                            }

                        } catch (Exception ex) {
                            COLLECTING = false;
                            LOGGER.error("抓取数据发生错误,当前已抓取数据{}页 共 {}条", page + "", count + "", ex);
                        } finally {
                            page++;
                        }
                    }
                    LOGGER.info("抓取数据任务停止,当前已抓取数据{}页 共 {}条", page + "", count + "");
                }
            }).start();
            this.success(result);
        } catch (Exception ex) {
            this.failed(result, ex.getMessage());
            LOGGER.error("抓取数据发生错误", ex);
        }
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "/task/stop", method = RequestMethod.POST, name = MediaTypes.JSON_UTF_8)
    private Map stopTask(ModelMap modelMap) {
        Map result = new HashMap();
        try {
            COLLECTING = false;
            this.success(result);
        } catch (Exception ex) {
            this.failed(result, ex.getMessage());
            LOGGER.error("抓取数据发生错误", ex);
        }
        return result;
    }


    private Collection<DBObject> colData(String url, Map param, String statusKey, String successValue, String listKey) throws Exception {
        String responseStr = RequestUtils.post(url, param);
        Map result = JSON.parseObject(responseStr, Map.class);
        Collection<DBObject> datas = new ArrayList<>();
        if (MapUtils.isNotEmpty(result) && successValue.equals(MapUtils.getString(result, statusKey, null).toLowerCase())) {
            Collection<JSONObject> data = (Collection) MapUtils.getObject(result, listKey);
            for (JSONObject obj : data) {
                DBObject dbObject = new BasicDBObject();
                dbObject.putAll(obj);
                datas.add(dbObject);
//                String proPic = obj.getString("ProPic");
//                if(StringUtils.isNotEmpty(proPic)){
//                    RequestUtils.httpRequestFile("http://www.2016927.com"+proPic,"D:\\项目\\wechat-website\\app\\target\\app-1.0.0\\resources\\cheyiwang\\"+proPic.replace("/","\\"));
//                }
            }
            if (CollectionUtils.isNotEmpty(datas)) {
                return datas;
            }
        }
        return CollectionUtils.EMPTY_COLLECTION;
    }

    final ConcurrentLinkedQueue<String> failedPicUri = new ConcurrentLinkedQueue<>();


    @ResponseBody
    @RequestMapping(value = "/img", method = RequestMethod.POST, name = MediaTypes.JSON_UTF_8)
    private Map colImg(ModelMap modelMap) {
        Map result = new HashMap();
        COLLECTING = true;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int count = 0;
                    final ConcurrentHashMultiset downloadedImage = ConcurrentHashMultiset.create();
                    try {
                        Collection<DBObject> products = mongoDao.findAll(DBObject.class, MongoTable.cyw_products);
                        Collection<DBObject> purchase = mongoDao.findAll(DBObject.class, MongoTable.cyw_purchase);

                        final CountDownLatch cdl = new CountDownLatch(products.size() + purchase.size());
                        SemaphoreExecutor executor = new SemaphoreExecutor(50, "picDownloadThread");
                        for (DBObject pro : products) {
                            final String pic = (String) pro.get("ProPic");
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (!downloadedImage.contains(pic)) {
                                            if (StringUtils.isNotEmpty(pic)) {

                                                RequestHeader header = new RequestHeader();
                                                header.setAccept("image/webp,image/*,*/*;q=0.8");
                                                header.setAcceptEncoding("gzip, deflate, sdch");
                                                header.setAcceptLanguage("zh-CN,zh;q=0.8");
                                                header.setConnection("keep-alive");
                                                header.setCookie("__cfduid=d4c6454d129c7e518d6a449c386a9fc7e1488074382; yjs_use_ob=0; user_lng=118.10388605; user_lat=24.48923061; Hm_lvt_6d3936097458e84313a0e6d1aa60f655=1488074383,1488119584; Hm_lpvt_6d3936097458e84313a0e6d1aa60f655=1488122314");
                                                header.setHost("www.2016927.com");
                                                header.setReferer("http://www.2016927.com/ProList.aspx");
                                                RequestUtils.httpRequestFile("http://www.2016927.com" + pic,
                                                        "D:\\项目\\wechat-website\\app\\target\\app-1.0.0\\resources\\cheyiwang\\" + pic.replace("/", "\\"),
                                                        header);
                                            }
                                        } else {
                                            LOGGER.info("已经下载过的图片:{}", pic);
                                        }
                                    } catch (Exception ex) {
                                        LOGGER.error("获取图片：{}发生错误，已放入失败队列", pic, ex);
                                        failedPicUri.add(pic);
                                    } finally {
                                        downloadedImage.add(pic);
                                        cdl.countDown();
                                    }
                                }
                            });
                        }

                        for (DBObject pur : purchase) {
                            final String himg = (String) pur.get("himg");
                            final String pic1 = (String) pur.get("pic1");
                            final String pic2 = (String) pur.get("pic2");
                            final String pic3 = (String) pur.get("pic3");
                            final String pic4 = (String) pur.get("pic4");

                            final String pics[] = {himg, pic1, pic2, pic3, pic4};
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    for (String pic : pics) {
                                        try {
                                            if (!downloadedImage.contains(pic)) {
                                                if (StringUtils.isNotEmpty(pic)) {
                                                    RequestHeader header = new RequestHeader();
                                                    header.setAccept("image/webp,image/*,*/*;q=0.8");
                                                    header.setAcceptEncoding("gzip, deflate, sdch");
                                                    header.setAcceptLanguage("zh-CN,zh;q=0.8");
                                                    header.setConnection("keep-alive");
                                                    header.setCookie("__cfduid=d4c6454d129c7e518d6a449c386a9fc7e1488074382; yjs_use_ob=0; user_lng=118.10388605; user_lat=24.48923061; Hm_lvt_6d3936097458e84313a0e6d1aa60f655=1488074383,1488119584; Hm_lpvt_6d3936097458e84313a0e6d1aa60f655=1488122314");
                                                    header.setHost("www.2016927.com");
                                                    header.setReferer("http://www.2016927.com/PurchaseList.aspx");
                                                    RequestUtils.httpRequestFile("http://www.2016927.com" + pic,
                                                            "D:\\项目\\wechat-website\\app\\target\\app-1.0.0\\resources\\cheyiwang\\" + pic.replace("/", "\\"),
                                                            header);
                                                }
                                            } else {
                                                LOGGER.info("已经下载过的图片:{}", pic);
                                            }
                                        } catch (Exception ex) {
                                            LOGGER.error("获取图片：{}发生错误，已放入失败队列", pic, ex);
                                            failedPicUri.add(pic);
                                        } finally {
                                            downloadedImage.add(pic);
                                            cdl.countDown();
                                        }
                                    }
                                }
                            });
                        }
                        cdl.await();
                    } catch (Exception ex) {
                        COLLECTING = false;
                        LOGGER.error("获取图片反生错误", ex);
                    } finally {
                        LOGGER.info("获取图片反生错误任务停止，共获取图片：{}张", count);
                    }
                }
            }).start();
            this.success(result);
        } catch (Exception ex) {
            this.failed(result, ex.getMessage());
            LOGGER.error("抓取数据发生错误", ex);
        }
        return result;
    }


    @ResponseBody
    @RequestMapping("/import/user/img")
    public Map loadUserImg() {
        Map res = new HashMap();
        try {

            List<Map> users = userService.getAllMerchantUser();

            if (CollectionUtils.isNotEmpty(users)) {
                for (Map user : users) {
                    String pic = (String) user.get("headimgurl");
                    if (StringUtils.isEmpty(pic)) {
                        continue;
                    }
//                    if (StringUtils.startsWith(pic, "http://")) {
//                        continue;
//                    }
                    pic = pic.substring(pic.lastIndexOf("/"), pic.length());
                    String fileSavePrefix = ConfigCache.getConfig("avatar.path", "/Users/apple/Desktop/avatar");
                    try {
                        RequestHeader header = new RequestHeader();
                        header.setAccept("image/webp,image/*,*/*;q=0.8");
                        header.setAcceptEncoding("gzip, deflate, sdch");
                        header.setAcceptLanguage("zh-CN,zh;q=0.8");
                        header.setConnection("keep-alive");
                        header.setCookie("__cfduid=d4c6454d129c7e518d6a449c386a9fc7e1488074382; yjs_use_ob=0; user_lng=118.10388605; user_lat=24.48923061; Hm_lvt_6d3936097458e84313a0e6d1aa60f655=1488074383,1488119584; Hm_lpvt_6d3936097458e84313a0e6d1aa60f655=1488122314");
                        header.setHost("www.2016927.com");
                        header.setReferer("http://www.2016927.com/ProList.aspx");
                        RequestUtils.httpRequestFile("http://www.2016927.com/images/Avatar/" + pic, fileSavePrefix + pic,
                                header);
                    } catch (Exception ex) {
                        LOGGER.error("下载失败：" + pic);
                    }
                }
            }

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @Autowired
    MerchantService merchantService;


    @ResponseBody
    @RequestMapping("/import/merchant/img")
    public Map loadMerchantImage() {
        Map res = new HashMap();
        try {

            List<Map> users = userService.getAllMerchantUser();

            if (CollectionUtils.isNotEmpty(users)) {
                for (Map user : users) {
                    Map param = new HashMap();
                    param.put("id", user.get("id"));

                    Map authInfo = merchantService.getMerchantAuthInfo(param);
                    if (authInfo == null) {
                        continue;
                    }
                    if (authInfo.containsKey("idcard_pic") == false) {
                        continue;
                    }
                    String pic = (String) authInfo.get("idcard_pic");
                    if (StringUtils.isEmpty(pic)) {
                        continue;
                    }
//                    if (StringUtils.startsWith(pic, "http://")) {
//                        continue;
//                    }
                    if (StringUtils.isEmpty(pic)) {
                        continue;
                    }
                    try {
                        pic = pic.substring(pic.lastIndexOf("/"), pic.length());
                        String fileSavePrefix = ConfigCache.getConfig("avatar.path", "/Users/apple/Desktop/auth");
                        RequestHeader header = new RequestHeader();
                        header.setAccept("image/webp,image/*,*/*;q=0.8");
                        header.setAcceptEncoding("gzip, deflate, sdch");
                        header.setAcceptLanguage("zh-CN,zh;q=0.8");
                        header.setConnection("keep-alive");
                        header.setCookie("__cfduid=d4c6454d129c7e518d6a449c386a9fc7e1488074382; yjs_use_ob=0; user_lng=118.10388605; user_lat=24.48923061; Hm_lvt_6d3936097458e84313a0e6d1aa60f655=1488074383,1488119584; Hm_lpvt_6d3936097458e84313a0e6d1aa60f655=1488122314");
                        header.setHost("www.2016927.com");
                        header.setReferer("http://www.2016927.com/ProList.aspx");
                        RequestUtils.httpRequestFile("http://www.2016927.com/images/auth/" + pic, fileSavePrefix + pic,
                                header);
                    } catch (Exception ex) {
                        LOGGER.error("下载失败：" + pic);
                    }
                }
            }

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @Autowired
    GoodMgrService goodMgrService;

    @ResponseBody
    @RequestMapping("/import/good/img")
    public Map loadGoodImage() {
        Map res = new HashMap();
        try {

            List<DBObject> goods = goodMgrService.getList(new HashMap(), 1, 10000);


            if (CollectionUtils.isNotEmpty(goods)) {
                for (DBObject good : goods) {

                    if (good.containsKey("img") == false) {
                        continue;
                    }
                    List<String> imgs = (List<String>) good.get("img");
                    if (CollectionUtils.isEmpty(imgs)) {
                        continue;
                    }
                    for (String img : imgs) {
//                    if (StringUtils.startsWith(pic, "http://")) {
//                        continue;
//                    }
                        if (StringUtils.isEmpty(img)) {
                            continue;
                        }
                        try {
                            img = img.substring(img.lastIndexOf("/"), img.length());
                            String fileSavePrefix = ConfigCache.getConfig("avatar.path", "/Users/apple/Desktop/good/Product");
                            RequestHeader header = new RequestHeader();
                            header.setAccept("image/webp,image/*,*/*;q=0.8");
                            header.setAcceptEncoding("gzip, deflate, sdch");
                            header.setAcceptLanguage("zh-CN,zh;q=0.8");
                            header.setConnection("keep-alive");
                            header.setCookie("__cfduid=d4c6454d129c7e518d6a449c386a9fc7e1488074382; yjs_use_ob=0; user_lng=118.10388605; user_lat=24.48923061; Hm_lvt_6d3936097458e84313a0e6d1aa60f655=1488074383,1488119584; Hm_lpvt_6d3936097458e84313a0e6d1aa60f655=1488122314");
                            header.setHost("www.2016927.com");
                            header.setReferer("http://www.2016927.com/ProList.aspx");
                            RequestUtils.httpRequestFile("http://www.2016927.com/images/Product/" + img, fileSavePrefix + img,
                                    header);
                        } catch (Exception ex) {
                            LOGGER.error("下载失败：" + img);
                        }
                    }
                }
            }

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @Autowired
    ShopMgrService shopMgrService;
    @Autowired
    MongoBaseDao mongoBaseDao;

    @ResponseBody
    @RequestMapping("/update/good/classInfo")
    public Map updateGoodClassInfo() {
        Map res = new HashMap();
        try {
            List<DBObject> goods = goodMgrService.getList(new HashMap(), 1, 10000);

            Map param = new HashMap();
            param.put("uuid", Long.parseLong(ConfigCache.getConfig("DEFAULT_UUID")));
            DBObject all = shopMgrService.getGoodClassByUuid(param);
            DBObject list = (DBObject) all.get("list");


            Map<String, String> nameWithIds = getChildInfo(list);

            if (CollectionUtils.isNotEmpty(goods)) {
                for (DBObject good : goods) {
                    String goodClassName = (String) good.get("good_class_name");
                    if (StringUtils.isEmpty(goodClassName)) {
                        continue;
                    }
                    if (nameWithIds.containsKey(goodClassName.toString().trim())) {
                        BasicDBObject update = new BasicDBObject("wx_user_id", Long.parseLong(good.get("wx_user_id")+""));
                        update.append("good_class_id",nameWithIds.get(goodClassName));

                        DBObject query = new BasicDBObject("good_id", good.get("good_id"));

                        mongoBaseDao.updateOne(query.toMap(), update.toMap(), MongoTable.good);

                    }

                }
            }
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    private Map<String, String> getChildInfo(DBObject item) {
        Map<String, String> nameWithId = new HashMap<>();

        nameWithId.put(item.get("title").toString().trim(), item.get("_id").toString().trim().replaceAll(" ", ""));

        List<DBObject> childrens = (List<DBObject>) item.get("children");
        if (CollectionUtils.isNotEmpty(childrens)) {
            for (DBObject children : childrens) {
                nameWithId.putAll(getChildInfo(children));
            }
        }
        return nameWithId;
    }
}
