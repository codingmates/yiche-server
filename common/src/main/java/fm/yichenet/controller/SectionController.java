package fm.yichenet.controller;

import com.alibaba.fastjson.JSON;
import fm.controller.BaseController;
import fm.yichenet.service.SectionService;
import fm.web.MediaTypes;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-05-30.
 */
@Controller
@RequestMapping("section")
public class SectionController extends BaseController {

    @Autowired
    SectionService sectionService;

    /**
     * 获取启用的板块列表
     *
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getSectionList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getSectionList(ModelMap modelMap) {
        List sections = sectionService.getAllSection(1);
        modelMap.put("data", sections);
        this.success(modelMap);
        return JSON.toJSONString(modelMap);
    }

    /**
     * 分页获取板块列表
     *
     * @param pageSize
     * @param pageNum
     * @param modelMap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getSectionPageList", method = RequestMethod.POST, produces = MediaTypes.JSON_UTF_8)
    public String getSectionPageList(@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                                     @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum, ModelMap modelMap) {
        List sections = sectionService.getSectionPageList(pageSize, pageNum);
        Long total = sectionService.countSection();
        Map<String, Object> data = new HashedMap();

        data.put("sections", sections);
        data.put("total", total);
        modelMap.put("data", data);
        this.success(modelMap);
        return JSON.toJSONString(modelMap);
    }
}
