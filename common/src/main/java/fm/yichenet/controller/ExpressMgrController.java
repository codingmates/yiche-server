package fm.yichenet.controller;

import com.mongodb.DBObject;
import fm.cache.AreaCache;
import fm.controller.BaseController;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.yichenet.mongo.service.ExpressService;
import fm.web.CurrentRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CM on 2017/6/25.
 */
@Controller
@RequestMapping("/express/mgr")
public class ExpressMgrController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExpressMgrController.class);

    @Autowired
    ExpressService expressService;

    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        modelMap.put("citys", AreaCache.getAllProvince());

        return "admin/express/index";
    }


    @ResponseBody
    @RequestMapping("/list")
    public Map getExpressList(Long shopId, String keyword, String area) {
        Map res = new HashMap();
        try {
            if (CurrentRequest.getCurrentUser() instanceof WxUser) {
                shopId = CurrentRequest.getCurrentUserId();
            }
            List<DBObject> expresses = expressService.getExpressList(keyword, shopId, area);
            res.put("data", expresses);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping("/add")
    public Map addExpress(Double fee, String name, String area) {
        Map res = new HashMap();
        try {
            DBObject object = expressService.addExpress(name, fee, area);
            res.put("data", object);

            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }


    @ResponseBody
    @RequestMapping("/delete")
    public Map deleteExpress(String expressId) {
        Map res = new HashMap();
        try {
            expressService.deleteExpress(expressId);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }

    @ResponseBody
    @RequestMapping("/update")
    public Map updateExpress(String name, Double fee, String expressId, String area) {
        Map res = new HashMap();
        try {
            DBObject result = expressService.updateExpress(name, area, fee, expressId);
            res.put("data", result);
            this.success(res);
        } catch (BizException ex) {
            LOGGER.error("occur business error", ex);
            this.failed(res, ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("occur un-expected error", ex);
            this.failed(res, "服务器发生未知错误，请稍后再试活着联系管理员解决！");
        }
        return res;
    }
}
