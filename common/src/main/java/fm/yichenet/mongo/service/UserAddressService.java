package fm.yichenet.mongo.service;

import com.mongodb.DBObject;
import fm.entity.WxUser;

import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-06-19.
 */
public interface UserAddressService {

    Map mergeAddress(Map dbObject, WxUser wxUser) throws Exception;

    void deleteAddress(WxUser wxUser, String addressId) throws Exception;

    List addressList(WxUser wxUser) throws Exception;

    DBObject getById(String addressId) throws Exception;
}
