package fm.yichenet.mongo.service;

import com.mongodb.DB;
import com.mongodb.DBObject;
import fm.entity.WxUser;
import fm.exception.BizException;
import org.hibernate.persister.walking.spi.WalkingException;

import java.util.List;
import java.util.Map;

/**
 * @修改人：CM
 * @修改时间：2017/3/15 13:19
 */
public interface ShopMgrService {

    List<DBObject> getContactInfoByPage(Integer pageNum, Integer pageSize) throws Exception;

    void updateMapConfig(Map mapConfig) throws Exception;

    void updateAboutContent(Map content) throws Exception;

    DBObject getAboutConfig(Map param) throws Exception;

    DBObject getHomeConfig(Map param) throws Exception;

    void updateHomeConfig(Map config) throws Exception;

    DBObject getGoodClassByUuid(Map param) throws Exception;

    void addGoodClass(Map data) throws Exception;

    void updateGoodClass(Map data);

    /**
     * 首页推广店铺
     *
     * @param shopId
     * @param action
     */
    void flagShop(Long shopId, String action, String city) throws Exception;

    /**
     * 获取首页推广店铺列表
     *
     * @return
     */
    List<DBObject> getFlagShop(String city) throws Exception;

    /**
     * 获取商铺列表
     *
     * @param keyword
     * @return
     * @throws Exception
     */
    List getShopList(String shopType, String keyword, Double longitude, Double latitude,String province,String city, Integer pageSize, Integer pageNum) throws Exception;

    void collectShop(Long shopId,WxUser wxUser) throws Exception;

    void unCollectShop(Long shopId,WxUser wxUser) throws Exception;

    List getCollectionShopList(WxUser wxUser,Integer pageSize,Integer pageNum) throws Exception;
    
}
