package fm.yichenet.mongo.service.impl;

import com.mongodb.DBObject;
import fm.yichenet.mongo.service.EnginemenService;
import fm.dao.HibernateBaseDao;
import fm.dao.MongoBaseDao;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import fm.util.CommonUtils;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by 63267 on 2017/5/21.
 * TODO 这里的hibernateBaseDao调用会有事务问题 ，参考商户认证资料的提交修改
 */
@Service
public class EnginemenServiceImpl implements EnginemenService {

    @Autowired
    private MongoBaseDao mongoBaseDao;

    @Autowired
    private HibernateBaseDao hibernateBaseDao;

    @Override
    public void addEnginemenAuthInfo(Map params) throws Exception {
        if (CommonUtils.isEmpty(params.get("id"))) {
            throw new BizException("用户id不存在");
        }
        if (CommonUtils.isEmpty(params.get("real_name"))) {
            throw new BizException("真实姓名不能为空");
        }
        if (CommonUtils.isEmpty(params.get("phone_number"))) {
            throw new BizException("手机号码不能为空");
        }
        if (CommonUtils.isEmpty(params.get("employ_expes"))) {
            throw new BizException("工作经验不能为空");
        }
        if (CommonUtils.isEmpty(params.get("skills"))) {
            throw new BizException("技能不能为空");
        }
        if (CommonUtils.isEmpty(params.get("idcard"))) {
            throw new BizException("身份证号码不能为空");
        }
        if (CommonUtils.isEmpty(params.get("idcard_img1"))) {
            throw new BizException("身份证件照正面不能为空");
        }
        if (CommonUtils.isEmpty(params.get("idcard_img2"))) {
            throw new BizException("身份证件照背面不能为空");
        }

        Date now = new Date(System.currentTimeMillis());
        params.put("auth_time", now);

        String hql = "from WxUser where id = ?";
        WxUser user = (WxUser) hibernateBaseDao.getUnique(hql, Long.parseLong(String.valueOf(params.get("id"))));
        user.setReviewStatus(1);
        user.setUserType(2);
        hibernateBaseDao.update(user);
        mongoBaseDao.insert(params, MongoTable.engineer_auth_info);
    }

    @Override
    public void updateAuthInfo(Map params) throws BizException {
        Map<String, Object> query = new HashedMap();
        query.put("id", params.get("id"));
        DBObject authInfo = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.engineer_auth_info);
        if (authInfo == null) {
            throw new BizException("未找到对应的维修技师认证信息，修改失败");
        }
        authInfo.putAll(params);
        mongoBaseDao.updateOne(query, authInfo.toMap(), MongoTable.engineer_auth_info);
        String hql = "from WxUser where id = ?";
        WxUser user = (WxUser) hibernateBaseDao.getUnique(hql, Long.parseLong(String.valueOf(params.get("id"))));
        user.setReviewStatus(1);
        hibernateBaseDao.update(user);
    }

    @Override
    public Map getEnginemenAuthInfo(Map params) throws Exception {
        return ((DBObject) mongoBaseDao.findOne(params, DBObject.class, MongoTable.engineer_auth_info)).toMap();
    }

    @Override
    public Map mergeExperience(Map params) throws Exception {

        if (CommonUtils.isEmpty(params.get("start_time"))) {
            throw new BizException("开始时间不能为空");
        }
        if (CommonUtils.isEmpty(params.get("end_time"))) {
            throw new BizException("结束时间不能为空");
        }
        if (CommonUtils.isEmpty(params.get("company"))) {
            throw new BizException("公司名称不能为空");
        }
        if (CommonUtils.isEmpty(params.get("salary"))) {
            throw new BizException("薪资不能为空");
        }
        if (CommonUtils.isEmpty(params.get("desc"))) {
            throw new BizException("工作描述不能为空");
        }

        Timestamp edit_time = new Timestamp(System.currentTimeMillis());
        params.put("edit_time", edit_time);
        if (CommonUtils.isEmpty(params.get("id"))) {
            params.put("id", UUID.randomUUID().toString().replaceAll("-", ""));
            mongoBaseDao.insert(params, MongoTable.engineer_experience);
            return params;
        }
        Map<String, Object> query = new HashedMap();
        query.put("id", params.get("id"));
        DBObject experience = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.engineer_experience);
        experience.putAll(params);
        mongoBaseDao.updateOne(query, experience.toMap(), MongoTable.engineer_experience);
        return params;
    }

    @Override
    public Map mergeSkill(Map params) throws Exception {
        if (CommonUtils.isEmpty(params.get("sikll_name"))) {
            throw new BizException("技能名称不能为空");
        }
        if (CommonUtils.isEmpty(params.get("desc"))) {
            throw new BizException("技能描述不能为空");
        }

        Timestamp edit_time = new Timestamp(System.currentTimeMillis());
        params.put("edit_time", edit_time);
        if (CommonUtils.isEmpty(params.get("id"))) {
            params.put("id", UUID.randomUUID().toString().replaceAll("-", ""));
            mongoBaseDao.insert(params, MongoTable.engineer_skill);
            return params;
        }
        Map<String, Object> query = new HashedMap();
        query.put("id", params.get("id"));
        DBObject skill = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.engineer_skill);
        skill.putAll(params);
        mongoBaseDao.updateOne(query, skill.toMap(), MongoTable.engineer_skill);
        return params;
    }

    @Override
    public List getExperienceList(Map params) throws Exception {
        return (List<DBObject>) mongoBaseDao.getList(params, DBObject.class, MongoTable.engineer_experience);
    }

    @Override
    public List getSkillList(Map params) throws Exception {
        return (List<DBObject>) mongoBaseDao.getList(params, DBObject.class, MongoTable.engineer_skill);
    }

    @Override
    public void deleteExperience(String id) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("id", id);
        mongoBaseDao.delete(query, MongoTable.engineer_experience);
    }

    @Override
    public void deleteSkill(String id) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("id", id);
        mongoBaseDao.delete(query, MongoTable.engineer_skill);
    }
}
