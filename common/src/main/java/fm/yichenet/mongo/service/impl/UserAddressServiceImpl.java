package fm.yichenet.mongo.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import fm.util.CommonUtils;
import fm.yichenet.mongo.service.UserAddressService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by 宏炜 on 2017-06-19.
 */
@Service
public class UserAddressServiceImpl implements UserAddressService {


    @Autowired
    MongoBaseDao mongoBaseDao;

    @Override
    public Map mergeAddress(Map dbObject, WxUser wxUser) throws Exception {

        if(CommonUtils.isEmpty(dbObject.get("city"))){
            throw new BizException("城市不能为空");
        }
        if(CommonUtils.isEmpty(dbObject.get("district"))){
            throw new BizException("区县不能为空");
        }
        if(CommonUtils.isEmpty(dbObject.get("street"))){
            throw new BizException("街道详细地址不能为空");
        }
        if(CommonUtils.isEmpty(dbObject.get("phone"))){
            throw new BizException("电话号码不能为空");
        }
        if(CommonUtils.isEmpty(dbObject.get("contacts"))){
            throw new BizException("联系人不能为空");
        }
        if(CommonUtils.isEmpty(dbObject.get("default"))){
            dbObject.put("default","false");
        }
        if(!CommonUtils.isEmpty(dbObject.get("id"))){
            Map<String,Object> query = new HashedMap();
            query.put("id",dbObject.get("id"));
            query.put("wx_user_id",wxUser.getId());
            if("true".equals(String.valueOf(dbObject.get("default")))){
                Map<String,Object> updateAll = new HashedMap();
                updateAll.put("wx_user_id",wxUser.getId());
                Map<String,Object> updateItem = new HashedMap();
                updateItem.put("default","false");
                mongoBaseDao.updateAll(updateAll,updateItem,MongoTable.receive_address);
            }
            dbObject.put("wx_user_id",wxUser.getId());
            DBObject address = (DBObject)mongoBaseDao.findOne(query, DBObject.class, MongoTable.receive_address);
            if(CommonUtils.isEmpty(address)){
                throw new BizException("地址信息不存在");
            }
            address.putAll(dbObject);
            mongoBaseDao.updateOne(query,address.toMap(),MongoTable.receive_address);

        }else{
            if("true".equals(String.valueOf(dbObject.get("default")))){
                Map<String,Object> updateAll = new HashedMap();
                updateAll.put("wx_user_id",wxUser.getId());
                Map<String,Object> updateItem = new HashedMap();
                updateItem.put("default","false");
                mongoBaseDao.updateAll(updateAll,updateItem,MongoTable.receive_address);
            }
            dbObject.put("id", UUID.randomUUID().toString().replaceAll("-",""));
            dbObject.put("wx_user_id",wxUser.getId());
            mongoBaseDao.insert(dbObject,MongoTable.receive_address);
        }
        return dbObject;
    }

    @Override
    public void deleteAddress(WxUser wxUser, String addressId) throws Exception {
        Map<String,Object> query = new HashedMap();
        query.put("wx_user_id",wxUser.getId());
        query.put("id",addressId);
        mongoBaseDao.delete(query,MongoTable.receive_address);
    }

    @Override
    public List addressList(WxUser wxUser) throws Exception {
        Map<String,Object> query = new HashedMap();
        query.put("wx_user_id",wxUser.getId());
        return (List<DBObject>)mongoBaseDao.getList(query,DBObject.class,MongoTable.receive_address);
    }

    @Override
    public DBObject getById(String addressId) throws Exception {

        return (DBObject) mongoBaseDao.findOne(new BasicDBObject("id",addressId).toMap(),DBObject.class,MongoTable.receive_address);
    }


}
