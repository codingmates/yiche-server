package fm.yichenet.mongo.service;

import com.mongodb.DB;
import com.mongodb.DBObject;
import fm.entity.WxUser;
import fm.exception.BizException;
import org.apache.solr.client.solrj.SolrQuery;

import java.util.List;
import java.util.Map;

/**
 * 文件描述：
 * 更新时间：2017/4/3
 * 更新人： codingmates@gmail.com .
 */
public interface GoodMgrService {

    List<DBObject> getList(Map param, Integer pageNum, Integer pageSize) throws Exception;

    long countAll(Map param) throws Exception;

    void addGood(Map param) throws Exception;

    /**
     * 次方法针对的是更新单一字段，如果需要全量更新 ，内部的逻辑会导致全量更新发生错误，
     * 特别是含有img字段的时候，具体参考实现代码
     *
     * @param param
     * @throws Exception
     */
    void updateGood(Map param) throws Exception;

    void delGood(Map param) throws Exception;


    DBObject getGood(Map param) throws Exception;

    /**
     * 从SOLR中获取数据
     *
     * @param param
     * @return
     */
    List getListFromSolr(Map param, Integer pageSize, Integer pageNum, Map<String, SolrQuery.ORDER> orderMap) throws Exception;

    /**
     * 获取商铺首页商品列表
     *
     * @param params
     * @param sort
     * @return
     * @throws Exception
     */
    List<DBObject> getShopGoodList(Map params, DBObject sort) throws Exception;

    /**
     * 评论商品
     *
     * @param params
     * @return
     * @throws BizException
     */
    Map goodComment(Map params, WxUser wxUser) throws Exception;

    /**
     * 获取订单的评论信息
     *
     * @param orderId
     * @param wxUserId 评论用户ID
     * @return
     * @throws Exception
     */
    DBObject getOrderComment(String orderId, Long wxUserId) throws Exception;

    /**
     * 获取商品的评论信息
     *
     * @param params
     * @param pageSize
     * @param pageNum
     * @return
     * @throws Exception
     */
    List<DBObject> getGoodCommentListForPage(Map params, Integer pageSize, Integer pageNum) throws Exception;

    /**
     * 获取评论的级别
     *
     * @return
     * @throws Exception
     */
    Map<String, Long> facetCommentLevel(String good_id) throws Exception;

    /**
     * 增加商品销量
     * @throws Exception
     */
    void addGoodSales(String goodId) throws Exception;
}
