package fm.yichenet.mongo.service;

import fm.entity.OrderGood;
import fm.entity.WxUser;

import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-06-19.
 */
public interface OrderCheckService {

    Map<String,Object> addOrder(List<OrderGood> paramsList, WxUser wxUser) throws Exception;

    void cancelOrder(String orderId,WxUser wxUser) throws Exception;

    List orderList(Integer pageSize, Integer pageNum, WxUser wxUser) throws Exception;
}
