package fm.yichenet.mongo.service.impl;

import com.mongodb.DB;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.entity.OrderGood;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import fm.util.CommonUtils;
import fm.yichenet.mongo.service.OrderCheckService;
import fm.yichenet.service.OrderService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-06-19.
 */
@Service
public class OrderCheckServiceImpl implements OrderCheckService {

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Autowired
    OrderService orderService;

    @Override
    public Map<String, Object> addOrder(List<OrderGood> paramsList, WxUser wxUser) throws Exception {

        for(OrderGood orderGood : paramsList){
            if(CommonUtils.isEmpty(orderGood.getGoodId())){
                throw new BizException("商品ID不能为空");
            }
            Map<String,Object> query = new HashedMap();
            query.put("good_id",orderGood.getGoodId());
            DBObject good = (DBObject) mongoBaseDao.findOne(query,DBObject.class, MongoTable.good);
            if(CommonUtils.isEmpty(good)){
                throw new BizException("商品信息不存在");
            }
            if(CommonUtils.isEmpty(orderGood.getGoodNum())){
                throw new BizException("商品数量不能为空");
            }
            if(CommonUtils.isEmpty(orderGood.getExpressId())){
                throw new BizException("物流信息不能为空");
            }
            Map<String,Object> expressQuery = new HashedMap();
            expressQuery.put("express_id",orderGood.getExpressId());
            DBObject express = (DBObject)mongoBaseDao.findOne(expressQuery,DBObject.class,MongoTable.express);
            orderGood.setExpressPrice(Double.parseDouble(String.valueOf(express.get("fee"))));

            Integer amount = Integer.parseInt(String.valueOf(good.get("amount")));
            if(orderGood.getGoodNum() > amount){
                throw new BizException("商品["+good.get("goodName")+"]库存不足");
            }
            amount = amount - orderGood.getGoodNum();
            good.put("amount",amount);
            mongoBaseDao.updateOne(query,good.toMap(),MongoTable.good);

            orderGood.setUnitPrice(Double.parseDouble(String.valueOf(good.get("sale_price"))));
            orderGood.setShopId(Long.parseLong(String.valueOf(good.get("wx_user_id"))));

            Map<String,Object> addressQuery = new HashedMap();
            addressQuery.put("wx_user_id",wxUser.getId());
            addressQuery.put("id",orderGood.getAddressId());
            DBObject address = (DBObject)mongoBaseDao.findOne(addressQuery,DBObject.class,MongoTable.receive_address);
            if(CommonUtils.isEmpty(address)){
                throw new BizException("收货地址信息不存在");
            }
            orderGood.setReceivePhone(String.valueOf(address.get("phone")));
            orderGood.setReceiveAddress(String.valueOf(address.get("province")) + String.valueOf(address.get("city")) + String.valueOf(address.get("district")) + String.valueOf(address.get("street")) + "  邮编:" + (CommonUtils.isEmpty(address.get("zip_code")) ? "" : String.valueOf(address.get("zip_code"))));
            orderGood.setReceivePerson(String.valueOf(address.get("contacts")));


            Map<String,Object> delCart = new HashedMap();
            delCart.put("good_id",orderGood.getGoodId());
            delCart.put("wx_user_id",wxUser.getId());
            mongoBaseDao.delete(delCart,MongoTable.shopping_cart);
        }
        return orderService.addOrder(paramsList,wxUser);
    }

    @Override
    public void cancelOrder(String orderId,WxUser wxUser) throws Exception {
        OrderGood orderGood = orderService.cancelOrder(orderId,wxUser);
        String goodId = orderGood.getGoodId();
        Map<String,Object> query = new HashedMap();
        query.put("good_id",goodId);
        DBObject good = (DBObject)mongoBaseDao.findOne(query,DBObject.class,MongoTable.good);
        Integer amount = Integer.parseInt(String.valueOf(good.get("amount")));
        amount += orderGood.getGoodNum();
        good.put("amount",amount);
        mongoBaseDao.updateOne(query,good.toMap(),MongoTable.good);
    }

    @Override
    public List orderList(Integer pageSize, Integer pageNum, WxUser wxUser) throws Exception {
        List<OrderGood> orderGoods = orderService.orderList(pageSize,pageNum,wxUser);
        List<Map<String,Object>> list = new ArrayList<>();

        for(OrderGood orderGood : orderGoods){
            Map<String,Object> map = new HashedMap();
            Map<String,Object> query = new HashedMap();
            query.put("good_id",orderGood.getGoodId());
            DBObject good = (DBObject) mongoBaseDao.findOne(query,DBObject.class,MongoTable.good);
            good.removeField("content");
            map.put("id",CommonUtils.isEmpty(orderGood.getId()) ? "" : orderGood.getId());
            map.put("type",CommonUtils.isEmpty(orderGood.getType()) ? "" : orderGood.getType());
            map.put("createTime",CommonUtils.isEmpty(orderGood.getCreateTime()) ? "" : orderGood.getCreateTime());
            map.put("payTime",CommonUtils.isEmpty(orderGood.getPayTime()) ? "" : orderGood.getPayTime());
            map.put("deliverTime",CommonUtils.isEmpty(orderGood.getDeliverTime()) ? "" : orderGood.getDeliverTime());
            map.put("completeTime",CommonUtils.isEmpty(orderGood.getCompleteTime()) ? "" : orderGood.getCompleteTime());
            map.put("checkTime",CommonUtils.isEmpty(orderGood.getCheckTime()) ? "" : orderGood.getCheckTime());
            map.put("status",CommonUtils.isEmpty(orderGood.getStatus()) ? "" : orderGood.getStatus());
            map.put("expressNo",CommonUtils.isEmpty(orderGood.getExpressNo()) ? "" : orderGood.getExpressNo());
            map.put("goodNum",CommonUtils.isEmpty(orderGood.getGoodNum()) ? "" : orderGood.getGoodNum());
            map.put("unitPrice",CommonUtils.isEmpty(orderGood.getUnitPrice()) ? "" : orderGood.getUnitPrice());
            map.put("sumPrice",CommonUtils.isEmpty(orderGood.getSumPrice()) ? "" : orderGood.getSumPrice());
            map.put("realPay",CommonUtils.isEmpty(orderGood.getRealPay()) ? "" : orderGood.getRealPay());
            map.put("remarks",CommonUtils.isEmpty(orderGood.getRemarks()) ? "" : orderGood.getRemarks());
            map.put("goodId",CommonUtils.isEmpty(orderGood.getGoodId()) ? "" : orderGood.getGoodId());
            map.put("shopId",CommonUtils.isEmpty(orderGood.getShopId()) ? "" : orderGood.getShopId());
            map.put("expressPrice",CommonUtils.isEmpty(orderGood.getExpressPrice()) ? "" : orderGood.getExpressPrice());
            map.put("receiveAddress",CommonUtils.isEmpty(orderGood.getReceiveAddress()) ? "" : orderGood.getReceiveAddress());
            map.put("receivePerson",CommonUtils.isEmpty(orderGood.getReceivePerson()) ? "" : orderGood.getReceivePerson());
            map.put("receivePhone",CommonUtils.isEmpty(orderGood.getReceivePhone()) ? "" : orderGood.getReceivePhone());

            map.put("goodInfo",CommonUtils.isEmpty(good) ? "" : good);
            list.add(map);

        }
        return list;
    }
}
