package fm.yichenet.mongo.service;

import com.mongodb.DBObject;
import fm.exception.BizException;

import java.util.Map;
/**
 * Created by 63267 on 2017/5/21.
 */
public interface MerchantService {

    void addMerchantAuthInfo(Map params) throws Exception;

    void updateAuthInfo(Map params) throws BizException;

    Map getMerchantAuthInfo(Map params) throws Exception;

    String getMerchantAuthName(Map params) throws Exception;
}
