package fm.yichenet.mongo.service;

import com.mongodb.DBObject;

import java.util.List;

/**
 * 招聘管理业务接口
 * Created by CM on 2017/7/1.
 */
public interface RecruitmentService {

    /**
     * 添加招聘
     *
     * @param title    招聘标题（职位名称）
     * @param salary   薪资范围描述
     * @param worktime 工作时间描述
     * @param content  工作内容描述
     * @return
     */
    DBObject addRecurit(String title, String salary, String worktime, String content) throws Exception;


    /**
     * 删除招聘
     *
     * @param recuritId
     * @return
     */
    void delRecurit(String recuritId);

    /**
     * 招聘列表获取接口
     *
     * @param keyword  标题搜索关键字
     * @param shopId   商铺ID
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<DBObject> recuritList(String keyword, Long shopId, Integer pageNum, Integer pageSize) throws Exception;
}
