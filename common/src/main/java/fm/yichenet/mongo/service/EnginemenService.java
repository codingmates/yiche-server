package fm.yichenet.mongo.service;

import fm.exception.BizException;

import java.util.List;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/21.
 */
public interface EnginemenService {
    void addEnginemenAuthInfo(Map params) throws Exception;

    void updateAuthInfo(Map params) throws BizException;

    Map getEnginemenAuthInfo(Map params) throws Exception;

    Map mergeExperience(Map params) throws Exception;

    Map mergeSkill(Map params) throws Exception;

    List getExperienceList(Map params) throws Exception;

    List getSkillList(Map params) throws Exception;

    void deleteExperience(String id) throws Exception;

    void deleteSkill(String id) throws Exception;
}
