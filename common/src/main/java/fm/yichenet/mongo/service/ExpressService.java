package fm.yichenet.mongo.service;

import com.mongodb.DBObject;

import java.util.List;

/**
 * Created by CM on 2017/6/27.
 */
public interface ExpressService {

    List<DBObject> getExpressList(String keyword, Long shopId, String area) throws Exception;

    DBObject getExpress(String expressId);

    DBObject updateExpress(String name, String area, Double fee, String expressId);

    void deleteExpress(String expressId);

    DBObject addExpress(String name, Double fee, String area) throws Exception;

    DBObject addDefaultExpressWithShop(Long shopId) throws Exception;

    List<DBObject> getExpressListByGoodId(String goodId,Long shopId,String area) throws Exception;

}
