package fm.yichenet.mongo.service;

import com.mongodb.DBObject;
import fm.entityEnum.CarouselScope;
import fm.entityEnum.CarouselType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by CM on 17/5/22.
 */
public interface CarouselService {

    /**
     * 根据条件配置轮播图
     *
     * @param
     * @return
     */
    List<DBObject> getList(CarouselType type, CarouselScope scope, String city, Long shopId, Date currentTime) throws Exception;


    /**
     * 添加轮播图
     *
     * @param uri       图片完整URL
     * @param type      轮播图响应类型
     * @param scope     轮播图所在模块
     * @param typeValue 响应类型对应的值，：GOOD 对应商品ID LINK 对应文章链接
     */
    DBObject addCarousel(String uri, CarouselType type, CarouselScope scope, String typeValue, String city, String startTime, String endTime) throws Exception;


    /**
     * 删除轮播图
     *
     * @param cid
     */
    void delCarousel(String cid);
}
