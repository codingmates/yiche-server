package fm.yichenet.mongo.service;

import com.mongodb.DBObject;

import java.util.List;

/**
 * Created by CM on 2017/7/29.
 */
public interface VisitHistoryService {

    /**
     * 存储浏览记录，主要是商品的浏览记录，数据应该至少包含以下几项中的一项：
     * 商品类别ID
     * 商品大类： 新品、二手、配件
     * 商品搜索关键字
     * 商品店铺ID
     * 店铺区域信息
     *
     * 调用地方：
     * 商品详情接口（用户已登录的情况，记录商品类别ID）、
     * 店铺详情详情接口（用户已登录的情况，记录店铺ID）、
     * 订单下单（商品类别ID、店铺、区域）、
     * 加入购物车接口（记录，商品类别ID，店铺、区域）、(暂时不做)
     * 商品列表搜索接口（有传关键字的情况：记录关键字，有传类别ID ：记录类别ID）
     *
     * @param log
     */
    void addVisitLog(DBObject log) throws Exception;


    /**
     * 获取浏览记录列表提取数据推荐的商品，只获取最新的10条 如果没有就随意返回10件商品
     * 猜你喜欢接口根据这里返回的数据提取上面几项调用商品列表接口获取数据，返回给前端
     *
     */
    List getLikeGoodListOfVisitLog() throws Exception;
}
