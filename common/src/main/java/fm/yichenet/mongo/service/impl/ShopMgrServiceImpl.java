package fm.yichenet.mongo.service.impl;

import com.alibaba.fastjson.JSON;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.sun.tools.corba.se.idl.constExpr.Times;
import fm.cache.AreaCache;
import fm.dao.HibernateBaseDao;
import fm.dao.MongoBaseDao;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.nio.SemaphoreExecutor;
import fm.util.CommonUtils;
import fm.yichenet.mongo.service.ShopMgrService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * @修改人：CM
 * @修改时间：2017/3/15 13:20
 */
@Service
public class ShopMgrServiceImpl implements ShopMgrService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopMgrService.class);
    @Autowired
    MongoBaseDao baseDao;
    @Autowired
    HibernateBaseDao hibernateBaseDao;

    @Override
    public List<DBObject> getContactInfoByPage(Integer pageNum, Integer pageSize) throws Exception {
        Map param = new HashMap();
        return (List<DBObject>) baseDao.getPageList(param, DBObject.class, pageSize, pageNum, MongoTable.contact);
    }

    @Override
    public void updateMapConfig(Map mapConfig) throws Exception {
        DBObject query = new BasicDBObject("uuid", mapConfig.get("uuid"));
        Object queryOne = baseDao.findOne(query.toMap(), DBObject.class, MongoTable.aboutConfig);
        if (queryOne != null) {
            baseDao.updateOne(query.toMap(), mapConfig, MongoTable.aboutConfig);
        } else {
            baseDao.insert(mapConfig, MongoTable.aboutConfig);
        }
    }

    @Override
    public void updateAboutContent(Map content) throws Exception {
        DBObject query = new BasicDBObject("uuid", content.get("uuid"));
        Object queryOne = baseDao.findOne(query.toMap(), DBObject.class, MongoTable.aboutConfig);
        if (queryOne != null) {
            baseDao.updateOne(query.toMap(), content, MongoTable.aboutConfig);
        } else {
            baseDao.insert(content, MongoTable.aboutConfig);
        }

    }

    @Override
    public DBObject getAboutConfig(Map param) throws Exception {
        return (DBObject) baseDao.findOne(param, DBObject.class, MongoTable.aboutConfig);
    }

    @Override
    public DBObject getHomeConfig(Map param) throws Exception {
        DBObject queryOne = (DBObject) baseDao.findOne(param, DBObject.class, MongoTable.homeConfig);
        return queryOne;
    }

    @Override
    public void updateHomeConfig(Map config) throws Exception {
        DBObject query = new BasicDBObject("uuid", config.get("uuid"));
        Object queryOne = baseDao.findOne(query.toMap(), DBObject.class, MongoTable.homeConfig);
        if (queryOne != null) {
            baseDao.updateOne(query.toMap(), config, MongoTable.aboutConfig);
        } else {
            baseDao.insert(config, MongoTable.aboutConfig);
        }
    }

    @Override
    public DBObject getGoodClassByUuid(Map param) throws Exception {
        return (DBObject) baseDao.findOne(param, DBObject.class, MongoTable.goodClass);
    }

    @Override
    public void addGoodClass(Map data) throws Exception {
        baseDao.insert(data, MongoTable.goodClass);
    }

    @Override
    public void updateGoodClass(Map data) {
        Map query = new HashMap();
        query.put("uuid", data.get("uuid"));
        baseDao.updateOne(query, data, MongoTable.goodClass);
    }

    @Override
    public void flagShop(Long shopId, String action, String city) throws Exception {
        MCondition mc = MCondition.create(MRel.and);
        mc.append("shop_id", shopId);
        mc.append("city", city);

        if ("ADD".equals(action)) {
            DBObject existsShop = (DBObject) baseDao.findOne(mc.toDBObject().toMap(), DBObject.class, MongoTable.portal_shop);
            if (existsShop != null) {
                return;
            } else {
                DBObject shopConfig = new BasicDBObject("shop_id", shopId);
                shopConfig.put("create_time", new Date());
                if ("default".equals(city)) {
                    shopConfig.put("city", city);
                    shopConfig.put("city_desc", "默认全国");
                } else {
                    shopConfig.put("city", city);
                    shopConfig.put("city_desc", AreaCache.getAddress(city).getName());
                }
                baseDao.insert(shopConfig, MongoTable.portal_shop);
            }
        } else if ("DELETE".equals(action)) {
            baseDao.delete(mc.toDBObject().toMap(), MongoTable.portal_shop);
        }
    }

    @Override
    public List<DBObject> getFlagShop(String city) throws Exception {
        List<DBObject> shopIds = null;
        if (StringUtils.isEmpty(city)) {
            shopIds = (List<DBObject>) baseDao.findAll(DBObject.class, MongoTable.portal_shop);
        } else {
            MCondition mc = MCondition.create(MRel.and);
            mc.append("city", city);
            shopIds = (List<DBObject>) baseDao.getList(mc.toDBObject().toMap(), DBObject.class, MongoTable.portal_shop);
            if (CollectionUtils.isEmpty(shopIds)) {
                MCondition mc1 = MCondition.create(MRel.and);
                mc1.append("city", "default");
                shopIds = (List<DBObject>) baseDao.getList(mc1.toDBObject().toMap(), DBObject.class, MongoTable.portal_shop);
            }
        }

        if (CollectionUtils.isNotEmpty(shopIds)) {
            SemaphoreExecutor executor = new SemaphoreExecutor(10, "queryWxUserThreads");
            for (final DBObject shopId : shopIds) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Long id = (Long) shopId.get("shop_id");
                        String hql = "from WxUser u where u.id=? and u.reviewStatus=2 and (u.userType=3 or u.userType=4)";
                        WxUser wxUser = (WxUser) hibernateBaseDao.getUnique(hql, id);
                        String city = (String) shopId.get("city");
                        if (wxUser != null) {
                            shopId.putAll(JSON.parseObject(JSON.toJSONString(wxUser)));
                            shopId.removeField("telephone");
                            shopId.put("flag_city", city);
                        }
                    }
                });
            }
        }
        return shopIds;
    }

    @Override
    public List getShopList(String shopType, String keyword, Double longitude, Double latitude, String province, String city, Integer pageSize, Integer pageNum) throws Exception {
        Integer start;
        Integer end;

        if (pageNum == 0) {
            pageNum = 1;
        }

        start = (pageNum - 1) * pageSize;

        List<Map> list;
        String sql = getShopQueryString(latitude, longitude, keyword, province, city, shopType);
        sql += " limit " + start + "," + pageSize;


        LOGGER.info("店铺查询条件:{}", sql);
        Query query = hibernateBaseDao.getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        list = query.list();
        final List<Map<String, Object>> resList = new ArrayList<>();

//        if (CollectionUtils.isNotEmpty(list)) {
//
//            //多线程异步需要同步机制
//            final CountDownLatch cdl = new CountDownLatch(list.size());
//            SemaphoreExecutor executor = new SemaphoreExecutor(10, "queryMerchantThreads");
//            for (final Map objects : list) {
//                executor.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//
//                            Long id = Long.parseLong(String.valueOf(objects[0]));
//                            Map<String, Object> query = new HashedMap();
//                            query.put("id", id);
//                            DBObject merchant = (DBObject) baseDao.findOne(query, DBObject.class, MongoTable.merchant_auth_info);
//
//                            Map<String, Object> map = new HashedMap();
//
//                            map.put("id", id);
//                            map.put("distance", CommonUtils.isEmpty(objects[1]) ? "" : objects[1]);
//                            map.put("headimgurl", CommonUtils.isEmpty(objects[2]) ? "" : objects[2]);
//                            map.put("portal_img", CommonUtils.isEmpty(objects[3]) ? "" : objects[3]);
//                            if (!CommonUtils.isEmpty(merchant)) {
//                                map.put("company_name", CommonUtils.isEmpty(merchant.get("company_name")) ? null : merchant.get("company_name"));
//                                map.put("address", CommonUtils.isEmpty(merchant.get("address")) ? null : merchant.get("address"));
//                                map.put("charge_person_name", CommonUtils.isEmpty(merchant.get("charge_person_name")) ? "" : merchant.get("charge_person_name"));
//                                map.put("telephone", CommonUtils.isEmpty(merchant.get("telephone")) ? null : merchant.get("telephone"));
//                                map.put("pics", CommonUtils.isEmpty(merchant.get("pics")) ? null : merchant.get("pics"));
//                                map.put("desc", CommonUtils.isEmpty(merchant.get("desc")) ? null : merchant.get("desc"));
//                                map.put("province", CommonUtils.isEmpty(merchant.get("province")) ? null : merchant.get("province"));
//                                map.put("city", CommonUtils.isEmpty(merchant.get("city")) ? null : merchant.get("city"));
//                                map.put("district", CommonUtils.isEmpty(merchant.get("district")) ? null : merchant.get("district"));
//                                map.put("street", CommonUtils.isEmpty(merchant.get("street")) ? null : merchant.get("street"));
//                                map.put("street_number", CommonUtils.isEmpty(merchant.get("street_number")) ? null : merchant.get("street_number"));
//                                Map<String, Object> queryCount = new HashedMap();
//                                queryCount.put("wx_user_id", id);
//                                Long good_count = baseDao.count(queryCount, MongoTable.good);
//                                map.put("good_count", good_count);
//                            }
//                            resList.add(map);
//                        } catch (Exception ex) {
//                            LOGGER.error("获取商铺列表认证资料发生错误:", ex);
//                        } finally {
//                            //多线程执行结果等待计数器，-1
//                            cdl.countDown();
//                        }
//                    }
//                });
//            }
//
//            //等待线程同步结束后再继续后面的代码片段
//            cdl.await();
//        }
        return list;
    }

    @Override
    public void collectShop(Long shopId, WxUser wxUser) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("user_id", wxUser.getId());
        query.put("shop_id", shopId);
        DBObject shopCollection = (DBObject) baseDao.findOne(query, DBObject.class, MongoTable.shop_collection);
        if (!CommonUtils.isEmpty(shopCollection)) {
            throw new BizException("该店铺已被收藏");
        } else {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            query.put("collection_time", now);
            baseDao.insert(query, MongoTable.shop_collection);
        }
    }

    @Override
    public void unCollectShop(Long shopId, WxUser wxUser) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("user_id", wxUser.getId());
        query.put("shop_id", shopId);
        baseDao.delete(query, MongoTable.shop_collection);
    }

    @Override
    public List getCollectionShopList(WxUser wxUser, Integer pageSize, Integer pageNum) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("user_id", wxUser.getId());
        DBObject sort = new BasicDBObject("collection_time", Sort.Direction.DESC.toString());
        List<DBObject> list = (List<DBObject>) baseDao.getPageList(query, DBObject.class, pageSize, pageNum, MongoTable.shop_collection, sort);
        final List<Map<String, Object>> resList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(list)) {

            //多线程异步需要同步机制
            final CountDownLatch cdl = new CountDownLatch(list.size());
            SemaphoreExecutor executor = new SemaphoreExecutor(10, "queryCollectionShopThreads");

            for (final DBObject dbObject : list) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Long id = Long.parseLong(String.valueOf(dbObject.get("shop_id")));
                            Map<String, Object> query = new HashedMap();
                            query.put("id", id);
                            DBObject merchant = (DBObject) baseDao.findOne(query, DBObject.class, MongoTable.merchant_auth_info);
                            WxUser wxUser = (WxUser) hibernateBaseDao.getById(WxUser.class, id);
                            Map<String, Object> map = new HashedMap();
                            map.put("id", id);
                            map.put("headimgurl", CommonUtils.isEmpty(wxUser.getHeadimgurl()) ? "" : wxUser.getHeadimgurl());
                            if (!CommonUtils.isEmpty(merchant)) {
                                map.put("company_name", CommonUtils.isEmpty(merchant.get("company_name")) ? "" : merchant.get("company_name"));
                                map.put("address", CommonUtils.isEmpty(merchant.get("address")) ? "" : merchant.get("address"));
                                map.put("charge_person_name", CommonUtils.isEmpty(merchant.get("charge_person_name")) ? "" : merchant.get("charge_person_name"));
                                map.put("telephone", CommonUtils.isEmpty(merchant.get("telephone")) ? "" : merchant.get("telephone"));
                                map.put("pics", CommonUtils.isEmpty(merchant.get("pics")) ? "" : merchant.get("pics"));
                                map.put("desc", CommonUtils.isEmpty(merchant.get("desc")) ? "" : merchant.get("desc"));
                                map.put("province", CommonUtils.isEmpty(merchant.get("province")) ? "" : merchant.get("province"));
                                map.put("city", CommonUtils.isEmpty(merchant.get("city")) ? "" : merchant.get("city"));
                                map.put("district", CommonUtils.isEmpty(merchant.get("district")) ? "" : merchant.get("district"));
                                map.put("street", CommonUtils.isEmpty(merchant.get("street")) ? "" : merchant.get("street"));
                                map.put("street_number", CommonUtils.isEmpty(merchant.get("street_number")) ? "" : merchant.get("street_number"));
                                Map<String, Object> queryCount = new HashedMap();
                                queryCount.put("wx_user_id", id);
                                Long good_count = baseDao.count(queryCount, MongoTable.good);
                                map.put("good_count", good_count);
                            }
                            resList.add(map);
                        } catch (Exception ex) {
                            LOGGER.error("获取收藏店铺列表发生错误:", ex);
                        } finally {
                            //多线程执行结果等待计数器，-1
                            cdl.countDown();
                        }
                    }
                });
            }

            //等待线程同步结束后再继续后面的代码片段
            cdl.await();
        }
        return resList;
    }


    private String getShopQueryString(Double lat, Double lon, String keyword, String province, String city, String shopType) {
        if (lat == null) {
            lat = 0d;
        }
        if (lon == null) {
            lon = 0d;
        }
        double realLat = Math.min(lat, lon);
        double realLng = Math.max(lat, lon);

        DecimalFormat lonDf = new DecimalFormat("#.000000");
        DecimalFormat latDf = new DecimalFormat("#.00000");

        StringBuffer sb = new StringBuffer();
        sb.append("select * from (");

        //经纬度查询条件
        sb.append("select u.*,6378.138 * 2 * ASIN(SQRT(POW(SIN((").append(latDf.format(realLat)).append(" * PI() / 180 - u.latitude * PI() / 180) / 2),2)");
        sb.append("+ COS( ").append(latDf.format(realLat)).append(" * PI() / 180) * COS(u.latitude * PI() / 180) * POW(SIN((").append(lonDf.format(realLng)).append(" * PI() / 180 ");
        sb.append("- u.longitude * PI() / 180 ) / 2 ),2))) AS distance ");
        sb.append("from wx_user as u where (u.user_type = 3 or u.user_type = 4) and review_status = 2 and u.latitude is not null and u.longitude is not null");

        if (StringUtils.isNotEmpty(keyword)) {
            sb.append(" and u.nickname like '%").append(keyword).append("%' ");
        }

        if (StringUtils.isNotEmpty(province)) {
            sb.append(" and u.province='").append(province).append("' ");
        }

        if (StringUtils.isNotEmpty(city)) {
            sb.append(" and u.city='").append(city).append("' ");
        }

        if (StringUtils.isNotEmpty(shopType)) {
            sb.append(" and u.review_notes='").append(shopType).append("' ");

        }
        //无经纬度店铺的展示
        sb.append(" union  (select u1.*,999999 as distance  from wx_user u1 ")
                .append(" where  (u1.user_type = 3 or u1.user_type = 4) ")
                .append(" and u1.review_status = 2 and u1.longitude is null and u1.latitude is null");

        if (StringUtils.isNotEmpty(keyword)) {
            sb.append(" and u1.nickname like '%").append(keyword).append("%' ");
        }

        if (StringUtils.isNotEmpty(province)) {
            sb.append(" and u1.province='").append(province).append("' ");
        }

        if (StringUtils.isNotEmpty(city)) {
            sb.append(" and u1.city='").append(city).append("' ");
        }

        if (StringUtils.isNotEmpty(shopType)) {
            sb.append(" and u1.review_notes='").append(shopType).append("' ");

        }
        sb.append(")) a order by a.distance asc");


        return sb.toString();

    }
}
