package fm.yichenet.mongo.service;

import com.mongodb.DBObject;
import fm.entity.WxUser;

import java.util.List;

/**
 * Created by 63267 on 2017/6/18.
 */
public interface ShoppingCartService {

    List getShoppingCartForPage(WxUser wxUser,Integer pageSize,Integer pageNum) throws Exception;

    DBObject addShoppingCart(WxUser wxUser,Integer goodNum,String goodId) throws Exception;

    void deleteShoppingCart(WxUser wxUser,String goodId) throws Exception;

    DBObject editShoppingCart(WxUser wxUser,String goodId,Integer goodNum) throws Exception;
}
