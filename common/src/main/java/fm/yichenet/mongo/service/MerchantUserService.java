package fm.yichenet.mongo.service;

import com.mongodb.DBObject;

import java.util.Map;

/**
 * Created by CM on 2017/6/17.
 */
public interface MerchantUserService {

    /**
     * 更新商户地址信息：包括认证地址信息、用户信息
     *
     * @param param
     * @throws Exception
     */
    void updateMerchantAuthInfo(Map param) throws Exception;


    /**
     * 获取用户认证数据
     *
     * @param userId
     * @return
     * @throws Exception
     */
    DBObject getAuthInfoByUserIdAndType(Long userId, Integer userType) throws Exception;

}
