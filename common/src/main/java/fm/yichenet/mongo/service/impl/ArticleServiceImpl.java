package fm.yichenet.mongo.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import fm.cache.ConfigCache;
import fm.dao.HibernateBaseDao;
import fm.dao.MongoBaseDao;
import fm.entity.ArticleComment;
import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.huanxin.api.MessageApi;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.util.CommonUtils;
import fm.web.CurrentRequest;
import fm.yichenet.dto.ArticleDto;
import fm.yichenet.mongo.service.ArticleService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.*;

/**
 * Created by 宏炜 on 2017-05-26.
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Autowired
    HibernateBaseDao hibernateBaseDao;

    @Override
    public DBObject mergeArticle(ArticleDto articleDto) throws Exception {
        if (CommonUtils.isEmpty(articleDto.getType())) {
            throw new BizException("图文类型不能为空");
        }
        if (CommonUtils.isEmpty(articleDto.getSection_id())) {
            throw new BizException("板块不能为空");
        }

        if (articleDto.getType().equals(1)) {
            if (CommonUtils.isEmpty(articleDto.getPortal_img())) {
                throw new BizException("文章封面不能为空");
            }
            if (CommonUtils.isEmpty(articleDto.getMain_title())) {
                throw new BizException("文章标题不能为空");
            }
            if (CommonUtils.isEmpty(articleDto.getSub_title())) {
                throw new BizException("文章副标题不能为空");
            }
        }
        if (CommonUtils.isEmpty(articleDto.getContent())) {
            throw new BizException("内容不能为空");
        }
        if (CommonUtils.isEmpty(articleDto.getId())) {
            articleDto.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            articleDto.setCreate_time(new Date(System.currentTimeMillis()));
            articleDto.setIs_top(0);
            Object user = CurrentRequest.getCurrentLoginUser();
            if (user instanceof PublicNumber) {
                articleDto.setUser_nickname("衣车网官方");
                articleDto.setUser_head_img_url(CommonUtils.isEmpty(ConfigCache.getConfig("DEFAULT_LOGO")) ? "" : ConfigCache.getConfig("DEFAULT_LOGO"));
                articleDto.setUuid(-1L);
            } else if (user instanceof WxUser) {
                articleDto.setUser_nickname(CommonUtils.isEmpty(((WxUser) user).getNickname()) ? "" : ((WxUser) user).getNickname());
                articleDto.setUser_head_img_url(CommonUtils.isEmpty(((WxUser) user).getHeadimgurl()) ? "" : ((WxUser) user).getHeadimgurl());
                articleDto.setUuid(((WxUser) user).getId());
            } else {
                throw new BizException("无法获取当前用户信息,发布失败");
            }
            mongoBaseDao.insert(articleDto.toDBObject(), MongoTable.community_article);
            return articleDto.toDBObject();
        } else {
            Map<String, Object> query = new HashedMap();
            query.put("id", articleDto.getId());
            DBObject dbObject = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.community_article);
            if (CommonUtils.isEmpty(dbObject)) {
                throw new BizException("所修改的内容不存在");
            }
            dbObject.put("content", articleDto.getContent());
            dbObject.put("pics", articleDto.getPics());
            dbObject.put("portal_img", articleDto.getPortal_img());
            dbObject.put("main_title", articleDto.getMain_title());
            dbObject.put("sub_title", articleDto.getSub_title());
            dbObject.put("section_id", articleDto.getSection_id());
            mongoBaseDao.updateOne(query, dbObject.toMap(), MongoTable.community_article);
            return articleDto.toDBObject();
        }
    }

    @Override
    public void topArticle(String id) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("id", id);
        DBObject dbObject = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.community_article);
        if (CommonUtils.isEmpty(dbObject)) {
            throw new BizException("所置顶的内容不存在");
        }
        dbObject.put("is_top", 1);
        mongoBaseDao.updateOne(query, dbObject.toMap(), MongoTable.community_article);
    }

    @Override
    public void unTopArticle(String id) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("id", id);
        DBObject dbObject = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.community_article);
        if (CommonUtils.isEmpty(dbObject)) {
            throw new BizException("取消置顶的内容不存在");
        }
        dbObject.put("is_top", 0);
        mongoBaseDao.updateOne(query, dbObject.toMap(), MongoTable.community_article);
    }

    @Override
    public List getArticleForPage(Map params, Integer pageSize, Integer pageNum) throws Exception {
        DBObject sort = new BasicDBObject("create_time", Sort.Direction.DESC.toString());
        sort.put("is_top", Sort.Direction.DESC.toString());
        if (pageNum != null && pageSize != null) {
            return (List<DBObject>) mongoBaseDao.getPageList(params, DBObject.class, pageSize, pageNum, MongoTable.community_article, sort);
        } else {
            return (List<DBObject>) mongoBaseDao.getList(params, DBObject.class, MongoTable.community_article, sort);
        }
    }

    @Override
    public long countArticles(Map params) throws Exception {
        return mongoBaseDao.count(params, MongoTable.community_article);
    }

    @Override
    public DBObject commentArticle(ArticleComment articleComment) throws Exception {
        articleComment.setComment_time(new Date(System.currentTimeMillis()));
        if (CommonUtils.isEmpty(articleComment.getType())) {
            throw new BizException("类型不能为空");
        }

        if (articleComment.getType().equals(1)) {


            if (CommonUtils.isEmpty(articleComment.getContent())) {
                throw new BizException("评论内容不能为空");
            }
            if (!CommonUtils.isEmpty(articleComment.getTo_user_id())) {
                if (articleComment.getTo_user_id() != -1) {
                    String hql = "from WxUser where id = ?";
                    WxUser toUser = (WxUser) hibernateBaseDao.getUnique(hql, articleComment.getTo_user_id());
                    if (CommonUtils.isEmpty(toUser)) {
                        throw new BizException("评论对象不存在");
                    }
                    articleComment.setTo_user_nickname(toUser.getNickname());
                    articleComment.setTo_user_head_img_url(toUser.getHeadimgurl());
                } else {
                    articleComment.setTo_user_head_img_url("衣车网官方");
                    articleComment.setTo_user_head_img_url(CommonUtils.isEmpty(ConfigCache.getConfig("DEFAULT_LOGO")) ? "" : ConfigCache.getConfig("DEFAULT_LOGO"));
                }

            }
        }

        Map<String, Object> query = new HashedMap();
        query.put("id", articleComment.getArticle_id());

        DBObject article = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.community_article);
        if (!articleComment.getType().equals(1)) {
            WxUser user = (WxUser) CurrentRequest.getCurrentLoginUser();
            if (article.get("uuid").toString().equals(user.getId())) {
                throw new BizException("无法给自己点赞");
            }
            Map<String, Object> queryComment = new HashedMap();
            queryComment.put("article_id", articleComment.getArticle_id());
            queryComment.put("user_id", articleComment.getUser_id());
            queryComment.put("type", 0);
            long selLikesCount = mongoBaseDao.count(queryComment, MongoTable.article_comment);
            if (selLikesCount > 0) {
                throw new BizException("该条文章或图文已经点赞");
            }
        }
        if (!CommonUtils.isEmpty(articleComment.getTo_user_id())) {
            if (articleComment.getTo_user_id() != -1) {
                MessageApi.sendForumMsg("有新的社区动态#" + articleComment.getArticle_id() + "#", "wx_" + article.get("uuid"));
            }
        }

        List<DBObject> comments = (List<DBObject>) (CommonUtils.isEmpty(article.get("comments")) ? new ArrayList<DBObject>() : article.get("comments"));
        List<DBObject> likes = (List<DBObject>) (CommonUtils.isEmpty(article.get("likes")) ? new ArrayList<DBObject>() : article.get("likes"));

        if (articleComment.getType().equals(1)) {
            if (comments.size() < 5) {
                comments.add(articleComment.toDBObject());
                article.put("comments", comments);
            }
        } else {
            if (likes.size() < 10) {
                likes.add(articleComment.toDBObject());
                article.put("likes", likes);
            }
        }

        mongoBaseDao.updateOne(query, article.toMap(), MongoTable.community_article);

        mongoBaseDao.insert(articleComment.toDBObject(), MongoTable.article_comment);
        return articleComment.toDBObject();
    }

    @Override
    public List getCommentsList(Integer pageSize, Integer pageNum, String article_id, Integer type) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("article_id", article_id);
        query.put("type", type);
        return (List<DBObject>) mongoBaseDao.getPageList(query, DBObject.class, pageSize, pageNum, MongoTable.article_comment);
    }

    @Override
    public long countComments(String article_id, Integer type) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("article_id", article_id);
        query.put("type", type);
        return mongoBaseDao.count(query, MongoTable.article_comment);
    }

    @Override
    public DBObject getArticleInfo(String article_id) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("id", article_id);
        DBObject article = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.community_article);
        if (article == null) {
            throw new BizException("未找到相关的文章信息!");
        }
        Long comments_count = countComments(article_id, 1);
        Long likes_count = countComments(article_id, 0);
        article.put("comments_count", comments_count == null ? 0 : comments_count);
        article.put("likes_count", likes_count == null ? 0 : likes_count);

//        article.removeField("likes");
//        article.removeField("comments");
        article.removeField("_id");
        return article;
    }

    @Override
    public void deleteArticle(String article_id) throws Exception {
        Map<String, Object> query = new HashedMap();
        query.put("id", article_id);
        mongoBaseDao.delete(query, MongoTable.community_article);
    }

    @Override
    public void updateArticle(String articleId, DBObject article) {
        MCondition mc = MCondition.create(MRel.and);
        mc.append("article_id", articleId);

        mongoBaseDao.updateOne(mc.toDBObject().toMap(), article.toMap(), MongoTable.community_article);
    }

    @Override
    public long countInfoArticle(Map param) {
        long count = mongoBaseDao.count(param, MongoTable.portal_article);
        return count;
    }

    @Override
    public List<DBObject> getInfoArticleList(DBObject query) throws Exception {
        List<DBObject> infos = (List<DBObject>) mongoBaseDao.getList(query.toMap(), DBObject.class, MongoTable.portal_article);
        return infos;
    }

    @Override
    public void addInfoArticle(DBObject info) throws Exception {
        mongoBaseDao.insert(info, MongoTable.portal_article);
    }

    @Override
    public void deleteInfoArticle(DBObject query) {
        mongoBaseDao.delete(query.toMap(), MongoTable.portal_article);
    }

    @Override
    public void deleteUserArticle(WxUser wxUser, String articleId) {
        Map<String,Object> query = new HashedMap();
        query.put("uuid",wxUser.getId());
        query.put("id",articleId);
        mongoBaseDao.delete(query, MongoTable.community_article);
    }
}
