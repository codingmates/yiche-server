package fm.yichenet.mongo.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.exception.BizException;
import fm.mongo.MongoTable;
import fm.util.CommonUtils;
import fm.yichenet.mongo.service.MerchantService;
import fm.yichenet.service.AdminUserService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/21.
 */
@Service
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    private MongoBaseDao mongoBaseDao;
    @Autowired
    AdminUserService adminUserService;

    @Override
    public void addMerchantAuthInfo(Map params) throws Exception {
        if (CommonUtils.isEmpty(params.get("id"))) {
            throw new BizException("用户id不存在");
        }
        if (CommonUtils.isEmpty(params.get("company_name"))) {
            throw new BizException("公司名称不能为空");
        }
        if (CommonUtils.isEmpty(params.get("address"))) {
            throw new BizException("公司地址不能为空");
        }
        if (CommonUtils.isEmpty(params.get("charge_person_name"))) {
            throw new BizException("负责人姓名不能为空");
        }
        if (CommonUtils.isEmpty(params.get("telephone"))) {
            throw new BizException("固定电话号码不能为空");
        }
        if (CommonUtils.isEmpty(params.get("idcard_pic"))) {
            throw new BizException("身份证照片不能为空");
        }
        if (CommonUtils.isEmpty(params.get("phone_number"))) {
            throw new BizException("联系人手机号码不能为空");
        }
        if (CommonUtils.isEmpty(params.get("desc"))) {
            throw new BizException("公司主营业描述不能为空");
        }
        if (CommonUtils.isEmpty(params.get("cret_card"))) {
            throw new BizException("银行卡号不能为空");
        }
        if (CommonUtils.isEmpty(params.get("longitude"))) {
            throw new BizException("所在地经度不能为空");
        }
        if (CommonUtils.isEmpty(params.get("latitude"))) {
            throw new BizException("所在地纬度不能为空");
        }
        if (CommonUtils.isEmpty(params.get("user_type"))) {
            throw new BizException("商户类型不能为空");
        }

        Integer userType = (Integer) params.get("user_type");
        if (userType != 3 && userType != 4) {
            throw new BizException("商户类型只能是：个人商户、企业商户");
        } else if (userType == 4) {
            if (CommonUtils.isEmpty(params.get("business_license_pic"))) {
                throw new BizException("营业执照不能为空");
            }
            if (CommonUtils.isEmpty(params.get("pics"))) {
                throw new BizException("场景照片不能为空");
            }
        }

        Date now = new Date(System.currentTimeMillis());
        params.put("auth_time", now);

        adminUserService.updateMerchantAuthInfo(Long.parseLong(params.get("id")+""),
                (String) params.get("longitude"), (String) params.get("latitude"), (Integer) params.get("user_type"));

        DBObject item = (DBObject) mongoBaseDao.findOne(new BasicDBObject("id",params.get("id")).toMap(),DBObject.class,MongoTable.merchant_auth_info);

        if(item!=null){
            updateAuthInfo(params);
        }else{
            mongoBaseDao.insert(params, MongoTable.merchant_auth_info);
        }
    }

    @Override
    public void updateAuthInfo(Map params) throws BizException {
        Map<String, Object> query = new HashedMap();
        query.put("id", params.get("id"));
        DBObject merchant = (DBObject) mongoBaseDao.findOne(query, DBObject.class, MongoTable.merchant_auth_info);
        if (merchant == null) {

            throw new BizException("未找到对应的商户认证信息，修改失败");
        }
        merchant.putAll(params);
        mongoBaseDao.updateOne(query, merchant.toMap(), MongoTable.merchant_auth_info);
        adminUserService.updateReviewStatus(Long.parseLong(String.valueOf(params.get("id"))));
    }

    @Override
    public Map getMerchantAuthInfo(Map params) throws Exception {
        DBObject shop = ((DBObject) mongoBaseDao.findOne(params, DBObject.class, MongoTable.merchant_auth_info));
        if (CommonUtils.isEmpty(shop)) {
            return Collections.EMPTY_MAP;
        }
        Map res = shop.toMap();
        Map<String, Object> query = new HashedMap();
        query.put("shop_id", params.get("id"));

        Long collect_amount = mongoBaseDao.count(query, MongoTable.shop_collection);
        res.put("collect_amount", collect_amount);
        return res;
    }

    @Override
    public String getMerchantAuthName(Map params) throws Exception {
        DBObject shop = ((DBObject) mongoBaseDao.findOne(params, DBObject.class, MongoTable.merchant_auth_info));
        if (CommonUtils.isEmpty(shop)) {
            return null;
        }else{
            return String.valueOf(shop.get("company_name"));
        }
    }
}
