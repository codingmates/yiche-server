package fm.yichenet.mongo.service;

/**
 * Created by CM on 17/5/7.
 */
public interface SmsService {

    void sendSms(String phone, String content) throws Exception;

    void businessSmsSend(String phone, String actionType) throws Exception;

    void checkAuthUserSms(String phone,String code) throws Exception;
}
