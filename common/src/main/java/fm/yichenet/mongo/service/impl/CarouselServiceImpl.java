package fm.yichenet.mongo.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.entity.WxUser;
import fm.util.DateUtils;
import fm.yichenet.mongo.service.CarouselService;
import fm.cache.AreaCache;
import fm.dao.MongoBaseDao;
import fm.entityEnum.CarouselScope;
import fm.entityEnum.CarouselType;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.web.CurrentRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by CM on 17/5/22.
 */
@Service
public class CarouselServiceImpl implements CarouselService {
    @Autowired
    MongoBaseDao baseDao;

    @Override
    public List<DBObject> getList(CarouselType type, CarouselScope scope, String city, Long shopId, Date currentTime) throws Exception {
        MCondition mc = MCondition.create(MRel.and);
        if (type != null) {
            mc.append("type", type.toString());
        }
        if (scope != null) {
            mc.append("scope", scope.toString());
        }
        if (StringUtils.isNotEmpty(city)) {
            mc.append("city", city);
        }
        if (shopId != null) {
            mc.append("uuid", shopId);
        }

        if (currentTime != null) {
            mc.append(MCondition.create(MRel.lte).append("startTime", currentTime));
            mc.append(MCondition.create(MRel.gte).append("endTime", currentTime));
        }
        return (List<DBObject>) baseDao.getList(mc.toDBObject().toMap(), DBObject.class, MongoTable.carousel_img);
    }

    @Override
    public DBObject addCarousel(String uri, CarouselType type, CarouselScope scope, String typeValue, String city,
                                String startTime, String endTime) throws Exception {
        String cid = UUID.randomUUID().toString().replaceAll("-", "");
        DBObject object = new BasicDBObject("cid", cid);
        object.put("url", uri);
        object.put("thumb_url", uri.substring(0, uri.lastIndexOf(".")) + "-thumb.jpg");
        object.put("type", type.toString());
        object.put("type_desc", type.getDesc());
        object.put("scope", scope.toString());
        object.put("scope_desc", scope.getDesc());
        object.put("value", typeValue);
        if ("default".equals(city)) {
            object.put("city", city);
            object.put("city_desc", "默认全国");
        } else {
            object.put("city", city);
            object.put("city_desc", AreaCache.getAddress(city).getName());
        }
        object.put("startTime", DateUtils.StringToDate(startTime + " 00:00:00"));
        if (!CurrentRequest.isAdmin()) {
            object.put("uuid", CurrentRequest.getCurrentUserId());
        } else {
            object.put("uuid", "admin");
        }
        object.put("endTime", DateUtils.StringToDate(endTime + " 23:59:59"));

        baseDao.insert(object, MongoTable.carousel_img);
        return object;
    }

    @Override
    public void delCarousel(String cid) {
        MCondition mc = MCondition.create(MRel.and);
        mc.append("cid", cid);
        baseDao.delete(mc.toDBObject().toMap(), MongoTable.carousel_img);
    }
}
