package fm.yichenet.mongo.service;

import com.mongodb.DBObject;
import fm.entity.ArticleComment;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.yichenet.dto.ArticleDto;

import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-05-26.
 */
public interface ArticleService {

    DBObject mergeArticle(ArticleDto articleDto) throws Exception;

    void topArticle(String id) throws Exception;

    void unTopArticle(String id) throws Exception;

    List getArticleForPage(Map params, Integer pageSize, Integer pageNum) throws Exception;

    long countArticles(Map params) throws Exception;

    DBObject commentArticle(ArticleComment articleComment) throws Exception;

    List getCommentsList(Integer pageSize, Integer pageNum, String article_id, Integer type) throws Exception;

    long countComments(String article_id, Integer type) throws Exception;

    DBObject getArticleInfo(String article_id) throws Exception;

    void deleteArticle(String article_id) throws Exception;


    void updateArticle(String articleId, DBObject article);


    long countInfoArticle(Map param);

    List<DBObject> getInfoArticleList(DBObject query) throws Exception;

    void addInfoArticle(DBObject info) throws Exception;

    void deleteInfoArticle(DBObject query);

    void deleteUserArticle(WxUser wxUser, String articleId);
}
