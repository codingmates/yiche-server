package fm.yichenet.mongo.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.entity.WxUser;
import fm.mongo.MongoTable;
import fm.nio.SemaphoreExecutor;
import fm.util.CommonUtils;
import fm.yichenet.mongo.service.ShoppingCartService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.hadoop.util.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Map;
import java.util.List;

/**
 * Created by 63267 on 2017/6/18.
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Override
    public List getShoppingCartForPage(WxUser wxUser, Integer pageSize, Integer pageNum) throws Exception {

        Map<String,Object> query = new HashedMap();
        query.put("wx_user_id",wxUser.getId());
        DBObject sort = new BasicDBObject("create_time", Sort.Direction.DESC.toString());

        List<DBObject> carts = (List<DBObject>)mongoBaseDao.getPageList(query,DBObject.class,pageSize,pageNum, MongoTable.shopping_cart,sort);
        if (CollectionUtils.isNotEmpty(carts)) {
            SemaphoreExecutor executor = new SemaphoreExecutor(10, "queryGoodThreads");
            for (final DBObject cart : carts) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        String goodId = String.valueOf(cart.get("good_id"));
                        Map<String,Object> goodQuery = new HashedMap();
                        goodQuery.put("good_id",goodId);
                        DBObject good = (DBObject) mongoBaseDao.findOne(goodQuery,DBObject.class,MongoTable.good);
                        good.removeField("content");
                        cart.put("good_info",good);
                    }
                });
            }
        }
        return carts;
    }

    @Override
    public DBObject addShoppingCart(WxUser wxUser, Integer goodNum, String goodId) throws Exception {
        Map<String,Object> query = new HashedMap();
        query.put("good_id",goodId);
        query.put("wx_user_id",wxUser.getId());
        Object object = mongoBaseDao.findOne(query,DBObject.class,MongoTable.shopping_cart);
        if(CommonUtils.isEmpty(object)){
            DBObject dbObject = new BasicDBObject();
            dbObject.put("wx_user_id",wxUser.getId());
            dbObject.put("good_id",goodId);
            dbObject.put("good_num",goodNum);
            dbObject.put("create_time",new Timestamp(System.currentTimeMillis()));
            mongoBaseDao.insert(dbObject.toMap(),MongoTable.shopping_cart);
            return dbObject;
        }else{
            DBObject cart = (DBObject)object;
            goodNum = goodNum + Integer.parseInt(String.valueOf(cart.get("good_num")));
            return editShoppingCart(wxUser,goodId,goodNum);

        }

    }

    @Override
    public void deleteShoppingCart(WxUser wxUser, String goodId) throws Exception {
        Map<String,Object> query = new HashedMap();
        query.put("wx_user_id",wxUser.getId());
        query.put("good_id",goodId);
        mongoBaseDao.delete(query,MongoTable.shopping_cart);
    }

    @Override
    public DBObject editShoppingCart(WxUser wxUser, String goodId, Integer goodNum) throws Exception {
        Map<String,Object> query = new HashedMap();
        query.put("wx_user_id",wxUser.getId());
        query.put("good_id",goodId);
        DBObject cart = (DBObject) mongoBaseDao.findOne(query,DBObject.class,MongoTable.shopping_cart);
        cart.put("good_num",goodNum);
        mongoBaseDao.updateOne(query,cart.toMap(),MongoTable.shopping_cart);
        return cart;
    }
}
