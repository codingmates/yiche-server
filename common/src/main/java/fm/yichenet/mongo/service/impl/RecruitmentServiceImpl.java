package fm.yichenet.mongo.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.dao.MongoBaseDao;
import fm.exception.BizException;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.web.CurrentRequest;
import fm.yichenet.mongo.service.RecruitmentService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by CM on 2017/7/1.
 */
@Service
public class RecruitmentServiceImpl implements RecruitmentService {
    @Autowired
    MongoBaseDao mongoBaseDao;

    @Override
    public DBObject addRecurit(String title, String salary, String worktime, String content) throws Exception {
        if (StringUtils.isEmpty(title)) {
            throw new BizException("请务必填写招聘职位！");
        }
        if (StringUtils.isEmpty(salary)) {
            salary = "面议";
        }
        if (StringUtils.isEmpty(worktime)) {
            throw new BizException("请填写工作时间范围");
        }
        if (StringUtils.isEmpty(content)) {
            content = "了解详细内容，请联系企业~";
        }

        String id = UUID.randomUUID().toString().replaceAll("-", "");
        DBObject recuritment = new BasicDBObject("_id", id);

        recuritment.put("title", title);
        recuritment.put("salary", salary);
        recuritment.put("worktime", worktime);
        recuritment.put("content", content);
        recuritment.put("shop_id", CurrentRequest.getCurrentUserId());
        recuritment.put("create_time", new Date());

        mongoBaseDao.insert(recuritment, MongoTable.shopRecuritment);
        return recuritment;
    }

    @Override
    public void delRecurit(String recuritId) {
        if (StringUtils.isEmpty(recuritId)) {
            throw new BizException("参数缺失！");
        }

        mongoBaseDao.delete(new BasicDBObject("_id", recuritId).toMap(), MongoTable.shopRecuritment);
    }

    @Override
    public List<DBObject> recuritList(String keyword, Long shopId, Integer pageNum, Integer pageSize) throws Exception {
        MCondition mc = MCondition.create(MRel.and);
        if (StringUtils.isNotEmpty(keyword)) {
            mc.append(MCondition.create(MRel.regex).append("title", keyword));
        }
        if (shopId != null) {
            mc.append("shop_id", shopId);
        }
        if (pageNum != null && pageSize != null) {
            Collection<DBObject> data = mongoBaseDao.getPageList(mc.toDBObject().toMap(), DBObject.class, pageSize, pageNum,
                    MongoTable.shopRecuritment, new BasicDBObject("create_time", Sort.Direction.DESC.toString()));
            return (List<DBObject>) data;
        } else {
            Collection<DBObject> data = mongoBaseDao.getList(mc.toDBObject().toMap(), DBObject.class,
                    MongoTable.shopRecuritment, new BasicDBObject("create_time", Sort.Direction.DESC.toString()));
            return (List<DBObject>) data;
        }
    }
}
