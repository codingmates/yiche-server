package fm.yichenet.service;

import fm.entity.OrderGood;
import fm.entity.OrderTransaction;
import fm.entity.WxUser;
import fm.entityEnum.OrderEnum;
import fm.entityEnum.PayStatusEnum;
import fm.exception.BizException;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-06-19.
 */
public interface OrderService {

    Map<String, Object> addOrder(List<OrderGood> paramsList, WxUser wxUser) throws Exception;

    OrderGood cancelOrder(String orderId, WxUser wxUser) throws Exception;

    List<OrderGood> orderList(Integer pageSize, Integer pageNum, WxUser wxUser) throws Exception;

    OrderTransaction addOrderTransaction(List<String> orderGoodIds) throws BizException;

    /**
     * 获取店铺订单数据
     *
     * @param pageSize
     * @param pageNum
     * @param shopId    店铺ID
     * @param minAmount 最小金额
     * @param maxAmount 最大金额
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param phone     客户手机号码
     * @return
     */
    List<OrderGood> getShopOrder(Integer pageSize, Integer pageNum, Long shopId, Double minAmount,
                                 Double maxAmount, Date startTime, Date endTime, String phone, String status);

    /**
     * 统计店铺订单数量
     *
     * @param shopId
     * @param minAmount
     * @param maxAmount
     * @param startTime
     * @param endTime
     * @param phone
     * @return
     */
    long countShopOrder(Long shopId, Double minAmount,
                        Double maxAmount, Date startTime, Date endTime, String phone, String status);

    /**
     * 获取订单信息
     *
     * @param orderId
     * @return
     */
    OrderGood getOrderById(String orderId);

    /**
     * 修改订单金额
     *
     * @param price
     * @param remarks
     */
    void updateOrderPrice(Double price, String remarks, String orderId) throws Exception;

    /**
     * 订单取消
     *
     * @param orderId
     */
    void cancelOrder(String orderId);

    /**
     * 订单发货
     *
     * @param orderId
     */
    void postOrder(String orderId, String expressNo);

    /**
     * 订单退款
     *
     * @param orderId
     * @param reason
     * @param amount
     */
    void refundsOrder(String orderId, String reason, Double amount);

    void refundsOrderDeal(String orderId, String reason, Integer status);

    /**
     * 修改订单
     *
     * @param orderGood
     */
    void updateOrder(OrderGood orderGood);

    /**
     * 获取个人订单
     *
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<OrderGood> getUserOrder(OrderEnum status, Long userId, Integer pageNum, Integer pageSize);

    /**
     * 统计用户的订单数量
     *
     * @param status
     * @param userId
     * @return
     */
    long countUserOrder(OrderEnum status, Long userId);

    /**
     * 获取店铺订单
     *
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<OrderGood> getShopOrder(OrderEnum status, Long userId,Integer refundsStatus, Integer pageNum, Integer pageSize);

    /**
     * 统计商铺 的订单数量
     *
     * @param status
     * @param userId
     * @return
     */
    long countShopOrder(OrderEnum status, Long userId);

    /**
     * 获取订单的完整信息，包括店铺、商品、地址、物流等信息
     *
     * @param orders
     * @return
     */
    List<Map<String, Object>> getFullInfoOrder(List<OrderGood> orders);

    /**
     * 获取交易订单
     *
     * @param orderId
     * @return
     */
    OrderTransaction getOrderTransactionById(String orderId);

    void updateOrderTransactionWxOrder(String orderId, String wxOrderId, String totalFee) throws Exception;

    void updateOrderTransactionStatus(String orderId, PayStatusEnum payStatusEnum) throws Exception;

    void updateOrderGoodStatus(String orderId, OrderEnum orderEnum) throws Exception;

    List<OrderTransaction> getOrderTransactionPayingList();

    void updateUserOrderComplete(WxUser wxUser,String orderId) throws BizException;

    /**
     * 获取全部自动收货订单
     * @return
     */
    List<OrderGood> getAutoCompleteOrderGoodList();

    /**
     * 更新自动收货订单
     * @param orderGood
     */
    void updateAutoCompleteOrderGood(OrderGood orderGood);

    /**
     * 完成订单金额结算到用户
     * @param orderGood
     */
    void updateTransferCompleteOrder(OrderGood orderGood);

    /**
     * 获取可结算订单
     * @return
     */
    List<OrderGood> getTransferCompleteOrderList();
}
