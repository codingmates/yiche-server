package fm.yichenet.service;

import fm.entity.OrderRepair;
import fm.entity.OrderTransaction;
import fm.entity.WxUser;
import fm.entityEnum.RepairEnum;
import fm.exception.BizException;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by 宏炜 on 2017-10-24.
 */
public interface RepairService {
    OrderRepair addPublish(String content,  String title,  String type,
                        String startTime, String endTime,  String address,  String province,
                         String city,  String district,String pics,
                         String phone,  String loc) throws Exception;

    List getEnginemenOrder(WxUser wxUser,Integer pageNum,Integer pageSize,String status,String type) throws InterruptedException;

    List getPublishOrder(WxUser wxUser,Integer pageNum,Integer pageSize,String status,String type) throws InterruptedException;

    void updateCheckRepairOrder(WxUser wxUser,String orderId) throws BizException;

    OrderRepair getCheckRepairOrder(WxUser wxUser,String orderId) throws BizException;

    void updateOrderRepairComplete(WxUser wxUser,String orderId,String amount) throws BizException;

    void commentsRepairOrder(WxUser wxUser,String orderId,Integer level,String content,String pics) throws Exception;

    OrderTransaction addOrderTransaction(String orderId) throws BizException;

    void updateRepairStatus(String orderId, RepairEnum repairEnum) throws BizException;

    List getRepairListByTimeDistance(Double lat, Double lon, String startTime, String province, String city, String district, String type) throws BizException;
}
