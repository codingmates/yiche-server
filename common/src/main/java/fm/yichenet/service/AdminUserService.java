package fm.yichenet.service;

import com.mongodb.DBObject;
import fm.entity.WxUser;

/**
 * Created by CM on 17/5/8.
 */
public interface AdminUserService {

    /**
     * 验证登录短信验证码
     *
     * @param phone
     * @param vrcd
     * @return
     */
    boolean checkSms(String phone, String vrcd) throws Exception;

    /**
     * 验证密码
     *
     * @param phone
     * @param pass
     * @return
     */
    boolean checkPass(String phone, String pass);

    /**
     * 根据手机号码获取登录的商户信息
     *
     * @param phone
     * @return
     */
    WxUser getMerchantByPhone(String phone);


    /**
     * 更新商户认证资料的信息
     *
     * @param userId
     * @param lng
     * @param lat
     * @param userType
     */
    void updateMerchantAuthInfo(Long userId, String lng, String lat, Integer userType);

    /**
     * 重置审核状态
     *
     * @param userId
     */
    void updateReviewStatus(Long userId);


}
