package fm.yichenet.service;

import fm.entity.AccountBalance;
import fm.entity.AccountBalanceOperationLog;

import java.math.BigDecimal;

/**
 * Created by CM on 2017/6/28.
 */
public interface AccountBalanceService {

    /**
     * 获取用户的账户信息
     *
     * @param userId
     * @return
     */
    AccountBalance addOrGetBalance(Long userId);

    /**
     * 更新余额：正数为加 负数为减
     *
     * @param amount
     */
    void updateBalance(BigDecimal amount, Long userId, String operationRemark);

    /**
     * 创建账户
     *
     * @param wxUserId
     */
    void createBalance(Long wxUserId);

    /**
     * 添加操作日志
     *
     * @param log
     */
    void addOperationLog(AccountBalanceOperationLog log);


}
