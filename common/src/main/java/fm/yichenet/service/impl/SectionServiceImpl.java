package fm.yichenet.service.impl;

import fm.dao.HibernateBaseDao;
import fm.entity.Section;
import fm.exception.BizException;
import fm.yichenet.service.SectionService;
import fm.util.CommonUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2017-05-30.
 */
@Service
public class SectionServiceImpl implements SectionService {
    @Autowired
    HibernateBaseDao hibernateBaseDao;

    @Override
    public Section getById(long id) {

        return (Section) hibernateBaseDao.getById(Section.class, id);
    }

    @Override
    public List getAllSection(Integer status) {
        List<Section> sections;
        if (status != null) {
            String hql = "from Section where status = " + status;
            sections = (List<Section>) hibernateBaseDao.queryForList(hql);

        } else {
            sections = (List<Section>) hibernateBaseDao.getAll(Section.class);
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (Section section : sections) {
            Map<String, Object> map = new HashedMap();
            map.put("id", section.getId());
            map.put("sectionName", CommonUtils.isEmpty(section.getSectionName()) ? "" : section.getSectionName());
            map.put("status", CommonUtils.isEmpty(section.getStatus()) ? "" : section.getStatus());
            map.put("sectionDesc", CommonUtils.isEmpty(section.getSectionDesc()) ? "" : section.getSectionDesc());
            map.put("sectionImage", CommonUtils.isEmpty(section.getSectionImage()) ? "" : section.getSectionImage());
            list.add(map);

        }
        return list;
    }

    @Override
    public List getSectionPageList(Integer pageSize, Integer pageNum) {
        String hql = "from Section";
        List<Section> sections = (List<Section>) hibernateBaseDao.pageQuery(hql, pageNum, pageSize);
        List<Map<String, Object>> list = new ArrayList<>();
        for (Section section : sections) {
            Map<String, Object> map = new HashedMap();
            map.put("id", section.getId());
            map.put("sectionName", CommonUtils.isEmpty(section.getSectionName()) ? "" : section.getSectionName());
            map.put("status", CommonUtils.isEmpty(section.getStatus()) ? "" : section.getStatus());
            map.put("sectionDesc", CommonUtils.isEmpty(section.getSectionDesc()) ? "" : section.getSectionDesc());
            map.put("sectionImage", CommonUtils.isEmpty(section.getSectionImage()) ? "" : section.getSectionImage());
            list.add(map);

        }
        return list;
    }

    @Override
    public long countSection() {
        String hql = "select count(*) from Section";
        return (Long) hibernateBaseDao.getUnique(hql);
    }

    @Override
    public void updateSection(Section section) throws BizException {
        if (CommonUtils.isEmpty(section.getSectionName())) {
            throw new BizException("板块名称不能为空");
        }
        if (CommonUtils.isEmpty(section.getStatus())) {
            throw new BizException("板块状态不能为空");
        }

        hibernateBaseDao.merge(section);
    }

    @Override
    public void addSection(Section section) throws BizException {
        if (CommonUtils.isEmpty(section.getSectionName())) {
            throw new BizException("板块名称不能为空");
        }
        if (CommonUtils.isEmpty(section.getStatus())) {
            throw new BizException("板块状态不能为空");
        }

        hibernateBaseDao.add(section);
    }


}
