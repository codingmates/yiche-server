package fm.yichenet.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.tencent.common.MD5;
import fm.dao.HibernateBaseDao;
import fm.dao.MongoBaseDao;
import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.entityEnum.ActionTypeEnum;
import fm.exception.BizException;
import fm.mongo.MCondition;
import fm.mongo.MRel;
import fm.mongo.MongoTable;
import fm.util.DateUtils;
import fm.yichenet.service.AdminUserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by CM on 17/5/8.
 */
@Service
public class AdminUserServiceImpl implements AdminUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminUserService.class);

    @Autowired
    HibernateBaseDao hibernateBaseDao;

    @Autowired
    MongoBaseDao mongoBaseDao;

    @Override
    public boolean checkSms(String phone, String vrcd) throws Exception {
//        if ("18359219330".equals(phone)) {
//            return true;
//        }
        MCondition mc = MCondition.create(MRel.and);
        mc.append("phone", phone);
        mc.append("type", ActionTypeEnum.LOGIN_VERIFY.toString());
        mc.append("check_status", 0);
        mc.append(MCondition.create(MRel.lte).append("latm", new Date()));
        mc.append(MCondition.create(MRel.gte).append("latm", DateUtils.addMinute(new Date(), -30)));

        List<DBObject> items = (List<DBObject>) mongoBaseDao.getPageList(mc.toDBObject().toMap(), DBObject.class, 1, 1, MongoTable.sms_log, new BasicDBObject("latm", Sort.Direction.DESC.toString()));
        if (CollectionUtils.isEmpty(items)) {
            throw new BizException("短信验证码无效");
        }

        boolean result = String.valueOf(items.get(0).get("content")).equals(vrcd);
        if (result == true) {
            mongoBaseDao.updateOne(mc.toDBObject().toMap(), new BasicDBObject("check_status", 1).toMap(), MongoTable.sms_log);
        }
        return result;
    }

    @Override
    public boolean checkPass(String phone, String pass) {
        String hql = "from PublicNumber pn where pn.telephoneNumber=? and pn.pass=?";
        PublicNumber publicNumber = (PublicNumber) hibernateBaseDao.getUnique(hql, phone, MD5.MD5Encode(pass));
        return publicNumber != null;
    }

    @Override
    public WxUser getMerchantByPhone(String phone) {
        String hql = "from WxUser wu where wu.telephone =? and (wu.userType=3 or wu.userType=4)";
        WxUser user = (WxUser) hibernateBaseDao.getUnique(hql, phone);
        return user;
    }

    @Override
    public void updateMerchantAuthInfo(Long userId, String lng, String lat, Integer userType) {
        String hql = "from WxUser where id = ?";
        WxUser user = (WxUser) hibernateBaseDao.getUnique(hql, userId);
        user.setReviewStatus(1);
        user.setUserType(userType);
        user.setLongitude(new BigDecimal(Double.parseDouble(lng)));
        user.setLatitude(new BigDecimal(Double.parseDouble(lat)));

        hibernateBaseDao.update(user);
    }

    @Override
    public void updateReviewStatus(Long userId) {
        String hql = "from WxUser where id = ?";
        WxUser user = (WxUser) hibernateBaseDao.getUnique(hql, userId);
        user.setReviewStatus(1);
        hibernateBaseDao.update(user);
    }

}
