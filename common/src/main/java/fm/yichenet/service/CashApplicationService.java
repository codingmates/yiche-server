package fm.yichenet.service;

import fm.entity.CashApplication;
import fm.entityEnum.CrashStatusEnum;

import java.util.List;

/**
 * Created by CM on 2017/6/28.
 */
public interface CashApplicationService {

    /**
     * 获取用户的提现列表数据
     *
     * @param userId
     * @return
     */
    List<CashApplication> getUserCashApplications(Long userId, String startTime, String endTime, CrashStatusEnum status, Integer pageNum, Integer pageSize);

    /**
     * 统计提现申请数据
     *
     * @param userId
     * @param startTime
     * @param endTime
     * @return
     */
    long countUserCashApplications(Long userId, String startTime, String endTime, CrashStatusEnum status);

    /**
     * 获取申请
     *
     * @param id
     * @return
     */
    CashApplication getById(Long id);

    /**
     * 更新
     *
     * @return
     */
    CashApplication update(CashApplication application);

    /**
     * 发起体现申请
     *
     * @param application
     * @return
     */
    CashApplication add(CashApplication application) throws Exception;

    /**
     * 通过申请
     *
     * @param id
     * @param remark 备注
     */
    void applyApplication(Long id, String remark);

    /**
     * 驳回申请
     *
     * @param id
     * @param remark
     */
    void refuseApplication(Long id, String remark);


}
