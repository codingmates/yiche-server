package fm.yichenet.service;

import fm.entity.Section;
import fm.exception.BizException;

import java.util.List;

/**
 * Created by 宏炜 on 2017-05-30.
 */
public interface SectionService {
    Section getById(long id);

    List getAllSection(Integer status);

    List getSectionPageList(Integer pageSize, Integer pageNum);

    long countSection();

    void updateSection(Section section) throws BizException;

    void addSection(Section section) throws BizException;
}
