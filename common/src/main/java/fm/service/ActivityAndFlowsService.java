package fm.service;

import fm.dto.ActivityAndFlows;
import fm.entity.Activity;
import fm.entity.ActivityFlow;
import fm.entity.Flow;
import fm.exception.BizException;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2016-03-07.
 */
public interface ActivityAndFlowsService {


    public Map addActivityAndFlows(ActivityAndFlows activityAndFlows) throws BizException;

    public Integer addFlows(List<ActivityFlow> activityFlowList,Activity activity);

    public Map updateActivityAndFlows(ActivityAndFlows activityAndFlows) throws BizException;

    /**
     * 获取活动信息
     * @param id
     * @return
     */
    public Activity getActivity(Long id) throws BizException;

    /**
     * 删除活动
     * @return
     */
    public void deleteActivity(Long id) throws BizException;

    /**
     * 获取活动列表
     * @return
     */
    public List getActivityList(Integer page,Integer size) throws BizException;

    /**
     * 获取活动总数
     * @return
     */
    public Long getActivityCount() throws BizException;

    /**
     * 获取奖品列表
     * @param activityId
     * @param page
     * @param size
     * @return
     * @throws BizException
     */
    public List getActivityFlowList(Long activityId) throws BizException;

    /**
     * 获取奖品数量
     * @param activityId
     * @return
     * @throws BizException
     */
    public Long getActivityFlowCount(Long activityId) throws BizException;

    public List<Activity> getAllActivity();

    public void updateActivityStatus(Activity activity);

    public Flow getFlow(Long id);
}
