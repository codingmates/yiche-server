package fm.service;

import fm.entity.Flow;
import fm.entityEnum.Carrier;
import fm.exception.BizException;

import java.util.List;

/**
 * Created by 宏炜 on 2016-03-08.
 */
public interface FlowService {

    /**
     * 获取流量包列表
     * @return
     */
    public List getFlowList(Carrier carrier) throws BizException;

    /**
     * 获取流量包个数
     * @return
     */
    public Long getFlowCount() throws BizException;
}
