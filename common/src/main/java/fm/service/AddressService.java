package fm.service;

import fm.entity.Address;

import java.util.List;

/**
 * Created by CM on 17/5/23.
 */
public interface AddressService {


    List<Address> getAll();


    List<Address> getByParentId(String parentid);


    Address getById(String addressId);
}
