package fm.service;

import fm.entity.PublicNumber;

/**
 * 类名：fm.service.PublicNumberService
 * 创建者： CM .
 * 创建时间：2016/3/7
 */
public interface PublicNumberService {
    PublicNumber getByUuid(String uuid);

    PublicNumber getByPhone(String phoen);
}
