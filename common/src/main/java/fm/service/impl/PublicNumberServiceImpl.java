package fm.service.impl;

import fm.dao.HibernateBaseDao;
import fm.entity.PublicNumber;
import fm.service.PublicNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 类名：fm.service.impl.PublicNumberService
 * 创建者： CM .
 * 创建时间：2016/3/7
 */
@Service("publicNumberService")
public class PublicNumberServiceImpl implements PublicNumberService {
    @Autowired
    HibernateBaseDao baseDao;

    public PublicNumber getByUuid(String uuid) {
        String hql = "from PublicNumber pn where pn.uuid=?";
        return (PublicNumber) baseDao.getUnique(hql, uuid);
    }

    @Override
    public PublicNumber getByPhone(String phone) {
        String hql = "from PublicNumber pn where pn.telephoneNumber=?";
        return (PublicNumber) baseDao.getUnique(hql, phone);
    }


}
