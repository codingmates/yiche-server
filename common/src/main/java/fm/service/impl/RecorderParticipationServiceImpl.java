package fm.service.impl;

import fm.dao.HibernateBaseDao;
import fm.entity.Activity;
import fm.entity.RecorderParticipation;
import fm.entity.WxUser;
import fm.entityEnum.ExchangeStatus;
import fm.exception.BizException;
import fm.service.RecorderParticipationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏炜 on 2016-03-09.
 */
@Service
public class RecorderParticipationServiceImpl implements RecorderParticipationService {
    @Autowired
    HibernateBaseDao baseDao;

    @Override
    public List getUserParticipationDetailList(Integer page, Integer size, Long activityId) throws BizException {
        Activity activity = (Activity) baseDao.getById(Activity.class, activityId);
        String hql = "from RecorderParticipation where activity = ? ";
        List<RecorderParticipation> list = (List<RecorderParticipation>) baseDao.pageQuery(hql, page, size, activity);
        List<Map> reslist = new ArrayList<>();
        for (RecorderParticipation recorderParticipation : list) {
            Map<String, Object> res = new HashMap<>();
            res.put("telephone", recorderParticipation.getTelephone());
            res.put("openid", recorderParticipation.getWxUser().getOpenid());
            res.put("nickName", recorderParticipation.getWxUser().getNickname());
            res.put("creatTime", recorderParticipation.getCreateTime());
            res.put("winning", recorderParticipation.getWinning());
            res.put("flowName", recorderParticipation.getFlow().getName());
            reslist.add(res);
        }
        return reslist;
    }

    @Override
    public Long getUserParticipationDetailCount(Long activityId) throws BizException {
        Activity activity = (Activity) baseDao.getById(Activity.class, activityId);
        String hql = "select count(id) from RecorderParticipation as a where a.activity = ?";
        return (Long) baseDao.getUnique(hql, activity);
    }

    @Override
    public List getUserAwardingDetailsList(Integer page, Integer size, Long activityId) throws BizException {
        Activity activity = (Activity) baseDao.getById(Activity.class, activityId);
        String hql = "from RecorderParticipation as a where a.activity = ? and a.exchangeStatus = ?";
        List<RecorderParticipation> list = (List<RecorderParticipation>) baseDao.pageQuery(hql, page, size, activity, ExchangeStatus.EXCHANGED);
        List<Map> reslist = new ArrayList<>();
        for (RecorderParticipation recorderParticipation : list) {
            Map<String, Object> res = new HashMap<>();
            res.put("createTime", recorderParticipation.getCreateTime());
            res.put("exchangeTime", recorderParticipation.getExchangeTime());
            res.put("telephone", recorderParticipation.getTelephone());

            reslist.add(res);
        }
        return reslist;
    }

    @Override
    public Long getUserAwardingDetailsCount(Long activityId) throws BizException {
        Activity activity = (Activity) baseDao.getById(Activity.class, activityId);
        String hql = "select count(id) from RecorderParticipation as a where a.activity = ? and a.exchangeStatus = ?";
        return (Long) baseDao.getUnique(hql, activity, ExchangeStatus.EXCHANGED);
    }

    @Override
    public List<RecorderParticipation> getUserRecorderByPhone(WxUser user, String tel) {
        String hql = "from RecorderParticipation rp where rp.wxUser=? and rp.telephone=?";
        return (List<RecorderParticipation>) baseDao.queryForList(hql, user, tel);
    }

    @Override
    public List<RecorderParticipation> getRecorderByPhoneAndActivity(String tel, Activity activity) {
        String hql = "from RecorderParticipation rp where rp.telephone=? and rp.activity=?";
        return (List<RecorderParticipation>) baseDao.queryForList(hql, tel, activity);
    }

    @Override
    public List<RecorderParticipation> getRecorderByPhone(String tel) {

        return null;
    }

    @Override
    public void updateRecorder(RecorderParticipation recorder) {
        baseDao.update(recorder);
    }

    @Override
    public List<RecorderParticipation> getAllNotExchangeRecorder() {
        String hql = "from RecorderParticipation rp where rp.exchangeStatus=?";
        return (List<RecorderParticipation>) baseDao.queryForList(hql, ExchangeStatus.NOEXCHANGE);
    }
}
