package fm.service.impl;

import fm.dao.HibernateBaseDao;
import fm.entity.Flow;
import fm.entityEnum.Carrier;
import fm.exception.BizException;
import fm.service.FlowService;
import fm.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 宏炜 on 2016-03-08.
 */
@Service
public class FlowServiceImpl implements FlowService{
    @Autowired
    HibernateBaseDao baseDao;

    @Override
    public List getFlowList(Carrier carrier) throws BizException {
        if(CommonUtils.isEmpty(carrier)){
            return null;
        }
        String hql = "from Flow where carrier = ?";
        return baseDao.queryForList(hql,carrier);
    }

    @Override
    public Long getFlowCount()  throws BizException {
        String hql = "select count(*) from Flow";
        return (Long)baseDao.getUnique(hql);
    }
}
