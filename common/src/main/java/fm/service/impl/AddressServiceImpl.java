package fm.service.impl;

import fm.dao.HibernateBaseDao;
import fm.entity.Address;
import fm.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by CM on 17/5/23.
 */
@Service("addressService")
public class AddressServiceImpl implements AddressService {
    @Autowired
    HibernateBaseDao baseDao;


    @Override
    public List<Address> getAll() {
        return (List<Address>) baseDao.getAll(Address.class);
    }

    @Override
    public List<Address> getByParentId(String parentid) {
        String hql = "from Address ad where ad.parentId=?";
        return (List<Address>) baseDao.queryForList(hql, parentid);
    }

    @Override
    public Address getById(String addressId) {
        String hql = "from Address ad where ad.addressId=?";
        return (Address) baseDao.getUnique(hql, addressId);
    }
}
