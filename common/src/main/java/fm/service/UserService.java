package fm.service;

import com.mongodb.DBObject;
import fm.entity.WxUser;
import fm.exception.BizException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 类名：fm.service.UserService
 * 创建者： CM .
 * 创建时间：2016/3/7
 */
public interface UserService {

    /**
     * 根据openId和公众号id更新
     *
     * @param user
     * @return
     */
    long saveOrUpdate(WxUser user);

    /**
     * 根据openid 和公众号uuid获取用户信息
     *
     * @param openId
     * @param uuid
     * @return
     */
    WxUser getByOpenIdAndUuid(String openId, String uuid);

    /**
     * 根据三种openid混合取用户
     *
     * @param openId
     * @return
     */
    WxUser getByOpenId(String openId);

    WxUser getUserByIosOpenid(String openId);

    WxUser getUserByAndroidOpenid(String openId);

    WxUser getUserByWechatOpenid(String openId);

    /**
     * 调用 WechatUtils工具类，直接拉取用户信息
     *
     * @param openId
     * @param appId
     * @param appSecret
     * @param request
     * @return
     */
    WxUser getByAuth2(String openId, String appId, String appSecret, HttpServletRequest request) throws Exception;

    /**
     * 根据用户在平台上的ID获取用户信息
     *
     * @param userId
     * @return
     */
    WxUser getById(Long userId);

    /**
     * 获取商铺／所有指定类型的用户水
     *
     * @param userType 用户注册类型
     * @param shopUid  关联商户ID 为空的时候 查询所有数据
     * @return
     */
    List<WxUser> getByTypeAndUserId(String keyword, Integer reviewStatus, Integer userType, Long shopUid, String startTime, String endTime, Integer pageSize,
                                    Integer pageNum);

    /**
     * 统计
     *
     * @param userType
     * @param shopUid
     * @param startTime
     * @param endTime
     * @return
     */
    long countUser(String keyword, Integer reviewStatus, Integer userType, Long shopUid, String startTime, String endTime,
                   Integer pageSize, Integer pageNum);


    /**
     * 根据用户ID 获取用户认证资料
     *
     * @param userId
     * @return
     */
    DBObject getUserAuthInfo(Long userId);


    /**
     * 添加用户认证资料
     *
     * @param userId
     * @param authInfo
     */
    void insertUserAuthInfo(Long userId, DBObject authInfo, Integer userType) throws Exception;

    /**
     * 更新用户认证资料
     *
     * @param userId
     * @param params
     */
    void changeUserAuthInfo(Long userId, Map params, Integer userType);


    List getAllMerchantUser();

    List<WxUser> getAllMerchantUserFull();

    WxUser getMerchantByPhone(String phone);

    WxUser updateUserInfo(Map userInfo, WxUser wxUser) throws BizException;


    WxUser getUserByUnionId(String unionId);

    WxUser getUserByAppOpenId(String openId);

    /**
     * 绑定手机号码 （找回老平台用户）
     *
     * @param wxUser
     * @param phone
     * @return
     * @throws BizException
     */
    WxUser updateOldUserInfo(WxUser wxUser, String phone, String state) throws BizException;

    WxUser getUserByPhone(String phone);


    WxUser getByHxUserName(String userName);

    List<WxUser> getAllUser();
}
