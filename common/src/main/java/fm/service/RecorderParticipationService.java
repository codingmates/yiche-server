package fm.service;

import fm.entity.Activity;
import fm.entity.RecorderParticipation;
import fm.entity.WxUser;
import fm.exception.BizException;

import java.util.List;

/**
 * Created by 宏炜 on 2016-03-09.
 */
public interface RecorderParticipationService {

    /**
     * 用户参与情况明细
     *
     * @param page
     * @param size
     * @param activityId
     * @return
     */
    public List getUserParticipationDetailList(Integer page, Integer size, Long activityId) throws BizException;

    public Long getUserParticipationDetailCount(Long activityId) throws BizException;

    /**
     * 用户兑奖明细
     *
     * @param page
     * @param size
     * @param activityId
     * @return
     */
    public List getUserAwardingDetailsList(Integer page, Integer size, Long activityId) throws BizException;

    public Long getUserAwardingDetailsCount(Long activityId) throws BizException;

    /**
     * 获取用户在活动下的所有中奖纪录
     *
     * @param user
     * @param tel
     * @return
     */
    public List<RecorderParticipation> getUserRecorderByPhone(WxUser user, String tel);

    /**
     * 根据手机号码获取记录
     *
     * @param tel
     * @return
     */
    public List<RecorderParticipation> getRecorderByPhoneAndActivity(String tel, Activity activity);
    public List<RecorderParticipation> getRecorderByPhone(String tel);


    /**
     * 更新记录（兑奖状态）
     *
     * @param recorder
     */
    public void updateRecorder(RecorderParticipation recorder);

    /**
     * 获取所有未兑奖的记录
     *
     * @return
     */
    public List<RecorderParticipation> getAllNotExchangeRecorder();
}
