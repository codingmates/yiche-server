package fm.service;

import fm.entity.VisitorLog;

import java.util.Date;


/**
 * @修改人：CM
 * @修改时间：2017/2/26 0:42
 */
public interface VisitorLogService {
    VisitorLog getByIpAndTime(String ip , Date date);

    void updateOrAdd(String ip);

    long todayVisitor();
}
