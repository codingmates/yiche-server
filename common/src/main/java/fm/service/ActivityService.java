package fm.service;

import com.alibaba.fastjson.JSON;
import fm.dao.HibernateBaseDao;
import fm.entity.*;
import fm.entityEnum.Carrier;
import fm.entityEnum.ExchangeStatus;
import fm.exception.BizException;
import fm.util.CommonUtils;
import fm.util.MathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * class name： fm.servcie.ActivityService
 * author： CM
 * create time： 2016/3/8.
 */
@Service
public interface ActivityService {
    Activity getById(Long id);

    List<RecorderParticipation> getRecorderParticipationsByActivityId(Activity activity);

    List<RecorderParticipation> getUserRecorders(WxUser user, Activity activity);

    List<ActivityFlow> getActivityFlows(Activity activity, Carrier carrier);

    ActivityFlow getPrize(List<ActivityFlow> activityFlows) throws BizException;

    List getWinnerList(Activity activity);

    RecorderParticipation addRecorder(Flow prize, Activity activity, WxUser user, boolean isWinner, String tel);

    Activity updateActivity(Activity activity);

    ShareConfig getShareConfigBuyUser(WxUser user);

    Long addShareConfig(ShareConfig config);
}
