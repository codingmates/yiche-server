package fm.filter;

import fm.entity.WxUser;
import fm.util.CommonUtils;
import fm.util.Constant;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.security.auth.Subject;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 类名： fm.filter.PermisionFilter
 * 创建人： CM
 * 创建时间： 2016/3/6.
 */
public class PermisionFilter extends AuthorizationFilter {
    private static Logger LOGGER = LoggerFactory.getLogger(PermisionFilter.class);


    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object o) throws Exception {
        if(SecurityUtils.getSubject().isAuthenticated()){
            return true;
        }else{
            return false;
        }
    }


    public boolean checkRoleWithResources(){
        return false;
    }
}
