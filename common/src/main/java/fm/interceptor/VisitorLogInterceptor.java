package fm.interceptor;

import fm.dao.HibernateBaseDao;
import fm.entity.VisitorLog;
import fm.service.VisitorLogService;
import fm.util.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 访客记录拦截器
 * 记录访客访问时间
 * TODO 后续此功能不直接访问数据库 而是放入缓存中，避免请求大量资源时影响访问速度
 *
 * @修改人：CM
 * @修改时间：2017/2/25 23:08
 */
public class VisitorLogInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(VisitorLogInterceptor.class);
    @Autowired
    VisitorLogService visitorLogService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        String ip = httpServletRequest.getRemoteHost();
        visitorLogService.updateOrAdd(ip);
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
