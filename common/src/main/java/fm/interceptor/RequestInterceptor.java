package fm.interceptor;

import fm.cache.ConfigCache;
import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.web.CurrentRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * class name： fm.interceptor.TokenInterceptor
 * author： CM
 * create time： 2016/3/12.
 */
public class RequestInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler) throws Exception {

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            modelAndView.addObject("WEB_TITLE", ConfigCache.getConfig("WEB_TITLE"));
            if(CurrentRequest.getCurrentLoginUser() != null){
                Object user = CurrentRequest.getCurrentLoginUser();

                if(user instanceof WxUser){
                    modelAndView.addObject("username",((WxUser) user).getNickname());
                    modelAndView.addObject("uid",((WxUser)user).getId());
                    modelAndView.addObject("userType",0);
                }else if(user instanceof PublicNumber){
                    modelAndView.addObject("username",((PublicNumber) user).getOrganizationName());
                    modelAndView.addObject("uid",((PublicNumber)user).getId());
                    modelAndView.addObject("userType",1);

                }

            }
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }

}
