package fm.huanxin.utils;

import fm.cache.ConfigCache;
import fm.exception.BizException;
import org.apache.commons.lang.StringUtils;

/**
 * Created by CM on 17/6/4.
 */
public class OrgInfo {
    public static String orgName;
    public static String appName;
    public static String clientId;
    public static String clientSecret;
    public static String serverPrefix;
    public static String grantType;


    public static void init() {
        orgName = ConfigCache.getConfig("HUANXIN_ORGNAME");
        appName = ConfigCache.getConfig("HUANXIN_APPNAME");
        clientId = ConfigCache.getConfig("HUANXIN_CLIENT_ID");
        clientSecret = ConfigCache.getConfig("HUANXIN_CLIENT_SECRET");
        serverPrefix = ConfigCache.getConfig("HUANXIN_SERVER_PREFIX");
        grantType = ConfigCache.getConfig("HUANXIN_GRANT_TYPE");
    }

    public static String getOrgName() {
        if (StringUtils.isEmpty(orgName)) {
            throw new BizException("环信相关参数未配置");
        }
        return orgName;
    }

    public static String getAppName() {
        if (StringUtils.isEmpty(appName)) {
            throw new BizException("环信相关参数未配置");
        }
        return appName;
    }

    public static String getClientId() {
        if (StringUtils.isEmpty(clientId)) {
            throw new BizException("环信相关参数未配置");
        }
        return clientId;
    }

    public static String getClientSecret() {
        if (StringUtils.isEmpty(serverPrefix)) {
            throw new BizException("环信相关参数未配置");
        }
        return clientSecret;
    }

    public static String getServerPrefix() {
        if (StringUtils.isEmpty(orgName)) {
            throw new BizException("环信相关参数未配置");
        }
        return serverPrefix;
    }

    public static String getGrantType() {
        if (StringUtils.isEmpty(grantType)) {
            throw new BizException("环信相关参数未配置");
        }
        return grantType;
    }
}
