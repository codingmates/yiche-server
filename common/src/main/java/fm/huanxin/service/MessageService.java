package fm.huanxin.service;

/**
 * Created by CM on 2017/7/12.
 */
public interface MessageService {

    /**
     * 添加一条待发送的信息
     *
     * @param from
     * @param to
     * @param message
     */
    void addMessage(String from, String to, String message) throws Exception;

    /**
     * 发送一条系统消息
     *
     * @param to
     * @param message
     */
    void addSystemMessage(String to, String message) throws Exception;

    /**
     * 发送一条社区消息
     *
     * @param to
     * @param message
     */
    void addForumMessage(String to, String message) throws Exception;
}
