package fm.huanxin.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.cache.ConfigCache;
import fm.dao.MongoBaseDao;
import fm.huanxin.service.MessageService;
import fm.mongo.MongoTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * Created by CM on 2017/7/12.
 */
@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MongoBaseDao mongoBaseDao;

    @Override
    public void addMessage(String from, String to, String message) throws Exception {
        DBObject msg = new BasicDBObject("create_time", new Date());

        msg.put("to", to);
        msg.put("from", from);
        msg.put("message", message);
        msg.put("_id", UUID.randomUUID().toString().replaceAll("-", ""));
        msg.put("status", 0);

        mongoBaseDao.insert(msg, MongoTable.huanxin_message);
    }

    @Override
    public void addSystemMessage(String to, String message) throws Exception {
        addMessage(ConfigCache.getConfig("HUANXIN_SYS_USER", "system"), to, message);
    }

    @Override
    public void addForumMessage(String to, String message) throws Exception {
        addMessage(ConfigCache.getConfig("HUANXIN_FORUM_USER", "forum"), to, message);

    }
}
