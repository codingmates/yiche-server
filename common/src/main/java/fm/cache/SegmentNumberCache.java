package fm.cache;

import fm.dao.HibernateBaseDao;
import fm.entity.Area;
import fm.entity.SegmentNumber;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 类名：fm.cache.SegmentNumberCache
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
public class SegmentNumberCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(SegmentNumberCache.class);

    private static HibernateBaseDao baseDao = (HibernateBaseDao) new
            ClassPathXmlApplicationContext("classpath:applicationContext.xml").getBean("baseDao");
    private static String hql = "from SegmentNumber sn where sn.headNumber=? and CONVERT(sn.midNumberStart,SIGNED)<=? and CONVERT(sn.midNumberEnd,SIGNED)>=?";
    private static final ConcurrentHashMap<String, ConcurrentHashMap<String, SegmentNumber>>
            SEGMENT_NUMBER_CACHE = new ConcurrentHashMap();

    private SegmentNumberCache() {

    }

    /**
     * 重新载入号段
     */
    public static void reloadSegmentNumber() {
        SEGMENT_NUMBER_CACHE.clear();
        List<SegmentNumber> segmentNumbers = (List<SegmentNumber>) baseDao.getAll(SegmentNumber.class);
        for (SegmentNumber segmentNumber : segmentNumbers) {
            String headNumber = segmentNumber.getHeadNumber();
            ConcurrentHashMap map = SEGMENT_NUMBER_CACHE.get(headNumber);
            if (map == null) {
                map = new ConcurrentHashMap();
            }
            map.put(segmentNumber.getMidNumberStart(), segmentNumber);
            SEGMENT_NUMBER_CACHE.put(headNumber, map);
        }
    }

    /**
     * 获取手机归属地
     *
     * @param phoneOrSegment
     * @return
     */
    public static Area getArea(String phoneOrSegment) {
        SegmentNumber segmentNumber = getSegmentNumber(phoneOrSegment);
        if (segmentNumber != null) {
            String areaCodeHql = "from Area a where a.code=?";
            return (Area) baseDao.getUnique(areaCodeHql, segmentNumber.getProvince());
        }
        return null;
    }

    /**
     * 根据手机号码查询号段
     *
     * @param phoneOrSegment
     * @return
     */
    public static SegmentNumber getSegmentNumber(String phoneOrSegment) {
        try {
            if (StringUtils.isBlank(phoneOrSegment)) {
                return null;
            }

            String segmentNumber = phoneOrSegment.substring(0, 7);
            String headNumber = segmentNumber.substring(0, 4);
            String midNumber = segmentNumber.substring(4, 7);
            ConcurrentHashMap<String, SegmentNumber> map = SEGMENT_NUMBER_CACHE.get(headNumber);
            if (CollectionUtils.isEmpty(map)) {
                //若缓存里面没找到，则去数据库里面找
                SegmentNumber segNum = (SegmentNumber) baseDao.getUnique(hql, headNumber, midNumber,midNumber);
                if (segNum == null) {
                    return null;
                }
                map = new ConcurrentHashMap();
                map.put(segNum.getMidNumberStart(), segNum);
                SEGMENT_NUMBER_CACHE.put(headNumber, map);
                return segNum;
            }
            TreeSet<String> keySet = new TreeSet();
            keySet.addAll(map.keySet());
            boolean isNotExists = keySet.add(midNumber);
            //若能添加进去，则返回true
            String key = midNumber;
            if (isNotExists) {
                //为true说明原来的keySet里面不包含这个midNumber,该midNumber不是midNumberStart,则应该取比midNumber小的值为key去取号段信息
                key = keySet.lower(midNumber);
            }
            SegmentNumber segment = null;
            if (StringUtils.isBlank(key)) {
                segment = (SegmentNumber) baseDao.getUnique(hql, headNumber, midNumber,midNumber);
                if (segment != null) {
                    map.put(segment.getMidNumberStart(), segment);
                    return segment;
                }
                return null;
            }
            segment = map.get(key);
            if (segment != null && segment.getMidNumberEnd().compareTo(midNumber) >= 0) {
                //只有当midNumber同时比midNumberEnd小或相等才说明这个来查询的号段是有效的
                return segment;
            } else {

                segment = (SegmentNumber) baseDao.getUnique(hql, headNumber, midNumber,midNumber);
                if (segment != null) {
                    map.put(segment.getMidNumberStart(), segment);
                }
                return segment;
            }
        } catch (Exception e) {
            LOGGER.error("get segment number error:", e);
        }
        return null;
    }

    /**
     * 添加号段
     *
     * @param segmentNumber
     */
    public static void addSegmentNumber(SegmentNumber segmentNumber) {
        String headNumber = segmentNumber.getHeadNumber();
        ConcurrentHashMap midNumberMap = new ConcurrentHashMap();

        midNumberMap.put(segmentNumber.getMidNumberStart(), segmentNumber);
        SEGMENT_NUMBER_CACHE.put(headNumber, midNumberMap);
    }

}
