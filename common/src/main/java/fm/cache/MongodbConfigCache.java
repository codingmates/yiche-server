package fm.cache;

import fm.util.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @修改人：CM
 * @修改时间：2017/2/26 15:58
 */
public class MongodbConfigCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongodbConfigCache.class);
    private static final ConcurrentHashMap<String, MongoDBConfig> mongodbConfig = new ConcurrentHashMap();

    public static void loadCollectionConfig() {
        mongodbConfig.clear();
        PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:mongodb.properties");
        Properties properties = propertiesLoader.getProperties();
        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            try {
                String key = (String) keys.nextElement();
                String collectionName = key.substring(0, key.indexOf("."));
                String configItem = key.substring(key.indexOf("."), key.length());
                MongoDBConfig config = null;
                if (mongodbConfig.containsKey(collectionName)) {
                    config = mongodbConfig.get(collectionName);
                } else {
                    config = new MongoDBConfig();
                }

                if (configItem.indexOf("mongo.host") != -1) {
                    config.setIp(properties.getProperty(key));
                } else if (configItem.indexOf("mongo.port") != -1) {
                    config.setPort(Integer.parseInt(properties.getProperty(key)));
                }
                mongodbConfig.put(collectionName, config);
            } catch (Exception ex) {
                LOGGER.error("载入mongodb配置发生错误", ex);
            }
        }
    }

    /**
     * 获取角色权限
     *
     * @param collectionName
     * @return
     */
    public static MongoDBConfig getPermision(String collectionName) {
        if (mongodbConfig.containsKey(collectionName)) {
            return mongodbConfig.get(collectionName);
        }
        return null;
    }


    static class MongoDBConfig {
        private String ip;
        private int port;

        public MongoDBConfig() {
        }

        public MongoDBConfig(String ip, int port) {
            this.ip = ip;
            this.port = port;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }
    }
}
