package fm.cache;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.exception.BizException;
import fm.service.AddressService;
import fm.util.SpringContextHolder;
import fm.yichenet.mongo.service.GoodMgrService;
import fm.yichenet.mongo.service.ShopMgrService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 商品类别缓存
 */
public class GoodClassCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(GoodClassCache.class);

    private static final ConcurrentHashMap<Long, Boolean> CLASS_HIDE_PRICE_CACHE = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Long, String> CLASS_TITLE_CACHE = new ConcurrentHashMap<>();


    private static ShopMgrService shopMgrService = SpringContextHolder.getBean(ShopMgrService.class);

    public static void loadCache() {
        try {
            synchronized (GoodClassCache.class) {
                CLASS_TITLE_CACHE.clear();
                CLASS_TITLE_CACHE.clear();
                if (shopMgrService == null) {
                    shopMgrService = SpringContextHolder.getBean(ShopMgrService.class);
                }

                Map param = new HashMap();
                param.put("uuid", Long.parseLong(ConfigCache.getConfig("DEFAULT_UUID")));
                DBObject object = shopMgrService.getGoodClassByUuid(param);
                if (object == null) {
                    LOGGER.warn("商品类别信息未找到，缓存失败!");
                }
                BasicDBObject list = (BasicDBObject) object.get("list");
                if (list != null) {
                    BasicDBList firstLevelClass = (BasicDBList) list.get("children");
                    cacheItem(firstLevelClass);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("载入商品类别缓存发生错误:", ex);
        }
        LOGGER.info("商品类别缓存结束");

    }


    public static void cacheItem(BasicDBList children) {
        if (children != null && children.size() > 0) {
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                DBObject classItem = (DBObject) iterator.next();
                CLASS_TITLE_CACHE.put((Long) classItem.get("_id"), (String) classItem.get("title"));
                CLASS_HIDE_PRICE_CACHE.put((Long) classItem.get("_id"), (Boolean) classItem.get("hidePrice"));
                BasicDBList childs = (BasicDBList) classItem.get("children");
                if (childs != null && childs.size() > 0) {
                    cacheItem(childs);
                }
            }
        }
    }

    /**
     * 获取类别是否禁用价格显示
     *
     * @param goodClassId
     * @return
     */
    public static boolean isHidePrice(Long goodClassId) {
        Boolean isHidePrice = CLASS_HIDE_PRICE_CACHE.get(goodClassId);
        if (isHidePrice == null) {
            return false;
        }
        return isHidePrice;
    }

    /**
     * 获取类别标题
     *
     * @param goodClassId
     * @return
     */
    public static String getClassTitle(Long goodClassId) {
        if (CLASS_TITLE_CACHE.containsKey(goodClassId) == false) {
            throw new BizException("未找到对应的商品类别ID");
        }
        return CLASS_TITLE_CACHE.get(goodClassId);
    }
}
