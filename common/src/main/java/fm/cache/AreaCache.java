package fm.cache;

import fm.entity.Address;
import fm.entity.Area;
import fm.service.AddressService;
import fm.util.SpringContextHolder;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by CM on 17/5/23.
 */
public class AreaCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaCache.class);
    private static ConcurrentHashMap<String, Address> ADDRESS_CACHE = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Address> ADDRESS_NAME_CACHE = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Address> ADDRESS_SHORT_NAME_CACHE = new ConcurrentHashMap<>();

    private static ConcurrentHashMap<String, Set<Address>> PROVINCE_CACHE = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Set<Address>> CITY_CACHE = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Set<Address>> AREA_CACHE = new ConcurrentHashMap<>();

    private static List<Address> all_province = new ArrayList<>();
    private static List<Address> all_city = new ArrayList<>();
    private static List<Address> all_area = new ArrayList<>();
    private static List<Address> all_address = new ArrayList<>();

    private static AddressService addressService = SpringContextHolder.getBean(AddressService.class);

    public static void loadCache() {
        if (addressService == null) {
            addressService = SpringContextHolder.getBean(AddressService.class);
        }
        LOGGER.info("载入全国省市区县缓存信息");
        ADDRESS_CACHE.clear();
        PROVINCE_CACHE.clear();
        CITY_CACHE.clear();
        AREA_CACHE.clear();
        all_address.clear();
        all_area.clear();
        all_city.clear();
        all_province.clear();


        List<Address> allCity = addressService.getAll();
        for (Address city : allCity) {
            ADDRESS_NAME_CACHE.put(city.getName(),city);
            ADDRESS_SHORT_NAME_CACHE.put(city.getShortName(),city);
            ADDRESS_CACHE.put(city.getAddressId(), city);
            if (city.getParentId().equals("0")) {
                continue;
            }
            if (city.getParentId().endsWith("0000")) {
                all_province.add(city);
                if (PROVINCE_CACHE.containsKey(city.getParentId())) {
                    Set<Address> citys = PROVINCE_CACHE.get(city.getParentId());
                    citys.add(city);
                } else {
                    Set<Address> citys = new HashSet<>();
                    citys.add(city);
                    PROVINCE_CACHE.put(city.getParentId(), citys);
                }
            } else if (city.getParentId().endsWith("00")) {
                all_city.add(city);

                if (CITY_CACHE.containsKey(city.getParentId())) {
                    Set<Address> citys = CITY_CACHE.get(city.getParentId());
                    citys.add(city);
                } else {
                    Set<Address> citys = new HashSet<>();
                    citys.add(city);
                    CITY_CACHE.put(city.getParentId(), citys);
                }
            } else if (city.getParentId().endsWith("0")) {
                all_area.add(city);

                if (AREA_CACHE.containsKey(city.getParentId())) {
                    Set<Address> citys = AREA_CACHE.get(city.getParentId());
                    citys.add(city);
                } else {
                    Set<Address> citys = new HashSet<>();
                    citys.add(city);
                    AREA_CACHE.put(city.getParentId(), citys);
                }
            } else {
                all_address.add(city);
            }

        }
        LOGGER.info("载入全国省市区县缓存信息------完成");

    }

    public static Address getAddressByName(String name){
        if(MapUtils.isEmpty(ADDRESS_NAME_CACHE)){
            new AreaCache().loadCache();
        }
        return ADDRESS_NAME_CACHE.get(name);
    }

    public static Address getAddressByShortName(String name){
        if(MapUtils.isEmpty(ADDRESS_NAME_CACHE)){
            new AreaCache().loadCache();
        }
        return ADDRESS_SHORT_NAME_CACHE.get(name);
    }


    public static Address getAddress(String code) {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return ADDRESS_CACHE.get(code);
    }

    public static Set<Address> getProvinceCity(String provinceCode) {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return PROVINCE_CACHE.get(provinceCode);
    }

    public static Set<Address> getCityArea(String cityCode) {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return CITY_CACHE.get(cityCode);
    }


    public static Set<Address> getAreaChild(String parentCode) {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return AREA_CACHE.get(parentCode);
    }


    public static Set<Address> getAddressChild(String code) {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        if (code.endsWith("0000")) {
            return getProvinceCity(code);
        } else if (code.endsWith("00")) {
            return getCityArea(code);
        } else if (code.endsWith("0")) {
            return getAreaChild(code);
        } else {
            return null;
        }
    }


    public static List<Address> getAllProvince() {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return all_province;
    }

    public static List<Address> getAllCity() {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return all_city;
    }

    public static List<Address> getAllArea() {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return all_area;
    }

    public static List<Address> getAllAddress() {
        if (MapUtils.isEmpty(ADDRESS_CACHE)) {
            new AreaCache().loadCache();
        }
        return all_address;
    }
}
