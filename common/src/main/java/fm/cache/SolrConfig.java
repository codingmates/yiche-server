package fm.cache;

import fm.util.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Solr properties files cache
 *
 * Created by CM on 17/5/6.
 */
public class SolrConfig{

    private static final ConcurrentHashMap<String,String> solrConfig = new ConcurrentHashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(SolrConfig.class);


    public static void loadCache() {
        LOGGER.info("loading solr config into cache....");
        solrConfig.clear();
        PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:solr.properties");
        Properties properties = propertiesLoader.getProperties();
        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            solrConfig.put(key, properties.getProperty(key));
        }

    }

    public static Object getConfig(String key) {
        if (solrConfig.containsKey(key)) {
            return solrConfig.get(key);
        }
        return null;
    }
}
