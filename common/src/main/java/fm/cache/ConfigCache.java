package fm.cache;

import fm.util.PropertiesLoader;

import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 文件描述：
 * 更新时间：2017/2/23
 * 更新人： codingmates@gmail.com .
 */
public class ConfigCache {
    private static final ConcurrentHashMap<String, String> configCache = new ConcurrentHashMap();

    public static void loadProjectConfig() {
        configCache.clear();
        PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:config.properties");
        Properties properties = propertiesLoader.getProperties();
        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            configCache.put(key, properties.getProperty(key));
        }
    }

    public static String getConfig(String configName) {
        if (configCache.containsKey(configName)) {
            return configCache.get(configName);
        }
        return null;
    }

    public static String getConfig(String configName,String defaultValue) {
        if (configCache.containsKey(configName)) {
            return configCache.get(configName);
        }
        return defaultValue;
    }
}
