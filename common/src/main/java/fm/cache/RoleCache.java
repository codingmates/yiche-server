package fm.cache;

import fm.util.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 文件描述：角色权限缓存类
 * 更新时间：2017/2/23
 * 更新人： codingmates@gmail.com .
 */
public class RoleCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(RoleCache.class);
    private static final ConcurrentHashMap<String, List<String>> roleCache = new ConcurrentHashMap();

    public static void loadRoleConfig() {
        roleCache.clear();
        PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:role.properties");
        Properties properties = propertiesLoader.getProperties();
        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            try {
                String value = URLDecoder.decode(properties.getProperty(key), "UTF-8");
                List<String> roles = Arrays.asList(value.split(","));
                roleCache.put(key, roles);
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("获取角色配置失败:", e);
            }
        }
    }

    /**
     * 获取角色权限
     *
     * @param roleName
     * @return
     */
    public static List<String> getPermision(String roleName) {
        if (roleCache.containsKey(roleName)) {
            return roleCache.get(roleName);
        }
        return null;
    }


}
