package fm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 类名：fm.entity.WxUser
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "wx_user", schema = "fm_db", catalog = "")
public class WxUser {
    private long id;
    private String nickname;
    private String headimgurl;
    private String country;
    private String city;
    private String province;
    private String remark;
    private Integer subscribed;
    private String telephone;
    private String privilege;
    private String openid;
    private long publicNumberId;
    private Integer registerType;
    private String iosOpenid;
    private String androidOpenid;
    private Timestamp firstTime;
    private Long firstShopId;
    private Integer invalid;
    private String oldId;
    private Integer userType;
    private Integer reviewStatus;
    private String reviewNotes;
    private String hxUsername;
    private String hxPassword;
    private String token;
    private Integer status;
    private String sectionDesc;
    private String sectionImage;
    private String sectionName;
    private String portalImgUrl;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String district;
    private String street;
    private String streetNum;
    private String unionId;


    public void setPublicNumberId(Long publicNumberId) {
        this.publicNumberId = publicNumberId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nickname", nullable = true, insertable = true, updatable = true, length = 40)
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "headimgurl", nullable = true, insertable = true, updatable = true, length = 500)
    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    @Basic
    @Column(name = "country", nullable = true, insertable = true, updatable = true, length = 40)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "city", nullable = true, insertable = true, updatable = true, length = 40)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "province", nullable = true, insertable = true, updatable = true, length = 40)
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Basic
    @Column(name = "remark", nullable = true, insertable = true, updatable = true, length = 200)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "subscribed", nullable = true, insertable = true, updatable = true)
    public Integer getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(Integer subscribed) {
        this.subscribed = subscribed;
    }

    @Basic
    @Column(name = "telephone", nullable = true, insertable = true, updatable = true, length = 20)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "privilege", nullable = true, insertable = true, updatable = true, length = 255)
    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Basic
    @Column(name = "openid", nullable = true, insertable = true, updatable = true, length = 100)
    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Basic
    @Column(name = "public_number_id", nullable = true, insertable = true, updatable = true)
    public long getPublicNumberId() {
        return publicNumberId;
    }

    public void setPublicNumberId(long publicNumberId) {
        this.publicNumberId = publicNumberId;
    }

    @Basic
    @Column(name = "hx_username", nullable = true, insertable = true, updatable = true, length = 30)
    public String getHxUsername() {
        return hxUsername;
    }

    public void setHxUsername(String hxUsername) {
        this.hxUsername = hxUsername;
    }

    @Basic
    @Column(name = "hx_password", nullable = true, insertable = true, updatable = true, length = 50)
    public String getHxPassword() {
        return hxPassword;
    }

    public void setHxPassword(String hxPassword) {
        this.hxPassword = hxPassword;
    }

    @Basic
    @Column(name = "token", nullable = true, insertable = true, updatable = true, length = 200)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WxUser user = (WxUser) o;

        if (id != user.id) return false;
        if (publicNumberId != user.publicNumberId) return false;
        if (nickname != null ? !nickname.equals(user.nickname) : user.nickname != null) return false;
        if (headimgurl != null ? !headimgurl.equals(user.headimgurl) : user.headimgurl != null) return false;
        if (country != null ? !country.equals(user.country) : user.country != null) return false;
        if (city != null ? !city.equals(user.city) : user.city != null) return false;
        if (province != null ? !province.equals(user.province) : user.province != null) return false;
        if (remark != null ? !remark.equals(user.remark) : user.remark != null) return false;
        if (subscribed != null ? !subscribed.equals(user.subscribed) : user.subscribed != null) return false;
        if (telephone != null ? !telephone.equals(user.telephone) : user.telephone != null) return false;
        if (privilege != null ? !privilege.equals(user.privilege) : user.privilege != null) return false;
        if (openid != null ? !openid.equals(user.openid) : user.openid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + (headimgurl != null ? headimgurl.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (province != null ? province.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (subscribed != null ? subscribed.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (privilege != null ? privilege.hashCode() : 0);
        result = 31 * result + (openid != null ? openid.hashCode() : 0);
        result = 31 * result + (int) (publicNumberId ^ (publicNumberId >>> 32));
        return result;
    }

    @Basic
    @Column(name = "register_type", nullable = true, length = 20)
    public Integer getRegisterType() {
        return registerType;
    }

    public void setRegisterType(Integer registerType) {
        this.registerType = registerType;
    }

    @Basic
    @Column(name = "ios_openid", nullable = true, length = 100)
    public String getIosOpenid() {
        return iosOpenid;
    }

    public void setIosOpenid(String iosOpenid) {
        this.iosOpenid = iosOpenid;
    }

    @Basic
    @Column(name = "android_openid", nullable = true, length = 100)
    public String getAndroidOpenid() {
        return androidOpenid;
    }

    public void setAndroidOpenid(String androidOpenid) {
        this.androidOpenid = androidOpenid;
    }

    @Basic
    @Column(name = "first_time", nullable = true)
    public Timestamp getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(Timestamp firstTime) {
        this.firstTime = firstTime;
    }

    @Basic
    @Column(name = "first_shop_id", nullable = true)
    public Long getFirstShopId() {
        return firstShopId;
    }

    public void setFirstShopId(Long firstShopId) {
        this.firstShopId = firstShopId;
    }

    @Basic
    @Column(name = "invalid", nullable = true)
    public Integer getInvalid() {
        return invalid;
    }

    public void setInvalid(Integer invalid) {
        this.invalid = invalid;
    }

    @Basic
    @Column(name = "old_id", nullable = true, length = 50)
    public String getOldId() {
        return oldId;
    }

    public void setOldId(String oldId) {
        this.oldId = oldId;
    }

    @Basic
    @Column(name = "user_type", nullable = true)
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Basic
    @Column(name = "review_status", nullable = true, updatable = true)
    public Integer getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(Integer reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    @Basic
    @Column(name = "review_notes", nullable = true, length = 500)
    public String getReviewNotes() {
        return reviewNotes;
    }

    public void setReviewNotes(String reviewNotes) {
        this.reviewNotes = reviewNotes;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "section_desc", nullable = true, length = 100)
    public String getSectionDesc() {
        return sectionDesc;
    }

    public void setSectionDesc(String sectionDesc) {
        this.sectionDesc = sectionDesc;
    }

    @Basic
    @Column(name = "section_image", nullable = true, length = 500)
    public String getSectionImage() {
        return sectionImage;
    }

    public void setSectionImage(String sectionImage) {
        this.sectionImage = sectionImage;
    }

    @Basic
    @Column(name = "section_name", nullable = true, length = 50)
    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @Basic
    @Column(name = "portal_img_url", nullable = true, length = 1000)
    public String getPortalImgUrl() {
        return portalImgUrl;
    }

    public void setPortalImgUrl(String portalImgUrl) {
        this.portalImgUrl = portalImgUrl;
    }

    @Basic
    @Column(name = "longitude", nullable = true, precision = 8)
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "latitude", nullable = true, precision = 8)
    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "district", nullable = true, length = 100)
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Basic
    @Column(name = "street", nullable = true, length = 100)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "street_num", nullable = true, length = 100)
    public String getStreetNum() {
        return streetNum;
    }

    public void setStreetNum(String streetNum) {
        this.streetNum = streetNum;
    }

    @Basic
    @Column(name = "union_id", nullable = true, length = 100)
    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }
}
