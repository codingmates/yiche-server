package fm.entity;

import javax.persistence.*;

/**
 * Created by CM on 17/5/7.
 */
@Entity
@Table(name = "address",catalog = "fm_db")
public class Address {
    private String addressId;
    private String name;
    private String parentId;
    private String shortName;
    private String levelType;
    private String cityCode;
    private String zipCode;
    private String mergerName;
    private String lng;
    private String lat;
    private String pinyin;
    private long id;

    @Basic
    @Column(name = "address_id", nullable = true, length = 255)
    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "parent_id", nullable = true, length = 255)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "short_name", nullable = true, length = 255)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Basic
    @Column(name = "level_type", nullable = true, length = 255)
    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    @Basic
    @Column(name = "city_code", nullable = true, length = 255)
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @Basic
    @Column(name = "zip_code", nullable = true, length = 255)
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Basic
    @Column(name = "merger_name", nullable = true, length = 255)
    public String getMergerName() {
        return mergerName;
    }

    public void setMergerName(String mergerName) {
        this.mergerName = mergerName;
    }

    @Basic
    @Column(name = "lng", nullable = true, length = 255)
    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Basic
    @Column(name = "lat", nullable = true, length = 255)
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Basic
    @Column(name = "pinyin", nullable = true, length = 255)
    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (id != address.id) return false;
        if (addressId != null ? !addressId.equals(address.addressId) : address.addressId != null) return false;
        if (name != null ? !name.equals(address.name) : address.name != null) return false;
        if (parentId != null ? !parentId.equals(address.parentId) : address.parentId != null) return false;
        if (shortName != null ? !shortName.equals(address.shortName) : address.shortName != null) return false;
        if (levelType != null ? !levelType.equals(address.levelType) : address.levelType != null) return false;
        if (cityCode != null ? !cityCode.equals(address.cityCode) : address.cityCode != null) return false;
        if (zipCode != null ? !zipCode.equals(address.zipCode) : address.zipCode != null) return false;
        if (mergerName != null ? !mergerName.equals(address.mergerName) : address.mergerName != null) return false;
        if (lng != null ? !lng.equals(address.lng) : address.lng != null) return false;
        if (lat != null ? !lat.equals(address.lat) : address.lat != null) return false;
        if (pinyin != null ? !pinyin.equals(address.pinyin) : address.pinyin != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = addressId != null ? addressId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
        result = 31 * result + (levelType != null ? levelType.hashCode() : 0);
        result = 31 * result + (cityCode != null ? cityCode.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (mergerName != null ? mergerName.hashCode() : 0);
        result = 31 * result + (lng != null ? lng.hashCode() : 0);
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (pinyin != null ? pinyin.hashCode() : 0);
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
