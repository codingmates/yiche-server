package fm.entity;

import fm.entityEnum.Carrier;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 类名：fm.entity.Flow
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "flow", schema = "", catalog = "fm_db")
public class Flow {
    private long id;
    private String sn;
    private BigDecimal carrierPrice;
    private BigDecimal discount;
    private Integer capacity;
    private Carrier carrier;
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sn", nullable = true, insertable = true, updatable = true, length = 40)
    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    @Basic
    @Column(name = "carrier_price", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getCarrierPrice() {
        return carrierPrice;
    }

    public void setCarrierPrice(BigDecimal carrierPrice) {
        this.carrierPrice = carrierPrice;
    }

    @Basic
    @Column(name = "discount", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Basic
    @Column(name = "capacity", nullable = true, insertable = true, updatable = true)
    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "carrier", nullable = true, insertable = true, updatable = true, length = 20)
    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flow flow = (Flow) o;

        if (id != flow.id) return false;
        if (sn != null ? !sn.equals(flow.sn) : flow.sn != null) return false;
        if (carrierPrice != null ? !carrierPrice.equals(flow.carrierPrice) : flow.carrierPrice != null) return false;
        if (discount != null ? !discount.equals(flow.discount) : flow.discount != null) return false;
        if (capacity != null ? !capacity.equals(flow.capacity) : flow.capacity != null) return false;
        if (carrier != null ? !carrier.equals(flow.carrier) : flow.carrier != null) return false;
        if (name != null ? !name.equals(flow.name) : flow.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (sn != null ? sn.hashCode() : 0);
        result = 31 * result + (carrierPrice != null ? carrierPrice.hashCode() : 0);
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
        result = 31 * result + (carrier != null ? carrier.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
