package fm.entity;

import javax.persistence.*;

/**
 * 类名：fm.entity.Area
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "area", schema = "", catalog = "fm_db")
public class Area {
    private long id;
    private String code;
    private String name;
    private String parentCode;
    private String type;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "code", nullable = false, insertable = true, updatable = true, length = 6)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "parent_code", nullable = false, insertable = true, updatable = true, length = 6)
    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    @Basic
    @Column(name = "type", nullable = false, insertable = true, updatable = true, length = 10)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        if (id != area.id) return false;
        if (code != null ? !code.equals(area.code) : area.code != null) return false;
        if (name != null ? !name.equals(area.name) : area.name != null) return false;
        if (parentCode != null ? !parentCode.equals(area.parentCode) : area.parentCode != null) return false;
        if (type != null ? !type.equals(area.type) : area.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (parentCode != null ? parentCode.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
