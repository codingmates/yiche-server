package fm.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CM on 17/5/7.
 */
@Entity
@Table(name = "merchant_auth_info", schema = "fm_db", catalog = "")
public class MerchantAuthInfo {
    private String companyName;
    private String address;
    private String chargePersonName;
    private String telephone;
    private String businessLicensePic;
    private String idcardPic;
    private String picFid;
    private String phoneNumber;
    private String desc;
    private Timestamp authTime;
    private Integer status;
    private String reviewNotes;
    private Timestamp reviewTime;
    private long id;

    @Basic
    @Column(name = "company_name", nullable = true, length = 200)
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 200)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "charge_person_name", nullable = true, length = 40)
    public String getChargePersonName() {
        return chargePersonName;
    }

    public void setChargePersonName(String chargePersonName) {
        this.chargePersonName = chargePersonName;
    }

    @Basic
    @Column(name = "telephone", nullable = true, length = 20)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "business_license_pic", nullable = true, length = 100)
    public String getBusinessLicensePic() {
        return businessLicensePic;
    }

    public void setBusinessLicensePic(String businessLicensePic) {
        this.businessLicensePic = businessLicensePic;
    }

    @Basic
    @Column(name = "idcard_pic", nullable = true, length = 500)
    public String getIdcardPic() {
        return idcardPic;
    }

    public void setIdcardPic(String idcardPic) {
        this.idcardPic = idcardPic;
    }

    @Basic
    @Column(name = "pic_fid", nullable = true, length = 100)
    public String getPicFid() {
        return picFid;
    }

    public void setPicFid(String picFid) {
        this.picFid = picFid;
    }

    @Basic
    @Column(name = "phone_number", nullable = true, length = 20)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "desc", nullable = true, length = 200)
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Basic
    @Column(name = "auth_time", nullable = true)
    public Timestamp getAuthTime() {
        return authTime;
    }

    public void setAuthTime(Timestamp authTime) {
        this.authTime = authTime;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "review_notes", nullable = true, length = 500)
    public String getReviewNotes() {
        return reviewNotes;
    }

    public void setReviewNotes(String reviewNotes) {
        this.reviewNotes = reviewNotes;
    }

    @Basic
    @Column(name = "review_time", nullable = true)
    public Timestamp getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(Timestamp reviewTime) {
        this.reviewTime = reviewTime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MerchantAuthInfo that = (MerchantAuthInfo) o;

        if (id != that.id) return false;
        if (companyName != null ? !companyName.equals(that.companyName) : that.companyName != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (chargePersonName != null ? !chargePersonName.equals(that.chargePersonName) : that.chargePersonName != null)
            return false;
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) return false;
        if (businessLicensePic != null ? !businessLicensePic.equals(that.businessLicensePic) : that.businessLicensePic != null)
            return false;
        if (idcardPic != null ? !idcardPic.equals(that.idcardPic) : that.idcardPic != null) return false;
        if (picFid != null ? !picFid.equals(that.picFid) : that.picFid != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (desc != null ? !desc.equals(that.desc) : that.desc != null) return false;
        if (authTime != null ? !authTime.equals(that.authTime) : that.authTime != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (reviewNotes != null ? !reviewNotes.equals(that.reviewNotes) : that.reviewNotes != null) return false;
        if (reviewTime != null ? !reviewTime.equals(that.reviewTime) : that.reviewTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = companyName != null ? companyName.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (chargePersonName != null ? chargePersonName.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (businessLicensePic != null ? businessLicensePic.hashCode() : 0);
        result = 31 * result + (idcardPic != null ? idcardPic.hashCode() : 0);
        result = 31 * result + (picFid != null ? picFid.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + (authTime != null ? authTime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (reviewNotes != null ? reviewNotes.hashCode() : 0);
        result = 31 * result + (reviewTime != null ? reviewTime.hashCode() : 0);
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
