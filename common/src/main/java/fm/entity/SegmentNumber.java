package fm.entity;

import fm.entityEnum.Carrier;

import javax.persistence.*;

/**
 * 类名：fm.entity.SegmentNumber
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "segment_number", schema = "", catalog = "fm_db")
public class SegmentNumber {
    private long id;
    private Carrier carrier;
    private String province;
    private String city;
    private String headNumber;
    private String midNumberStart;
    private String midNumberEnd;
    private Long createTime;
    private Long updateTime;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "carrier", nullable = false, insertable = true, updatable = true, length = 256)
    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    @Basic
    @Column(name = "province", nullable = false, insertable = true, updatable = true, length = 256)
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Basic
    @Column(name = "city", nullable = true, insertable = true, updatable = true, length = 256)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "head_number", nullable = false, insertable = true, updatable = true, length = 4)
    public String getHeadNumber() {
        return headNumber;
    }

    public void setHeadNumber(String headNumber) {
        this.headNumber = headNumber;
    }

    @Basic
    @Column(name = "mid_number_start", nullable = false, insertable = true, updatable = true, length = 3)
    public String getMidNumberStart() {
        return midNumberStart;
    }

    public void setMidNumberStart(String midNumberStart) {
        this.midNumberStart = midNumberStart;
    }

    @Basic
    @Column(name = "mid_number_end", nullable = false, insertable = true, updatable = true, length = 3)
    public String getMidNumberEnd() {
        return midNumberEnd;
    }

    public void setMidNumberEnd(String midNumberEnd) {
        this.midNumberEnd = midNumberEnd;
    }

    @Basic
    @Column(name = "create_time", nullable = true, insertable = true, updatable = true)
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "update_time", nullable = true, insertable = true, updatable = true)
    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SegmentNumber that = (SegmentNumber) o;

        if (id != that.id) return false;
        if (carrier != null ? !carrier.equals(that.carrier) : that.carrier != null) return false;
        if (province != null ? !province.equals(that.province) : that.province != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (headNumber != null ? !headNumber.equals(that.headNumber) : that.headNumber != null) return false;
        if (midNumberStart != null ? !midNumberStart.equals(that.midNumberStart) : that.midNumberStart != null)
            return false;
        if (midNumberEnd != null ? !midNumberEnd.equals(that.midNumberEnd) : that.midNumberEnd != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (carrier != null ? carrier.hashCode() : 0);
        result = 31 * result + (province != null ? province.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (headNumber != null ? headNumber.hashCode() : 0);
        result = 31 * result + (midNumberStart != null ? midNumberStart.hashCode() : 0);
        result = 31 * result + (midNumberEnd != null ? midNumberEnd.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
