package fm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by CM on 2017/6/28.
 */
@Entity
@Table(name = "account_balance_operation_log", schema = "fm_db", catalog = "")
public class AccountBalanceOperationLog {
    private long id;
    private String content;
    private Timestamp time;
    private BigDecimal amountBalance;
    private long wxUserId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "content", nullable = true)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "time", nullable = true)
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "amount_balance", nullable = true, precision = 2)
    public BigDecimal getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(BigDecimal amountBalance) {
        this.amountBalance = amountBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountBalanceOperationLog that = (AccountBalanceOperationLog) o;

        if (id != that.id) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (amountBalance != null ? !amountBalance.equals(that.amountBalance) : that.amountBalance != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (amountBalance != null ? amountBalance.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "wx_user_id", nullable = false)
    public long getWxUserId() {
        return wxUserId;
    }

    public void setWxUserId(long wxUserId) {
        this.wxUserId = wxUserId;
    }
}
