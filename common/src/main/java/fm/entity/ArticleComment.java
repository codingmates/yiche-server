package fm.entity;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import fm.util.CommonUtils;

import java.util.Date;

/**
 * Created by 宏炜 on 2017-05-29.
 */
public class ArticleComment {

    private String article_id;
    private Long user_id;
    private Integer type;
    private String content;
    private Long to_user_id;
    private Date comment_time;
    private String user_nickname;
    private String user_head_img_url;
    private String to_user_nickname;
    private String to_user_head_img_url;

    public String getArticle_id() {
        return article_id;
    }

    public void setArticle_id(String article_id) {
        this.article_id = article_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(Long to_user_id) {
        this.to_user_id = to_user_id;
    }

    public Date getComment_time() {
        return comment_time;
    }

    public void setComment_time(Date comment_time) {
        this.comment_time = comment_time;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getUser_nickname() {
        return user_nickname;
    }

    public void setUser_nickname(String user_nickname) {
        this.user_nickname = user_nickname;
    }

    public String getUser_head_img_url() {
        return user_head_img_url;
    }

    public void setUser_head_img_url(String user_head_img_url) {
        this.user_head_img_url = user_head_img_url;
    }

    public String getTo_user_nickname() {
        return to_user_nickname;
    }

    public void setTo_user_nickname(String to_user_nickname) {
        this.to_user_nickname = to_user_nickname;
    }

    public String getTo_user_head_img_url() {
        return to_user_head_img_url;
    }

    public void setTo_user_head_img_url(String to_user_head_img_url) {
        this.to_user_head_img_url = to_user_head_img_url;
    }

    public DBObject toDBObject(){
        DBObject dbObject = new BasicDBObject();
        dbObject.put("article_id",this.article_id);
        dbObject.put("user_id",this.user_id);
        dbObject.put("type",this.type);
        dbObject.put("content",CommonUtils.isEmpty(this.content) ? "" : this.content);
        dbObject.put("to_user_id",CommonUtils.isEmpty(this.to_user_id) ? "" : this.to_user_id);
        dbObject.put("comment_time",this.comment_time);
        dbObject.put("user_nickname",this.user_nickname);
        dbObject.put("user_head_img_url",this.user_head_img_url);
        dbObject.put("to_user_nickname", CommonUtils.isEmpty(this.to_user_nickname) ? "" : this.to_user_nickname);
        dbObject.put("to_user_head_img_url",CommonUtils.isEmpty(this.to_user_head_img_url) ? "" : this.to_user_head_img_url);
        return dbObject;
    }
}
