package fm.entity;

import javax.persistence.*;

/**
 * Created by 宏炜 on 2017-05-30.
 */
@Entity
@Table(name = "section", schema = "fm_db", catalog = "")
public class Section {
    private Long id;
    private String sectionName;
    private Integer status;
    private String sectionDesc;
    private String sectionImage;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "section_name", nullable = true, insertable = true, updatable = true, length = 50)
    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @Basic
    @Column(name = "status", nullable = true, insertable = true, updatable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "section_desc", nullable = true, insertable = true, updatable = true, length = 100)
    public String getSectionDesc() {
        return sectionDesc;
    }

    public void setSectionDesc(String sectionDesc) {
        this.sectionDesc = sectionDesc;
    }

    @Basic
    @Column(name = "section_image", nullable = true, insertable = true, updatable = true, length = 500)
    public String getSectionImage() {
        return sectionImage;
    }

    public void setSectionImage(String sectionImage) {
        this.sectionImage = sectionImage;
    }
}
