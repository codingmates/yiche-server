package fm.entity;

import fm.entityEnum.ExchangeStatus;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 类名：fm.entity.RecorderParticipation
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "recorder_participation", schema = "", catalog = "fm_db")
public class RecorderParticipation {
    private long id;
    private Byte winning;
    private Timestamp createTime;
    private Timestamp exchangeTime;
    private ExchangeStatus exchangeStatus;
    private WxUser wxUser;
    private Activity activity;
    private Flow flow;
    private String telephone;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "winning", nullable = true, insertable = true, updatable = true)
    public Byte getWinning() {
        return winning;
    }

    public void setWinning(Byte winning) {
        this.winning = winning;
    }

    @Basic
    @Column(name = "create_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "exchange_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getExchangeTime() {
        return exchangeTime;
    }

    public void setExchangeTime(Timestamp exchangeTime) {
        this.exchangeTime = exchangeTime;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "exchange_status", nullable = true, insertable = true, updatable = true, length = 20)
    public ExchangeStatus getExchangeStatus() {
        return exchangeStatus;
    }

    public void setExchangeStatus(ExchangeStatus exchangeStatus) {
        this.exchangeStatus = exchangeStatus;
    }


    @ManyToOne(targetEntity = WxUser.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "wx_user_id")
    public WxUser getWxUser() {
        return wxUser;
    }

    public void setWxUser(WxUser wxUser) {
        this.wxUser = wxUser;
    }

    @ManyToOne(targetEntity = Activity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "activity_id")
    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @ManyToOne(targetEntity = Flow.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "flow_id")
    public Flow getFlow() {
        return flow;
    }

    public void setFlow(Flow flow) {
        this.flow = flow;
    }

    @Basic
    @Column(name = "telephone", nullable = true, insertable = true, updatable = true)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
