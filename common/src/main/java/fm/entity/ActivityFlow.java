package fm.entity;

import javax.persistence.*;

/**
 * 类名：fm.entity.ActivityFlow
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "activity_flow", schema = "", catalog = "fm_db")
public class ActivityFlow {
    private long id;
    private int probability;
    private Activity activity;
    private Flow flow;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "probability", nullable = false, insertable = true, updatable = true)
    public int getProbability() {
        return probability;
    }

    public void setProbability(int probability) {
        this.probability = probability;
    }

    @ManyToOne(targetEntity = Activity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "activity_id")
    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @ManyToOne(targetEntity = Flow.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "flow_id")
    public Flow getFlow() {
        return flow;
    }

    public void setFlow(Flow flow) {
        this.flow = flow;
    }
}
