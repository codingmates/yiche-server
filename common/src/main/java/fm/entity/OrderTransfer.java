package fm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by 宏炜 on 2017-09-25.
 */
@Entity
@Table(name = "order_transfer", schema = "fm_db", catalog = "")
public class OrderTransfer {
    private String id;
    private Long toShopId;
    private Long fromUserId;
    private String orderId;
    private Timestamp transferTime;
    private BigDecimal fee;

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "to_shop_id")
    public Long getToShopId() {
        return toShopId;
    }

    public void setToShopId(Long toShopId) {
        this.toShopId = toShopId;
    }

    @Basic
    @Column(name = "from_user_id")
    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    @Basic
    @Column(name = "order_id")
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "transfer_time")
    public Timestamp getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(Timestamp transferTime) {
        this.transferTime = transferTime;
    }

    @Basic
    @Column(name = "fee")
    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderTransfer that = (OrderTransfer) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (toShopId != null ? !toShopId.equals(that.toShopId) : that.toShopId != null) return false;
        if (fromUserId != null ? !fromUserId.equals(that.fromUserId) : that.fromUserId != null) return false;
        if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null) return false;
        if (transferTime != null ? !transferTime.equals(that.transferTime) : that.transferTime != null) return false;
        if (fee != null ? !fee.equals(that.fee) : that.fee != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (toShopId != null ? toShopId.hashCode() : 0);
        result = 31 * result + (fromUserId != null ? fromUserId.hashCode() : 0);
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (transferTime != null ? transferTime.hashCode() : 0);
        result = 31 * result + (fee != null ? fee.hashCode() : 0);
        return result;
    }
}
