package fm.entity;

import fm.entityEnum.ShareTypeEnum;

import javax.persistence.*;

/**
 * class name： fm.entity.ShareConfig
 * author： CM
 * create time： 2016/3/23.
 */
@Entity
@Table(name = "share_config", schema = "", catalog = "fm_db")
public class ShareConfig {
    private long id;
    private ShareTypeEnum shareType;
    private String downloadLink;
    private String tdCode;
    private WxUser wxUser;

    @JoinColumn(name = "wx_user_id")
    @ManyToOne(targetEntity = WxUser.class, fetch = FetchType.LAZY)
    public WxUser getWxUser() {
        return wxUser;
    }

    public void setWxUser(WxUser wxUser) {
        this.wxUser = wxUser;
    }

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "share_type", nullable = true, insertable = true, updatable = true, length = 40)
    public ShareTypeEnum getShareType() {
        return shareType;
    }

    public void setShareType(ShareTypeEnum shareType) {
        this.shareType = shareType;
    }

    @Basic
    @Column(name = "download_link", nullable = true, insertable = true, updatable = true, length = 500)
    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    @Basic
    @Column(name = "td_code", nullable = true, insertable = true, updatable = true, length = 500)
    public String getTdCode() {
        return tdCode;
    }

    public void setTdCode(String tdCode) {
        this.tdCode = tdCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShareConfig that = (ShareConfig) o;

        if (id != that.id) return false;
        if (downloadLink != null ? !downloadLink.equals(that.downloadLink) : that.downloadLink != null) return false;
        if (shareType != null ? !shareType.equals(that.shareType) : that.shareType != null) return false;
        if (tdCode != null ? !tdCode.equals(that.tdCode) : that.tdCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (shareType != null ? shareType.hashCode() : 0);
        result = 31 * result + (downloadLink != null ? downloadLink.hashCode() : 0);
        result = 31 * result + (tdCode != null ? tdCode.hashCode() : 0);
        return result;
    }
}
