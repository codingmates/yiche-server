package fm.entity;

import fm.entityEnum.ActivityStatus;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 类名：fm.entity.Activity
 * 创建者： CM .
 * 创建时间：2016/3/10
 */
@Entity
@Table(name = "activity", schema = "", catalog = "fm_db")
public class Activity {
    private String title;
    private ActivityStatus status;
    private Timestamp startTime;
    private Timestamp endTime;
    private Integer probability;
    private Integer count;
    private Integer remainCount;
    private Integer times;
    private String description;
    private String link;
    private Timestamp createTime;
    private String rule;
    private String logoUrl;
    private long id;
    private int version;
    private String exchangeLink;


    @Version
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Basic
    @Column(name = "title", nullable = true, insertable = true, updatable = true, length = 256)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = true, insertable = true, updatable = true, length = 32)
    public ActivityStatus getStatus() {
        return status;
    }

    public void setStatus(ActivityStatus status) {
        this.status = status;
    }

    @Basic
    @Column(name = "start_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "probability", nullable = true, insertable = true, updatable = true)
    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    @Basic
    @Column(name = "count", nullable = true, insertable = true, updatable = true)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "remain_count", nullable = true, insertable = true, updatable = true)
    public Integer getRemainCount() {
        return remainCount;
    }

    public void setRemainCount(Integer remainCount) {
        this.remainCount = remainCount;
    }

    @Basic
    @Column(name = "times", nullable = true, insertable = true, updatable = true)
    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 1048)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "link", nullable = true, insertable = true, updatable = true, length = 256)
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Basic
    @Column(name = "create_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "rule", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    @Basic
    @Column(name = "logo_url", nullable = true, insertable = true, updatable = true, length = 255)
    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExchangeLink() {
        return exchangeLink;
    }

    public void setExchangeLink(String exchangeLink) {
        this.exchangeLink = exchangeLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Activity activity = (Activity) o;

        if (id != activity.id) return false;
        if (title != null ? !title.equals(activity.title) : activity.title != null) return false;
        if (status != null ? !status.equals(activity.status) : activity.status != null) return false;
        if (startTime != null ? !startTime.equals(activity.startTime) : activity.startTime != null) return false;
        if (endTime != null ? !endTime.equals(activity.endTime) : activity.endTime != null) return false;
        if (probability != null ? !probability.equals(activity.probability) : activity.probability != null)
            return false;
        if (count != null ? !count.equals(activity.count) : activity.count != null) return false;
        if (remainCount != null ? !remainCount.equals(activity.remainCount) : activity.remainCount != null)
            return false;
        if (times != null ? !times.equals(activity.times) : activity.times != null) return false;
        if (description != null ? !description.equals(activity.description) : activity.description != null)
            return false;
        if (link != null ? !link.equals(activity.link) : activity.link != null) return false;
        if (createTime != null ? !createTime.equals(activity.createTime) : activity.createTime != null) return false;
        if (rule != null ? !rule.equals(activity.rule) : activity.rule != null) return false;
        if (logoUrl != null ? !logoUrl.equals(activity.logoUrl) : activity.logoUrl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (probability != null ? probability.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (remainCount != null ? remainCount.hashCode() : 0);
        result = 31 * result + (times != null ? times.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (rule != null ? rule.hashCode() : 0);
        result = 31 * result + (logoUrl != null ? logoUrl.hashCode() : 0);
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
