package fm.entity;

import fm.entityEnum.CompleteEnum;
import fm.entityEnum.OrderEnum;
import fm.entityEnum.OrderTypeEnum;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by 宏炜 on 2017-06-08.
 */
@Entity
@Table(name = "order_good", schema = "fm_db", catalog = "")
public class OrderGood {
    private String id;
    private String transactionOrderId;
    private Timestamp createTime;
    private Timestamp payTime;
    private Timestamp deliverTime;
    private Timestamp completeTime;
    private Timestamp checkTime;
    private OrderEnum status;
    private OrderTypeEnum type;
    private String expressNo;
    private String expressId;
    private Double expressPrice;
    private Integer goodNum;
    private Double unitPrice;
    private Double sumPrice;
    private Double realPay;
    private String remarks;
    private Long userId;
    private String goodId;
    private String receiveAddress;
    private String receivePerson;
    private String receivePhone;
    private String addressId;
    private Long shopId;
    private Long version;
    private Timestamp datetime;
    private Timestamp refundTime;
    private BigDecimal refundAmount;
    private String refundReason;
    private Integer refundStatus;
    private Integer completeType;
    private Integer isTransfer;


    @Basic
    @Version
    @Column(name = "version", nullable = true)
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Id
    @Column(name = "id", insertable = true, updatable = true, nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "transaction_order_id", nullable = true, insertable = true, updatable = true, length = 32)
    public String getTransactionOrderId() {
        return transactionOrderId;
    }

    public void setTransactionOrderId(String transactionOrderId) {
        this.transactionOrderId = transactionOrderId;
    }

    @Basic
    @Column(name = "create_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "pay_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getPayTime() {
        return payTime;
    }

    public void setPayTime(Timestamp payTime) {
        this.payTime = payTime;
    }

    @Basic
    @Column(name = "deliver_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(Timestamp deliverTime) {
        this.deliverTime = deliverTime;
    }

    @Basic
    @Column(name = "complete_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Timestamp completeTime) {
        this.completeTime = completeTime;
    }

    @Basic
    @Column(name = "check_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Timestamp checkTime) {
        this.checkTime = checkTime;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = true, insertable = true, updatable = true, length = 20)
    public OrderEnum getStatus() {
        return status;
    }

    public void setStatus(OrderEnum status) {
        this.status = status;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = true, insertable = true, updatable = true, length = 20)
    public OrderTypeEnum getType() {
        return type;
    }

    public void setType(OrderTypeEnum type) {
        this.type = type;
    }

    @Basic
    @Column(name = "express_no", nullable = true, insertable = true, updatable = true, length = 30)
    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    @Basic
    @Column(name = "express_id", nullable = true, insertable = true, updatable = true, length = 32)
    public String getExpressId() {
        return expressId;
    }

    public void setExpressId(String expressId) {
        this.expressId = expressId;
    }

    @Basic
    @Column(name = "express_price", nullable = true, insertable = true, updatable = true, precision = 0)
    public Double getExpressPrice() {
        return expressPrice;
    }

    public void setExpressPrice(Double expressPrice) {
        this.expressPrice = expressPrice;
    }

    @Basic
    @Column(name = "good_num", nullable = true, insertable = true, updatable = true)
    public Integer getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(Integer goodNum) {
        this.goodNum = goodNum;
    }

    @Basic
    @Column(name = "unit_price", nullable = true, insertable = true, updatable = true, precision = 0)
    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "sum_price", nullable = true, insertable = true, updatable = true, precision = 0)
    public Double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Double sumPrice) {
        this.sumPrice = sumPrice;
    }

    @Basic
    @Column(name = "real_pay", nullable = true, insertable = true, updatable = true, precision = 0)
    public Double getRealPay() {
        return realPay;
    }

    public void setRealPay(Double realPay) {
        this.realPay = realPay;
    }

    @Basic
    @Column(name = "remarks", nullable = true, insertable = true, updatable = true, length = 100)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "user_id", nullable = true, insertable = true, updatable = true)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "good_id", nullable = true, insertable = true, updatable = true, length = 32)
    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    @Basic
    @Column(name = "receive_address", nullable = true, insertable = true, updatable = true, length = 300)
    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    @Basic
    @Column(name = "receive_person", nullable = true, insertable = true, updatable = true, length = 30)
    public String getReceivePerson() {
        return receivePerson;
    }

    public void setReceivePerson(String receivePerson) {
        this.receivePerson = receivePerson;
    }

    @Basic
    @Column(name = "receive_phone", nullable = true, insertable = true, updatable = true, length = 30)
    public String getReceivePhone() {
        return receivePhone;
    }

    public void setReceivePhone(String receivePhone) {
        this.receivePhone = receivePhone;
    }

    @Basic
    @Column(name = "address_id", nullable = true, insertable = true, updatable = true, length = 32)
    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "shop_id", nullable = true, insertable = true, updatable = true)
    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    @Basic
    @Column(name = "datetime", nullable = true)
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "refund_time", nullable = true)
    public Timestamp getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Timestamp refundTime) {
        this.refundTime = refundTime;
    }

    @Basic
    @Column(name = "refund_amount", nullable = true, precision = 2)
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    @Basic
    @Column(name = "refund_reason", nullable = true, length = 500)
    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    @Basic
    @Column(name = "refund_status", nullable = true)
    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    @Basic
    @Column(name = "complete_type", nullable = true)
    public Integer getCompleteType() {
        return completeType;
    }

    public void setCompleteType(Integer completeType) {
        this.completeType = completeType;
    }

    @Basic
    @Column(name = "is_transfer", nullable = true)
    public Integer getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(Integer isTransfer) {
        this.isTransfer = isTransfer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderGood orderGood = (OrderGood) o;

        if (id != null ? !id.equals(orderGood.id) : orderGood.id != null) return false;
        if (transactionOrderId != null ? !transactionOrderId.equals(orderGood.transactionOrderId) : orderGood.transactionOrderId != null)
            return false;
        if (createTime != null ? !createTime.equals(orderGood.createTime) : orderGood.createTime != null) return false;
        if (payTime != null ? !payTime.equals(orderGood.payTime) : orderGood.payTime != null) return false;
        if (deliverTime != null ? !deliverTime.equals(orderGood.deliverTime) : orderGood.deliverTime != null)
            return false;
        if (completeTime != null ? !completeTime.equals(orderGood.completeTime) : orderGood.completeTime != null)
            return false;
        if (checkTime != null ? !checkTime.equals(orderGood.checkTime) : orderGood.checkTime != null) return false;
        if (status != orderGood.status) return false;
        if (type != orderGood.type) return false;
        if (expressNo != null ? !expressNo.equals(orderGood.expressNo) : orderGood.expressNo != null) return false;
        if (expressId != null ? !expressId.equals(orderGood.expressId) : orderGood.expressId != null) return false;
        if (expressPrice != null ? !expressPrice.equals(orderGood.expressPrice) : orderGood.expressPrice != null)
            return false;
        if (goodNum != null ? !goodNum.equals(orderGood.goodNum) : orderGood.goodNum != null) return false;
        if (unitPrice != null ? !unitPrice.equals(orderGood.unitPrice) : orderGood.unitPrice != null) return false;
        if (sumPrice != null ? !sumPrice.equals(orderGood.sumPrice) : orderGood.sumPrice != null) return false;
        if (realPay != null ? !realPay.equals(orderGood.realPay) : orderGood.realPay != null) return false;
        if (remarks != null ? !remarks.equals(orderGood.remarks) : orderGood.remarks != null) return false;
        if (userId != null ? !userId.equals(orderGood.userId) : orderGood.userId != null) return false;
        if (goodId != null ? !goodId.equals(orderGood.goodId) : orderGood.goodId != null) return false;
        if (receiveAddress != null ? !receiveAddress.equals(orderGood.receiveAddress) : orderGood.receiveAddress != null)
            return false;
        if (receivePerson != null ? !receivePerson.equals(orderGood.receivePerson) : orderGood.receivePerson != null)
            return false;
        if (receivePhone != null ? !receivePhone.equals(orderGood.receivePhone) : orderGood.receivePhone != null)
            return false;
        if (addressId != null ? !addressId.equals(orderGood.addressId) : orderGood.addressId != null) return false;
        if (shopId != null ? !shopId.equals(orderGood.shopId) : orderGood.shopId != null) return false;
        if (version != null ? !version.equals(orderGood.version) : orderGood.version != null) return false;
        if (datetime != null ? !datetime.equals(orderGood.datetime) : orderGood.datetime != null) return false;
        if (refundTime != null ? !refundTime.equals(orderGood.refundTime) : orderGood.refundTime != null) return false;
        if (refundAmount != null ? !refundAmount.equals(orderGood.refundAmount) : orderGood.refundAmount != null)
            return false;
        if (refundReason != null ? !refundReason.equals(orderGood.refundReason) : orderGood.refundReason != null)
            return false;
        if (refundStatus != null ? !refundStatus.equals(orderGood.refundStatus) : orderGood.refundStatus != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (transactionOrderId != null ? transactionOrderId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (payTime != null ? payTime.hashCode() : 0);
        result = 31 * result + (deliverTime != null ? deliverTime.hashCode() : 0);
        result = 31 * result + (completeTime != null ? completeTime.hashCode() : 0);
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (expressNo != null ? expressNo.hashCode() : 0);
        result = 31 * result + (expressId != null ? expressId.hashCode() : 0);
        result = 31 * result + (goodNum != null ? goodNum.hashCode() : 0);
        result = 31 * result + (unitPrice != null ? unitPrice.hashCode() : 0);
        result = 31 * result + (sumPrice != null ? sumPrice.hashCode() : 0);
        result = 31 * result + (realPay != null ? realPay.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (checkTime != null ? checkTime.hashCode() : 0);
        result = 31 * result + (expressPrice != null ? expressPrice.hashCode() : 0);
        result = 31 * result + (goodId != null ? goodId.hashCode() : 0);
        result = 31 * result + (receiveAddress != null ? receiveAddress.hashCode() : 0);
        result = 31 * result + (receivePerson != null ? receivePerson.hashCode() : 0);
        result = 31 * result + (receivePhone != null ? receivePhone.hashCode() : 0);
        result = 31 * result + (addressId != null ? addressId.hashCode() : 0);
        result = 31 * result + (shopId != null ? shopId.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (refundTime != null ? refundTime.hashCode() : 0);
        result = 31 * result + (refundAmount != null ? refundAmount.hashCode() : 0);
        result = 31 * result + (refundReason != null ? refundReason.hashCode() : 0);
        result = 31 * result + (refundStatus != null ? refundStatus.hashCode() : 0);
        return result;
    }
}
