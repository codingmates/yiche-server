package fm.dto;

import fm.entity.Activity;
import fm.entity.ActivityFlow;

import java.util.List;

/**
 * Created by 宏炜 on 2016-03-10.
 */
public class ActivityAndFlows {
    private Activity activity;
    private List<ActivityFlow> flows;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public List<ActivityFlow> getFlows() {
        return flows;
    }

    public void setFlows(List<ActivityFlow> flows) {
        this.flows = flows;
    }
}
