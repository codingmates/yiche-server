package fm.util;

import com.alibaba.fastjson.JSONObject;
import fm.config.MainConfig;
import fm.util.HttpRequestUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by 63267 on 2017/5/22.
 */
public class HuanXinSdkUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXinSdkUtil.class);

    private final static String userRegistUrl = "https://a1.easemob.com/ORG_NAME/APP_NAME/users";


    public static String registUser(String username,String password,String nickname){
        try {

            JSONObject obj = new JSONObject();
            obj.put("username",username);
            obj.put("password",password);
            obj.put("nickname",nickname);

            return HttpRequestUtils.postJson(getUserRegistUrl(),obj);

        } catch (Exception e) {
            LOGGER.error("环信注册用户异常 用户名：{}",username);
        }
        return null;
    }

    public static String getUserRegistUrl(){
        return userRegistUrl.replaceAll("ORG_NAME", MainConfig.hxOrgName).replaceAll("APP_NAME",MainConfig.hxAppName);
    }
}
