package fm.util;

import fm.exception.BizException;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by CM on 2017/6/8.
 */
public class GeoUtils {

    /**
     * 获取经纬度正确排列的数组
     * 「纬度，精度」
     * @param loc
     * @return
     */
    public static List getLocArray(String loc) {
        String[] locs = loc.split(",");
        if (locs.length < 2) {
            throw new BizException("经纬度参数错误");
        }
        double lon = Math.min(Double.parseDouble(locs[0]), Double.parseDouble(locs[1]));
        double lat = Math.max(Double.parseDouble(locs[0]), Double.parseDouble(locs[1]));
        List<Double> locArray = new ArrayList<>();
        locArray.add(lon);
        locArray.add(lat);

        return locArray;
    }

    /**
     * 获取经纬度正确排列的字符串
     * 「纬度，精度」
     * @param locStr
     * @return
     */
    public static String parseLoc(String locStr) {
        return StringUtils.join(getLocArray(locStr), ",");
    }
}
