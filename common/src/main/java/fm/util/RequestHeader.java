package fm.util;

import java.lang.reflect.Field;

/**
 * @修改人：CM
 * @修改时间：2017/2/26 23:31
 */
public class RequestHeader {
    private String accept;
    private String acceptEncoding;
    private String acceptLanguage;
    private String connection;
    private String cookie;
    private String host;
    private String referer;
    private String userAgent;

    public RequestHeader() {
    }

    public RequestHeader(String accept, String acceptEncoding, String acceptLanguage, String connection, String cookie, String host, String referer, String userAgent) {
        this.accept = accept;
        this.acceptEncoding = acceptEncoding;
        this.acceptLanguage = acceptLanguage;
        this.connection = connection;
        this.cookie = cookie;
        this.host = host;
        this.referer = referer;
        this.userAgent = userAgent;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getAcceptEncoding() {
        return acceptEncoding;
    }

    public void setAcceptEncoding(String acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
    }

    public String getAcceptLanguage() {
        return acceptLanguage;
    }

    public void setAcceptLanguage(String acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public boolean isEmpty() throws IllegalAccessException {

        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(this);
            if ("".equals(value) == false && value != null) {
                return false;
            }
        }
        return true;
    }
}
