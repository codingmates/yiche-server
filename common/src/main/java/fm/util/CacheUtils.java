package fm.util;

import fm.api.yunpian.SmsApi;
import fm.cache.*;
import fm.dao.SolrBaseDao;
import fm.huanxin.utils.OrgInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 文件描述：
 * 更新时间：2017/2/23
 * 更新人： codingmates@gmail.com .
 */
public class CacheUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheUtils.class);

    public static void loadAllCache() {
        LOGGER.info("正在载入项目必要的缓存信息");


        //号段缓存
//        SegmentNumberCache.reloadSegmentNumber();
        //城市信息缓存
        AreaCache.loadCache();
        //项目配置文件 config.prop
        ConfigCache.loadProjectConfig();
        //角色配置文件 role.prop
        RoleCache.loadRoleConfig();
        //商品类别缓存
        GoodClassCache.loadCache();

        //mongoDb配置 mongodb.prop
        MongodbConfigCache.loadCollectionConfig();

        //solr配置缓存
        SolrConfig.loadCache();
        SolrBaseDao.init();

        SmsApi.init();

        OrgInfo.init();
        LOGGER.info("项目缓存载入完毕");
    }
}
