package fm.web;

import fm.entity.PublicNumber;
import fm.entity.WxUser;
import fm.exception.BizException;
import fm.exception.TokenInvalidException;
import fm.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.security.auth.login.LoginException;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @修改人：CM
 * @修改时间：2017/2/27 20:46
 */
@Component
public class CurrentRequest {



    public static Object getCurrentLoginUser() throws Exception {
        Object user = getSession().getAttribute(Constant.SESSION_LOGIN_USER);
        if (user == null) {
            throw new TokenInvalidException("身份验证已经超时，请刷新页面或者重新登陆！");
        }
        return user;
    }

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static HttpSession getSession() throws TokenInvalidException {
        HttpSession session = getRequest().getSession();
        if (session == null) {
            throw new TokenInvalidException("登录超时");
        }
        return getRequest().getSession();
    }

    public static void addSessionAttribute(String key, Object val) throws TokenInvalidException {
        if (getSession() != null) {
            getSession().setAttribute(key, val);
        } else {
            throw new TokenInvalidException("身份验证已经超时，请刷新页面或者重新登陆！");
        }
    }

    public static void addRequestAttribute(String key, String val) throws TokenInvalidException {
        if (getRequest() != null) {
            getRequest().setAttribute(key, val);
        } else {
            throw new TokenInvalidException("身份验证已经超时，请刷新页面或者重新登陆！");
        }
    }

    public static Object getRequestAttribute(String key) throws TokenInvalidException {
        if (getRequest() != null) {
            return getRequest().getAttribute(key);
        } else {
            throw new TokenInvalidException("身份验证已经超时，请刷新页面或者重新登陆！");
        }
    }

    public static Object getSessionAttribute(String key) throws TokenInvalidException {
        if (getSession() != null) {
            return getSession().getAttribute(key);
        } else {
            throw new TokenInvalidException("身份验证已经超时，请刷新页面或者重新登陆！");
        }
    }

    public static boolean isAdmin() throws Exception {
        Object user = getCurrentLoginUser();
        if (user instanceof WxUser) {
            return false;
        } else if (user instanceof PublicNumber) {
            return true;
        } else {
            throw new BizException("错误的用户类型");
        }
    }

    public static Object getCurrentUser() throws Exception {
        return getCurrentLoginUser();
    }

    public static long getCurrentUserId() throws Exception {
        Object user = getCurrentLoginUser();
        if (user instanceof WxUser) {
            return ((WxUser) user).getId();
        } else if (user instanceof PublicNumber) {
            return ((PublicNumber) user).getId();
        } else {
            throw new BizException("错误的用户类型");
        }
    }
}
