package fm.web;

import fm.util.CacheUtils;
import fm.util.SpringContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContextEvent;

/**
 * 文件描述：
 * 更新时间：2017/2/23
 * 更新人： codingmates@gmail.com .
 */
public class FMContextLoaderListener extends ContextLoaderListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(FMContextLoaderListener.class);


    public FMContextLoaderListener() {
        super();
    }

    public FMContextLoaderListener(WebApplicationContext context) {
        super(context);
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        WebApplicationContext context = initWebApplicationContext(event.getServletContext());
        context.getBean(SpringContextHolder.class).setApplicationContext(context);
        LOGGER.info("注入 application Context ====> SpringContextHolder");
        LOGGER.info("项目启动,载入缓存中...");
        CacheUtils.loadAllCache();
        LOGGER.info("项目启动,载入缓存结束");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        super.contextDestroyed(event);
    }
}
