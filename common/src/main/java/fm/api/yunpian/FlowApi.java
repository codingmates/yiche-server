package fm.api.yunpian;

import com.alibaba.fastjson.JSON;
import fm.cache.ConfigCache;
import fm.util.Constant;
import fm.util.RequestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.security.krb5.internal.PAData;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * package :fm.api.yunpian.FlowApi
 * author: CM .
 * create time:2016/3/6
 */
public class FlowApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowApi.class);

    /**
     * 向云片平台发起流量订购
     *
     * @param mobile      手机号码
     * @param sn          云片平台对应的流量包规格sn码
     * @param callbackUrl 回调地址 ，默认不开发
     * @return
     */
    public static FlowBuyMessage buyFlow(String mobile, String sn, String callbackUrl) throws Exception {
        String res = null;
        Map<String, String> params = new HashMap();
        params.put("apikey", ConfigCache.getConfig("YUNPIAN_APIKEY"));
        params.put("mobile", mobile);
        params.put("sn", sn);
        LOGGER.info("buy flow request params:{}", JSON.toJSONString(params));
        res = RequestUtils.post(ConfigCache.getConfig("URI_FLOW_BUY"), params);
        LOGGER.info("buy flow request response:{}", res);
        return JSON.parseObject(res, FlowBuyMessage.class);
    }

}
