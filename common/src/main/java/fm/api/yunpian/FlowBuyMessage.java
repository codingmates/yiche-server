package fm.api.yunpian;

import java.math.BigDecimal;

/**
 * 流量订购返回信息
 * 类名：fm.api.yunpian.FlowBuyMessage
 * 创建者： CM .
 * 创建时间：2016/3/11
 */
public class FlowBuyMessage {
    public static final String SUCCESS = "0";
    private String code;
    private String msg;
    private Result result;
    private String detail;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Result getResult() {
        return result;
    }

    private class Result {
        private Integer count;
        private BigDecimal fee;
        private String sid;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public BigDecimal getFee() {
            return fee;
        }

        public void setFee(BigDecimal fee) {
            this.fee = fee;
        }

        public String getSid() {
            return sid;
        }

        public void setSid(String sid) {
            this.sid = sid;
        }
    }
}
